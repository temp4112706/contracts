﻿// ReSharper disable PropertyCanBeMadeInitOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Domain.AdditionalInsuranceSettings.ValueObjects;

/// <summary>
///     Страхование квартиры - доп.вид страхования
/// </summary>
public class FlatAdditionalInsuranceRates
{
    /// <summary>
    ///     Гражданская ответственность
    /// </summary>
    public AdditionalInsuranceRateRange[]? CivilResponsibility { get; set; }

    /// <summary>
    ///     Внутренняя отделка
    /// </summary>
    public AdditionalInsuranceRateRange[]? InteriorDecoration { get; set; }

    /// <summary>
    ///     Движимое имущество
    /// </summary>
    public AdditionalInsuranceRateRange[]? MovableProperty { get; set; }

    /// <summary>
    ///     Стены и перекрытия
    /// </summary>
    public AdditionalInsuranceRateRange[]? WallsAndCeilings { get; set; }
}