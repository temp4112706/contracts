﻿// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Domain.AdditionalInsuranceSettings.ValueObjects;

/// <summary>
///     Тарифный рендж
/// </summary>
public record AdditionalInsuranceRateRange
{
    /// <summary>
    ///     Cтраховая сумма от
    /// </summary>
    public int From { get; init; }

    /// <summary>
    ///     Тарифная сетка
    /// </summary>
    public decimal Rate { get; set; }

    /// <summary>
    ///     Cтраховая сумма до
    /// </summary>
    public int To { get; init; }
}