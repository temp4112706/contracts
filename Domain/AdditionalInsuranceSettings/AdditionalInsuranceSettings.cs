﻿using System.ComponentModel.DataAnnotations.Schema;
using Ct.Module.Domain.Entities;
using Some.Abstractions.AdditionalInsuranceSettings;
using Some.Abstractions.Enums;
using Some.Domain.Insurers;

// ReSharper disable ReplaceAutoPropertyWithComputedProperty
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local

namespace Some.Domain.AdditionalInsuranceSettings;

/// <inheritdoc cref="IAdditionalInsuranceSettings{TInsurer}" />
[Table("additional_insurance_settings")]
public class AdditionalInsuranceSettings : TrackableEntity<Guid>, IAdditionalInsuranceSettings<Insurer>
{
    /// <inheritdoc />
    public AdditionalInsuranceSettings(Guid id, Insurer insurer, AdditionalInsuranceType insuranceType, string? rates)
    {
        Id = id;
        InsuranceType = insuranceType;

        InsurerId = insurer.Id;
        Insurer = insurer;

        Rates = rates;
    }

    // ReSharper disable once UnusedMember.Local
    private AdditionalInsuranceSettings()
    {
    }

    /// <summary>
    ///     ID СК
    /// </summary>
    [Column("insurer_id")]
    public string InsurerId { get; private set; } = null!;

    /// <inheritdoc />
    [Column("insurance_type")]
    public AdditionalInsuranceType InsuranceType { get; private set; }

    /// <inheritdoc />
    public Insurer Insurer { get; private set; } = null!;

    /// <inheritdoc />
    [Column("rates")]
    public string? Rates { get; private set; }

    /// <summary>
    ///     Обновить тарифы
    /// </summary>
    /// <param name="rates"></param>
    public void ChangeRates(string rates) => Rates = rates;

    /// <summary>
    ///     Сбросить тарифы
    /// </summary>
    public void DiscardRates() => Rates = null;
}