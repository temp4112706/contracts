﻿using Some.Abstractions.Insurers;

namespace Some.Domain.AdditionalInsuranceSettings.Extensions;

/// <summary>
///     Расширения для <see cref="IScalarInsurerAdditionalInsuranceCommission"/>
/// </summary>
public static class AdditionalInsuranceCommissionExtensions
{
    /// <summary>
    ///     Нет ограничений по датам
    /// </summary>
    public static bool Infinity(this IScalarInsurerAdditionalInsuranceCommission commission) =>
        commission.PolicyFrom == null
        && commission.PolicyTo == null;
}