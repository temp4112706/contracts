﻿using Some.Abstractions.Attachments;
using Some.Abstractions.Enums;

namespace Some.Domain.Attachments;

/// <inheritdoc />
public class UploadAttachment : IUploadAttachment
{
    /// <summary>
    ///     Создание объекта
    /// </summary>
    public UploadAttachment(Guid entityId, FileEntityType entityType, string? action, string? description)
    {
        EntityId = entityId;
        EntityType = entityType;
        Action = action;
        Description = description;
    }

    /// <inheritdoc />
    public string? Action { get; }

    /// <inheritdoc />
    public string? Description { get; }

    /// <inheritdoc />
    public Guid EntityId { get; }

    /// <inheritdoc />
    public FileEntityType EntityType { get; }
}