using System.ComponentModel.DataAnnotations.Schema;
using Ct.Module.Domain.Entities;
using Some.Abstractions.Attachments;
using Some.Abstractions.Enums;

namespace Some.Domain.Attachments;

/// <summary>
///     Базовая сущность вложенного файла
/// </summary>
[Table("attachment")]
public sealed class Attachment : BaseEntity<Guid>, IAttachment
{
    /// <summary>
    ///     Создание объекта
    /// </summary>
    public Attachment(Guid id, string externalId, string name, FileEntityType entityType, Guid entityId,
        string author, string size, string hash, DateTime? createdAt, string? action, string? description,
        bool removable, bool temp) : base(id)
    {
        ExternalId = externalId;
        Name = name;
        EntityType = entityType;
        EntityId = entityId;
        Author = author;
        Size = size;
        Hash = hash;
        Description = description;
        Action = action;
        Removable = removable;
        Temp = temp;
        if (createdAt.HasValue)
        {
            CreatedAt = createdAt.Value;
        }
    }

    /// <summary>
    ///     Создание объекта
    /// </summary>
    public Attachment(IAttachment attachment, string externalId) : base(attachment.Id)
    {
        Change(attachment, externalId);
    }

    /// <inheritdoc />
    // ReSharper disable once UnusedMember.Local
    private Attachment()
    {
    }

    /// <inheritdoc />
    [Column("action")]
    public string? Action { get; private set; }

    /// <inheritdoc />
    [Column("author")]
    public string Author { get; private set; } = null!;

    /// <inheritdoc />
    [Column("description")]
    public string? Description { get; private set; }

    /// <inheritdoc />
    [Column("entity_id")]
    public Guid EntityId { get; private set; }

    /// <inheritdoc />
    [Column("entity_type")]
    public FileEntityType EntityType { get; private set; }

    /// <summary>
    ///     Идентификатор вложения в хранилище
    ///     По этому идентификатору происходит поиск вложения
    /// </summary>
    [Column("external_id")]
    public string? ExternalId { get; private set; }

    /// <inheritdoc />
    [Column("hash")]
    public string Hash { get; private set; } = null!;

    /// <inheritdoc />
    [Column("name")]
    public string Name { get; private set; } = null!;

    /// <inheritdoc />
    [Column("removable")]
    public bool? Removable { get; private set; }

    /// <inheritdoc />
    [Column("size")]
    public string Size { get; private set; } = null!;

    /// <inheritdoc />
    [Column("temp")]
    public bool? Temp { get; private set; }

    /// <inheritdoc />
    [Column("created_at")]
    public DateTime CreatedAt { get; private set; }

    /// <summary>
    ///     Изменить данные
    /// </summary>
    public void Change(IAttachment attachment, string externalId)
    {
        ExternalId = externalId;
        Name = attachment.Name;
        EntityType = attachment.EntityType;
        EntityId = attachment.EntityId;
        Author = attachment.Author;
        Size = attachment.Size;
        Hash = attachment.Hash;
        Description = attachment.Description;
        Action = attachment.Action;
        CreatedAt = attachment.CreatedAt;
        Removable = attachment.Removable;
        Temp = attachment.Temp;
    }

    /// <summary>
    ///     Установить как удаляемый файл
    /// </summary>
    public void ChangeRemovable(bool? removable) => Removable = removable;
}