﻿using Some.Abstractions.MortgageUnderwritingRates;

namespace Some.Domain.MortgageUnderwritingRates;

/// <summary>
///     Диапазон от и до
/// </summary>
public record MortgageUnderwritingRatesRange(int? From, int? To) : IRange;