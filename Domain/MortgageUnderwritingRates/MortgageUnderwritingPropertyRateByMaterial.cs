﻿using Some.Abstractions.Enums;
using Some.Abstractions.MortgageUnderwritingRates;
using Newtonsoft.Json;

namespace Some.Domain.MortgageUnderwritingRates;

/// <inheritdoc cref="IMortgageUnderwritingPropertyRateByMaterial" />
public class MortgageUnderwritingPropertyRateByMaterial : IMortgageUnderwritingPropertyRateByMaterial
{
    [Newtonsoft.Json.JsonConstructor]
    public MortgageUnderwritingPropertyRateByMaterial(
        [JsonProperty] InterfloorOverlapMaterial? interfloorOverlap,
        [JsonProperty] FoundationMaterial? foundation,
        [JsonProperty] ConstructionMaterial? construction,
        [JsonProperty] decimal rate)
    {
        InterfloorOverlap = interfloorOverlap;
        Foundation = foundation;
        Construction = construction;
        Rate = rate;
    }

    private MortgageUnderwritingPropertyRateByMaterial()
    {
    }

    /// <inheritdoc />
    public ConstructionMaterial? Construction { get; }

    /// <inheritdoc />
    public FoundationMaterial? Foundation { get; }

    /// <inheritdoc />
    public InterfloorOverlapMaterial? InterfloorOverlap { get; }

    /// <inheritdoc />
    public decimal Rate { get; }
}