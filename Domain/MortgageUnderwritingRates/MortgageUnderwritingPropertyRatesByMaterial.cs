﻿using Some.Abstractions.MortgageUnderwritingRates;
using Newtonsoft.Json;

namespace Some.Domain.MortgageUnderwritingRates;

/// <inheritdoc />
public class MortgageUnderwritingPropertyRatesByMaterial : IMortgageUnderwritingPropertyRatesByMaterial<
    MortgageUnderwritingPropertyRateByMaterial>
{
    [Newtonsoft.Json.JsonConstructor]
    public MortgageUnderwritingPropertyRatesByMaterial(
        [JsonProperty] MortgageUnderwritingPropertyRateByMaterial[]? custom,
        [JsonProperty] decimal? @default)
    {
        Custom = custom;
        Default = @default;
    }

    private MortgageUnderwritingPropertyRatesByMaterial()
    {
    }

    /// <inheritdoc />
    public MortgageUnderwritingPropertyRateByMaterial[]? Custom { get; }

    /// <inheritdoc />
    public decimal? Default { get; }
}