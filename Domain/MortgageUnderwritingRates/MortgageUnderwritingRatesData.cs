﻿using Some.Abstractions.MortgageUnderwritingRates;
using Newtonsoft.Json;

namespace Some.Domain.MortgageUnderwritingRates;

public class MortgageUnderwritingRatesData : IMortgageUnderwritingRates<MortgageUnderwritingLifeRate,
    MortgageUnderwritingPropertyRates, MortgageUnderwritingPropertyRatesByMaterial,
    MortgageUnderwritingPropertyRateByMaterial,
    MortgageUnderwritingTitleRates, MortgageUnderwritingTitleByPropertyTypeRates,
    MortgageUnderwritingTitleByAmountOfDeals, MortgageUnderwritingRatesRange>
{
    [JsonConstructor]
    public MortgageUnderwritingRatesData(
        [JsonProperty] MortgageUnderwritingLifeRate[] life,
        [JsonProperty] MortgageUnderwritingPropertyRates property,
        [JsonProperty] MortgageUnderwritingTitleRates title)
    {
        Life = life;
        Property = property;
        Title = title;
    }

#pragma warning disable CS8618
    private MortgageUnderwritingRatesData()
#pragma warning restore CS8618
    {
    }

    /// <inheritdoc />
    public MortgageUnderwritingLifeRate[] Life { get; }

    /// <inheritdoc />
    public MortgageUnderwritingPropertyRates Property { get; }

    /// <inheritdoc />
    public MortgageUnderwritingTitleRates Title { get; }
}