﻿using Some.Abstractions.MortgageUnderwritingRates;
using Newtonsoft.Json;

namespace Some.Domain.MortgageUnderwritingRates;

/// <inheritdoc />
public class MortgageUnderwritingTitleRates : IMortgageUnderwritingTitleRates<
    MortgageUnderwritingTitleByPropertyTypeRates, MortgageUnderwritingTitleByAmountOfDeals, MortgageUnderwritingRatesRange>
{
    [JsonConstructor]
    public MortgageUnderwritingTitleRates(decimal @default,
        MortgageUnderwritingTitleByPropertyTypeRates[]? byPropertyType)
    {
        Default = @default;
        ByPropertyType = byPropertyType;
    }

    /// <inheritdoc />
    public MortgageUnderwritingTitleByPropertyTypeRates[]? ByPropertyType { get; }

    /// <inheritdoc />
    public decimal Default { get; }
}