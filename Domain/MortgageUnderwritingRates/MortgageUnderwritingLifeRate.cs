﻿using Some.Abstractions.MortgageUnderwritingRates;
using Newtonsoft.Json;

namespace Some.Domain.MortgageUnderwritingRates;

/// <summary>
///     Тариф страхования жизни
/// </summary>
public class MortgageUnderwritingLifeRate : IMortgageUnderwritingLifeRate
{
    [Newtonsoft.Json.JsonConstructor]
    public MortgageUnderwritingLifeRate([JsonProperty] int age, [JsonProperty] decimal femalePercent,
        [JsonProperty] decimal malePercent)
    {
        Age = age;
        FemalePercent = femalePercent;
        MalePercent = malePercent;
    }

    private MortgageUnderwritingLifeRate()
    {
    }

    /// <inheritdoc />
    public int Age { get; }

    /// <inheritdoc />
    public decimal FemalePercent { get; }

    /// <inheritdoc />
    public decimal MalePercent { get; }
}