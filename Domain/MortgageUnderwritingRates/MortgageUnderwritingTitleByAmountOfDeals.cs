﻿using Some.Abstractions.MortgageUnderwritingRates;

namespace Some.Domain.MortgageUnderwritingRates;

public class MortgageUnderwritingTitleByAmountOfDeals : IMortgageUnderwritingTitleByAmountOfDeals<MortgageUnderwritingRatesRange>
{
    public MortgageUnderwritingTitleByAmountOfDeals(MortgageUnderwritingRatesRange range, decimal rate, MortgageUnderwritingRatesRange termOfInsurance)
    {
        Range = range;
        Rate = rate;
        TermOfInsurance = termOfInsurance;
    }

    /// <inheritdoc />
    public MortgageUnderwritingRatesRange Range { get; }

    /// <inheritdoc />
    public decimal Rate { get; }

    /// <inheritdoc />
    public MortgageUnderwritingRatesRange TermOfInsurance { get; }
}