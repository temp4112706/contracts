﻿using Some.Abstractions.Enums;
using Some.Abstractions.MortgageUnderwritingRates;

namespace Some.Domain.MortgageUnderwritingRates;

public class MortgageUnderwritingTitleByPropertyTypeRates : IMortgageUnderwritingTitleByPropertyTypeRates<
    MortgageUnderwritingTitleByAmountOfDeals, MortgageUnderwritingRatesRange>
{
    public MortgageUnderwritingTitleByPropertyTypeRates(MortgageUnderwritingTitleByAmountOfDeals[] amountOfDeals,
        PropertyType[]? types)
    {
        AmountOfDeals = amountOfDeals;
        Types = types;
    }

    /// <inheritdoc />
    public MortgageUnderwritingTitleByAmountOfDeals[] AmountOfDeals { get; }

    /// <inheritdoc />
    public PropertyType[]? Types { get; }
}