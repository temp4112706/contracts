﻿using Some.Abstractions.Enums;
using Some.Abstractions.MortgageUnderwritingRates;
using Newtonsoft.Json;

namespace Some.Domain.MortgageUnderwritingRates;

/// <inheritdoc cref="IMortgageUnderwritingPropertyRates{T1,T2}" />
public class MortgageUnderwritingPropertyRates : IMortgageUnderwritingPropertyRates<
    MortgageUnderwritingPropertyRatesByMaterial, MortgageUnderwritingPropertyRateByMaterial>
{
    [JsonConstructor]
    public MortgageUnderwritingPropertyRates(
        [JsonProperty] Dictionary<PropertyType, MortgageUnderwritingPropertyRatesByMaterial>? types,
        [JsonProperty] decimal @default)
    {
        Types = types;
        Default = @default;
    }

    internal MortgageUnderwritingPropertyRates()
    {
    }

    /// <inheritdoc />
    public decimal Default { get; }

    /// <inheritdoc />
    public Dictionary<PropertyType, MortgageUnderwritingPropertyRatesByMaterial>? Types { get; }
}