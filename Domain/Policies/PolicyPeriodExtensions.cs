﻿using Some.Abstractions.Enums;
using Some.Domain.ExtraProperties.Extensions;

namespace Some.Domain.Policies;

/// <summary>
///     Расширения для <see cref="PolicyPeriod" />
/// </summary>
public static class PolicyPeriodExtensions
{
    private const string HasPaymentPrintedFormKey = "hasPaymentPrintedForm";
    private const string CorrectionByInsurerKey = "correctionByInsurer";
    private const string CheckProlongationStateRetryCountKey = "checkProlongationStateRetryCount";

    /// <summary>
    ///     Указанный период корректировался сотрудников СК
    /// </summary>
    public static bool CorrectionByInsurer(this PolicyPeriod period)
    {
        return period.TryGetProperty<bool>(CorrectionByInsurerKey);
    }

    /// <summary>
    ///     Установить флаг корректировался сотрудников СК
    /// </summary>
    public static void SetCorrectionByInsurer(this PolicyPeriod period, bool value)
    {
        period.SetProperty(CorrectionByInsurerKey, value);
    }

    /// <summary>
    ///     Удалить флаг корректировался сотрудников СК
    /// </summary>
    public static void RemoveCorrectionByInsurer(this PolicyPeriod period)
    {
        period.RemoveProperty(CorrectionByInsurerKey);
    }

    /// <summary>
    ///     Есть печатная форма на оплату
    /// </summary>
    public static bool HasPaymentPrintedForm(this PolicyPeriod period)
    {
        return period.TryGetProperty(HasPaymentPrintedFormKey, false);
    }

    /// <summary>
    ///     Установить флаг печатная форма на оплату
    /// </summary>
    public static void SetHasPaymentPrintedForm(this PolicyPeriod period, bool value)
    {
        period.SetProperty(HasPaymentPrintedFormKey, value);
    }

    /// <summary>
    ///     Удалить флаг печатная форма на оплату
    /// </summary>
    public static void RemoveHasPaymentPrintedForm(this PolicyPeriod period)
    {
        period.RemoveProperty(HasPaymentPrintedFormKey);
    }

    /// <summary>
    ///     Можно ли иниализировать процесс оплаты
    ///     или счет на оплату либо ссылка
    /// </summary>
    public static bool CanWaitingForPayment(this PolicyPeriod period)
    {
        return !string.IsNullOrWhiteSpace(period.PaymentUrl) || period.HasPaymentPrintedForm();
    }

    /// <summary>
    ///     Можно ли провести оплату периода в массовой пролонгации?
    /// </summary>
    public static bool CanMassProlongation(this PolicyPeriod period, DateTime? premiumDate, decimal? premiumAmount)
    {
        if (!premiumDate.HasValue || !premiumAmount.HasValue)
        {
            return false;
        }

        if (premiumDate < period.PlannedPremium.PlannedDate.AddDays(-90) ||
            premiumDate > period.PlannedPremium.PlannedDate.AddDays(90))
        {
            return false;
        }

        // Временно убрана проверка на разницу в 500 р. https://jira.domrf.ru/browse/INS-718
        // if (premiumAmount < period.PlannedPremium.PlannedAmount.Amount - 500M ||
        //     premiumAmount > period.PlannedPremium.PlannedAmount.Amount + 500M)
        //     return false;

        return true;
    }

    /// <summary>
    ///     Кол-во попыток проверки статуса пролонгации
    /// </summary>
    public static int CheckProlongationStateRetryCount(this PolicyPeriod period)
    {
        return period.TryGetProperty(CheckProlongationStateRetryCountKey, 0);
    }

    /// <summary>
    ///     Установить кол-во попыток проверки статуса пролонгации
    /// </summary>
    public static void SetCheckProlongationStateRetryCount(this PolicyPeriod period, int retryCount)
    {
        period.SetProperty(CheckProlongationStateRetryCountKey, retryCount);
    }

    /// <summary>
    ///     Удалить флаг кол-во попыток проверки статуса пролонгации
    /// </summary>
    public static void RemoveCheckProlongationStateRetryCount(this PolicyPeriod period)
    {
        period.RemoveProperty(CheckProlongationStateRetryCountKey);
    }

    /// <summary>
    ///     Актуализация Автопролонгацию на основе периода
    /// </summary>
    public static void SyncAutoProlongationByPeriod(this PolicyPeriod period)
    {
        var plannedAmount = period.PlannedPremium.PlannedAmount.Amount;
        var actualAmount = period.ActualPremium?.TotalActualPayment?.ActualAmount.Amount;
        var hasAllPayments = period.ActualPremium?.Payments.All(x => x.ActualPayment != null) ?? false;
        var isPaidPeriod = plannedAmount <= actualAmount;
        if (!isPaidPeriod || !hasAllPayments)
        {
            return;
        }

        period.AutoProlongation?.ChangeStatus(AutoProlongationStatus.Paid);
        // https://jira.domrf.ru/browse/INS-909
        period.AutoProlongation?.SmsMessages
            .Where(x => x.Status == AutoProlongationSmsStatus.Created)
            .ToList()
            .ForEach(x => period.AutoProlongation.SmsMessages.Remove(x));
    }
}