﻿using System.Diagnostics.CodeAnalysis;
using Some.Abstractions.Enums;
using Some.Domain.Monies;
using Some.Domain.Policies.ValueObjects;

namespace Some.Domain.Policies;

/// <summary>
///     Расширения для <see cref="InsurancePeriod" />
/// </summary>
[SuppressMessage("ReSharper", "ArgumentsStyleOther")]
public static class InsurancePeriodExtensions
{
    /// <summary>
    ///     Получить периоды ДС
    /// </summary>
    [SuppressMessage("ReSharper", "ConditionIsAlwaysTrueOrFalse")]
    public static ICollection<PolicyPeriod> WrapToPolicyPeriods(this ICollection<InsurancePeriod> insurancePeriods,
        ICollection<PolicyLifeInsuranceRequest> lifeInsuranceRequests,
        ICollection<PolicyPropertyOrTitleInsuranceRequest> propertyOrTitleInsuranceRequests)
    {
        decimal CalculateTotalPremiumAmount(int periodNumber)
        {
            var rows = new List<InsuranceScheduleRow>();

            var lifeInsuranceScheduleRows = lifeInsuranceRequests
                .Select(x => x.Schedule.FindByPeriodNumber(periodNumber))
                .Where(x => x != null);
            rows.AddRange(lifeInsuranceScheduleRows!);

            var propertyInsuranceScheduleRows = propertyOrTitleInsuranceRequests
                .Select(x => x.PropertySchedule?.FindByPeriodNumber(periodNumber))
                .Where(x => x != null);
            rows.AddRange(propertyInsuranceScheduleRows!);

            var titleInsuranceScheduleRows = propertyOrTitleInsuranceRequests
                .Select(x => x.TitleSchedule?.FindByPeriodNumber(periodNumber))
                .Where(x => x != null);
            rows.AddRange(titleInsuranceScheduleRows!);

            return rows.Sum(r => r.Tariff.PremiumAmount.Amount);
        }

        decimal CalculateInsuranceAmount(int periodNumber)
        {
            decimal? insuranceAmount = null;

            if (lifeInsuranceRequests.Any())
            {
                insuranceAmount = lifeInsuranceRequests
                    .Select(x => x.Schedule.FindByPeriodNumber(periodNumber))
                    .Sum(r => r?.Tariff.InsuranceAmount.Amount);
            }

            if (insuranceAmount is null or 0 && propertyOrTitleInsuranceRequests.Any(x => x.PropertySchedule != null))
            {
                insuranceAmount = propertyOrTitleInsuranceRequests
                    .Select(x => x.PropertySchedule?.FindByPeriodNumber(periodNumber))
                    .Sum(r => r?.Tariff.InsuranceAmount.Amount);
            }

            if (insuranceAmount is null or 0 && propertyOrTitleInsuranceRequests.Any(x => x.TitleSchedule != null))
            {
                insuranceAmount = propertyOrTitleInsuranceRequests
                    .Select(x => x.TitleSchedule?.FindByPeriodNumber(periodNumber))
                    .Sum(r => r?.Tariff.InsuranceAmount.Amount);
            }

            return insuranceAmount ?? 0;
        }

        return insurancePeriods.Select(x =>
        {
            // https://wiki.domrf.ru/pages/viewpage.action?pageId=68856735
            var insuranceAmount = Money.Rub(CalculateInsuranceAmount(x.Number));
            var totalPremiumAmount = Money.Rub(CalculateTotalPremiumAmount(x.Number));
            var plannedPremium = new PolicyPlannedPremium(
                plannedDate: x.StartDate.AddDays(-1),
                plannedAmount: totalPremiumAmount
            );

            return new PolicyPeriod(
                id: Guid.Empty,
                state: PolicyInsurancePeriodState.Planned,
                period: x,
                insuranceAmount,
                plannedPremium,
                actualPremium: null
            );
        }).ToList();
    }
}