using System.ComponentModel.DataAnnotations.Schema;
using Ct.Module.Domain.Entities;
using Some.Abstractions.Common;
using Some.Abstractions.Enums;
using Some.Abstractions.Policies;
using Some.Domain.Companies;
using Some.Domain.Customers;
using Some.Domain.Employees;
using Some.Domain.ExtraProperties;
using Some.Domain.Insurers;
using Some.Domain.Monies;
using Some.Domain.Policies.ValueObjects;
using Some.Domain.PolicyEvents;

// ReSharper disable UnusedMethodReturnValue.Global
// ReSharper disable UnusedMember.Global
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local
// ReSharper disable UnusedAutoPropertyAccessor.Local
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Domain.Policies;

/// <summary>
///     ДС
/// </summary>
[Table("policy")]
public class CommonPolicy : TrackableEntity<Guid>, IPolicy<Money, Customer, Insurer,
        PolicyPeriod, Employee, PolicyInsuranceAccident, Company>,
    IHasExtraProperties, ISoftDeletable, IHasExternalId
{
    /// <summary>
    ///     Доп вид страхования
    /// </summary>
    public const string Additional = "additional";

    /// <summary>
    ///     Ипотека
    /// </summary>
    public const string Mortgage = "mortgage";

    /// <summary>
    ///     Создание объекта только с ИД
    /// </summary>
    /// <param name="id"></param>
    public CommonPolicy(Guid id) : base(id, false)
    {
    }

    /// <summary>
    ///     Создание договора страхования
    /// </summary>
    protected CommonPolicy(Guid id, Company? company, Employee? companyEmployee, bool manual, string number,
        PolicyStatus status, DateTime statusUpdatedAt, DateTime agreementDateTime,
        DateTime commencementDateTime, DateTime expiryDateTime, ICollection<PolicyPeriod> periods, Customer insurant,
        Insurer insurer, Employee? insurerEmployee, PolicyCompleteReason? completeReason, DateTime? completedAt,
        bool draft = false)
        : base(id)
    {
        Number = number;
        Manual = manual;
        Draft = draft;
        Status = status;
        StatusUpdatedAt = statusUpdatedAt;
        Insurant = insurant;
        Insurer = insurer;
        AgreementDateTime = agreementDateTime;
        CommencementDateTime = commencementDateTime;
        ExpiryDateTime = expiryDateTime;
        ChangePeriods(periods);
        InsurantId = insurant.Id;
        Company = company;
        CompanyId = company?.Id;

        CompanyEmployee = companyEmployee;
        CompanyEmployeeId = companyEmployee?.Id;

        InsurerEmployee = insurerEmployee;
        InsurerEmployeeId = insurerEmployee?.Id;

        if (status == PolicyStatus.Annulled || status == PolicyStatus.Completed)
        {
            CompletedAt = DateTime.UtcNow;
        }

        if (completedAt != null)
        {
            CompletedAt = completedAt;
        }

        CompleteReason = completeReason;
        Accidents = new List<PolicyInsuranceAccident>();
        Events = new List<PolicyEvent>();
    }

    /// <inheritdoc />
    // ReSharper disable once UnusedMember.Local
    protected CommonPolicy()
    {
    }

    /// <summary>
    ///     Эвенты ДС
    /// </summary>
    public ICollection<PolicyEvent> Events { get; private set; } = null!;

    /// <summary>
    ///     Тип ДС
    /// </summary>
    [Column("type")]
    public string Type { get; private set; } = null!;

    /// <inheritdoc />
    [Column("external_id")]
    public string? ExternalId { get; private set; }

    /// <inheritdoc />
    [Column("extra_properties")]
    public ExtraPropertyDictionary? ExtraProperties { get; private set; } = new();

    /// <summary>
    ///     Страховые случаи
    /// </summary>
    public ICollection<PolicyInsuranceAccident> Accidents { get; private set; } = null!;

    /// <inheritdoc />
    [Column("comment")]
    public string? Comment { get; set; }

    /// <inheritdoc />
    public Company? Company { get; private set; }

    /// <inheritdoc />
    public Employee? CompanyEmployee { get; private set; }

    /// <summary>
    ///     Причина закрытия ДС
    /// </summary>
    [Column("complete_reason")]
    public PolicyCompleteReason? CompleteReason { get; private set; }

    /// <inheritdoc />
    public Customer Insurant { get; private set; } = null!;

    /// <inheritdoc />
    public Insurer Insurer { get; private set; } = null!;

    /// <inheritdoc />
    public Employee? InsurerEmployee { get; private set; }

    /// <inheritdoc />
    [Column("last_payment_datetime")]
    public DateTime? LastPaymentDateTime { get; private set; }

    /// <inheritdoc />
    public ICollection<PolicyPeriod> Periods { get; private set; } = null!;

    /// <inheritdoc />
    [Column("agreement_datetime")]
    public DateTime AgreementDateTime { get; private set; }

    /// <inheritdoc />
    [Column("commencement_datetime")]
    public DateTime CommencementDateTime { get; private set; }

    /// <inheritdoc />
    [Column("completed_at")]
    public DateTime? CompletedAt { get; private set; }

    /// <inheritdoc />
    [Column("expiry_datetime")]
    public DateTime ExpiryDateTime { get; private set; }

    /// <inheritdoc />
    [Column("manual")]
    public bool Manual { get; }

    /// <inheritdoc />
    [Column("draft")]
    public bool Draft { get; private set; }

    /// <inheritdoc />
    [Column("number")]
    public string Number { get; private set; } = null!;

    /// <inheritdoc />
    [Column("status")]
    public PolicyStatus Status { get; private set; }

    /// <inheritdoc />
    [Column("status_updated_at")]
    public DateTime StatusUpdatedAt { get; private set; }

    /// <inheritdoc />
    [Column("deleted_at")]
    public DateTime? DeletedAt { get; private set; }

    /// <summary>
    ///     ИД <see cref="CompanyEmployee" />
    /// </summary>
    [Column("company_employee_id")]
    private string? CompanyEmployeeId { get; set; }

    /// <summary>
    ///     ИД компании
    /// </summary>
    [Column("company_id")]
    private string? CompanyId { get; set; }

    /// <summary>
    ///     ИД страхователя
    /// </summary>
    [Column("insurant_id")]
    private Guid InsurantId { get; set; }

    /// <summary>
    ///     ИД <see cref="InsurerEmployee" />
    /// </summary>
    [Column("insurer_employee_id")]
    private string? InsurerEmployeeId { get; set; }

    /// <summary>
    ///     ИД страховщика
    /// </summary>
    // ReSharper disable once UnassignedGetOnlyAutoProperty
    [Column("insurer_id")]
    public string InsurerId { get; set; } = null!;

    /// <summary>
    ///     Изменить дату ДС
    /// </summary>
    public void ChangeAgreementDateTime(DateTime value) => AgreementDateTime = value;

    /// <summary>
    ///     Изменить дату начала ДС
    /// </summary>
    public void ChangeCommencementDateTime(DateTime value) => CommencementDateTime = value;

    /// <summary>
    ///     Изменить комментарий
    /// </summary>
    public bool ChangeComment(string comment)
    {
        Comment = comment;
        return true;
    }

    /// <summary>
    ///     Завершить ДС
    /// </summary>
    public void ChangeComplete(PolicyCompleteReason? reason, DateTime? completedAt)
    {
        CompleteReason = reason;
        CompletedAt = completedAt;
    }

    /// <summary>
    ///     Пометить как удален
    /// </summary>
    /// <param name="deletedAt"></param>
    public void ChangeDelete(DateTime? deletedAt) => DeletedAt = deletedAt;

    /// <summary>
    ///     Изменить дату окончания ДС
    /// </summary>
    public void ChangeExpiryDateTime(DateTime value) => ExpiryDateTime = value;

    /// <summary>
    ///     Изменить внешний ИД
    /// </summary>
    public void ChangeExternalId(string? externalId) => ExternalId = externalId;

    /// <summary>
    ///     Изменить страхователя
    /// </summary>
    public void ChangeInsurant(Customer insurant)
    {
        InsurantId = insurant.Id;
        Insurant = insurant;
    }

    /// <summary>
    ///     Изменить страховую компанию
    /// </summary>
    /// <param name="insurer">Новая страховая компания</param>
    /// <param name="number">Новый номер ДС</param>
    public bool ChangeInsurer(Insurer insurer, string number)
    {
        if (Insurer.Equals(insurer) && Number == number)
        {
            return false;
        }

        Insurer = insurer;
        InsurerId = insurer.Id;
        Number = number;
        return true;
    }

    /// <summary>
    ///     Изменить дату посл. факт. оплаты
    /// </summary>
    public void ChangeLastPaymentDateTime(DateTime? value) => LastPaymentDateTime = value;

    /// <summary>
    ///     Изменение номер договора страхования
    /// </summary>
    /// <param name="number"></param>
    /// <returns></returns>
    public bool ChangeNumber(string number)
    {
        if (Number != number)
        {
            Number = number;
            return true;
        }

        return false;
    }
    
    /// <summary>
    ///     Изменение статуса черновика
    /// </summary>
    /// <param name="draft">Черновик?</param>
    /// <returns></returns>
    public bool ChangeDraft(bool draft)
    {
        if (Draft != draft)
        {
            Draft = draft;
            return true;
        }

        return false;
    }

    /// <summary>
    ///     Присвоить периоды
    /// </summary>
    public void ChangePeriods(IEnumerable<PolicyPeriod> periods) => Periods = periods.OrderBy(x => x.Number).ToList();

    /// <summary>
    ///     Изменение статуса
    /// </summary>
    public PolicyStatusChangesResult? ChangeStatus(PolicyStatus nextStatus, DateTime? changedAt = null)
    {
        if (Status == nextStatus)
        {
            return null;
        }

        var previousStatus = Status;
        Status = nextStatus;
        StatusUpdatedAt = changedAt ?? DateTime.UtcNow;
        return new PolicyStatusChangesResult(Status, previousStatus, StatusUpdatedAt);
    }
}