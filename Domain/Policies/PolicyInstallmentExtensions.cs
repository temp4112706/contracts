﻿namespace Some.Domain.Policies;

internal static class PolicyInstallmentExtensions
{
    public static IDictionary<int, (DateTime, decimal)> CalculateSchedule(DateTime startDate, DateTime endDate,
        int periods,
        decimal amount)
    {
        var stepPeriodAmount = Math.Floor(amount / periods);
        var stepPeriodDays = (int)Math.Floor((endDate - startDate).Days / (periods - 1f));
        DateTime? lastPaymentDate = null;
        var lastPaymentAmount = 0m;
        var result = new Dictionary<int, (DateTime, decimal)>();
        for (var i = 1; i <= periods; i++)
        {
            if (i == periods)
            {
                var paymentDate = endDate;
                var paymentAmount = amount - lastPaymentAmount;
                result[i] = (paymentDate, paymentAmount);
            }
            else
            {
                var paymentDate = lastPaymentDate?.AddDays(stepPeriodDays) ?? startDate;
                var paymentAmount = stepPeriodAmount;
                lastPaymentAmount += paymentAmount;
                result[i] = (paymentDate, paymentAmount);
                lastPaymentDate = paymentDate;
            }
        }

        return result;
    }
}