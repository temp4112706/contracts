using Ct.Module.Domain.Json;
using Some.Abstractions.Enums;
using Some.Abstractions.Exceptions;
using Some.Abstractions.Extensions;
using Some.Abstractions.Policies.Actions;
using Some.Domain.Policies.Actions;
using Some.Domain.Monies;
using Some.Domain.Policies.Mortgage;
using Some.Domain.Policies.ValueObjects;

namespace Some.Domain.Policies;

/// <summary>
///     Расширения для <see cref="CommonPolicy" />
/// </summary>
public static class CommonPolicyExtensions
{
    /// <summary>
    ///     Пролонгировать период
    /// </summary>
    public static PolicyStatusChangesResult? ProlongationPeriod(this CommonPolicy policy,
        PolicyPeriod paymentPeriod,
        IEntityJsonPropertyContext jsonContext,
        PolicyActualPremium actualPremium, 
        DateTime? paymentDate)
    {
        // на всякий проверяем
        if (policy.Periods.Any(x => x.State == PolicyInsurancePeriodState.Current))
        {
            throw new BusinessException(BusinessErrorCode.PolicyProlongationImpossible,
                "Уже есть установленный текущий период");
        }

        paymentPeriod.ChangeState(PolicyInsurancePeriodState.Current);
        paymentPeriod.ChangePaymentUrl(null);
        paymentPeriod.ChangeActualPremium(actualPremium);
        paymentPeriod.RemoveHasPaymentPrintedForm();
        paymentPeriod.RemoveCorrectionByInsurer();
        paymentPeriod.RemoveCheckProlongationStateRetryCount();
        paymentPeriod.SyncAutoProlongationByPeriod();
        jsonContext.TrackModify(paymentPeriod, x => x.ExtraProperties);
        if (paymentDate.HasValue)
        {
            policy.ChangeLastPaymentDateTime(paymentDate);
        }

        return policy.ChangeStatus(PolicyStatus.Active);
    }

    /// <summary>
    ///     Аннулировать ДС
    /// </summary>
    public static void Annul(this CommonPolicy policy, IPolicyAnnul annul, out PolicyStatusChangesResult? result)
    {
        //  если уже аннулирован и не принудительно
        if (!annul.Force && policy.Status != PolicyStatus.Annulled)
        {
            //  валидация
            if (policy.Status != PolicyStatus.Project && policy.Status != PolicyStatus.Active &&
                policy.Status != PolicyStatus.Overdue &&
                policy.Status != PolicyStatus.AutoProlongation && policy.Status != PolicyStatus.AnnulRequest)
            {
                throw new BusinessException(BusinessErrorCode.PolicyNotAnnulledWhenStatusIncorrect);
            }

            // 2 дня - погрешность
            if (annul.AnnulledAt.AddDays(2) < policy.CommencementDateTime)
            {
                throw new BusinessException(BusinessErrorCode.PolicyAnnulDateIncorrect);
            }
        }

        result = policy.ChangeStatus(PolicyStatus.Annulled);
        policy.ChangeComplete(annul.Reason, annul.AnnulledAt);
    }

    /// <summary>
    ///     Автопролонгация
    /// </summary>
    public static void AutoProlongation(this CommonPolicy policy, out PolicyStatusChangesResult? result)
    {
        if (policy.Status is not PolicyStatus.Active and not PolicyStatus.Overdue)
        {
            throw new BusinessException(BusinessErrorCode.PolicyNotAutoProlongationWhenStatusIncorrect);
        }

        result = policy.ChangeStatus(PolicyStatus.AutoProlongation);
    }

    /// <summary>
    ///     Можно ли провести пролонгацию?
    /// </summary>
    public static bool CanProlongation(this CommonPolicy policy, out string? reason)
    {
        if (policy.Draft)
        {
            reason = "Нельзя пролонгировать черновик договора страхования";
            return false;
        }

        // статус договора - Проект, Действующий, Просроченный, Автопролонгация
        if (policy.Status != PolicyStatus.Project && policy.Status != PolicyStatus.Active &&
            policy.Status != PolicyStatus.Overdue && policy.Status != PolicyStatus.WaitingForPayment &&
            policy.Status != PolicyStatus.AutoProlongation)
        {
            reason = "Ошибка пролонгации. Некорректный статус договора страхования для пролонгации." +
                     " Допустимые статусы ДС:" +
                     $" «{PolicyStatus.Project.GetCachedDescription()}»," +
                     $" «{PolicyStatus.Active.GetCachedDescription()}»," +
                     $" «{PolicyStatus.WaitingForPayment.GetCachedDescription()}»," +
                     $" «{PolicyStatus.Overdue.GetCachedDescription()}»," +
                     $" «{PolicyStatus.AutoProlongation.GetCachedDescription()}»";
            return false;
        }

        reason = null;
        return true;
    }

    /// <summary>
    ///     Изменить коммент
    /// </summary>
    public static bool ChangeComment(this CommonPolicy policy, IPolicyUpdateComment input) =>
        policy.ChangeComment(input.Comment!);

    /// <summary>
    ///     Обновить страховые премии и суммы
    /// </summary>
    public static void ChangeInsuranceValuesForCurrentPeriod(this CommonPolicy policy, PolicyUpdateInsuranceTariff input,
        IEntityJsonPropertyContext jsonContext)
    {
        var currentPeriod = policy.CurrentPeriod();
        if (currentPeriod == null)
        {
            throw new BusinessException(BusinessErrorCode.PolicyCurrentPeriodNotFound,
                "Отсутствует текущий период для обновления страховых премий и сумм.");
        }

        var insuranceAmount = Money.Rub(input.InsuranceAmount);
        ChangeInsuranceAmountForPeriod(policy, currentPeriod, input, insuranceAmount, jsonContext);
    }

   

    /// <summary>
    ///     Закрыть ДС
    /// </summary>
    public static void Complete(this CommonPolicy policy, IPolicyComplete complete, out PolicyStatusChangesResult? result)
    {
        if (!complete.Force && policy.Status == PolicyStatus.Completed)
        {
            throw new BusinessException(BusinessErrorCode.PolicyNotCompletedWhenStatusIncorrect);
        }

        result = policy.ChangeStatus(PolicyStatus.Completed);
        policy.ChangeComplete(complete.Reason, complete.CompletedAt);
    }

    /// <summary>
    ///     Обновить страховые премии для указанного периода
    /// </summary>
    /// <param name="policy">ДС</param>
    /// <param name="period">Период</param>
    /// <param name="input">Данные для обновления страховых премий</param>
    /// <param name="insuranceAmount">Страховая сумма</param>
    /// <param name="jsonContext">JSON контекст</param>
    private static void ChangeInsuranceAmountForPeriod(CommonPolicy policy, PolicyPeriod period,
        PolicyUpdatePlannedPremiumForPeriod input,
        Money? insuranceAmount, IEntityJsonPropertyContext jsonContext)
    {
        // обновляем страховую сумму
        if (insuranceAmount != null)
        {
            period.ChangeInsuranceAmount(insuranceAmount);
        }

        if (policy is MortgagePolicy mortgagePolicy)
        {
            // обновляем страховую премию жизни
            if (mortgagePolicy.RiskSupported(InsuranceRisk.Life))
            {
                foreach (var lifeInsurance in mortgagePolicy.LifeInsurances)
                {
                    var index = lifeInsurance.InsuredPerson.Index;
                    if (!input.LifePremiumInput.ContainsKey(index))
                    {
                        throw new BusinessException(BusinessErrorCode.ValueUndefined,
                            $"Не указано значение для застрахованного лица {index}");
                    }

                    var lifeInsuranceAmount = insuranceAmount != null
                        ? insuranceAmount.Multiply(lifeInsurance.InsuredPerson.LoanShare)
                        : null;
                    var lifePremiumInput = input.LifePremiumInput[index];
                    lifeInsurance.Schedule.ChangeInsuranceValues(
                        period.Number,
                        Money.Rub(lifePremiumInput.PremiumAmount),
                        lifeInsuranceAmount,
                        lifePremiumInput.Rate
                    );
                    jsonContext.TrackModify(lifeInsurance, x => x.Schedule);
                }
            }

            // обновляем страховую премию имущества
            if (mortgagePolicy.RiskSupported(InsuranceRisk.Property))
            {
                foreach (var propertyIns in mortgagePolicy.PropertyInsurances)
                {
                    if (!input.PropertyPremiumInput.ContainsKey(propertyIns.Id))
                    {
                        throw new BusinessException(BusinessErrorCode.ValueUndefined,
                            "Не указано значение для имущества.");
                    }

                    var propertyInsuranceAmount = insuranceAmount != null
                        ? insuranceAmount.Multiply(propertyIns.LoanShare ?? 1)
                        : null;
                    var propertyPremiumInput = input.PropertyPremiumInput[propertyIns.Id];
                    propertyIns.Schedule.ChangeInsuranceValues(
                        period.Number,
                        Money.Rub(propertyPremiumInput.PremiumAmount),
                        propertyInsuranceAmount,
                        propertyPremiumInput.Rate
                    );
                    jsonContext.TrackModify(propertyIns, x => x.Schedule);
                }
            }

            // обновляем страховую премию титула
            if (mortgagePolicy.RiskSupported(InsuranceRisk.Title))
            {
                foreach (var titleIns in mortgagePolicy.TitleInsurances)
                {
                    if (!input.TitlePremiumInput.ContainsKey(titleIns.Id))
                    {
                        throw new BusinessException(BusinessErrorCode.ValueUndefined,
                            "Не указано значение для титула.");
                    }

                    var titleInsuranceAmount = insuranceAmount != null
                        ? insuranceAmount.Multiply(titleIns.LoanShare ?? 1)
                        : null;
                    var titlePremiumInput = input.TitlePremiumInput[titleIns.Id];
                    titleIns.Schedule.ChangeInsuranceValues(
                        period.Number,
                        Money.Rub(titlePremiumInput.PremiumAmount),
                        titleInsuranceAmount,
                        titlePremiumInput.Rate
                    );
                    jsonContext.TrackModify(titleIns, x => x.Schedule);
                }
            }

            // обновляем плановую страховую премию
            var rows = MortgagePolicyExtensions
                .AllInsuranceScheduleForSomePeriod(mortgagePolicy, period.Number)
                .ToList();
            var totalPremiumAmount = rows.Sum(x => x.Tariff.PremiumAmount.Amount);
            period.PlannedPremium.ChangePlannedAmount(totalPremiumAmount);
            jsonContext.TrackModify(period, x => x.PlannedPremium);
        }
    }

    /// <summary>
    ///     Завершить текущий период
    /// </summary>
    public static PolicyPeriod? TryCompeteCurrentPeriod(this CommonPolicy policy)
    {
        // завершаем прошлый период
        var currentPeriod = policy.CurrentPeriod();
        currentPeriod?.ChangeState(PolicyInsurancePeriodState.Completed);

        return currentPeriod;
    }

    /// <summary>
    ///     Текущий период страхования
    /// </summary>
    public static PolicyPeriod? CurrentPeriod(this CommonPolicy policy) =>
        policy.Periods
            .FirstOrDefault(x => x.State == PolicyInsurancePeriodState.Current);
    
    /// <summary>
    ///     Период страхования в прцоессе оплаты
    /// </summary>
    public static PolicyPeriod? PaymentProcessPeriod(this CommonPolicy policy) =>
        policy.Periods
            .FirstOrDefault(x => x.State == PolicyInsurancePeriodState.PaymentProcess);

    /// <summary>
    ///     Следующий плановый период страхования
    /// </summary>
    public static PolicyPeriod? FindPeriod(this CommonPolicy policy, int period) =>
        policy.Periods.FirstOrDefault(x => x.Number == period);


    /// <summary>
    ///     Получение списка автопролонгаций
    /// </summary>
    public static IEnumerable<AutoProlongation.AutoProlongation> GetAutoProlongations(this CommonPolicy policy) =>
        policy.Periods.Select(x => x.AutoProlongation).Where(x => x != null)!;

    /// <summary>
    ///     Получение последней автопролонгации
    /// </summary>
    public static AutoProlongation.AutoProlongation? GetLastAutoProlongationOrNull(this CommonPolicy policy) =>
        policy.GetAutoProlongations().LastOrDefault();

    /// <summary>
    ///     Последний оплаченный период
    /// </summary>
    public static PolicyPeriod? LastPaidPeriod(this CommonPolicy policy) =>
        policy.Periods.LastOrDefault(x => x.HasActualPremium);

    /// <summary>
    ///     Следующий плановый период страхования
    /// </summary>
    public static PolicyPeriod? NextPlannedPeriod(this CommonPolicy policy) =>
        policy.Periods.OrderBy(x => x.Number).FirstOrDefault(x => x.State == PolicyInsurancePeriodState.Planned);
}