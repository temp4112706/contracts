using Some.Abstractions.Enums;
using Some.Domain.PolicyEvents;

namespace Some.Domain.Policies;

/// <summary>
///     Расширения для <see cref="PolicyEvent"/>
/// </summary>
public static class PolicyEventExtensions
{
    /// <summary>
    ///     Название эвента
    /// </summary>
    public static PolicyEventName ToEventName(this PolicyEvent @event)
    {
        return Enum.TryParse<PolicyEventName>(@event.Event, out var e) ? e : PolicyEventName.Unknown;
    }
}