﻿using System.ComponentModel.DataAnnotations.Schema;
using Ct.Module.Domain.Entities;
using Some.Abstractions.Enums;
using Some.Abstractions.Policies;
using Some.Domain.Attachments;
using Some.Domain.Customers;
using Some.Domain.Policies.ValueObjects;

// ReSharper disable UnusedMember.Local
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local

namespace Some.Domain.Policies;

/// <summary>
///     Страховой случай
/// </summary>
[Table("policy_insurance_accident")]
public sealed class PolicyInsuranceAccident : TrackableEntity<Guid>,
    IPolicyInsuranceAccident<PolicyInsuranceAccidentPaymentOperation>
{
    /// <summary>
    ///     Создание объекта
    /// </summary>
    public PolicyInsuranceAccident(
        Guid id,
        IScalarPolicy policy,
        InsuranceRisk? risk,
        Customer customer,
        DateTime accidentDate,
        PolicyInsuranceAccidentReason requestReason,
        DateTime requestDate,
        Attachment? attachment,
        string? comment,
        string? nAcc) : base(id)
    {
        PolicyId = policy.Id;
        State = string.IsNullOrWhiteSpace(nAcc)
            ? InsuranceAccidentState.CheckingAccountNumber
            : InsuranceAccidentState.WaitingPayment;
        Risk = risk;
        Customer = customer;
        CustomerId = customer.Id;
        AccidentDate = accidentDate;
        RequestReason = requestReason;
        RequestDate = requestDate;
        Attachment = attachment;
        AttachmentId = attachment?.Id;
        Comment = comment;
        NAcc = nAcc;
    }

    /// <summary>
    ///     Создание объекта
    /// </summary>
    public PolicyInsuranceAccident(
        Guid id,
        IScalarPolicy policy,
        InsuranceRisk risk,
        Guid customerId,
        DateTime accidentDate,
        PolicyInsuranceAccidentReason requestReason,
        DateTime requestDate,
        Guid? attachmentId,
        string? comment) : base(id)
    {
        PolicyId = policy.Id;
        State = InsuranceAccidentState.CheckingAccountNumber;
        Risk = risk;
        CustomerId = customerId;
        AccidentDate = accidentDate;
        RequestReason = requestReason;
        RequestDate = requestDate;
        AttachmentId = attachmentId;
        Comment = comment;
    }

    /// <summary>
    ///     Создание объекта
    /// </summary>
    private PolicyInsuranceAccident()
    {
    }

    /// <summary>
    ///     Файл
    /// </summary>
    public Attachment? Attachment { get; private set; }

    /// <summary>
    ///     Застрахованное лицо
    /// </summary>
    public Customer Customer { get; private set; } = null!;

    /// <summary>
    ///     ДС
    /// </summary>
    public CommonPolicy Policy { get; private set; } = null!;

    /// <summary>
    ///     Завершен
    /// </summary>
    [NotMapped]
    [Newtonsoft.Json.JsonIgnore]
    public bool Precessed => State == InsuranceAccidentState.Dispute || State == InsuranceAccidentState.Processed;

    /// <inheritdoc />
    [Column("comment")]
    public string? Comment { get; private set; }

    /// <summary>
    ///     Операции оплаты
    /// </summary>
    [Column("payment_operations")]
    public PolicyInsuranceAccidentPaymentOperation[]? PaymentOperations { get; private set; }

    /// <inheritdoc />
    [Column("request_date")]
    public DateTime RequestDate { get; private set; }

    /// <inheritdoc />
    [Column("request_reason")]
    public PolicyInsuranceAccidentReason RequestReason { get; private set; }

    /// <inheritdoc />
    [Column("risk")]
    public InsuranceRisk? Risk { get; private set; }

    /// <inheritdoc />
    [Column("accident_date")]
    public DateTime AccidentDate { get; private set; }

    /// <summary>
    ///     Номер счета для зачисления
    /// </summary>
    [Column("n_acc")]
    public string? NAcc { get; private set; }

    /// <summary>
    ///     Статус
    /// </summary>
    [Column("state")]
    public InsuranceAccidentState State { get; private set; }

    /// <summary>
    ///     Ид файла
    /// </summary>
    [Column("attachment_id")]
    private Guid? AttachmentId { get; set; }

    /// <summary>
    ///     Ид застрахованного лица
    /// </summary>
    [Column("customer_id")]
    private Guid CustomerId { get; set; }

    /// <summary>
    ///     ИД ДС
    /// </summary>
    [Column("policy_id")]
    private Guid PolicyId { get; set; }

    /// <summary>
    ///     Изменить
    /// </summary>
    public void Change(InsuranceAccidentState state, string? nAcc,
        PolicyInsuranceAccidentPaymentOperation[]? paymentOperations)
    {
        State = state;
        NAcc = nAcc;
        PaymentOperations = paymentOperations;
    }

    /// <summary>
    ///     Изменить клиента
    /// </summary>
    public void ChangeCustomer(Customer customer)
    {
        CustomerId = customer.Id;
        Customer = customer;
    }

    /// <summary>
    ///     Переход к статусу "Диспут"
    /// </summary>
    public void MoveToDispute(PolicyInsuranceAccidentPaymentOperation[] paymentOperations)
    {
        PaymentOperations = paymentOperations;
        State = InsuranceAccidentState.Dispute;
    }

    /// <summary>
    ///     Переход к статусу "Обработан"
    /// </summary>
    public void MoveToProcessed(PolicyInsuranceAccidentPaymentOperation[] paymentOperations)
    {
        PaymentOperations = paymentOperations;
        State = InsuranceAccidentState.Processed;
    }

    /// <summary>
    ///     Переход к статусу "Ожидание оплаты"
    /// </summary>
    public void MoveToWaitingPayment(string nAcc)
    {
        NAcc = nAcc;
        State = InsuranceAccidentState.WaitingPayment;
    }
}