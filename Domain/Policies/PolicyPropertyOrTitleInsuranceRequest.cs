﻿using System.Diagnostics.CodeAnalysis;
using Some.Domain.Policies.ValueObjects;
using Some.Domain.Properties;

namespace Some.Domain.Policies;

/// <summary>
///     Реквест для создания индивидуального страхования имущества
/// </summary>
public class PolicyPropertyOrTitleInsuranceRequest
{
    [SuppressMessage("ReSharper", "UnusedMember.Local")]
    private PolicyPropertyOrTitleInsuranceRequest()
    {
    }
        
    /// <summary>
    ///     Создание объекта
    /// </summary>
    public PolicyPropertyOrTitleInsuranceRequest(Property? property, InsuranceSchedule? propertySchedule,
        InsuranceSchedule? titleSchedule)
    {
        Property = property;
        PropertySchedule = propertySchedule;
        TitleSchedule = titleSchedule;
    }

    /// <summary>
    ///     Создание объекта
    /// </summary>
    public PolicyPropertyOrTitleInsuranceRequest(Property? property, InsuranceSchedule? propertySchedule,
        InsuranceSchedule? titleSchedule, decimal? propertyInsuranceLoanShare, decimal? titleInsuranceLoanShare)
        : this(property, propertySchedule, titleSchedule)
    {
        PropertyInsuranceLoanShare = propertyInsuranceLoanShare;
        TitleInsuranceLoanShare = titleInsuranceLoanShare;
    }

    /// <summary>
    ///     График страхования имущества
    /// </summary>
    public InsuranceSchedule? PropertySchedule { get; }
        
    /// <summary>
    ///     График страхования титула
    /// </summary>
    public InsuranceSchedule? TitleSchedule { get; }
        
    /// <summary>
    ///     Застрахованное имущество
    /// </summary>
    public Property? Property { get; }

    /// <summary>
    ///     Процент страхования имущества
    /// </summary>
    public decimal? PropertyInsuranceLoanShare { get; }

    /// <summary>
    ///     Процент страхования титула
    /// </summary>
    public decimal? TitleInsuranceLoanShare { get; }
}