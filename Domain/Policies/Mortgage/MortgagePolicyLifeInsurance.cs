using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using Ct.Module.Domain.Entities;
using Some.Abstractions.Policies;
using Some.Abstractions.Policies.Mortgage;
using Some.Domain.InsuredPersons;
using Some.Domain.Monies;
using Some.Domain.Policies.ValueObjects;
using Some.Domain.Underwriting.ValueObjects;

namespace Some.Domain.Policies.Mortgage;

/// <summary>
///     Индивидуальные условия страхования жизни
/// </summary>
[Table("policy_life_insurance")]
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Local")]
[SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local")]
public sealed class MortgagePolicyLifeInsurance : BaseEntity<Guid>, IMortgagePolicyLifeInsurance<Money, InsuredPerson,
    InsurancePeriod, InsuranceTariff, InsuranceScheduleRow, InsuranceSchedule>
{
    /// <summary>
    ///     Создание условий страхования жизни
    /// </summary>
    public MortgagePolicyLifeInsurance(Guid id, IScalarPolicy policy, InsuredPerson insuredPerson,
        InsuranceSchedule schedule) : base(id)
    {
        InsuredPerson = insuredPerson;
        Schedule = schedule;
        PolicyId = policy.Id;
        InsuredPersonId = insuredPerson.Id;
    }

    // ReSharper disable once UnusedMember.Local
    private MortgagePolicyLifeInsurance()
    {
    }

    /// <summary>
    ///     График страхования
    /// </summary>
    [Column("schedule")]
    public InsuranceSchedule Schedule { get; private set; } = null!;

    /// <summary>
    ///     Застрахованное лицо
    /// </summary>
    public InsuredPerson InsuredPerson { get; private set; } = null!;

    /// <summary>
    ///     ИД застрахованного лица
    /// </summary>
    // ReSharper disable once UnassignedGetOnlyAutoProperty
    [Column("insured_person_id")]
    private Guid InsuredPersonId { get; set; }

    /// <summary>
    ///     ИД ДС
    /// </summary>
    [Column("policy_id")]
    private Guid PolicyId { get; }

    /// <summary>
    ///     Изменить график страхования
    /// </summary>
    /// <param name="schedule">Новый график страхования</param>
    public void ChangeSchedule(InsuranceSchedule schedule) => Schedule = schedule;
}