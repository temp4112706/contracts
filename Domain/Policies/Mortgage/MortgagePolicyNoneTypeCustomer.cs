﻿using System.ComponentModel.DataAnnotations.Schema;
using Ct.Module.Domain.Entities;
using Some.Domain.Customers;

namespace Some.Domain.Policies.Mortgage;

/// <summary>
///     Клиент, не участвующий в ДС
/// </summary>
[Table("policy_none_type_customer")]
public class MortgagePolicyNoneTypeCustomer : BaseEntity<Guid>
{
    /// <summary>
    ///     Создание объекта
    /// </summary>
    public MortgagePolicyNoneTypeCustomer(Guid id, MortgagePolicy policy, Customer customer) : base(id)
    {
        Policy = policy;
        Customer = customer;
    }

    /// <summary>
    ///     Создание объекта
    /// </summary>
    public MortgagePolicyNoneTypeCustomer(Guid id, Guid policyId, Guid customerId) : base(id)
    {
        PolicyId = policyId;
        CustomerId = customerId;
    }

    /// <summary>
    ///     Создание объекта
    /// </summary>
    private MortgagePolicyNoneTypeCustomer()
    {
    }

    /// <summary>
    ///     Клиент
    /// </summary>
    public Customer Customer { get; private set; } = null!;

    /// <summary>
    ///     ДС
    /// </summary>
    public MortgagePolicy Policy { get; } = null!;

    /// <summary>
    ///     ИД клиента
    /// </summary>
    [Column("customer_id")]
    private Guid CustomerId { get; set; }

    /// <summary>
    ///     ИД ДС
    /// </summary>
    [Column("policy_id")]
    private Guid PolicyId { get; }

    /// <summary>
    ///     Изменить клиента
    /// </summary>
    public void ChangeCustomer(Customer customer)
    {
        CustomerId = customer.Id;
        Customer = customer;
    }
}