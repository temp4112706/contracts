using Some.Abstractions.Enums;
using Some.Domain.Customers;
using Some.Domain.InsuredPersons;
using Some.Domain.Properties;

namespace Some.Domain.Policies.Mortgage;

/// <summary>
///     Расширения для <see cref="MortgagePolicy" />
/// </summary>
public static class MortgagePolicyExtensions
{
    /// <summary>
    ///     Найти застрахованное лицо по индексу
    /// </summary>
    /// <param name="policy">ДС</param>
    /// <param name="index">Индекс застрахованного лица</param>
    /// <returns>Застрахованное лицо</returns>
    public static InsuredPerson? FindInsuredPerson(this MortgagePolicy policy, InsuredPersonIndex index) =>
        FindLifeInsurance(policy, index)?.InsuredPerson;

    /// <summary>
    ///     Найти страхование жизни по индексу
    /// </summary>
    /// <param name="policy">ДС</param>
    /// <param name="index">Индекс застрахованного лица</param>
    /// <returns>Страхование жизни</returns>
    public static MortgagePolicyLifeInsurance? FindLifeInsurance(this MortgagePolicy policy, InsuredPersonIndex index)
    {
        var insuranceRequest = policy.LifeInsurances.FirstOrDefault(x => x.InsuredPerson.Index == index);
        return insuranceRequest;
    }

    /// <summary>
    ///     Получение списка имущества в ДС
    /// </summary>
    public static IEnumerable<Property> GetAllProperties(this MortgagePolicy policy) =>
        policy.PropertyInsurances.Select(x => x.Property)
            .Concat(policy.TitleInsurances.Select(x => x.Property))
            .Where(x => x != null)
            .GroupBy(x => x!.Id)
            .Select(x => x.First())
            .ToList()!;

    /// <summary>
    ///     Получение списка застрахованных клиентов в ДС
    /// </summary>
    public static IReadOnlyCollection<Customer> GetInsuredCustomers(this MortgagePolicy policy) =>
        policy.GetAllInsuredPersons().Select(x => x.Customer).ToArray();

    /// <summary>
    ///     Получение списка застрахованных лиц в ДС
    /// </summary>
    public static IEnumerable<InsuredPerson> GetAllInsuredPersons(this MortgagePolicy policy) =>
        policy.LifeInsurances.Select(x => x.InsuredPerson);

    /// <summary>
    ///     Получить условия страхования жизни на основе индекса застрахованного лица
    /// </summary>
    /// <param name="policy">ДС</param>
    /// <param name="index">Индекс застрахованного лица</param>
    /// <returns>Условия страхования жизни</returns>
    public static MortgagePolicyLifeInsurance? GetLifeInsurance(this MortgagePolicy policy, InsuredPersonIndex index)
    {
        if (policy.LifeInsurances.Count == 0)
        {
            return null;
        }

        return policy.LifeInsurances.FirstOrDefault(x =>
            x.InsuredPerson.Index == index);
    }

    /// <summary>
    ///     Поддерживается риск
    /// </summary>
    /// <param name="policy">ДС</param>
    /// <param name="risk">Проверяемый риск</param>
    /// <returns>Результат проверки</returns>
    public static bool RiskSupported(this MortgagePolicy policy, InsuranceRisk risk) => policy.Risks.Contains(risk);

    /// <summary>
    ///     Обновить риски
    /// </summary>
    public static void SyncRisks(this MortgagePolicy policy)
    {
        var risks = new List<InsuranceRisk>();

        if (policy.LifeInsurances.Any())
        {
            risks.Add(InsuranceRisk.Life);
        }

        if (policy.PropertyInsurances.Any())
        {
            risks.Add(InsuranceRisk.Property);
        }

        if (policy.TitleInsurances.Any())
        {
            risks.Add(InsuranceRisk.Title);
        }

        policy.ChangeRisks(risks.ToArray());
    }
}