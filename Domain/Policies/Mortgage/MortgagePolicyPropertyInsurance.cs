using System.ComponentModel.DataAnnotations.Schema;
using Ct.Module.Domain.Entities;
using Some.Abstractions.Policies;
using Some.Abstractions.Policies.Mortgage;
using Some.Domain.Addresses;
using Some.Domain.Monies;
using Some.Domain.Policies.ValueObjects;
using Some.Domain.Properties;
using Some.Domain.Underwriting.ValueObjects;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local
// ReSharper disable UnusedAutoPropertyAccessor.Local
// ReSharper disable MemberCanBePrivate.Global

namespace Some.Domain.Policies.Mortgage;

/// <summary>
///     Условия страхования имущества
/// </summary>
[Table("policy_property_insurance")]
public class MortgagePolicyPropertyInsurance : BaseEntity<Guid>, IMortgagePolicyPropertyInsurance<Money, Property,
    InsurancePeriod, InsuranceTariff, InsuranceScheduleRow, InsuranceSchedule, Address>
{
    /// <summary>
    ///     Создание условий страхования имущества
    /// </summary>
    public MortgagePolicyPropertyInsurance(Guid id, IScalarPolicy policy, Property property, InsuranceSchedule schedule,
        decimal? loanShare) : this(id, policy, property, schedule)
    {
        LoanShare = loanShare ?? 1;
    }

    /// <summary>
    ///     Создание условий страхования имущества
    /// </summary>
    private MortgagePolicyPropertyInsurance(Guid id, IScalarPolicy policy, Property property,
        InsuranceSchedule schedule)
        : base(id)
    {
        Property = property;
        Schedule = schedule;
        PolicyId = policy.Id;
        PropertyId = property.Id;
    }

    // ReSharper disable once UnusedMember.Local
    private MortgagePolicyPropertyInsurance()
    {
    }

    /// <summary>
    ///     Доля страхования в процентах (0 ≤ LoanShare ≤ 1)
    /// </summary>
    [Column("loan_share")]
    public decimal? LoanShare { get; private set; }

    /// <summary>
    ///     Застрахованное имущество
    /// </summary>
    public Property? Property { get; private set; }

    /// <summary>
    ///     График страхования
    /// </summary>
    [Column("schedule")]
    public InsuranceSchedule Schedule { get; internal set; } = null!;

    /// <summary>
    ///     ИД страхового договора
    /// </summary>
    [Column("policy_id")]
    private Guid PolicyId { get; set; }

    /// <summary>
    ///     ИД застрахованного имущества
    /// </summary>
    [Column("property_id")]
    private Guid? PropertyId { get; set; }

    /// <summary>
    ///     Изменение доли страхования
    /// </summary>
    public void ChangeLoanShare(decimal value) => LoanShare = value;

    /// <summary>
    ///     Изменение данных об имуществе
    /// </summary>
    public bool ChangeProperty(Property property)
    {
        if (PropertyId == property.Id)
        {
            return false;
        }

        Property = property;
        PropertyId = property.Id;
        return true;
    }

    /// <summary>
    ///     Изменение графика страхования
    /// </summary>
    public void ChangeSchedule(InsuranceSchedule schedule) => Schedule = schedule;
}