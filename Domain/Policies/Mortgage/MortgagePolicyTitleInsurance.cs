using System.ComponentModel.DataAnnotations.Schema;
using Ct.Module.Domain.Entities;
using Some.Abstractions.Policies;
using Some.Abstractions.Policies.Mortgage;
using Some.Domain.Addresses;
using Some.Domain.Monies;
using Some.Domain.Policies.ValueObjects;
using Some.Domain.Properties;
using Some.Domain.Underwriting.ValueObjects;

// ReSharper disable UnusedAutoPropertyAccessor.Local

namespace Some.Domain.Policies.Mortgage;

/// <summary>
///     Индивидуальные условия титульного страхования
/// </summary>
[Table("policy_title_insurance")]
public sealed class MortgagePolicyTitleInsurance : BaseEntity<Guid>, IMortgagePolicyTitleInsurance<Money, Property,
    InsurancePeriod, InsuranceTariff, InsuranceScheduleRow, InsuranceSchedule, Address>
{
    /// <summary>
    ///     Создание условий страхования жизни
    /// </summary>
    public MortgagePolicyTitleInsurance(Guid id, IScalarPolicy policy, Property? property, InsuranceSchedule schedule,
        decimal? loanShare) : this(id, policy, property, schedule)
    {
        LoanShare = loanShare ?? 1;
    }

    /// <summary>
    ///     Создание условий страхования жизни
    /// </summary>
    private MortgagePolicyTitleInsurance(Guid id, IScalarPolicy policy, Property? property,
        InsuranceSchedule schedule) : base(id)
    {
        Property = property;
        Schedule = schedule;
        PolicyId = policy.Id;
        PropertyId = property?.Id;
    }

    // ReSharper disable once UnusedMember.Local
    private MortgagePolicyTitleInsurance()
    {
    }

    /// <summary>
    ///     Доля страхования в процентах (0 ≤ LoanShare ≤ 1)
    /// </summary>
    [Column("loan_share")]
    public decimal? LoanShare { get; private set; }

    /// <summary>
    ///     Застрахованное имущество
    /// </summary>
    public Property? Property { get; private set; }

    /// <summary>
    ///     График страхования
    /// </summary>
    [Column("schedule")]
    public InsuranceSchedule Schedule { get; private set; } = null!;

    /// <summary>
    ///     ИД ДС
    /// </summary>
    [Column("policy_id")]
    private Guid PolicyId { get; }

    /// <summary>
    ///     ИД застрахованного имущества
    /// </summary>
    [Column("property_id")]
    private Guid? PropertyId { get; set; }

    /// <summary>
    ///     Изменение доли страхования
    /// </summary>
    public void ChangeLoanShare(decimal value) => LoanShare = value;

    /// <summary>
    ///     Изменение данных об имуществе
    /// </summary>
    public bool ChangeProperty(Property? property)
    {
        if (PropertyId == property?.Id)
        {
            return false;
        }

        Property = property;
        PropertyId = property?.Id;
        return true;
    }

    /// <summary>
    ///     Изменение графика страхования
    /// </summary>
    public void ChangeSchedule(InsuranceSchedule schedule) => Schedule = schedule;
}