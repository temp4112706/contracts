using System.ComponentModel.DataAnnotations.Schema;
using Some.Abstractions.Enums;
using Some.Abstractions.Policies.Mortgage;
using Some.Domain.Companies;
using Some.Domain.Customers;
using Some.Domain.Employees;
using Some.Domain.Insurers;
using Some.Domain.LoanAgreements;
using Some.Domain.Mirs;
using Some.Domain.Monies;

// ReSharper disable UnusedMethodReturnValue.Global
// ReSharper disable UnusedMember.Global
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local
// ReSharper disable UnusedAutoPropertyAccessor.Local
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Domain.Policies.Mortgage;

/// <summary>
///     ДС
/// </summary>
public sealed class MortgagePolicy : CommonPolicy, IMortgagePolicy<Money, Customer, Insurer, LoanAgreement,
    MortgagePolicyLifeInsurance, MortgagePolicyPropertyInsurance, MortgagePolicyTitleInsurance,
    PolicyPeriod, Employee, PolicyInsuranceAccident, Company>
{
    /// <summary>
    ///     Создание объекта только с ИД
    /// </summary>
    public MortgagePolicy(Guid id) : base(id)
    {
    }

    /// <summary>
    ///     Создание договора страхования
    /// </summary>
    public MortgagePolicy(Guid id, Company? company, Employee? companyEmployee, bool manual, string number,
        PolicyStatus status, DateTime statusUpdatedAt, LoanAgreement loanAgreement, DateTime agreementDateTime,
        DateTime commencementDateTime, DateTime expiryDateTime, ICollection<PolicyPeriod> periods, Customer insurant,
        Insurer insurer, Employee? insurerEmployee, InsuranceRisk[] risks, PolicyCompleteReason? completeReason,
        DateTime? completedAt, string? mortgageRegion, string? detentionPlace, Guid? mirId, bool draft = false) 
        : base(id, company, companyEmployee, manual, number, status, statusUpdatedAt, agreementDateTime, 
            commencementDateTime, expiryDateTime, periods, insurant, insurer, insurerEmployee,
            completeReason, completedAt, draft)
    {
        LoanAgreement = loanAgreement;
        LoanAgreementId = loanAgreement.Id;
        Risks = risks;
        MortgageRegion = mortgageRegion;
        DetentionPlace = detentionPlace;
        MirId = mirId;
        LifeInsurances = new List<MortgagePolicyLifeInsurance>();
        PropertyInsurances = new List<MortgagePolicyPropertyInsurance>();
        TitleInsurances = new List<MortgagePolicyTitleInsurance>();
    }

    /// <inheritdoc />
    // ReSharper disable once UnusedMember.Local
    private MortgagePolicy()
    {
    }

    /// <summary>
    ///     Заявка
    /// </summary>
    public Mir? Mir { get; private set; }

    /// <inheritdoc />
    [Column("detention_place")]
    public string? DetentionPlace { get; private set; }

    /// <inheritdoc />
    public ICollection<MortgagePolicyLifeInsurance> LifeInsurances { get; private set; } = null!;

    /// <inheritdoc />
    public LoanAgreement LoanAgreement { get; private set; } = null!;

    /// <inheritdoc />
    [Column("mortgage_region")]
    public string? MortgageRegion { get; private set; }

    /// <inheritdoc />
    public ICollection<MortgagePolicyPropertyInsurance> PropertyInsurances { get; private set; } = null!;

    /// <inheritdoc />
    [Column("risks")]
    public InsuranceRisk[] Risks { get; private set; } = null!;

    /// <inheritdoc />
    public ICollection<MortgagePolicyTitleInsurance> TitleInsurances { get; private set; } = null!;

    /// <summary>
    ///     ИД кредитного договора
    /// </summary>
    [Column("loan_agreement_id")]
    private Guid LoanAgreementId { get; }

    /// <summary>
    ///     ИД заявки
    /// </summary>
    // ReSharper disable once UnassignedGetOnlyAutoProperty
    [Column("mir_id")]
    private Guid? MirId { get; }

    /// <summary>
    ///     Изменить место выдачи
    /// </summary>
    public bool ChangeDetentionPlace(string? detentionPlace)
    {
        if (DetentionPlace == detentionPlace)
        {
            return false;
        }

        DetentionPlace = detentionPlace;
        return true;
    }

    /// <summary>
    ///     Изменить регион предмета ипотеки
    /// </summary>
    public bool ChangeMortgageRegion(string mortgageRegion)
    {
        if (MortgageRegion == mortgageRegion)
        {
            return false;
        }

        MortgageRegion = mortgageRegion;
        return true;
    }

    /// <summary>
    ///     Изменить риски
    /// </summary>
    public void ChangeRisks(InsuranceRisk[] risks) => Risks = risks;
}