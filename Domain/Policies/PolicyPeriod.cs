﻿using System.ComponentModel.DataAnnotations.Schema;
using Ct.Module.Domain.Entities;
using Some.Abstractions.Enums;
using Some.Abstractions.Policies;
using Some.Domain.ExtraProperties;
using Some.Domain.Monies;
using Some.Domain.MutualSettlements;
using Some.Domain.Policies.ValueObjects;

// ReSharper disable UnusedAutoPropertyAccessor.Local
// ReSharper disable UnusedAutoPropertyAccessor.Global

// ReSharper disable UnusedMember.Global
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local

namespace Some.Domain.Policies;

/// <summary>
///     Период страхования в ДС
/// </summary>
[Table("policy_period")]
public class PolicyPeriod : BaseEntity<Guid>, IPolicyPeriod<Money, Guid?, PolicyPlannedPayment,
    PolicyActualPayment, PolicyActualPremiumPayment, PolicyPlannedPremium, PolicyActualPremium>, IHasExtraProperties
{

    
    /// <summary>
    ///     Создание объекта
    /// </summary>
    public PolicyPeriod(
        Guid id,
        PolicyInsurancePeriodState state,
        InsurancePeriod period,
        Money insuranceAmount,
        PolicyPlannedPremium plannedPremium,
        PolicyActualPremium? actualPremium) : base(id)
    {
        State = state;
        Number = period.Number;
        Duration = period.Duration;
        StartDate = period.StartDate;
        EndDate = period.EndDate;
        InsuranceAmount = insuranceAmount;
        PlannedPremium = plannedPremium;
        ActualPremium = actualPremium;
        ExtraProperties = new ExtraPropertyDictionary();
    }

    // ReSharper disable once UnusedMember.Local
    private PolicyPeriod()
    {
    }

    /// <summary>
    ///     Автопролонгация
    /// </summary>
    public AutoProlongation.AutoProlongation? AutoProlongation { get; private set; }

    /// <summary>
    ///     Есть фактическая оплата?
    /// </summary>
    [NotMapped]
    public bool HasActualPremium => ActualPremium != null;

    /// <summary>
    ///     Строка АС
    /// </summary>
    public ICollection<MutualSettlementRow> MutualSettlementRows { get; private set; } = null!;

    /// <summary>
    ///     ДС
    /// </summary>
    public CommonPolicy Policy { get; private set; } = null!;

    /// <summary>
    ///     ИД ДС
    /// </summary>
    [Column("policy_id")]
    public Guid PolicyId { get; private set; }

    /// <inheritdoc />
    [Column("extra_properties")]
    public ExtraPropertyDictionary? ExtraProperties { get; private set; }

    /// <inheritdoc />
    [Column("duration")]
    public int Duration { get; private set; }

    /// <inheritdoc />
    [Column("end_date")]
    public DateTime EndDate { get; private set; }

    /// <inheritdoc />
    [Column("number")]
    public int Number { get; private set; }

    /// <inheritdoc />
    [Column("start_date")]
    public DateTime StartDate { get; private set; }

    /// <inheritdoc />
    [Column("actual_premium")]
    public PolicyActualPremium? ActualPremium { get; internal set; }

    /// <inheritdoc />
    [Column("insurance_amount")]
    public Money InsuranceAmount { get; private set; } = null!;

    /// <inheritdoc />
    [Column("planned_premium")]
    public PolicyPlannedPremium PlannedPremium { get; private set; } = null!;

    /// <inheritdoc />
    [Column("state")]
    public PolicyInsurancePeriodState State { get; internal set; }

    /// <inheritdoc />
    [Column("payment_url")]
    public string? PaymentUrl { get; private set; }

    /// <summary>
    ///     Изменить фактическую сумму
    /// </summary>
    public void ChangeActualPremium(PolicyActualPremium? actualPremium) => ActualPremium = actualPremium;

    /// <summary>
    ///     Изменить ссылку на оплату
    /// </summary>
    public void ChangePaymentUrl(string? paymentUrl) => PaymentUrl = paymentUrl;
    
    /// <summary>
    ///     Обновить страховую сумму
    /// </summary>
    public void ChangeInsuranceAmount(Money insuranceAmount) => InsuranceAmount = insuranceAmount;

    /// <summary>
    ///     Изменить планируемую страховую премию
    /// </summary>
    public void ChangePlannedPremium(PolicyPlannedPremium plannedPremium) => PlannedPremium = plannedPremium;

    /// <summary>
    ///     Обновить ДС
    /// </summary>
    public void ChangePolicy(CommonPolicy policy)
    {
        Policy = policy;
        PolicyId = policy.Id;
    }

    /// <summary>
    ///     Изменить статус
    /// </summary>
    public void ChangeState(PolicyInsurancePeriodState value) => State = value;
}