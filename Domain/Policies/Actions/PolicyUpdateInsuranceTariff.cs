using Some.Abstractions.Enums;

namespace Some.Domain.Policies.Actions;

/// <summary>
///     Данные для изменения страховых премий и сумм в текущем периоде
/// </summary>
/// <param name="InsuranceAmount">Страховая сумма</param>
/// <param name="LifePremiumInput">Плановые страховые премии жизни</param>
/// <param name="PropertyPremiumInput">Плановые страховые премии имущества</param>
/// <param name="TitlePremiumInput">Плановые страховые премии титула</param>
public record PolicyUpdateInsuranceTariff(
        decimal InsuranceAmount,
        IDictionary<InsuredPersonIndex, PolicyUpdatePeriodTariff> LifePremiumInput,
        IDictionary<Guid, PolicyUpdatePeriodTariff> PropertyPremiumInput,
        IDictionary<Guid, PolicyUpdatePeriodTariff> TitlePremiumInput)
    : PolicyUpdatePlannedPremiumForPeriod(LifePremiumInput, PropertyPremiumInput, TitlePremiumInput);