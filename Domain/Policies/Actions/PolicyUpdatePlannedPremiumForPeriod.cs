using Some.Abstractions.Enums;

namespace Some.Domain.Policies.Actions;

/// <summary>
///     Данные для изменения плановых страховых премий
/// </summary>
/// <param name="LifePremiumInput">Плановые страховые премии жизни</param>
/// <param name="PropertyPremiumInput">Плановые страховые премии имущества</param>
/// <param name="TitlePremiumInput">Плановые страховые премии титула</param>
public record PolicyUpdatePlannedPremiumForPeriod(
    IDictionary<InsuredPersonIndex, PolicyUpdatePeriodTariff> LifePremiumInput,
    IDictionary<Guid, PolicyUpdatePeriodTariff> PropertyPremiumInput,
    IDictionary<Guid, PolicyUpdatePeriodTariff> TitlePremiumInput);