using Some.Abstractions.Enums;
using Some.Abstractions.Policies.Actions;

namespace Some.Domain.Policies.Actions;

/// <summary>
///     Доп инф <see cref="IPolicyComplete"/>
/// </summary>
public record PolicyComplete(PolicyCompleteReason Reason, DateTime CompletedAt, bool Force) : IPolicyComplete;