namespace Some.Domain.Policies.Actions;

/// <summary>
///     Данные для изменения план. страховых премий
/// </summary>
/// <param name="Rate">Тариф</param>
/// <param name="PremiumAmount">Страховая премия</param>
public record PolicyUpdatePeriodTariff(decimal? Rate, decimal PremiumAmount);