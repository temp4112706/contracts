﻿using System.Diagnostics.CodeAnalysis;
using Some.Abstractions.Policies.Actions;
using Some.Domain.Monies;
using Some.Domain.Policies.ValueObjects;

namespace Some.Domain.Policies.Actions;

/// <summary>
///     Период пролонгации в рассрочке пролонгации ДС 
/// </summary>
[SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local")]
public class PolicyInstallmentPeriod : IPolicyInstallmentPeriod<Money, Guid?,
    PolicyPlannedPayment, PolicyActualPayment>
{
    // ReSharper disable once UnusedMember.Local
    private PolicyInstallmentPeriod()
    {
            
    }
        
    /// <summary>
    ///     Создание объект
    /// </summary>
    public PolicyInstallmentPeriod(int period, PolicyPlannedPayment plannedPayment,
        PolicyActualPayment? actualPayment)
    {
        Period = period;
        PlannedPayment = plannedPayment;
        ActualPayment = actualPayment;
    }

    /// <inheritdoc />
    public int Period { get; private set; }

    /// <inheritdoc />
    public PolicyPlannedPayment PlannedPayment { get; private set; } = null!;

    /// <inheritdoc />
    public PolicyActualPayment? ActualPayment { get; private set; }
}