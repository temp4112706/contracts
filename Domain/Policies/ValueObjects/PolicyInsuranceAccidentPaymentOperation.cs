﻿using Some.Abstractions.Policies;
using Newtonsoft.Json;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local

namespace Some.Domain.Policies.ValueObjects;

/// <inheritdoc />
public class PolicyInsuranceAccidentPaymentOperation : IPolicyInsuranceAccidentPaymentOperation
{
    /// <summary>
    ///     Создание объекта
    /// </summary>
    [Newtonsoft.Json.JsonConstructor]
    public PolicyInsuranceAccidentPaymentOperation(
        [JsonProperty] string accountType,
        [JsonProperty] int currencyCode,
        [JsonProperty] decimal transactionSum,
        [JsonProperty] decimal balance,
        [JsonProperty] DateTime operationDate,
        [JsonProperty] string operationType)
    {
        AccountType = accountType;
        CurrencyCode = currencyCode;
        TransactionSum = transactionSum;
        Balance = balance;
        OperationDate = operationDate;
        OperationType = operationType;
    }

    private PolicyInsuranceAccidentPaymentOperation()
    {
    }

    /// <summary>
    ///     Тип аккаунта
    /// </summary>
    public string AccountType { get; private set; } = null!;

    /// <summary>
    ///     Баланс
    /// </summary>
    public decimal Balance { get; private set; }

    /// <summary>
    ///     Код валюты транзакции
    /// </summary>
    public int CurrencyCode { get; private set; }

    /// <summary>
    ///     Дата операции
    /// </summary>
    public DateTime OperationDate { get; private set; }

    /// <summary>
    ///     Тип операции
    /// </summary>
    public string OperationType { get; private set; } = null!;

    /// <summary>
    ///     Сумма транзакции
    /// </summary>
    public decimal TransactionSum { get; private set; }
}