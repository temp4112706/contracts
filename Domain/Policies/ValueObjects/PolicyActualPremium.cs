﻿using Some.Abstractions.Enums;
using Some.Abstractions.Policies;
using Some.Domain.Common.ValueObjects;
using Some.Domain.Monies;
using Newtonsoft.Json;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local

namespace Some.Domain.Policies.ValueObjects;

/// <summary>
///     Фактическая оплата периода в ДС
/// </summary>
public class PolicyActualPremium : IPolicyActualPremium<Money, Guid?, PolicyPlannedPayment, PolicyActualPayment,
    PolicyActualPremiumPayment>
{
    /// <summary>
    ///     Создание объекта
    /// </summary>
    [Newtonsoft.Json.JsonConstructor]
    public PolicyActualPremium(
        [JsonProperty] PolicyActualPremiumType type,
        [JsonProperty] ICollection<PolicyActualPremiumPayment> payments
    )
    {
        Type = type;
        Payments = payments.OrderBy(x => x.Period).ToList();
    }

    private PolicyActualPremium()
    {
        Type = PolicyActualPremiumType.OneTime;
        Payments = new List<PolicyActualPremiumPayment>();
    }

    /// <summary>
    ///     Получить последнюю оплату
    /// </summary>
    [Newtonsoft.Json.JsonIgnore]
    public PolicyActualPremiumPayment? LastPayment => Payments
        .LastOrDefault(x => x.ActualPayment != null);

    /// <summary>
    ///     Последняя дата оплаты
    /// </summary>
    public DateTime? LastPaymentDateTime => LastPayment?.ActualPayment?.PaymentDate;

    /// <summary>
    ///     Получить общую сумму оплаты.
    /// </summary>
    [Newtonsoft.Json.JsonIgnore]
    public ActualPayment? TotalActualPayment
    {
        get
        {
            switch (Type)
            {
                case PolicyActualPremiumType.OneTime:
                    var lastActualPayment = LastPayment?.ActualPayment;
                    return lastActualPayment != null
                        ? new ActualPayment(lastActualPayment.PaymentDate, lastActualPayment.ActualAmount)
                        : null;
                case PolicyActualPremiumType.Installment:
                    return Payments
                        .Where(x => x.ActualPayment != null)
                        .Aggregate(new ActualPayment(DateTime.UtcNow, Money.ZeroRub()), (c, p) =>
                        {
                            if (p.ActualPayment == null)
                            {
                                return c;
                            }

                            c.PaymentDate = p.ActualPayment.PaymentDate;
                            c.ActualAmount = c.ActualAmount.Plus(p.ActualPayment.ActualAmount);
                            return c;
                        });
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }

    /// <inheritdoc />
    public ICollection<PolicyActualPremiumPayment> Payments { get; private set; }

    /// <inheritdoc />
    public PolicyActualPremiumType Type { get; private set; }
}