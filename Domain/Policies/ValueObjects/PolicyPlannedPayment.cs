﻿using Some.Abstractions.Monies;
using Some.Abstractions.Policies;
using Some.Domain.Monies;
using Newtonsoft.Json;

namespace Some.Domain.Policies.ValueObjects;

/// <summary>
///     Плановая страховая оплата
/// </summary>
public class PolicyPlannedPayment : IPolicyPlannedPayment<Money>
{
    /// <summary>
    ///     Создание объекта
    /// </summary>
    [Newtonsoft.Json.JsonConstructor]
    public PolicyPlannedPayment(
        [JsonProperty] DateTime plannedDate,
        [JsonProperty] Money plannedAmount
    )
    {
        PlannedDate = plannedDate;
        PlannedAmount = plannedAmount;
    }

    /// <summary>
    ///     Создание объекта
    /// </summary>
    protected PolicyPlannedPayment()
    {
        PlannedAmount = Money.Rub(0);
    }

    /// <inheritdoc />
    public Money PlannedAmount { get; private set; }

    /// <inheritdoc />
    public DateTime PlannedDate { get; }

    /// <summary>
    ///     Обновить плановую страховую премию
    /// </summary>
    public void ChangePlannedAmount(decimal amount) => PlannedAmount = Money.Create(amount, PlannedAmount.Currency);

    /// <summary>
    ///     Обновить плановую страховую премию
    /// </summary>
    public void ChangePlannedAmount<TMoney>(TMoney amount)
        where TMoney : class, IMoney =>
        PlannedAmount = Money.Create(amount.Amount, amount.Currency);
}