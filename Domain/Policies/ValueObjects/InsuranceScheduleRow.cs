using Some.Abstractions.Policies;
using Some.Domain.Monies;
using Some.Domain.Underwriting.ValueObjects;
using Newtonsoft.Json;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local

namespace Some.Domain.Policies.ValueObjects;

/// <summary>
///     График страхования
/// </summary>
public class InsuranceScheduleRow : IInsuranceScheduleRow<Money, InsurancePeriod, InsuranceTariff>
{
    /// <summary>
    ///     Создание графика страхования
    /// </summary>
    [Newtonsoft.Json.JsonConstructor]
    public InsuranceScheduleRow(
        [JsonProperty] InsuranceTariff tariff,
        [JsonProperty] InsurancePeriod period)
    {
        Tariff = tariff;
        Period = period;
    }

    private InsuranceScheduleRow()
    {
    }

    /// <summary>
    ///     Период (год) страхования
    /// </summary>
    public InsurancePeriod Period { get; private set; } = null!;

    /// <summary>
    ///     Тариф
    /// </summary>
    public InsuranceTariff Tariff { get; private set; } = null!;
}