﻿using Some.Abstractions.Policies;
using Some.Domain.Monies;
using Newtonsoft.Json;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local

namespace Some.Domain.Policies.ValueObjects;

/// <summary>
///     Оплата периода в договоре страхования
/// </summary>
public class PolicyActualPremiumPayment : IPolicyActualPremiumPayment<Money, Guid?,
    PolicyPlannedPayment, PolicyActualPayment>
{
    /// <summary>
    ///     Создание объекта
    /// </summary>
    [Newtonsoft.Json.JsonConstructor]
    public PolicyActualPremiumPayment(
        [JsonProperty] int? period,
        [JsonProperty] PolicyPlannedPayment? plannedPayment,
        [JsonProperty] PolicyActualPayment? actualPayment
    )
    {
        Period = period;
        PlannedPayment = plannedPayment;
        ActualPayment = actualPayment;
    }

    private PolicyActualPremiumPayment()
    {
    }

    /// <inheritdoc />
    public PolicyActualPayment? ActualPayment { get; private set; }

    /// <inheritdoc />
    public int? Period { get; private set; }

    /// <inheritdoc />
    public PolicyPlannedPayment? PlannedPayment { get; private set; }

    /// <summary>
    ///     Обновить фактическую страховую оплату
    /// </summary>
    public void ChangeActualPayment(PolicyActualPayment actualPayment) => ActualPayment = actualPayment;
}