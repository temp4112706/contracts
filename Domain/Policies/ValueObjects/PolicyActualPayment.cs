﻿using Some.Abstractions.Policies;
using Some.Domain.Common.ValueObjects;
using Some.Domain.Monies;
using Newtonsoft.Json;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local

namespace Some.Domain.Policies.ValueObjects;

/// <summary>
///     Фактическая страховая оплата
/// </summary>
public class PolicyActualPayment : ActualPayment, IPolicyActualPayment<Money, Guid?>
{
    /// <summary>
    ///     Создание объекта
    /// </summary>
    [Newtonsoft.Json.JsonConstructor]
    public PolicyActualPayment(
        [JsonProperty] DateTime paymentDate,
        [JsonProperty] Money actualAmount,
        [JsonProperty] Guid? attachment
    ) : base(paymentDate, actualAmount)
    {
        Attachment = attachment;
    }

    /// <summary>
    ///     Создание объекта
    /// </summary>
    private PolicyActualPayment()
    {
    }

    /// <inheritdoc />
    public Guid? Attachment { get; private set; }
}