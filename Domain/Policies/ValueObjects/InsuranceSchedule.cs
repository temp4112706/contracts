﻿using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using Some.Abstractions.Policies;
using Some.Domain.Monies;
using Some.Domain.Underwriting.ValueObjects;
using Newtonsoft.Json;

namespace Some.Domain.Policies.ValueObjects;

/// <summary>
///     График страхования
/// </summary>
[SuppressMessage("ReSharper", "ConditionIsAlwaysTrueOrFalse")]
[SuppressMessage("ReSharper", "HeuristicUnreachableCode")]
public class InsuranceSchedule : ReadOnlyCollection<InsuranceScheduleRow>,
    IInsuranceSchedule<Money, InsurancePeriod, InsuranceTariff, InsuranceScheduleRow>
{
    /// <summary>
    ///     Создание графика платежа
    /// </summary>
    [Newtonsoft.Json.JsonConstructor]
    public InsuranceSchedule([JsonProperty] IEnumerable<InsuranceScheduleRow> list)
        : base(list.OrderBy(x => x.Period.Number).ToList())
    {
    }

    /// <summary>
    ///     Обновить страховую сумму  премию для указанного периода
    /// </summary>
    /// <param name="periodNumber">Номер периода</param>
    /// <param name="insuranceAmount">Страховая сумма для изменения</param>
    /// <param name="premiumAmount">Страховая премия для изменения</param>
    /// <param name="rate">Тариф</param>
    /// <returns>Результат операции</returns>
    public bool ChangeInsuranceValues(int periodNumber, Money? premiumAmount, Money? insuranceAmount, decimal? rate)
    {
        var row = this.FirstOrDefault(x => x.Period.Number == periodNumber);
        if (row == null)
        {
            return false;
        }

        if (insuranceAmount != null)
        {
            row.Tariff.ChangeInsuranceAmount(insuranceAmount);
        }

        if (premiumAmount != null)
        {
            row.Tariff.ChangePremiumAmount(premiumAmount);
        }

        if (rate != null)
        {
            row.Tariff.ChangeRate(rate.Value);
        }

        return true;
    }

    /// <summary>
    ///     Поиск строки графика по номеру периода
    /// </summary>
    /// <param name="periodNumber">Номер периода</param>
    /// <returns></returns>
    public InsuranceScheduleRow? FindByPeriodNumber(int? periodNumber) =>
        periodNumber == null ? null : this.FirstOrDefault(x => x.Period.Number == periodNumber);
}