﻿using System.Diagnostics.CodeAnalysis;
using Some.Abstractions.Enums;
using Some.Abstractions.Policies;
using Newtonsoft.Json;

namespace Some.Domain.Policies.ValueObjects;

/// <summary>
///     Результат перехода статуса в ДС
/// </summary>
[SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local")]
public class PolicyStatusChangesResult : IPolicyStatusChangesResult
{
    /// <summary>
    ///     Создание объекта
    /// </summary>
    [Newtonsoft.Json.JsonConstructor]
    public PolicyStatusChangesResult(
        [JsonProperty] PolicyStatus nextStatus,
        [JsonProperty] PolicyStatus? previousStatus,
        [JsonProperty] DateTime changedAt)
    {
        NextStatus = nextStatus;
        PreviousStatus = previousStatus;
        ChangedAt = changedAt;
    }

    private PolicyStatusChangesResult()
    {
    }

    /// <summary>
    ///     Дата и время перехода
    /// </summary>
    public DateTime ChangedAt { get; private set; }

    /// <inheritdoc />
    public PolicyStatus NextStatus { get; private set; }

    /// <inheritdoc />
    public PolicyStatus? PreviousStatus { get; private set; }
}