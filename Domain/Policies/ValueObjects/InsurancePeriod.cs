using Some.Abstractions.Common;
using Newtonsoft.Json;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global

namespace Some.Domain.Policies.ValueObjects;

/// <summary>
///     Период по страхованию
/// </summary>
public class InsurancePeriod : IInsurancePeriod, IComparable<InsurancePeriod>
{
    /// <summary>
    ///     Создание периода страхования
    /// </summary>
    [Newtonsoft.Json.JsonConstructor]
    public InsurancePeriod(
        [JsonProperty] int number,
        [JsonProperty] DateTime startDate,
        [JsonProperty] DateTime endDate,
        [JsonProperty] int duration)
    {
        Number = number;
        StartDate = startDate;
        EndDate = endDate;
        Duration = duration;
    }

    /// <summary>
    ///     Создание периода страхования
    /// </summary>
    protected InsurancePeriod()
    {
    }

    /// <summary>
    ///     Период
    /// </summary>
    public int Duration { get; protected set; }

    /// <summary>
    ///     Дата завершения
    /// </summary>
    public DateTime EndDate { get; protected set; }

    /// <summary>
    ///     Позиция
    /// </summary>
    public int Number { get; protected set; }

    /// <summary>
    ///     Дата начала
    /// </summary>
    public DateTime StartDate { get; protected set; }

    /// <summary>
    ///     Изменить дату завершения
    /// </summary>
    public void ChangeEndDate(DateTime date)
    {
        if (EndDate != date)
        {
            EndDate = date;
        }
    }

    /// <inheritdoc />
    public int CompareTo(InsurancePeriod? other)
    {
        if (ReferenceEquals(this, other))
        {
            return 0;
        }

        if (ReferenceEquals(null, other))
        {
            return 1;
        }

        return Number.CompareTo(other.Number);
    }
}