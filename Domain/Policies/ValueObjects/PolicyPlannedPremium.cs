﻿using Some.Abstractions.Policies;
using Some.Domain.Monies;
using Newtonsoft.Json;

namespace Some.Domain.Policies.ValueObjects;

/// <summary>
///     Плановая оплата периода в ДС
/// </summary>
public class PolicyPlannedPremium : PolicyPlannedPayment, IPolicyPlannedPremium<Money>
{
    /// <inheritdoc />
    [Newtonsoft.Json.JsonConstructor]
    public PolicyPlannedPremium(
        [JsonProperty] DateTime plannedDate,
        [JsonProperty] Money plannedAmount
    ) : base(plannedDate, plannedAmount)
    {
    }

    private PolicyPlannedPremium()
    {
    }
}