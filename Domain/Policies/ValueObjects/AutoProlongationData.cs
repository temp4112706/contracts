﻿using Some.Abstractions.Policies.AutoProlongation;
using Newtonsoft.Json;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local

namespace Some.Domain.Policies.ValueObjects;

/// <summary>
///     Value object <inheritdoc cref="IAutoProlongationData" />
/// </summary>
public class AutoProlongationData : IAutoProlongationData
{
    /// <summary>
    ///     Создание объекта
    /// </summary>
    [Newtonsoft.Json.JsonConstructor]
    public AutoProlongationData(
        [JsonProperty] string insurer,
        [JsonProperty] string policyNumber,
        [JsonProperty] DateTime policyDate,
        [JsonProperty] string insurant,
        [JsonProperty] decimal premiumAmount,
        [JsonProperty] DateTime paymentDate,
        [JsonProperty] string phoneNumber,
        [JsonProperty] string email
    )
    {
        Insurer = insurer;
        PolicyNumber = policyNumber;
        PolicyDate = policyDate;
        Insurant = insurant;
        PremiumAmount = premiumAmount;
        PaymentDate = paymentDate;
        PhoneNumber = phoneNumber;
        Email = email;
    }

    private AutoProlongationData()
    {
    }

    /// <inheritdoc />
    public string Email { get; private set; } = null!;

    /// <inheritdoc />
    public string Insurant { get; private set; } = null!;

    /// <inheritdoc />
    public string Insurer { get; private set; } = null!;

    /// <inheritdoc />
    public DateTime PaymentDate { get; private set; }

    /// <inheritdoc />
    public string PhoneNumber { get; private set; } = null!;

    /// <inheritdoc />
    public DateTime PolicyDate { get; private set; }

    /// <inheritdoc />
    public string PolicyNumber { get; private set; } = null!;

    /// <inheritdoc />
    public decimal PremiumAmount { get; private set; }
}