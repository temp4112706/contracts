using Some.Abstractions.Enums;
using Some.Abstractions.Policies.Additional;
using Some.Domain.Companies;
using Some.Domain.Customers;
using Some.Domain.Employees;
using Some.Domain.Insurers;
using Some.Domain.Monies;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local

namespace Some.Domain.Policies.Additional;

/// <summary>
///     ДС на доп вид страхования
/// </summary>
public class AdditionalPolicy : CommonPolicy, IAdditionalPolicy<Money, Customer, Insurer,
    AdditionalPolicyInsurance, PolicyPeriod, Employee, PolicyInsuranceAccident, Company>
{
    /// <summary>
    ///     Создание объекта только с ИД
    /// </summary>
    // ReSharper disable once UnusedMember.Global
    public AdditionalPolicy(Guid id) : base(id)
    {
    }

    /// <summary>
    ///     Создание договора страхования
    /// </summary>
    public AdditionalPolicy(Guid id, Company? company, Employee? companyEmployee, bool manual, string number,
        PolicyStatus status, DateTime statusUpdatedAt, DateTime agreementDateTime,
        DateTime commencementDateTime, DateTime expiryDateTime, ICollection<PolicyPeriod> periods, Customer insurant,
        Insurer insurer, Employee? insurerEmployee, PolicyCompleteReason? completeReason, DateTime? completedAt, 
        bool draft = false) : base(id, company, companyEmployee, manual, number, status, statusUpdatedAt,
        agreementDateTime, commencementDateTime, expiryDateTime, periods, insurant, insurer,
            insurerEmployee, completeReason, completedAt, draft)
    {
    }

    /// <inheritdoc />
    // ReSharper disable once UnusedMember.Local
    private AdditionalPolicy()
    {
    }

    /// <inheritdoc />
    public AdditionalPolicyInsurance AdditionalInsurance { get; private set; } = null!;
}