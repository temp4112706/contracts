using System.ComponentModel.DataAnnotations.Schema;
using Ct.Module.Domain.Entities;
using Some.Abstractions.Enums;
using Some.Abstractions.Policies.Additional;
using Some.Domain.Monies;

// ReSharper disable UnusedAutoPropertyAccessor.Local
// ReSharper disable UnusedMember.Local
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local

namespace Some.Domain.Policies.Additional;

/// <inheritdoc cref="IAdditionalPolicyInsurance{T}" />
[Table("policy_additional_insurance")]
public class AdditionalPolicyInsurance : BaseEntity<Guid>, IAdditionalPolicyInsurance<Money>
{
    /// <inheritdoc />
    public AdditionalPolicyInsurance(Guid id, Guid policyId, string data, AdditionalInsuranceType type,
        Money insuranceAmount, Money premiumAmount) : base(id)
    {
        PolicyId = policyId;
        Data = data;
        Type = type;
        PremiumAmount = premiumAmount;
        InsuranceAmount = insuranceAmount;
    }

    private AdditionalPolicyInsurance()
    {
    }

    /// <summary>
    ///     Страховые данные
    /// </summary>
    [Column("data")]
    public string Data { get; private set; } = null!;

    /// <inheritdoc />
    [Column("insurance_amount")]
    public Money? InsuranceAmount { get; private set; }

    /// <inheritdoc />
    [Column("premium_amount")]
    public Money? PremiumAmount { get; private set; }

    /// <inheritdoc />
    [Column("type")]
    public AdditionalInsuranceType Type { get; private set; }

    /// <summary>
    ///     ИД ДС
    /// </summary>
    [Column("policy_id")]
    private Guid PolicyId { get; set; }
}