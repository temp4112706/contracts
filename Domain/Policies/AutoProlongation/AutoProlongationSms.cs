using System.ComponentModel.DataAnnotations.Schema;
using Ct.Module.Domain.Entities;
using Some.Abstractions.Enums;
using Some.Abstractions.Policies.AutoProlongation;

// ReSharper disable UnusedMember.Global
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Domain.Policies.AutoProlongation;

/// <summary>
///     SMS сообщение автопролонгации
/// </summary>
[Table("auto_prolongation_sms")]
public class AutoProlongationSms : TrackableEntity<Guid>, IAutoProlongationSms
{
    /// <summary>
    ///     Создание периода страхования
    /// </summary>
    public AutoProlongationSms(Guid id, string autoProlongationId, string phoneNumber, DateTime prolongationDate,
        string paymentOrderUrl, AutoProlongationSmsStatus status, string wave) : base(id)
    {
        AutoProlongationId = autoProlongationId;
        PhoneNumber = phoneNumber;
        ProlongationDate = prolongationDate;
        PaymentOrderUrl = paymentOrderUrl;
        Status = status;
        Wave = wave;
    }

    /// <inheritdoc />
    // ReSharper disable once UnusedMember.Local
    private AutoProlongationSms()
    {
    }

    /// <summary>
    ///     Автопролонгация
    /// </summary>
    public AutoProlongation AutoProlongation { get; private set; } = null!;

    /// <summary>
    ///     ИД автопролонгации
    /// </summary>
    [Column("auto_prolongation_id")]
    public string AutoProlongationId { get; } = null!;

    /// <summary>
    ///     Номер мобильного телефона для уведомления
    /// </summary>
    [Column("phone_number")]
    public string PhoneNumber { get; } = null!;

    /// <summary>
    ///     План. дата пролонгации
    /// </summary>
    [Column("prolongation_date")]
    public DateTime ProlongationDate { get; }

    /// <summary>
    ///     Код кампании в CRM
    /// </summary>
    [Column("wave")]
    public string Wave { get; } = null!;

    /// <inheritdoc />
    [Column("payment_order_url")]
    public string PaymentOrderUrl { get; } = null!;

    /// <inheritdoc />
    [Column("sent_at")]
    public DateTime? SentAt { get; private set; }

    /// <inheritdoc />
    [Column("status")]
    public AutoProlongationSmsStatus Status { get; private set; }

    /// <summary>
    ///     Изменить <see cref="SentAt" />
    /// </summary>
    public void ChangeSentAt(DateTime value) => SentAt = value;

    /// <summary>
    ///     Изменить <see cref="Status" />
    /// </summary>
    public void ChangeStatus(AutoProlongationSmsStatus value) => Status = value;
}