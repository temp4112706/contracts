using System.ComponentModel.DataAnnotations.Schema;
using Ct.Module.Domain.Entities;
using Some.Abstractions.Enums;
using Some.Abstractions.Policies.AutoProlongation;
using Some.Domain.Policies.ValueObjects;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local
// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable UnusedAutoPropertyAccessor.Local

namespace Some.Domain.Policies.AutoProlongation;

/// <summary>
///     Автопролонгация
/// </summary>
[Table("auto_prolongation")]
public class AutoProlongation : TrackableEntity<string>, IAutoProlongation<AutoProlongationSms>
{
    /// <inheritdoc />
    public AutoProlongation(string id, AutoProlongationData data, AutoProlongationStatus status,
        Guid policyPeriodId, Guid? importId) : base(id)
    {
        Data = data;
        Status = status;
        SmsMessages = new List<AutoProlongationSms>();
        PolicyPeriodId = policyPeriodId;
        ImportId = importId;
    }

    /// <inheritdoc />
    // ReSharper disable once UnusedMember.Local
    private AutoProlongation()
    {
    }

    /// <summary>
    ///     Данные автопролонгации
    /// </summary>
    [Column("data")]
    public AutoProlongationData Data { get; private set; } = null!;

    /// <summary>
    ///     ИД загрузки автопролонгаций
    /// </summary>
    [Column("import_id")]
    public Guid? ImportId { get; private set; }

    /// <summary>
    ///     Последнее СМС сообщение
    /// </summary>
    public AutoProlongationSms? LastSmsMessage => SmsMessages.LastOrDefault();

    /// <summary>
    ///     Период ДС
    /// </summary>
    public PolicyPeriod? PolicyPeriod { get; private set; }

    /// <summary>
    ///     ИД периода страхования
    /// </summary>
    [Column("policy_period_id")]
    public Guid? PolicyPeriodId { get; private set; }

    /// <inheritdoc />
    public ICollection<AutoProlongationSms> SmsMessages { get; private set; } = null!;

    /// <inheritdoc />
    [Column("status")]
    public AutoProlongationStatus? Status { get; private set; }

    /// <summary>
    ///     Изменить статус
    /// </summary>
    public void ChangeStatus(AutoProlongationStatus value) => Status = value;
}