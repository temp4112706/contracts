using Some.Abstractions.Enums;
using Some.Domain.Policies.Mortgage;
using Some.Domain.Policies.ValueObjects;

namespace Some.Domain.Policies;

/// <summary>
///     Расширения для <see cref="MortgagePolicy" />
/// </summary>
public static class MortgagePolicyExtensions
{
    /// <summary>
    ///     Список графиков страхования по рискам
    /// </summary>
    internal static IEnumerable<InsuranceScheduleRow> AllInsuranceScheduleForSomePeriod(MortgagePolicy policy,
        int periodNumber)
    {
        var result = new List<InsuranceScheduleRow>();

        bool Where(InsuranceScheduleRow row)
        {
            return row.Period.Number == periodNumber;
        }

        if (policy.RiskSupported(InsuranceRisk.Life))
        {
            var lifeRows = policy.LifeInsurances
                .Select(x => x.Schedule.FirstOrDefault(Where))
                .Where(x => x != null);
            result.AddRange(lifeRows!);
        }

        if (policy.RiskSupported(InsuranceRisk.Property))
        {
            var propertyRows = policy.PropertyInsurances
                .Select(x => x.Schedule.FirstOrDefault(Where))
                .Where(x => x != null);
            result.AddRange(propertyRows!);
        }

        if (policy.RiskSupported(InsuranceRisk.Title))
        {
            var titleRows = policy.TitleInsurances
                .Select(x => x.Schedule.FirstOrDefault(Where))
                .Where(x => x != null);
            result.AddRange(titleRows!);
        }

        return result;
    }
}