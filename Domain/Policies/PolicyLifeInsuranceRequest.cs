﻿using System.Diagnostics.CodeAnalysis;
using Some.Domain.InsuredPersons;
using Some.Domain.Policies.ValueObjects;

namespace Some.Domain.Policies;

/// <summary>
///     Реквест для создания индивидуального страхования жизни
/// </summary>
public class PolicyLifeInsuranceRequest
{
    [SuppressMessage("ReSharper", "UnusedMember.Local")]
    private PolicyLifeInsuranceRequest()
    {
    }
        
    /// <summary>
    ///     Создание объекта
    /// </summary>
    public PolicyLifeInsuranceRequest(InsuredPerson insuredPerson, InsuranceSchedule schedule)
    {
        InsuredPerson = insuredPerson;
        Schedule = schedule;
    }

    /// <summary>
    ///     График страхования
    /// </summary>
    public InsuranceSchedule Schedule { get; } = null!;

    /// <summary>
    ///     Застрахованное лицо
    /// </summary>
    public InsuredPerson InsuredPerson { get; } = null!;
}