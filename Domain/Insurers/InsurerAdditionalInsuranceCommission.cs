﻿using System.ComponentModel.DataAnnotations.Schema;
using Some.Abstractions.Enums;
using Some.Abstractions.Extensions;
using Some.Abstractions.Insurers;
using Newtonsoft.Json;

// ReSharper disable ReplaceAutoPropertyWithComputedProperty
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local

namespace Some.Domain.Insurers;

/// <inheritdoc />
[Table("insurer_additional_insurance_commission")]
public class InsurerAdditionalInsuranceCommission : IInsurerAdditionalInsuranceCommission
{
    /// <summary>
    ///     ctor
    /// </summary>
    [JsonConstructor]
    public InsurerAdditionalInsuranceCommission(
        int order,
        InsurerId insurerId,
        AdditionalInsuranceType insuranceType,
        string companyId,
        [JsonProperty] DateTime? policyFrom,
        [JsonProperty] DateTime? policyTo,
        [JsonProperty] decimal percent)
    {
        Order = order;
        InsurerId = insurerId.ToValue();
        InsuranceType = insuranceType;
        CompanyId = companyId;
        PolicyFrom = policyFrom;
        PolicyTo = policyTo;
        Percent = percent;
    }

    private InsurerAdditionalInsuranceCommission()
    {
    }

    /// <inheritdoc />
    public decimal Percent { get; private set; }

    /// <inheritdoc />
    public DateTime? PolicyFrom { get; private set; }

    /// <inheritdoc />
    public DateTime? PolicyTo { get; private set; }

    /// <inheritdoc />
    public AdditionalInsuranceType InsuranceType { get;private set; }

    /// <inheritdoc />
    public string InsurerId { get; private set; } = null!;

    /// <inheritdoc />
    public string CompanyId { get; private set; } = null!;
    
    /// <inheritdoc />
    public int Order { get; private set;}
}