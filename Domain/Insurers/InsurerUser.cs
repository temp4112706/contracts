using System.ComponentModel.DataAnnotations.Schema;
using Ct.Module.Domain.Entities;
using Some.Abstractions.Enums;
using Some.Abstractions.Extensions;
using Some.Domain.Companies;

namespace Some.Domain.Insurers;

/// <summary>
///     Пользователь СК
/// </summary>
[Table("insurer_user")]
public class InsurerUser : TrackableEntity<Guid>
{
    /// <inheritdoc />
    public InsurerUser(Guid id, InsurerId insurerId, string fullName, string email, bool enabled,
        Company company) : base(id)
    {
        InsurerId = insurerId.ToValue();
        FullName = fullName;
        Email = email;
        Enabled = enabled;

        Company = company;
        CompanyId = company.Id;
    }

    // ReSharper disable once UnusedMember.Local
    private InsurerUser()
    {
    }


    public Company? Company { get; }


    /// <summary>
    ///     ID связанной компании владелеца
    /// </summary>
    [Column("company_id")]
    public string? CompanyId { get; }

    /// <summary>
    ///     Email
    /// </summary>
    [Column("email")]
    public string Email { get; } = null!;

    /// <summary>
    ///     Включен/отключен
    /// </summary>
    [Column("enabled")]
    public bool Enabled { get; private set; }

    /// <summary>
    ///     ФИО
    /// </summary>
    [Column("full_name")]
    public string FullName { get; private set; } = null!;

    /// <summary>
    ///     Хешированный пароль
    /// </summary>
    [Column("hashed_password")]
    public string HashedPassword { get; private set; } = null!;

    /// <summary>
    ///     ИД СК
    /// </summary>
    [Column("insurer_id")]
    public string InsurerId { get; } = null!;

    /// <summary>
    ///     Изменить хешированный пароль
    /// </summary>
    public void ChangePassword(string hashedPassword) => HashedPassword = hashedPassword;

    /// <summary>
    ///     Изменить данные
    /// </summary>
    public void ChangeProps(string fullName, bool enabled)
    {
        FullName = fullName;
        Enabled = enabled;
    }
}