﻿using System.ComponentModel.DataAnnotations.Schema;
using Some.Abstractions.Enums;
using Some.Abstractions.Extensions;
using Some.Abstractions.Insurers;
using Newtonsoft.Json;

// ReSharper disable ReplaceAutoPropertyWithComputedProperty
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local

namespace Some.Domain.Insurers;

/// <inheritdoc />
[Table("insurer_mortgage_insurance_commission")]
public class InsurerMortgageInsuranceCommission : IInsurerMortgageInsuranceCommission
{
    /// <summary>
    ///     Создание объекта
    /// </summary>
    [JsonConstructor]
    public InsurerMortgageInsuranceCommission(
        int order,
        InsurerId insurerId,
        BeneficiaryId beneficiaryId,
        string companyId,
        DateTime? loanAgreementFrom,
        DateTime? loanAgreementTo,
        decimal percent,
        DateTime? policyFrom,
        DateTime? policyTo
    )
    {
        Order = order;
        InsurerId = insurerId.ToValue();
        BeneficiaryId = beneficiaryId.ToValue();
        CompanyId = companyId;
        LoanAgreementFrom = loanAgreementFrom;
        LoanAgreementTo = loanAgreementTo;
        Percent = percent;
        PolicyFrom = policyFrom;
        PolicyTo = policyTo;
    }

    private InsurerMortgageInsuranceCommission()
    {
    }


    /// <inheritdoc />
    public DateTime? LoanAgreementFrom { get; private set; }

    /// <inheritdoc />
    public DateTime? LoanAgreementTo { get; private set; }

    /// <inheritdoc />
    public decimal Percent { get; private set; }

    /// <inheritdoc />
    public DateTime? PolicyFrom { get; private set; }

    /// <inheritdoc />
    public DateTime? PolicyTo { get; private set; }

    /// <inheritdoc />
    public string BeneficiaryId { get; private set; } = null!;

    /// <inheritdoc />
    public string InsurerId { get; private set; } = null!;

    /// <inheritdoc />
    public string CompanyId { get; private set;} = null!;

    /// <inheritdoc />
    public int Order { get; private set;}
}