using System.ComponentModel.DataAnnotations.Schema;
using Ct.Module.Domain.Entities;
using Some.Abstractions.Enums;
using Some.Abstractions.Extensions;
using Some.Abstractions.Insurers;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local

namespace Some.Domain.Insurers;

/// <inheritdoc cref="Some.Abstractions.Insurers.IInsurerPolicyNumberTemplate{T}" />
[Table("insurer_policy_number_template")]
public class InsurerPolicyNumberTemplate : TrackableEntity<Guid>,
    IInsurerPolicyNumberTemplate<InsurerPolicyNumberTemplateConfig>
{
    /// <summary>
    ///     Конструктор
    /// </summary>
    public InsurerPolicyNumberTemplate(Guid id, InsurerId insurerId, string template,
        InsurerPolicyNumberTemplateConfig config, Dictionary<string, string>? remappingRegions = null) : base(id)
    {
        InsurerId = insurerId.ToValue();
        Change(template, config, remappingRegions);
    }

    // ReSharper disable once UnusedMember.Local
    private InsurerPolicyNumberTemplate()
    {
    }

    /// <summary>
    ///     ИД CR
    /// </summary>
    [Column("insurer_id")]
    public string InsurerId { get; private set; } = null!;

    /// <inheritdoc />
    [Column("config")]
    public InsurerPolicyNumberTemplateConfig Config { get; private set; } = null!;

    /// <inheritdoc />
    [Column("remapping_regions")]
    public Dictionary<string, string>? RemappingRegions { get; private set; }

    /// <inheritdoc />
    [Column("template")]
    public string Template { get; private set; } = null!;

    /// <summary>
    ///     Изменить данные
    /// </summary>
    public void Change(string template, InsurerPolicyNumberTemplateConfig config,
        Dictionary<string, string>? remappingRegions = null)
    {
        Template = template;
        Config = config;
        RemappingRegions = remappingRegions;
    }
}