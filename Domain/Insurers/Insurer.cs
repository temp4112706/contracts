using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using Ct.Module.Domain.Entities;
using Some.Abstractions.Addresses;
using Some.Abstractions.Enums;
using Some.Abstractions.Extensions;
using Some.Abstractions.Insurers;
using Some.Domain.Addresses;
using Some.Domain.Insurers.ValueObjects;

// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local
// ReSharper disable UnusedAutoPropertyAccessor.Local

namespace Some.Domain.Insurers;

/// <summary>
///     Страховая компания
/// </summary>
[Table("insurer")]
public sealed class Insurer : TrackableEntity<string>, IInsurer<Address, BankAccount, Kid, Signer, SignerProxy>
{
    /// <summary>
    ///     Создание СК
    /// </summary>
    public Insurer(InsurerId id) : base(id.ToValue())
    {
        Active = false;
        Name = id.GetDescription();
        AgencyAgreementNumber = string.Empty;
        AgencyAgreementDate = DateTime.MinValue;
        BankAccount = new BankAccount(string.Empty, string.Empty, string.Empty, string.Empty);
        Signer = new Signer(null, null, null, null, null, string.Empty, string.Empty, null);
        Signature = string.Empty;
        Stamp = string.Empty;
    }

    // ReSharper disable once UnusedMember.Local
    private Insurer()
    {
    }

    /// <summary>
    ///     КВ по дом видам страхования
    /// </summary>
    public ICollection<InsurerAdditionalInsuranceCommission> AdditionalCommissions { get; private set; } = null!;

    /// <summary>
    ///     TODO: Внутренний ИД
    /// </summary>
    public InsurerId InternalId => EnumExtensions.ParseValue<InsurerId>(Id);

    /// <summary>
    ///     КВ по ипотечному страхованию
    /// </summary>
    public ICollection<InsurerMortgageInsuranceCommission> MortgageCommissions { get; private set; } = null!;

    /// <summary>
    ///     TODO: брать из матрицы
    /// </summary>
    [NotMapped]
    // ReSharper disable once MemberCanBeMadeStatic.Global
    public bool OfflineUnderwriting { get; private set; } = true;

    /// <summary>
    ///     Подпись
    /// </summary>
    [Column("signature")]
    public string Signature { get; private set; } = null!;

    /// <summary>
    ///     Печать
    /// </summary>
    [Column("stamp")]
    public string Stamp { get; private set; } = null!;

    /// <inheritdoc />
    [Column("name")]
    public string Name { get; private set; } = null!;

    /// <inheritdoc />
    [Column("agency_agreement_date")]
    public DateTime AgencyAgreementDate { get; private set; }

    /// <inheritdoc />
    [Column("agency_agreement_number")]
    public string AgencyAgreementNumber { get; private set; } = null!;

    /// <inheritdoc />
    [Column("annexes")]
    public string[]? Annexes { get; private set; }

    /// <inheritdoc />
    [Column("bank_account")]
    public BankAccount BankAccount { get; private set; } = null!;

    /// <inheritdoc />
    [Column("kid")]
    public Kid? Kid { get; private set; }

    /// <inheritdoc />
    [Column("license")]
    public string? License { get; private set; }

    /// <inheritdoc />
    [Column("offline_underwriting_email")]
    public string? OfflineUnderwritingEmail { get; private set; }

    /// <inheritdoc />
    [Column("report_email")]
    public string? ReportEmail { get; private set; }

    /// <inheritdoc />
    [Column("signer")]
    public Signer Signer { get; private set; } = null!;

    /// <summary>
    ///     Дата активации СК
    /// </summary>
    [Column("activated_at")]
    public DateTime? ActivatedAt { get; private set; }

    /// <summary>
    ///     Активный
    /// </summary>
    [Column("active")]
    public bool Active { get; private set; }

    /// <inheritdoc />
    public Address? Address { get; private set; }

    /// <inheritdoc />
    [Column("contact_phone")]
    public string? ContactPhone { get; private set; }

    /// <inheritdoc />
    [Column("inn")]
    public string? Inn { get; private set; }

    /// <inheritdoc />
    [Column("kpp")]
    public string? Kpp { get; private set; }

    /// <inheritdoc />
    [Column("ogrn")]
    public string? Ogrn { get; private set; }

    /// <inheritdoc />
    [Column("okpo")]
    public string? Okpo { get; private set; }

    /// <inheritdoc />
    [Column("okved")]
    public string? Okved { get; private set; }

    /// <inheritdoc />
    [Column("web_site")]
    public string? WebSite { get; private set; }

    /// <summary>
    ///     Ид адреса
    /// </summary>
    [Column("address_id")]
    private Guid? AddressId { get; set; }

    /// <inheritdoc />
    public override bool Equals(object? obj) => ReferenceEquals(this, obj) || (obj is Insurer other && Equals(other));

    /// <inheritdoc />
    [SuppressMessage("ReSharper", "NonReadonlyMemberInGetHashCode")]
    public override int GetHashCode() => Id.GetHashCode();

    /// <summary>
    ///     Активация СК
    /// </summary>
    public void Activate(DateTime agencyAgreementDate, string agencyAgreementNumber, DateTime activatedAt)
    {
        Active = true;
        ActivatedAt = activatedAt;
        ChangeAgency(agencyAgreementDate, agencyAgreementNumber);
    }

    /// <summary>
    ///     Изменить агентские данные
    /// </summary>
    public void ChangeAgency(DateTime agencyAgreementDate, string agencyAgreementNumber)
    {
        AgencyAgreementNumber = agencyAgreementNumber;
        AgencyAgreementDate = agencyAgreementDate;
    }

    /// <summary>
    ///     Изменить Emails данные
    /// </summary>
    public void ChangeEmails(string? reportEmail, string? offlineUnderwritingEmail)
    {
        ReportEmail = reportEmail;
        OfflineUnderwritingEmail = offlineUnderwritingEmail;
    }

    /// <summary>
    ///     Изменить название
    /// </summary>
    public void ChangeName(string name) => Name = name;

    /// <summary>
    ///     Изменить данные для печати
    /// </summary>
    public void ChangePrint(string[] annexes, Signer signer, string? license, IKid? kid)
    {
        Annexes = annexes;
        Kid = kid == null ? null : new Kid(kid.Rule, kid.RulesUrl);
        License = license;
        Signer = signer;
    }

    /// <summary>
    ///     Изменить основные данные
    /// </summary>
    public void ChangeRequisites(string? inn, string? kpp, string? ogrn, string? okpo,
        string? okved, string? contactPhone, IAddress? address, BankAccount bankAccount, string? webSite)
    {
        Inn = inn;
        Kpp = kpp;
        Ogrn = ogrn;
        Okpo = okpo;
        ContactPhone = contactPhone;
        BankAccount = bankAccount;
        Okved = okved;
        WebSite = webSite;
        ChangeAddress(address);
    }

    /// <summary>
    ///     Изменить подпись
    /// </summary>
    public void ChangeSignature(string signature) => Signature = signature;

    /// <summary>
    ///     Изменить печать
    /// </summary>
    public void ChangeStamp(string stamp) => Stamp = stamp;

    /// <summary>
    ///     Деактивация
    /// </summary>
    public void Deactivate()
    {
        Active = false;
        ActivatedAt = null;
    }


    /// <summary>
    ///     Отключить офллайн андеррайтинг
    /// </summary>
    public IDisposable DisableOfflineUnderwriting() => new DisableOfflineUnderwritingDisposable(this);

    private void ChangeAddress(IAddress? address)
    {
        if (Address != null && address != null)
        {
            Address.Change(address);
        }
        else if (Address != null && address == null)
        {
            Address = null;
            AddressId = null;
        }
        else if (Address == null && address != null)
        {
            Address = new Address(Guid.NewGuid(), address);
        }
    }

    private bool Equals(Insurer other) => Id == other.Id;

    private class DisableOfflineUnderwritingDisposable : IDisposable
    {
        private readonly Insurer _insurer;
        private readonly bool _origOfflineUnderwriting;

        public DisableOfflineUnderwritingDisposable(Insurer insurer)
        {
            _insurer = insurer;
            _origOfflineUnderwriting = insurer.OfflineUnderwriting;
            _insurer.OfflineUnderwriting = false;
        }

        public void Dispose() => _insurer.OfflineUnderwriting = _origOfflineUnderwriting;
    }
}