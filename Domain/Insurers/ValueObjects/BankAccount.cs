using Some.Abstractions.Insurers;
using Newtonsoft.Json;

namespace Some.Domain.Insurers.ValueObjects;

/// <summary>
///     Банковский счет
/// </summary>
public class BankAccount : IBankAccount
{
    /// <summary>
    ///     Создание банковского счета
    /// </summary>
    [Newtonsoft.Json.JsonConstructor]
    public BankAccount(
        [JsonProperty] string accountNumber,
        [JsonProperty] string bic,
        [JsonProperty] string correspondentAccount,
        [JsonProperty] string? name)
    {
        Change(
            accountNumber: accountNumber,
            bic: bic,
            correspondentAccount: correspondentAccount,
            name: name
        );
    }

    private BankAccount()
    {
    }

    /// <summary>
    ///     20-значный номер счета
    /// </summary>
    public string AccountNumber { get; private set; } = null!;

    /// <summary>
    ///     БИК Банка
    /// </summary>
    public string Bic { get; private set; } = null!;

    /// <summary>
    ///     Корреспондентский счет
    /// </summary>
    public string CorrespondentAccount { get; private set; } = null!;

    /// <summary>
    ///     Название банка
    /// </summary>
    public string? Name { get; private set; }

    /// <summary>
    ///     Обновить данные
    /// </summary>
    public void Change(string accountNumber, string bic, string correspondentAccount, string? name)
    {
        AccountNumber = accountNumber;
        Bic = bic;
        CorrespondentAccount = correspondentAccount;
        Name = name;
    }

    /// <summary>
    ///     Обновить данные
    /// </summary>
    public void Change(IBankAccount bankAccount)
    {
        AccountNumber = bankAccount.AccountNumber;
        Bic = bankAccount.Bic;
        CorrespondentAccount = bankAccount.CorrespondentAccount;
        Name = bankAccount.Name;
    }
}