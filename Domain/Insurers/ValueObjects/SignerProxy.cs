using Some.Abstractions.Insurers;

namespace Some.Domain.Insurers.ValueObjects;

/// <inheritdoc />
public record SignerProxy(string Number, DateTime Date, string? Note) : ISignerProxy;