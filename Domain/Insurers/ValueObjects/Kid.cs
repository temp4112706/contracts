using Some.Abstractions.Insurers;
using Newtonsoft.Json;

namespace Some.Domain.Insurers.ValueObjects;

/// <summary>
///     Value object: <inheritdoc cref="IKid" />
/// </summary>
public class Kid : IKid
{
    /// <summary>
    ///     Создание объекта
    /// </summary>
    [Newtonsoft.Json.JsonConstructor]
    public Kid(
        [JsonProperty] string? rule,
        [JsonProperty] string rulesUrl
    )
    {
        Rule = rule;
        RulesUrl = rulesUrl;
    }

    /// <inheritdoc />
    public string? Rule { get; private set; }

    /// <inheritdoc />
    public string RulesUrl { get; private set; }

    /// <summary>
    ///     Обновить данные
    /// </summary>
    public void Change(IKid kid)
    {
        Rule = kid.Rule;
        RulesUrl = kid.RulesUrl;
    }
}