using Some.Abstractions.Insurers;

namespace Some.Domain.Insurers.ValueObjects;

/// <inheritdoc />
public record Signer(string? Phone, string? ContractNumber, string? Position, SignerProxy? Proxy, string? WorkingTime,
    string FirstName, string LastName, string? MiddleName) : ISigner<SignerProxy>;