using Some.Abstractions.Insurers;
using Newtonsoft.Json;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local
// ReSharper disable UnusedAutoPropertyAccessor.Local

namespace Some.Domain.Insurers;

/// <inheritdoc />
public class InsurerPolicyNumberTemplateConfig : IInsurerPolicyNumberTemplateConfig
{
    /// <summary>
    ///     Создание результата периода страхования
    /// </summary>
    [Newtonsoft.Json.JsonConstructor]
    public InsurerPolicyNumberTemplateConfig(
        [JsonProperty] bool useShortYear,
        [JsonProperty] bool resetNumberAnnually,
        [JsonProperty] bool separateNumberForRegionCode,
        [JsonProperty] string? suffixWhenMirOffline
    )
    {
        UseShortYear = useShortYear;
        ResetNumberAnnually = resetNumberAnnually;
        SeparateNumberForRegionCode = separateNumberForRegionCode;
        SuffixWhenMirOffline = suffixWhenMirOffline;
    }

    /// <inheritdoc />
    public bool ResetNumberAnnually { get; private set; }

    /// <inheritdoc />
    public bool SeparateNumberForRegionCode { get; private set; }

    /// <inheritdoc />
    public string? SuffixWhenMirOffline { get; }

    /// <inheritdoc />
    public bool UseShortYear { get; private set; }
}