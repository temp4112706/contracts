using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using Ct.Module.Domain.Entities;
using Some.Abstractions.Enums;
using Some.Abstractions.Occupations;
using Some.Domain.Customers;

namespace Some.Domain.Occupations;

/// <summary>
///     Информация о занятости
/// </summary>
[Table("occupation")]
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Local")]
[SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local")]
public sealed class Occupation : BaseEntity<Guid>, IOccupation<Employer>
{
    /// <summary>
    ///     Создание информации о занятости
    /// </summary>
    public Occupation(Guid id, Customer customer, Employer? employer, string position, EmploymentKind employmentKind, bool isIncreasedRisk, bool isWorkingInDangerousPlace = false,
        bool isWorkingWithDangerousSubstances = false) : base(id)
    {
        CustomerId = customer.Id;
        Employer = employer;
        EmployerId = employer?.Id;
        Position = position;
        EmploymentKind = employmentKind;
        IsIncreasedRisk = isIncreasedRisk;
        IsWorkingInDangerousPlace = isWorkingInDangerousPlace;
        IsWorkingWithDangerousSubstances = isWorkingWithDangerousSubstances;
    }

    // ReSharper disable once UnusedMember.Local
    private Occupation()
    {
    }

    /// <inheritdoc />
    public Employer? Employer { get; private set; }

    /// <inheritdoc />
    [Column("position")]
    public string Position { get; private set; }

    /// <inheritdoc />
    [Column("employment_kind")]
    public EmploymentKind EmploymentKind { get; private set; }

    /// <inheritdoc />
    [Column("is_increased_risk")]
    public bool IsIncreasedRisk { get; private set; }

    /// <inheritdoc />
    [Column("is_working_in_dangerous_place")]
    public bool IsWorkingInDangerousPlace { get; private set; }

    /// <inheritdoc />
    [Column("is_working_with_dangerous_substances")]
    public bool IsWorkingWithDangerousSubstances { get; private set; }

    /// <summary>
    ///     Ид клиента
    /// </summary>
    [Column("customer_id")]
    private Guid CustomerId { get; }

    /// <summary>
    ///     Ид работодателя
    /// </summary>
    [Column("employer_id")]
    private Guid? EmployerId { get; set; }

    /// <summary>
    ///     Изменение информации о занятости
    /// </summary>
    /// <param name="occupation"></param>
    /// <returns></returns>
    public bool Change(IScalarOccupation occupation)
    {
        var res = false;
        if (occupation.Position != Position)
        {
            Position = occupation.Position;
            res = true;
        }

        if (occupation.EmploymentKind != EmploymentKind)
        {
            EmploymentKind = occupation.EmploymentKind;
            res = true;
        }

        if (occupation.IsIncreasedRisk != IsIncreasedRisk)
        {
            IsIncreasedRisk = occupation.IsIncreasedRisk;
            res = true;
        }
        
        if (occupation.IsWorkingInDangerousPlace != IsWorkingInDangerousPlace)
        {
            IsWorkingInDangerousPlace = occupation.IsWorkingInDangerousPlace;
            res = true;
        }

        if (occupation.IsWorkingWithDangerousSubstances != IsWorkingWithDangerousSubstances)
        {
            IsWorkingWithDangerousSubstances = occupation.IsWorkingWithDangerousSubstances;
            res = true;
        }

        return res;
    }

    /// <summary>
    ///     Изменение информации о работодателе
    /// </summary>
    /// <param name="employer">Работодатель</param>
    /// <returns></returns>
    public void ChangeEmployer(Employer? employer)
    {
        Employer = employer;
        EmployerId = employer?.Id;
    }
}