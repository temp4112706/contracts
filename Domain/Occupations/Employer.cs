using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using Ct.Module.Domain.Entities;
using Some.Abstractions.Occupations;
using Some.Domain.Addresses;

namespace Some.Domain.Occupations;

/// <summary>
///     Работодатель
/// </summary>
[Table("employer")]
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Local")]
[SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local")]
public class Employer : BaseEntity<Guid>, IEmployer<Address>
{
    /// <summary>
    ///     Создание работодателя
    /// </summary>
    public Employer(Guid id, string name, Address? legalAddress, string? contactPhone) : base(id)
    {
        Name = name;
        LegalAddress = legalAddress;
        LegalAddressId = legalAddress?.Id;
        ContactPhone = contactPhone;
    }

    // ReSharper disable once UnusedMember.Local
    private Employer()
    {
    }

    /// <summary>
    ///     Юридический адрес
    /// </summary>
    public Address? LegalAddress { get; private set; }

    /// <summary>
    ///     Контактный телефон
    /// </summary>
    [Column("contact_phone")]
    public string? ContactPhone { get; private set; }

    /// <summary>
    ///     Наименование работодателя
    /// </summary>
    [Column("name")]
    public string Name { get; private set; } = null!;

    /// <summary>
    ///     Ид юридического адреса
    /// </summary>
    [Column("legal_address_id")]
    private Guid? LegalAddressId { get; set; }

    /// <summary>
    ///     Изменить данные работодателя
    /// </summary>
    public bool Change(IScalarEmployer employer)
    {
        var res = false;
        if (employer.ContactPhone != ContactPhone)
        {
            ContactPhone = employer.ContactPhone;
            res = true;
        }

        if (employer.Name != Name)
        {
            Name = employer.Name;
            res = true;
        }

        return res;
    }

    /// <summary>
    ///     Изменить юридический адрес
    /// </summary>
    public void ChangeLegalAddress(Address? address)
    {
        LegalAddress = address;
        LegalAddressId = address?.Id;
    }
}