﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using Ct.Module.Domain.Entities;
using Some.Abstractions.Addresses;
using Some.Abstractions.Enums;
using Some.Abstractions.Monies;
using Some.Abstractions.Properties;
using Some.Abstractions.Properties.Mortgage;
using Some.Abstractions.Properties.Title;
using Some.Domain.Addresses;
using Some.Domain.Monies;
using Some.Domain.Properties.Title;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local
// ReSharper disable UnusedAutoPropertyAccessor.Local

namespace Some.Domain.Properties;

/// <summary>
///     Информация о страхуемом объекте
/// </summary>
[Table("property")]
public sealed class Property : BaseEntity<Guid>, IScalarProperty<Address, Money>,
    IMortgageProperty<Money>,
    ITitleProperty<TitlePropertyAnswerWithComments>
{
    /// <summary>
    ///     Создание имущества
    /// </summary>
    public Property(Guid id, PropertyType type, Address address, string cadastralNumber, Money actualPrice,
        Money? purchasePrice, float? totalArea, float? livingArea, int? yearOfConstruction, int? numberOfRooms,
        int? floorNumber, int? numberOfFloors, ConstructionMaterial? constructionMaterial,
        InterfloorOverlapMaterial? interfloorOverlapMaterial, FoundationMaterial? foundation,
        bool? isInEmergencyCondition, PropertyPercentWear? percentWear,
        bool? isInProgressConstruction, bool? isInNaturalHazardsZone, bool? isDamaged, bool? isRedeveloped,
        bool? isFireProtected, bool? isSecuritySystem, bool? isInFireDangerZone, bool? isInNeighborhoodDangerZone,
        bool? isInUseModeMismatch, bool? isInsuredByOtherInsurer, bool? isWindowsProtected, bool? isWaterLeakageSensors,
        //ITitleProperty
        DateTime? registrationDate, int? hopCount, string? documentRegistration, bool? pif, bool? isLegalEntity,
        bool? powerOfAttorney, int? amountOfDeals, bool? isPrimary, string? documentOfPrice,
        ICollection<TitlePropertyAnswerWithComments>? answerWithComments
    )
        : base(id)
    {
        Address = address;
        AddressId = address.Id;
        Type = type;
        CadastralNumber = cadastralNumber;
        ActualPrice = actualPrice;

        PurchasePrice = purchasePrice;
        TotalArea = totalArea;
        LivingArea = livingArea;
        YearOfConstruction = yearOfConstruction;
        NumberOfRooms = numberOfRooms;
        FloorNumber = floorNumber;
        NumberOfFloors = numberOfFloors;
        ConstructionMaterial = constructionMaterial;
        InterfloorOverlapMaterial = interfloorOverlapMaterial;
        Foundation = foundation;
        IsInEmergencyCondition = isInEmergencyCondition;
        PercentWear = percentWear;
        IsInProgressConstruction = isInProgressConstruction;
        IsInNaturalHazardsZone = isInNaturalHazardsZone;
        IsDamaged = isDamaged;
        IsRedeveloped = isRedeveloped;
        IsFireProtected = isFireProtected;
        IsSecuritySystem = isSecuritySystem;
        IsInFireDangerZone = isInFireDangerZone;
        IsInNeighborhoodDangerZone = isInNeighborhoodDangerZone;
        IsInUseModeMismatch = isInUseModeMismatch;
        IsInsuredByOtherInsurer = isInsuredByOtherInsurer;
        IsWindowsProtected = isWindowsProtected;
        IsWaterLeakageSensors = isWaterLeakageSensors;

        RegistrationDate = registrationDate;
        HopCount = hopCount;
        DocumentRegistration = documentRegistration;
        Pif = pif;
        IsLegalEntity = isLegalEntity;
        PowerOfAttorney = powerOfAttorney;
        AmountOfDeals = amountOfDeals;
        IsPrimary = isPrimary;
        DocumentOfPrice = documentOfPrice;
        AnswerWithComments = answerWithComments ?? new List<TitlePropertyAnswerWithComments>();
    }

    // ReSharper disable once UnusedMember.Local
    private Property()
    {
    }

    /// <inheritdoc />
    [Column("construction_material")]
    public ConstructionMaterial? ConstructionMaterial { get; private set; }

    /// <inheritdoc />
    [Column("floor_number")]
    public int? FloorNumber { get; private set; }

    /// <inheritdoc />
    [Column("foundation")]
    public FoundationMaterial? Foundation { get; private set; }

    /// <inheritdoc />
    [Column("interfloor_overlap_material")]
    public InterfloorOverlapMaterial? InterfloorOverlapMaterial { get; private set; }

    /// <inheritdoc />
    [Column("is_damaged")]
    public bool? IsDamaged { get; private set; }

    /// <inheritdoc />
    [Column("is_fire_protected")]
    public bool? IsFireProtected { get; private set; }

    /// <inheritdoc />
    [Column("is_in_emergency_condition")]
    public bool? IsInEmergencyCondition { get; private set; }

    /// <inheritdoc />
    [Column("is_in_fire_danger_zone")]
    public bool? IsInFireDangerZone { get; private set; }

    /// <inheritdoc />
    [Column("is_in_natural_hazards_zone")]
    public bool? IsInNaturalHazardsZone { get; private set; }

    /// <inheritdoc />
    [Column("is_in_neighborhood_danger_zone")]
    public bool? IsInNeighborhoodDangerZone { get; private set; }

    /// <inheritdoc />
    [Column("is_in_progress_construction")]
    public bool? IsInProgressConstruction { get; private set; }


    /// <inheritdoc />
    [Column("is_insured_by_other_insurer")]
    public bool? IsInsuredByOtherInsurer { get; private set; }


    /// <inheritdoc />
    [Column("is_in_use_mode_mismatch")]
    public bool? IsInUseModeMismatch { get; private set; }

    /// <inheritdoc />
    [Column("is_redeveloped")]
    public bool? IsRedeveloped { get; private set; }


    /// <inheritdoc />
    [Column("is_security_system")]
    public bool? IsSecuritySystem { get; private set; }

    /// <inheritdoc />
    [Column("is_water_leakage_sensors")]
    public bool? IsWaterLeakageSensors { get; private set; }


    /// <inheritdoc />
    [Column("is_windows_protected")]
    public bool? IsWindowsProtected { get; private set; }

    /// <inheritdoc />
    [Column("living_area")]
    public float? LivingArea { get; private set; }

    /// <inheritdoc />
    [Column("number_of_floors")]
    public int? NumberOfFloors { get; private set; }

    /// <inheritdoc />
    [Column("number_of_rooms")]
    public int? NumberOfRooms { get; private set; }

    /// <inheritdoc />
    [Column("percent_wear")]
    public PropertyPercentWear? PercentWear { get; private set; }

    /// <inheritdoc />
    [Column("purchase_price")]
    public Money? PurchasePrice { get; private set; }

    /// <inheritdoc />
    [Column("total_area")]
    public float? TotalArea { get; private set; }

    /// <inheritdoc />
    [Column("year_of_construction")]
    public int? YearOfConstruction { get; private set; }

    /// <inheritdoc />
    [Column("actual_price")]
    public Money ActualPrice { get; private set; } = null!;

    /// <inheritdoc />
    public Address Address { get; private set; } = null!;

    /// <inheritdoc />
    [Column("cadastral_number")]
    public string CadastralNumber { get; private set; } = null!;

    /// <inheritdoc />
    [Column("type")]
    public PropertyType Type { get; private set; }

    /// <inheritdoc />
    [Column("amount_of_deals")]
    public int? AmountOfDeals { get; private set; }

    /// <inheritdoc />
    [Column("title_answer_with_comments")]
    public ICollection<TitlePropertyAnswerWithComments>? AnswerWithComments { get; private set; }

    /// <inheritdoc />
    [Column("document_of_price")]
    public string? DocumentOfPrice { get; private set; }

    /// <inheritdoc />
    [Column("document_registration")]
    public string? DocumentRegistration { get; private set; }

    /// <inheritdoc />
    [Column("hop_count")]
    public int? HopCount { get; private set; }

    /// <inheritdoc />
    [Column("is_legal_entity")]
    public bool? IsLegalEntity { get; private set; }

    /// <inheritdoc />
    [Column("is_primary")]
    public bool? IsPrimary { get; private set; }

    /// <inheritdoc />
    [Column("pif")]
    public bool? Pif { get; private set; }

    /// <inheritdoc />
    [Column("power_of_attorney")]
    public bool? PowerOfAttorney { get; private set; }

    /// <inheritdoc />
    [Column("registration_date")]
    public DateTime? RegistrationDate { get; private set; }

    /// <summary>
    ///     Ид адреса
    /// </summary>
    [Column("address_id")]
    private Guid AddressId { get; set; }

    /// <summary>
    ///     Изменение основных полей имущества
    /// </summary>
    [SuppressMessage("ReSharper", "ArgumentsStyleOther")]
    public void Change<TAddress, TMoney>(IScalarProperty<TAddress, TMoney> input)
        where TAddress : class, IAddress
        where TMoney : class, IMoney
    {
        Type = input.Type;
        CadastralNumber = input.CadastralNumber;
        ActualPrice = Money.Create(input.ActualPrice);
        Address.Change(input.Address);
    }

    /// <summary>
    ///     Изменить адрес
    /// </summary>
    public void ChangeAddress(Address address)
    {
        Address = address;
        AddressId = address.Id;
    }

    /// <summary>
    ///     Изменение свойства имущества
    /// </summary>
    public void ChangeMortgagePropertyFields<TMoney>(IMortgageProperty<TMoney> input)
        where TMoney : class, IMoney
    {
        PurchasePrice = Money.Create(input.PurchasePrice);
        TotalArea = input.TotalArea;
        LivingArea = input.LivingArea;
        YearOfConstruction = input.YearOfConstruction;
        NumberOfRooms = input.NumberOfRooms;
        FloorNumber = input.FloorNumber;
        NumberOfFloors = input.NumberOfFloors;
        ConstructionMaterial = input.ConstructionMaterial;
        InterfloorOverlapMaterial = input.InterfloorOverlapMaterial;
        Foundation = input.Foundation;
        IsInEmergencyCondition = input.IsInEmergencyCondition;
        PercentWear = input.PercentWear;
        IsInProgressConstruction = input.IsInProgressConstruction;
        IsInNaturalHazardsZone = input.IsInNaturalHazardsZone;
        IsDamaged = input.IsDamaged;
        IsRedeveloped = input.IsRedeveloped;
        IsFireProtected = input.IsFireProtected;
        IsSecuritySystem = input.IsSecuritySystem;
        IsInFireDangerZone = input.IsInFireDangerZone;
        IsInNeighborhoodDangerZone = input.IsInNeighborhoodDangerZone;
        IsInUseModeMismatch = input.IsInUseModeMismatch;
        IsInsuredByOtherInsurer = input.IsInsuredByOtherInsurer;
        IsWindowsProtected = input.IsWindowsProtected;
        IsWaterLeakageSensors = input.IsWaterLeakageSensors;
    }

    /// <summary>
    ///     Изменение свойства титула
    /// </summary>
    public void ChangeTitleropertyFields<TAnswerWithComments>(ITitleProperty<TAnswerWithComments> input)
        where TAnswerWithComments : class, ITitlePropertyAnswerWithComments
    {
        RegistrationDate = input.RegistrationDate;
        HopCount = input.HopCount;
        DocumentRegistration = input.DocumentRegistration;
        Pif = input.Pif;
        IsLegalEntity = input.IsLegalEntity;
        PowerOfAttorney = input.PowerOfAttorney;
        AmountOfDeals = input.AmountOfDeals;
        IsPrimary = input.IsPrimary;
        DocumentOfPrice = input.DocumentOfPrice;

        AnswerWithComments = input.AnswerWithComments == null || !input.AnswerWithComments.Any()
            ? new List<TitlePropertyAnswerWithComments>()
            : input.AnswerWithComments.Select(e => new TitlePropertyAnswerWithComments(e.Code, e.Answer, e.Comments))
                .ToList();
    }
}