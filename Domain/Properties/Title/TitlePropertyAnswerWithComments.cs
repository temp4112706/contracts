﻿using Some.Abstractions.Enums;
using Some.Abstractions.Properties.Title;
using Newtonsoft.Json;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local
// ReSharper disable UnusedAutoPropertyAccessor.Local

namespace Some.Domain.Properties.Title;

/// <inheritdoc cref="ITitlePropertyAnswerWithComments" />
public class TitlePropertyAnswerWithComments : ITitlePropertyAnswerWithComments
{
    /// <summary>
    ///     Создание ответа с комментарием
    /// </summary>
    [JsonConstructor]
    public TitlePropertyAnswerWithComments(
        [JsonProperty] TitlePropertyQuestionCode code,
        [JsonProperty] bool answer,
        [JsonProperty] string? comments)
    {
        Code = code;
        Answer = answer;
        Comments = comments;
    }

    private TitlePropertyAnswerWithComments()
    {
    }

    /// <inheritdoc />
    public bool Answer { get; private set; }

    /// <inheritdoc />
    public TitlePropertyQuestionCode Code { get; private set; }

    /// <inheritdoc />
    public string? Comments { get; private set; }
}