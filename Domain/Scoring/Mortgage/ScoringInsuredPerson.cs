﻿using Some.Abstractions.Scoring.Mortgage;
using Some.Domain.HealthQuestionnaires.ValueObjects;

// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Domain.Scoring.Mortgage;

/// <summary>
///     Данные по застрахованному
/// </summary>
public record ScoringInsuredPerson : ScoringCustomer,
    IScoringInsuredPerson<ScoringHealthQuestionnaire, ArterialPressure>
{
    /// <inheritdoc />
    public decimal LoanShare { get; init; }
    
    /// <inheritdoc />
    public ScoringHealthQuestionnaire HealthQuestionnaire { get; init; } = null!;
    
}