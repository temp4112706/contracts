﻿using Some.Abstractions.Scoring.Mortgage;
using Some.Domain.HealthQuestionnaires.ValueObjects;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global

namespace Some.Domain.Scoring.Mortgage;

/// <inheritdoc />
public record MortgageScoringData : IMortgageScoringData<ScoringInsuredPerson, ScoringHealthQuestionnaire,
    ScoringCustomer, ArterialPressure, ScoringInsuredProperty, ScoringTitleRequest, ScoringPropertyRequest,
    ScoringPropertyData>
{
    /// <inheritdoc />
    public ScoringCustomer Insurant { get; init; } = null!;

    /// <inheritdoc />
    public ScoringInsuredPerson[] InsuredPersons { get; init; } = null!;

    /// <inheritdoc />
    public ScoringInsuredProperty[] InsuredProperties { get; init; } = null!;
}