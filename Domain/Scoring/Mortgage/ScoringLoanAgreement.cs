using Some.Abstractions.Scoring.Mortgage;
using Some.Domain.Monies;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Domain.Scoring.Mortgage;

/// <inheritdoc />
public record ScoringLoanAgreement : IScoringLoanAgreement<Money>
{
    /// <inheritdoc />
    public int AgreementPeriod { get; init; }

    /// <inheritdoc />
    public decimal? InterestRate { get; init; }

    /// <inheritdoc />
    public Money LoanAmount { get; init; } = null!;
}