﻿using Some.Abstractions.Scoring.Mortgage;

// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Domain.Scoring.Mortgage;

/// <inheritdoc />
public record ScoringTitleRequest : IScoringTitleRequest
{
    /// <inheritdoc />
    public decimal? LoanShare { get; init; }
}
