﻿using Some.Abstractions.Scoring.Mortgage;

// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Domain.Scoring.Mortgage;

/// <inheritdoc />
public record ScoringPropertyRequest : IScoringPropertyRequest<ScoringPropertyData>
{
    /// <inheritdoc />
    public ScoringPropertyData Data { get; init; } = null!;

    /// <inheritdoc />
    public decimal? LoanShare { get; init; }
}