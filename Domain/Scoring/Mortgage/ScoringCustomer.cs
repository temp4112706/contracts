﻿using Some.Abstractions.Enums;
using Some.Abstractions.Scoring.Mortgage;

// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Domain.Scoring.Mortgage;

/// <inheritdoc />
public record ScoringCustomer : IScoringCustomer
{
    /// <inheritdoc />
    public DateTime BirthDate { get; init; }

    /// <inheritdoc />
    public Gender Gender { get; init; }
    
    /// <inheritdoc />
    public string OccupationPosition { get; init; } = null!;
}