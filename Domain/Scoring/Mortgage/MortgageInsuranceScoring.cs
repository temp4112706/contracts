﻿using System.ComponentModel.DataAnnotations.Schema;
using Ct.Module.Domain.Entities;
using Some.Abstractions.Enums;
using Some.Abstractions.Scoring.Mortgage;
using Some.Domain.Beneficiaries;
using Some.Domain.Companies;
using Some.Domain.HealthQuestionnaires.ValueObjects;
using Some.Domain.Insurers;
using Some.Domain.Monies;

// ReSharper disable UnusedMember.Local

// ReSharper disable UnusedAutoPropertyAccessor.Local
// ReSharper disable UnusedAutoPropertyAccessor.Global

// ReSharper disable UnusedMember.Global
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local

namespace Some.Domain.Scoring.Mortgage;

/// <summary>
///     Калькуляция пи ипотечному страхованию
/// </summary>
[Table("mortgage_insurance_scoring")]
public class MortgageInsuranceScoring : TrackableEntity<Guid>,
    IMortgageInsuranceScoring<ScoringPolicyDates, ScoringLoanAgreement, Insurer, Company, Money,
        PremiumAmountInfo, MortgageScoringData, ScoringInsuredPerson,
        ScoringCustomer, ScoringHealthQuestionnaire, ArterialPressure, ScoringInsuredProperty,
        ScoringTitleRequest, ScoringPropertyRequest, ScoringPropertyData>
{
    /// <inheritdoc />
    public MortgageInsuranceScoring(Guid id, bool isOnlineUnderwriting,
        Company company, Insurer insurer, Beneficiary beneficiary,
        ScoringLoanAgreement loanAgreement, ScoringPolicyDates policyDates,
        PremiumAmountInfo premiumAmountInfo,
        Money insuranceAmount, Money premiumAmount,
        InsuranceRisk[] risks, MortgageScoringData scoringData, Money insurerCommission)
    {
        Id = id;
        IsOnlineUnderwriting = isOnlineUnderwriting;

        Insurer = insurer;
        InsurerId = insurer.Id;

        Company = company;
        CompanyId = company.Id;

        BeneficiaryId = beneficiary.Id;
        Beneficiary = beneficiary;

        PremiumAmountInfo = premiumAmountInfo;
        PolicyDates = policyDates;
        LoanAgreement = loanAgreement;

        InsuranceAmount = insuranceAmount;
        PremiumAmount = premiumAmount;

        Risks = risks;
        InsuranceData = scoringData;
        InsurerCommission = insurerCommission;
    }

    /// <inheritdoc />
    private MortgageInsuranceScoring()
    {
    }

    /// <summary>
    ///     Выгодоприобретатель
    /// </summary>
    public Beneficiary Beneficiary { get; private set; } = null!;

    /// <summary>
    ///     ID (code) выгодоприобретателя
    /// </summary>
    [Column("beneficiary_id")]
    public string BeneficiaryId { get; private set; } = null!;

    /// <summary>
    ///     ID компании владелеца
    /// </summary>
    [Column("company_id")]
    public string CompanyId { get; private set; } = null!;

    /// <summary>
    ///     ID СК
    /// </summary>
    [Column("insurer_id")]
    public string InsurerId { get; private set; } = null!;

    /// <inheritdoc />
    public Company? Company { get; private set; }

    /// <inheritdoc />
    [Column("insurance_amount")]
    public Money InsuranceAmount { get; private set; } = null!;

    /// <inheritdoc />
    [Column("insurance_data")]
    public MortgageScoringData InsuranceData { get; private set; } = null!;

    /// <inheritdoc />
    public Insurer Insurer { get; private set; } = null!;

    /// <inheritdoc />
    [Column("insurer_commission")]
    public Money InsurerCommission { get; private set; } = null!;

    /// <inheritdoc />
    [Column("is_online_underwriting")]
    public bool IsOnlineUnderwriting { get; private set; }

    /// <inheritdoc />
    [Column("loan_agreement")]
    public ScoringLoanAgreement LoanAgreement { get; private set; } = null!;

    /// <inheritdoc />
    [Column("policy_dates")]
    public ScoringPolicyDates PolicyDates { get; private set; } = null!;

    /// <inheritdoc />
    [Column("premium_amount")]
    public Money PremiumAmount { get; private set; } = null!;

    /// <inheritdoc />
    [Column("premium_amount_info")]
    public PremiumAmountInfo PremiumAmountInfo { get; private set; } = null!;

    /// <inheritdoc />
    [Column("risks")]
    public InsuranceRisk[] Risks { get; private set; } = null!;
}