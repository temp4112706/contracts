﻿using Some.Abstractions.Enums;
using Some.Abstractions.Scoring.Mortgage;

// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Domain.Scoring.Mortgage;

/// <inheritdoc />
public record
    ScoringInsuredProperty : IScoringInsuredProperty<ScoringTitleRequest, ScoringPropertyRequest, ScoringPropertyData>
{
    /// <inheritdoc />
    public ScoringPropertyRequest? PropertyRequest { get; init; }

    /// <inheritdoc />
    public ScoringTitleRequest? TitleRequest { get; init; }

    /// <inheritdoc />
    public PropertyType Type { get; init; }
}