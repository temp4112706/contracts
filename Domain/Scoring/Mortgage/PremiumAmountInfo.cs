﻿using Some.Abstractions.Scoring.Mortgage;
using Some.Domain.Monies;

// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Domain.Scoring.Mortgage;

/// <inheritdoc />
public record PremiumAmountInfo : IPremiumAmountInfo<Money>
{
    /// <inheritdoc />
    public Money[]? LifeInsurance { get; init; }

    /// <inheritdoc />
    public Money[]? PropertyInsurance { get; init; }

    /// <inheritdoc />
    public Money[]? TitleInsurance { get; init; }
}