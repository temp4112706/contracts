﻿using Some.Abstractions.Enums;
using Some.Abstractions.Scoring.Mortgage;

// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Domain.Scoring.Mortgage;

/// <inheritdoc />
public class ScoringPropertyData : IScoringPropertyData
{
    /// <inheritdoc />
    public ConstructionMaterial ConstructionMaterial { get; init; }

    /// <inheritdoc />
    public InterfloorOverlapMaterial InterfloorOverlapMaterial { get; init; }

    /// <inheritdoc />
    public int YearOfConstruction { get; init; }
}