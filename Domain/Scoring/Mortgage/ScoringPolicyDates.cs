using Some.Abstractions.Policies;
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Domain.Scoring.Mortgage;

/// <inheritdoc />
public record ScoringPolicyDates : IPolicyDates
{
    /// <inheritdoc />
    public DateTime AgreementDateTime { get; init; }
    
    /// <inheritdoc />
    public DateTime CommencementDateTime { get; init; }

    /// <inheritdoc />
    public DateTime ExpiryDateTime { get; init; }
}