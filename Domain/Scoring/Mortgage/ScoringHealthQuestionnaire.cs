using Some.Abstractions.Scoring.Mortgage;
using Some.Domain.HealthQuestionnaires.ValueObjects;

// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Domain.Scoring.Mortgage;

/// <inheritdoc />
public record ScoringHealthQuestionnaire : IScoringHealthQuestionnaire<ArterialPressure>
{
    /// <inheritdoc />
    public ArterialPressure ArterialPressure { get; init; } = null!;

    /// <inheritdoc />
    public float Height { get; init; }

    /// <inheritdoc />
    public float Weight { get; init; }
}