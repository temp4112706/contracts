using System.ComponentModel.DataAnnotations.Schema;
using Some.Domain.Common;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Domain.PolicyEvents;

/// <summary>
///     Эвенты ДС
/// </summary>
[Table("policy_event")]
public sealed class PolicyEvent : BaseEvent<PolicyEventPayload>
{
    /// <summary>
    ///     Создание эвента ДС
    /// </summary>
    /// <param name="policyId">Ид ДС</param>
    /// <param name="id">Ид ээвента</param>
    /// <param name="event">Название эвента</param>
    /// <param name="payload">Дополнительная информация</param>
    /// <param name="creator">Автор</param>
    /// <param name="createdAt">Создана, если отличается от текущей даты</param>
    public PolicyEvent(Guid id, Guid policyId, string @event, PolicyEventPayload payload,
        string? creator = null, DateTime? createdAt = null)
        : base(id, payload, creator, createdAt)
    {
        PolicyId = policyId;
        Event = @event;
    }

    // ReSharper disable once UnusedMember.Local
    private PolicyEvent()
    {
    }

    /// <summary>
    ///     Название эвента
    /// </summary>
    [Column("event")]
    public string Event { get; private set; } = null!;

    /// <summary>
    ///     Ид ДС
    /// </summary>
    [Column("policy_id")]
    public Guid PolicyId { get; private set; }
}