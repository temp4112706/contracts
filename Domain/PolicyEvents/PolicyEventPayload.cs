using Some.Abstractions.Enums;
using Some.Domain.Policies.ValueObjects;
using Newtonsoft.Json;

// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local

namespace Some.Domain.PolicyEvents;

/// <summary>
///     Дополнительная информация эвента ДС
/// </summary>
public class PolicyEventPayload
{
    /// <summary>
    ///     Создание объекта
    /// </summary>
    [Newtonsoft.Json.JsonConstructor]
    public PolicyEventPayload(
        [JsonProperty] PolicyStatusChangesResult? statusChangesResult = null,
        [JsonProperty] string? comment = null
    )
    {
        StatusChangesResult = statusChangesResult;
        Comment = comment;
    }

    /// <summary>
    ///     Создание дополнительной информации
    /// </summary>
    public PolicyEventPayload(PolicyStatus currentStatus, PolicyStatus? previousStatus, DateTime changedAt,
        string? comment = null)
    {
        StatusChangesResult = new PolicyStatusChangesResult(currentStatus, previousStatus, changedAt);
        Comment = comment;
    }

    private PolicyEventPayload()
    {
    }

    /// <summary>
    ///     Комментарий
    /// </summary>
    public string? Comment { get; private set; }

    /// <summary>
    ///     Результат перемещения статуса
    /// </summary>
    public PolicyStatusChangesResult? StatusChangesResult { get; private set; }
}