﻿using System.ComponentModel.DataAnnotations.Schema;
using Ct.Module.Domain.Entities;
using Some.Abstractions.AdditionalInsuranceScoring;
using Some.Abstractions.Enums;
using Some.Domain.Companies;
using Some.Domain.Insurers;
using Some.Domain.Monies;

// ReSharper disable UnusedAutoPropertyAccessor.Local
// ReSharper disable UnusedAutoPropertyAccessor.Global

// ReSharper disable UnusedMember.Global
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local

namespace Some.Domain.AdditionalInsuranceScoring;

/// <summary>
///     Калькуляция по доп видов страхования
/// </summary>
[Table("additional_insurance_scoring")]
public class AdditionalInsuranceScoring : TrackableEntity<Guid>, IAdditionalInsuranceScoring<Insurer, Company, Money>,
    IAdditionalInsuranceScoringData
{
    /// <inheritdoc />
    public AdditionalInsuranceScoring(Guid id, AdditionalInsuranceType type, Company? company, Insurer insurer,
        string data, int duration, Money insuranceAmount, Money premiumAmount, string criteria)
    {
        Id = id;
        Type = type;

        Insurer = insurer;
        InsurerId = insurer.Id;

        Company = company;
        CompanyId = company?.Id;

        Data = data;
        Criteria = criteria;
        Duration = duration;

        InsuranceAmount = insuranceAmount;
        PremiumAmount = premiumAmount;
    }

    // ReSharper disable once UnusedMember.Local
    private AdditionalInsuranceScoring()
    {
    }

    /// <summary>
    ///     ID компании владелеца
    /// </summary>
    [Column("company_id")]
    public string? CompanyId { get; private set; } = null!;

    /// <summary>
    ///     ID СК
    /// </summary>
    [Column("insurer_id")]
    public string InsurerId { get; private set; } = null!;

    /// <inheritdoc />
    public Company? Company { get; private set; }

    /// <inheritdoc />
    [Column("duration")]
    public int Duration { get; private set; }

    /// <inheritdoc />
    [Column("insurance_amount")]
    public Money InsuranceAmount { get; private set; } = null!;

    /// <inheritdoc />
    public Insurer Insurer { get; private set; } = null!;

    /// <inheritdoc />
    [Column("premium_amount")]
    public Money PremiumAmount { get; private set; } = null!;

    /// <inheritdoc />
    [Column("insurance_type")]
    public AdditionalInsuranceType Type { get; private set; }

    /// <inheritdoc />
    [Column("criteria")]
    public string Criteria { get; private set; } = null!;

    /// <inheritdoc />
    [Column("data")]
    public string Data { get; private set; } = null!;
}