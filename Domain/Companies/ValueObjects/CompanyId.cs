﻿

// ReSharper disable ReplaceAutoPropertyWithComputedProperty
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local

using Some.Abstractions.Common;

namespace Some.Domain.Companies.ValueObjects;

/// <summary>
///     Id/Код компании
/// </summary>
public sealed record CompanyId : IValueObject
{
    public CompanyId(string value)
    {
        Value = value;
    }

    /// <inheritdoc />
    public string Value { get; }

    /// <summary>
    ///     <see cref="CompanyId" /> -> <see cref="string" />
    /// </summary>
    public static implicit operator string?(CompanyId? company) => company?.Value;

    /// <summary>
    ///     <see cref="string" /> -> <see cref="CompanyId" />
    /// </summary>
    public static implicit operator CompanyId?(string? companyId) => companyId == null ? null : new(companyId);
}