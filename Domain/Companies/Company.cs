﻿using System.ComponentModel.DataAnnotations.Schema;
using Ct.Module.Domain.Entities;
using Some.Abstractions.Companies;
using Some.Domain.Addresses;

// ReSharper disable ReplaceAutoPropertyWithComputedProperty
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local

namespace Some.Domain.Companies;

/// <summary>
///     Компания владелец
/// </summary>
[Table("company")]
public class Company : TrackableEntity<string>, ICompany<Address>
{
    /// <inheritdoc />
    public Company(string id, string name, string fullName, Address address, string? email)
        : base(id)
    {
        Name = name;
        FullName = fullName;
        Email = email;

        Address = address;
        AddressId = address.Id;
    }

    // ReSharper disable once UnusedMember.Local
    private Company()
    {
    }

    /// <summary>
    ///     Адрес организации
    /// </summary>
    public Address Address { get; private set; } = null!;

    /// <inheritdoc />
    [Column("email")]
    public string? Email { get; private set; }

    /// <summary>
    ///     Полное наименование организации
    /// </summary>
    [Column("full_name")]
    public string FullName { get; private set; } = null!;

    /// <summary>
    ///     Сокращенное наименование организации
    /// </summary>
    [Column("name")]
    public string Name { get; private set; } = null!;

    /// <summary>
    ///     ID адреса компании
    /// </summary>
    [Column("address_id")]
    private Guid AddressId { get; set; }

    /// <summary>
    ///     Изменить адрес
    /// </summary>
    public void ChangeAddress(Address address)
    {
        Address = address;
        AddressId = address.Id;
    }
}