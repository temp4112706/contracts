using Some.Abstractions.Monies;
using Some.Abstractions.MutualSettlements.Actions;

namespace Some.Domain.MutualSettlements.Actions;

/// <inheritdoc />
public record MutualSettlementRowsResult(IMoney ActualPaymentAmount, IMoney AmountOfReward, int RowsCount)
    : IMutualSettlementRowsResult;