﻿using System.ComponentModel.DataAnnotations.Schema;
using Ct.Module.Domain.Entities;
using Some.Abstractions.Enums;
using Some.Abstractions.MutualSettlements;
using Some.Abstractions.MutualSettlements.Actions;
using Some.Domain.Companies;
using Some.Domain.Employees;
using Some.Domain.Insurers;
using Some.Domain.Monies;
using Some.Domain.MutualSettlements.ValueObjects;

namespace Some.Domain.MutualSettlements;

/// <summary>
///     АС
/// </summary>
[Table("mutual_settlement")]
public sealed class MutualSettlement : TrackableEntity<Guid>,
    IMutualSettlement<Money, Employee, Insurer, Company>
{
    /// <inheritdoc />
    public MutualSettlement(
        Guid id,
        string number,
        MutualSettlementStatus status,
        Money amountOfReward,
        Money actualPaymentAmount,
        long rowsCount,
        DateTime reportDate,
        Insurer insurer,
        string agencyAgreementNumber,
        DateTime agencyAgreementDate,
        Company company) : base(id)
    {
        Number = number;
        Status = status;
        ReportDate = reportDate;
        Insurer = insurer;
        InsurerId = insurer.Id;
        AmountOfReward = amountOfReward;
        ActualPaymentAmount = actualPaymentAmount;
        RowsCount = rowsCount;
        AgencyAgreementNumber = agencyAgreementNumber;
        AgencyAgreementDate = agencyAgreementDate;
        Company = company;
        CompanyId = company.Id;
        Rows = new List<MutualSettlementRow>();
    }

    /// <inheritdoc />
    // ReSharper disable once UnusedMember.Local
    private MutualSettlement()
    {
    }

    /// <summary>
    ///     ID компании владелеца
    /// </summary>
    [Column("company_id")]
    public string CompanyId { get; } = null!;

    /// <summary>
    ///     ИД СК
    /// </summary>
    [Column("insurer_id")]
    public string InsurerId { get; set; } = null!;

    /// <summary>
    ///     Записи
    ///     <remarks>Не использовать в Include, тк может нагрузить систему</remarks>
    /// </summary>
    public ICollection<MutualSettlementRow> Rows { get; } = null!;

    /// <inheritdoc />
    [Column("number")]
    public string Number { get; } = null!;

    /// <inheritdoc />
    [Column("report_date")]
    public DateTime ReportDate { get; }

    /// <summary>
    ///     Сотрудник компании
    /// </summary>
    public Employee? CompanyEmployee { get; private set; }
    
    /// <summary>
    ///     ИД <see cref="CompanyEmployee"/>
    /// </summary>
    [Column("company_employee_id")]
    private string? CompanyEmployeeId { get; set; }

    /// <inheritdoc />
    public Company Company { get; } = null!;

    /// <summary>
    ///     Сотрудник СК
    /// </summary>

    public Employee? InsurerEmployee { get; private set; }
    
    /// <summary>
    ///     ИД <see cref="InsurerEmployee"/>
    /// </summary>
    [Column("insurer_employee_id")]
    private string? InsurerEmployeeId { get; set; }

    /// <inheritdoc />
    [Column("status")]
    public MutualSettlementStatus Status { get; private set; }

    /// <inheritdoc />
    [Column("actual_payment_amount")]
    public Money ActualPaymentAmount { get; private set; } = null!;

    /// <inheritdoc />
    [Column("agency_agreement_date")]
    public DateTime AgencyAgreementDate { get; }

    /// <inheritdoc />
    [Column("agency_agreement_number")]
    public string AgencyAgreementNumber { get; } = null!;

    /// <inheritdoc />
    [Column("amount_of_reward")]
    public Money AmountOfReward { get; private set; } = null!;

    /// <summary>
    ///     СК
    /// </summary>
    [Column("insurer")]
    public Insurer Insurer { get; } = default!;

    /// <inheritdoc />
    [Column("rows_count")]
    public long RowsCount { get; private set; }

    /// <summary>
    ///     Согласовать сторонами АС
    /// </summary>
    public MutualSettlementStatusChangesResult? Agreed() =>
        // CompanyEmployee = bankEmployee;
        ChangeStatusWithResult(MutualSettlementStatus.Agreed);

    /// <summary>
    ///     Согласование АС в банке
    /// </summary>
    public MutualSettlementStatusChangesResult? AgreementOfBank(Employee? insurerEmployee)
    {
        InsurerEmployee = insurerEmployee;
        InsurerEmployeeId = insurerEmployee?.Id;
        return ChangeStatusWithResult(MutualSettlementStatus.AgreementOfCompany);
    }

    /// <summary>
    ///     Согласование АС в СК
    /// </summary>
    public MutualSettlementStatusChangesResult? AgreementOfInsurer(Employee? companyEmployee)
    {
        CompanyEmployee = companyEmployee;
        CompanyEmployeeId = companyEmployee?.Id;
        return ChangeStatusWithResult(MutualSettlementStatus.AgreementOfInsurer);
    }

    /// <summary>
    ///     Утвердить АС
    /// </summary>
    public MutualSettlementStatusChangesResult? Approved() => ChangeStatusWithResult(MutualSettlementStatus.Approved);

    /// <summary>
    ///     Изменить общие данные строк
    /// </summary>
    public void ChangeRowsResult(IMutualSettlementRowsResult result)
    {
        RowsCount = result.RowsCount;
        ActualPaymentAmount = Money.Create(result.ActualPaymentAmount);
        AmountOfReward = Money.Create(result.AmountOfReward);
    }

    /// <summary>
    ///     Обновить статус
    /// </summary>
    public void ChangeStatus(MutualSettlementStatus status)
    {
        if (Status == status)
        {
            return;
        }

        Status = status;
    }

    /// <summary>
    ///     Оплатить АС
    /// </summary>
    public MutualSettlementStatusChangesResult? Paid() => ChangeStatusWithResult(MutualSettlementStatus.Paid);

    private MutualSettlementStatusChangesResult? ChangeStatusWithResult(MutualSettlementStatus nextStatus)
    {
        if (Status == nextStatus)
        {
            return null;
        }

        var previousStatus = Status;
        ChangeStatus(nextStatus);
        return new MutualSettlementStatusChangesResult(Status, previousStatus, DateTime.UtcNow);
    }
}