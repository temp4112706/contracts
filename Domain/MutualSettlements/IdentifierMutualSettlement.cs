// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global

using Some.Abstractions.MutualSettlements;

namespace Some.Domain.MutualSettlements;

/// <summary>
///     АС с ИД, номером и датой
/// </summary>
public class IdentifierMutualSettlement : IIdentifierMutualSettlement
{
    /// <inheritdoc />
    public Guid Id { get; init; }

    /// <inheritdoc />
    public string Number { get; init; } = null!;

    /// <inheritdoc />
    public DateTime ReportDate { get; init; }
}