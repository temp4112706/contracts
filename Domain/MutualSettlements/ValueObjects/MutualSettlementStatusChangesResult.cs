﻿using System.Diagnostics.CodeAnalysis;
using Some.Abstractions.Enums;
using Some.Abstractions.MutualSettlements;
using Newtonsoft.Json;

namespace Some.Domain.MutualSettlements.ValueObjects;

/// <summary>
///     Результат перехода статуса в АС
/// </summary>
[SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local")]
public class MutualSettlementStatusChangesResult : IMutualSettlementStatusChangesResult
{
    /// <summary>
    ///     Создание объекта
    /// </summary>
    [JsonConstructor]
    public MutualSettlementStatusChangesResult(
        [JsonProperty] MutualSettlementStatus nextStatus,
        [JsonProperty] MutualSettlementStatus? previousStatus,
        [JsonProperty] DateTime changedAt)
    {
        NextStatus = nextStatus;
        PreviousStatus = previousStatus;
        ChangedAt = changedAt;
    }

    /// <summary>
    ///     Дата и время перехода
    /// </summary>
    public DateTime ChangedAt { get; private set; }

    /// <inheritdoc />
    public MutualSettlementStatus NextStatus { get; private set; }

    /// <inheritdoc />
    public MutualSettlementStatus? PreviousStatus { get; private set; }
}