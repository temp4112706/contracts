﻿using System.ComponentModel.DataAnnotations.Schema;
using Ct.Module.Domain.Entities;
using Some.Abstractions.Enums;
using Some.Abstractions.MutualSettlements;
using Some.Domain.Common.ValueObjects;
using Some.Domain.Monies;
using Some.Domain.Policies;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Domain.MutualSettlements;

/// <summary>
///     АС - ДС
/// </summary>
[Table("mutual_settlement_row")]
public sealed class MutualSettlementRow : BaseEntity<Guid>, IMutualSettlementRow<ActualPayment, Money>
{
    /// <summary>
    ///     Создание объекта
    /// </summary>
    public MutualSettlementRow(Guid id, MutualSettlement mutualSettlement, Guid policyPeriodId,
        MutualSettlementPolicyCompanyStatus companyStatus, MutualSettlementPolicyInsurerStatus insurerStatus,
        ActualPayment actualPayment, Money amountOfReward, decimal percentOfReward, bool partner,
        string? companyComment, string? insurerComment, int? paymentPeriodNumber)
        : base(id)
    {
        MutualSettlement = mutualSettlement;
        MutualSettlementId = mutualSettlement.Id;
        PolicyPeriodId = policyPeriodId;
        CompanyStatus = companyStatus;
        InsurerStatus = insurerStatus;
        ActualPayment = actualPayment;
        PercentOfReward = percentOfReward;
        Partner = partner;
        CompanyComment = companyComment;
        InsurerComment = insurerComment;
        AmountOfReward = amountOfReward;
        PaymentPeriodNumber = paymentPeriodNumber;
    }

    /// <summary>
    ///     Создание объекта
    /// </summary>
    public MutualSettlementRow(Guid id, MutualSettlement mutualSettlement, Guid policyPeriodId,
        MutualSettlementPolicyCompanyStatus companyStatus, MutualSettlementPolicyInsurerStatus insurerStatus,
        ActualPayment actualPayment, decimal percentOfReward, bool partner,
        string? companyComment, string? insurerComment, int? paymentPeriodNumber)
        : this(
            id,
            mutualSettlement,
            policyPeriodId,
            companyStatus,
            insurerStatus,
            actualPayment,
            actualPayment.ActualAmount.Multiply(percentOfReward),
            percentOfReward,
            partner,
            companyComment,
            insurerComment,
            paymentPeriodNumber)
    {
    }

    /// <summary>
    ///     Создание объекта
    /// </summary>
    public MutualSettlementRow(Guid id, MutualSettlement mutualSettlement, PolicyPeriod policyPeriod,
        MutualSettlementPolicyCompanyStatus companyStatus, MutualSettlementPolicyInsurerStatus insurerStatus,
        ActualPayment actualPayment, Money amountOfReward, decimal percentOfReward, bool partner,
        string? companyComment, string? insurerComment, int? paymentPeriodNumber)
        : base(id)
    {
        MutualSettlement = mutualSettlement;
        MutualSettlementId = mutualSettlement.Id;
        PolicyPeriod = policyPeriod;
        CompanyStatus = companyStatus;
        InsurerStatus = insurerStatus;
        ActualPayment = actualPayment;
        PercentOfReward = percentOfReward;
        Partner = partner;
        CompanyComment = companyComment;
        InsurerComment = insurerComment;
        AmountOfReward = amountOfReward;
        PolicyPeriodId = policyPeriod.Id;
        PaymentPeriodNumber = paymentPeriodNumber;
    }

    /// <summary>
    ///     Создание объекта
    /// </summary>
    public MutualSettlementRow(Guid id, MutualSettlement mutualSettlement, PolicyPeriod policyPeriod,
        MutualSettlementPolicyCompanyStatus companyStatus, MutualSettlementPolicyInsurerStatus insurerStatus,
        ActualPayment actualPayment, decimal percentOfReward, bool partner,
        string? companyComment, string? insurerComment, int? paymentPeriodNumber)
        : this(
            id,
            mutualSettlement,
            policyPeriod,
            companyStatus,
            insurerStatus,
            actualPayment,
            partner: partner,
            amountOfReward: actualPayment.ActualAmount.Multiply(percentOfReward),
            percentOfReward: percentOfReward,
            companyComment: companyComment,
            insurerComment: insurerComment,
            paymentPeriodNumber: paymentPeriodNumber)
    {
    }

    /// <summary>
    ///     Создание объекта
    /// </summary>
    // ReSharper disable once UnusedMember.Local
    private MutualSettlementRow()
    {
    }

    /// <summary>
    ///     АС
    /// </summary>
    public MutualSettlement MutualSettlement { get; private set; } = null!;

    /// <summary>
    ///     ИД АС
    /// </summary>
    [Column("mutual_settlement_id")]
    public Guid MutualSettlementId { get; private set; }

    /// <summary>
    ///     Номер периода оплаты
    /// </summary>
    [Column("payment_period_number")]
    public int? PaymentPeriodNumber { get; private set; }

    /// <summary>
    ///     Период ДС
    /// </summary>
    public PolicyPeriod PolicyPeriod { get; private set; } = null!;

    /// <summary>
    ///     ИД периода ДС
    /// </summary>
    [Column("policy_period_id")]
    public Guid PolicyPeriodId { get; private set; }

    /// <summary>
    ///     Фактическая оплата страховой премии
    /// </summary>
    [Column("actual_payment")]
    public ActualPayment ActualPayment { get; private set; } = null!;

    /// <summary>
    ///     Размер вознаграждения
    /// </summary>
    [Column("amount_of_reward")]
    public Money AmountOfReward { get; private set; } = null!;

    /// <summary>
    ///     Данные о страховой премии в СК
    /// </summary>
    [Column("insurer_actual_payment")]
    public ActualPayment? InsurerActualPayment { get; private set; }

    /// <summary>
    ///     Комиссионное вознаграждение, руб в СК
    /// </summary>
    [Column("insurer_amount_of_reward")]
    public Money? InsurerAmountOfReward { get; private set; }

    /// <summary>
    ///     Комментарий компании
    /// </summary>
    [Column("company_comment")]
    public string? CompanyComment { get; private set; }

    /// <summary>
    ///     Статус
    /// </summary>
    [Column("company_status")]
    public MutualSettlementPolicyCompanyStatus CompanyStatus { get; private set; }

    /// <summary>
    ///     Комментарий СК
    /// </summary>
    [Column("insurer_comment")]
    public string? InsurerComment { get; private set; }

    /// <summary>
    ///     Комиссионное вознаграждение, % в СК
    /// </summary>
    [Column("insurer_percent_of_reward")]
    public decimal? InsurerPercentOfReward { get; private set; }

    /// <summary>
    ///     Номер ДС в СК
    /// </summary>
    [Column("insurer_policy_number")]
    public string? InsurerPolicyNumber { get; private set; }

    /// <summary>
    ///     Статус
    /// </summary>
    [Column("insurer_status")]
    public MutualSettlementPolicyInsurerStatus InsurerStatus { get; private set; }

    /// <summary>
    ///     Партнерская сделка?
    /// </summary>
    [Column("partner")]
    public bool Partner { get; private set; }

    /// <summary>
    ///     Процент вознаграждения
    /// </summary>
    [Column("percent_of_reward")]
    public decimal PercentOfReward { get; private set; }

    /// <summary>
    ///     Изменение факт суммы оплаты
    /// </summary>
    public void ChangeActualPayment(ActualPayment value) => ActualPayment = value;

    /// <summary>
    ///     Изменение размера вознаграждения
    /// </summary>
    public void ChangeAmountOfReward(Money value) => AmountOfReward = value;

    /// <summary>
    ///     Изменение комментария банка
    /// </summary>
    public bool ChangeCompanyComment(string? comment)
    {
        if (CompanyComment == comment)
        {
            return false;
        }

        CompanyComment = comment;
        return true;
    }

    /// <summary>
    ///     Изменение статуса банка
    /// </summary>
    public void ChangeCompanyStatus(MutualSettlementPolicyCompanyStatus value) => CompanyStatus = value;

    /// <summary>
    ///     Изменение данных о страховой премии в СК
    /// </summary>
    public void ChangeInsurerActualPayment(decimal insurerActualPaymentAmount, DateTime insurerActualPaymentDate) =>
        InsurerActualPayment = new ActualPayment(insurerActualPaymentDate, Money.Rub(insurerActualPaymentAmount));

    /// <summary>
    ///     Изменить комиссионное вознаграждение в СК
    /// </summary>
    public void ChangeInsurerAmountOfReward(decimal insurerAmountOfReward) =>
        InsurerAmountOfReward = Money.Rub(insurerAmountOfReward);

    /// <summary>
    ///     Изменение комментария СК
    /// </summary>
    public bool ChangeInsurerComment(string? comment)
    {
        if (InsurerComment == comment)
        {
            return false;
        }

        InsurerComment = comment;
        return true;
    }

    /// <summary>
    ///     Изменить комиссионное вознаграждение, % в СК
    /// </summary>
    public void ChangeInsurerPercentOfReward(decimal insurerPercentOfReward) =>
        InsurerPercentOfReward = insurerPercentOfReward;

    /// <summary>
    ///     Изменить номер ДС в СК
    /// </summary>
    public void ChangeInsurerPolicyNumber(string insurerPolicyNumber) => InsurerPolicyNumber = insurerPolicyNumber;

    /// <summary>
    ///     Изменение статуса СК
    /// </summary>
    public void ChangeInsurerStatus(MutualSettlementPolicyInsurerStatus value) => InsurerStatus = value;

    /// <summary>
    ///     Изменение процента вознаграждения
    /// </summary>
    public void ChangePercentOfReward(decimal value) => PercentOfReward = value;

    /// <summary>
    ///     Пересчитать комис вознаграждение
    /// </summary>
    public void RecalculateAmountOfReward()
    {
        var newAmountOfReward = ActualPayment.ActualAmount.Multiply(PercentOfReward);
        if (!AmountOfReward.Equals(newAmountOfReward))
        {
            AmountOfReward = newAmountOfReward;
        }
    }
}