using System.ComponentModel.DataAnnotations.Schema;
using Ct.Module.Domain.Entities;
using Some.Abstractions.Enums;
using Some.Abstractions.InsuredPersons;
using Some.Domain.Customers;
using Some.Domain.HealthQuestionnaires;
using Some.Domain.HealthSurveys;

// ReSharper disable UnusedAutoPropertyAccessor.Local
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local

namespace Some.Domain.InsuredPersons;

/// <summary>
///     Застрахованное лицо
/// </summary>
[Table("insured_person")]
public sealed class InsuredPerson : BaseEntity<Guid>, IInsuredPerson<Customer, HealthQuestionnaire, HealthSurvey>
{
    /// <summary>
    ///     Создание застрахованного лица
    /// </summary>
    public InsuredPerson(Guid id, Customer customer, decimal loanShare, InsuredPersonIndex index)
        : base(id)
    {
        Customer = customer;
        CustomerId = customer.Id;
        LoanShare = loanShare;
        Index = index;
        HealthQuestionnaire = null;
        HealthSurvey = null;
    }

    // ReSharper disable once UnusedMember.Local
    private InsuredPerson()
    {
    }

    /// <summary>
    ///     Персональные данные застрахованного лица
    /// </summary>
    public Customer Customer { get; private set; } = null!;

    /// <summary>
    ///     Медицинская анкета
    /// </summary>
    public HealthQuestionnaire? HealthQuestionnaire { get; private set; }

    /// <summary>
    ///     Медицинское обследование
    /// </summary>
    public HealthSurvey? HealthSurvey { get; private set; }

    /// <summary>
    ///     Позиция
    /// </summary>
    [Column("index")]
    public InsuredPersonIndex Index { get; private set; }

    /// <summary>
    ///     Доля созаемщика в процентах
    /// </summary>
    [Column("loan_share")]
    public decimal LoanShare { get; private set; }

    /// <summary>
    ///     Ид персональных данных застрахованного лица
    /// </summary>
    // ReSharper disable once UnassignedGetOnlyAutoProperty
    [Column("customer_id")]
    public Guid CustomerId { get; private set; }

    /// <summary>
    ///     Изменить клиента
    /// </summary>
    public void ChangeCustomer(Customer customer)
    {
        CustomerId = customer.Id;
        Customer = customer;
    }

    /// <summary>
    ///     Добавить медицинскую анкету
    /// </summary>
    /// <param name="healthQuestionnaire">Мед. анкета</param>
    public void ChangeHealthQuestionnaire(HealthQuestionnaire healthQuestionnaire) =>
        HealthQuestionnaire = healthQuestionnaire;

    /// <summary>
    ///     Добавить медицинское обследование
    /// </summary>
    /// <param name="healthSurvey">Медицинское обследование</param>
    public void ChangeHealthSurvey(HealthSurvey healthSurvey) => HealthSurvey = healthSurvey;

    /// <summary>
    ///     Изменить долю созаемщика
    /// </summary>
    public void ChangeLoanShare(decimal loanShare) => LoanShare = loanShare;
}