﻿using Some.Abstractions.Beneficiaries;
using Newtonsoft.Json;

namespace Some.Domain.Beneficiaries;

/// <inheritdoc cref="IInsuranceAmountMultiplier" />
public class InsuranceAmountMultiplier : IInsuranceAmountMultiplier
{
    [Newtonsoft.Json.JsonConstructor]
    public InsuranceAmountMultiplier(
        [JsonProperty] DateTime? loanAgreementFrom,
        [JsonProperty] DateTime? loanAgreementTo,
        [JsonProperty] DateTime? mirFrom,
        [JsonProperty] DateTime? mirTo,
        [JsonProperty] decimal? percent,
        [JsonProperty] bool useLoanAgreementRate)
    {
        LoanAgreementFrom = loanAgreementFrom;
        LoanAgreementTo = loanAgreementTo;
        MirFrom = mirFrom;
        MirTo = mirTo;
        Percent = percent;
        UseLoanAgreementRate = useLoanAgreementRate;
    }

    private InsuranceAmountMultiplier()
    {
    }


    /// <inheritdoc />
    public DateTime? LoanAgreementFrom { get; }

    /// <inheritdoc />
    public DateTime? LoanAgreementTo { get; }

    /// <inheritdoc />
    public DateTime? MirFrom { get; }

    /// <inheritdoc />
    public DateTime? MirTo { get; }

    /// <inheritdoc />
    public decimal? Percent { get; }

    /// <inheritdoc />
    public bool UseLoanAgreementRate { get; }
}