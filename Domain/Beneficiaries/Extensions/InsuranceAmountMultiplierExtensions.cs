﻿using Some.Abstractions.Beneficiaries;

namespace Some.Domain.Beneficiaries.Extensions;

public static class InsuranceAmountMultiplierExtensions
{
    /// <summary>
    ///     Нет ограничений по датам
    /// </summary>
    public static bool Infinity(this IInsuranceAmountMultiplier commission) =>
        commission.LoanAgreementFrom == null
        && commission.LoanAgreementTo == null
        && commission.MirFrom == null
        && commission.MirTo == null;
}