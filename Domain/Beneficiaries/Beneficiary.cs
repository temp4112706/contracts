﻿using System.ComponentModel.DataAnnotations.Schema;
using Ct.Module.Domain.Entities;
using Some.Abstractions.Addresses;
using Some.Abstractions.Beneficiaries;
using Some.Abstractions.Enums;
using Some.Abstractions.Extensions;
using Some.Domain.Addresses;

// ReSharper disable UnusedAutoPropertyAccessor.Local

// ReSharper disable ReplaceAutoPropertyWithComputedProperty
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local

namespace Some.Domain.Beneficiaries;

/// <summary>
///     Выгодоприобретатель
/// </summary>
[Table("beneficiary")]
public class Beneficiary : TrackableEntity<string>, IBeneficiary<Address, InsuranceAmountMultiplier>
{
    /// <summary>
    ///     Создание
    /// </summary>
    public Beneficiary(BeneficiaryId id) : base(id.ToValue())
    {
        Name = id.GetDescription();
        Bic = id.GetBic();
        Type = id.GetBeneficiaryType();
        SupportedInsurers = Array.Empty<InsurerId>();
    }

    /// <summary>
    ///     Создание
    /// </summary>
    public Beneficiary(BeneficiaryId id, string name, string inn, string kpp, Address address, BeneficiaryType type,
        string? mfrAccountNumber, string? mfrAccountBankName, string? corrAccountNumber, string? corrAccountBankName,
        string? bic, string? okpo, InsuranceAmountMultiplier[]? insuranceAmountMultiplier) : base(id.ToValue())
    {
        Name = name;
        Inn = inn;
        Kpp = kpp;
        Type = type;
        Okpo = okpo;
        Bic = bic;
        CorrAccountNumber = corrAccountNumber;
        CorrAccountBankName = corrAccountBankName;
        MfrAccountBankName = mfrAccountBankName;
        MfrAccountNumber = mfrAccountNumber;
        InsuranceAmountMultiplier = insuranceAmountMultiplier;

        Address = address;
        AddressId = address.Id;

        Settings = new List<MortgageInsuranceSettings.MortgageInsuranceSettings>();
        SupportedInsurers = Array.Empty<InsurerId>();
    }

    // ReSharper disable once UnusedMember.Local
    private Beneficiary()
    {
    }

    /// <summary>
    ///     TODO: Внутренний ИД
    /// </summary>
    public BeneficiaryId InternalId => EnumExtensions.ParseValue<BeneficiaryId>(Id);

    public ICollection<MortgageInsuranceSettings.MortgageInsuranceSettings> Settings { get; private set; } = null!;

    /// <summary>
    ///     Поддерживаемые СК
    /// </summary>
    [Column("supported_insurers")]
    public InsurerId[] SupportedInsurers { get; private set; } = null!;

    /// <inheritdoc />
    [Column("bic")]
    public string? Bic { get; private set; }

    /// <inheritdoc />
    [Column("corr_account_bank")]
    public string? CorrAccountBankName { get; private set; }

    /// <inheritdoc />
    [Column("corr_account")]
    public string? CorrAccountNumber { get; private set; }

    /// <inheritdoc />
    [Column("insurance_amount_multiplier")]
    public InsuranceAmountMultiplier[]? InsuranceAmountMultiplier { get; private set; }

    /// <inheritdoc />
    [Column("mfr_account_bank")]
    public string? MfrAccountBankName { get; private set; }

    /// <inheritdoc />
    [Column("mfr_account")]
    public string? MfrAccountNumber { get; private set; }

    /// <summary>
    ///     Тип
    /// </summary>
    [Column("type")]
    public BeneficiaryType Type { get; private set; }

    /// <summary>
    ///     Наименование банка
    /// </summary>
    [Column("name")]
    public string Name { get; private set; } = null!;

    /// <summary>
    ///     Дата активации
    /// </summary>
    [Column("activated_at")]
    public DateTime? ActivatedAt { get; private set; }

    /// <summary>
    ///     Активный
    /// </summary>
    [Column("active")]
    public bool Active { get; private set; }

    /// <summary>
    ///     Адресс
    /// </summary>
    public Address? Address { get; private set; }

    /// <inheritdoc />
    [Column("contact_phone")]
    public string? ContactPhone { get; private set; }

    /// <summary>
    ///     ИНН
    /// </summary>
    [Column("inn")]
    public string? Inn { get; private set; }

    /// <summary>
    ///     КПП
    /// </summary>
    [Column("kpp")]
    public string? Kpp { get; private set; }

    /// <inheritdoc />
    [Column("ogrn")]
    public string? Ogrn { get; private set; }

    /// <inheritdoc />
    [Column("okpo")]
    public string? Okpo { get; private set; }

    /// <inheritdoc />
    [Column("okved")]
    public string? Okved { get; private set; }

    /// <inheritdoc />
    [Column("web_site")]
    public string? WebSite { get; private set; }

    /// <summary>
    ///     ID адреса
    /// </summary>
    [Column("address_id")]
    private Guid? AddressId { get; set; }

    /// <inheritdoc />
    public override bool Equals(object? obj) =>
        ReferenceEquals(this, obj) || (obj is Beneficiary other && Equals(other));

    /// <inheritdoc />
    // ReSharper disable once NonReadonlyMemberInGetHashCode
    public override int GetHashCode() => Id.GetHashCode();

    /// <summary>
    ///     Активация
    /// </summary>
    public void Activate(DateTime activatedAt)
    {
        Active = true;
        ActivatedAt = activatedAt;
    }

    /// <summary>
    ///     Добавить поддерживаемую СК
    /// </summary>
    public void AddSupportedInsurers(params InsurerId[] supportedInsurers)
    {
        var newSupportedInsurers = SupportedInsurers.ToList();
        newSupportedInsurers.AddRange(supportedInsurers);
        SupportedInsurers = newSupportedInsurers.Distinct().ToArray();
    }

    /// <summary>
    ///     Изменить адрес
    /// </summary>
    public void ChangeAddress(Address address)
    {
        Address = address;
        AddressId = address.Id;
    }

    /// <summary>
    ///     Изменить данные надбавки
    /// </summary>
    public void ChangeInsuranceAmountMultiplier(InsuranceAmountMultiplier[]? insuranceAmountMultiplier) =>
        InsuranceAmountMultiplier = insuranceAmountMultiplier;

    /// <summary>
    ///     Изменить название
    /// </summary>
    public void ChangeName(string name) => Name = name;

    /// <summary>
    ///     Изменить основные данные
    /// </summary>
    public void ChangeRequisites(string? inn, string? kpp, string? ogrn, string? okpo,
        string? okved, string? contactPhone, IAddress? address, string? webSite, string? corrAccountNumber,
        string? corrAccountBankName, string? mfrAccountBankName, string? mfrAccountNumber)
    {
        Inn = inn;
        Kpp = kpp;
        Ogrn = ogrn;
        Okpo = okpo;
        ContactPhone = contactPhone;
        Okved = okved;
        WebSite = webSite;
        CorrAccountNumber = corrAccountNumber;
        CorrAccountBankName = corrAccountBankName;
        MfrAccountBankName = mfrAccountBankName;
        MfrAccountNumber = mfrAccountNumber;
        ChangeAddress(address);
    }

    /// <summary>
    ///     Изменить поддерживаемые СК
    /// </summary>
    public void ChangeSupportedInsurers(InsurerId[] supportedInsurers) =>
        SupportedInsurers = supportedInsurers;

    /// <summary>
    ///     Деактивация
    /// </summary>
    public void Deactivate()
    {
        Active = false;
        ActivatedAt = null;
    }

    /// <summary>
    ///     Удалить поддерживаемую СК
    /// </summary>
    public void RemoveSupportedInsurers(params InsurerId[] supportedInsurers) =>
        SupportedInsurers = SupportedInsurers
            .Where(x => !supportedInsurers.Contains(x))
            .ToArray();

    private void ChangeAddress(IAddress? address)
    {
        if (Address != null && address != null)
        {
            Address.Change(address);
        }
        else if (Address != null && address == null)
        {
            Address = null;
            AddressId = null;
        }
        else if (Address == null && address != null)
        {
            Address = new Address(Guid.NewGuid(), address);
        }
    }

    private bool Equals(Beneficiary other) => Id == other.Id;
}