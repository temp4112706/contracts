using Some.Abstractions.Common;
using Some.Abstractions.Enums;

namespace Some.Domain.Common;

/// <inheritdoc />
public record MortgageSettingsIdentifiers : IMortgageSettingsIdentifiers
{
    /// <summary>
    ///     ctor
    /// </summary>
    /// <param name="insurerId">СК</param>
    /// <param name="beneficiaryId">Выгодоприобретатель</param>
    public MortgageSettingsIdentifiers(InsurerId insurerId, BeneficiaryId beneficiaryId)
    {
        InsurerId = insurerId;
        BeneficiaryId = beneficiaryId;
    }

    private MortgageSettingsIdentifiers()
    {
        InsurerId = null;
        BeneficiaryId = null;
    }

    /// <inheritdoc />
    public BeneficiaryId? BeneficiaryId { get; }

    /// <inheritdoc />
    public InsurerId? InsurerId { get; }

    /// <summary>
    ///     Глобальные идентификаторы
    /// </summary>
    public static MortgageSettingsIdentifiers Global => new();

    /// <inheritdoc />
    public override string ToString()
    {
        return InsurerId.HasValue && BeneficiaryId.HasValue
            ? $"СК: {InsurerId}, Выгодоприобретатель: {BeneficiaryId}"
            : "Глобальные идентификаторы";
    }
}