﻿using Some.Abstractions.Common;
using Some.Domain.Monies;
using Newtonsoft.Json;

namespace Some.Domain.Common.ValueObjects;

/// <summary>
///     Фактическая страховая оплата
/// </summary>
public class ActualPayment : IActualPayment<Money>
{
    /// <summary>
    ///     Создание объекта
    /// </summary>
    [Newtonsoft.Json.JsonConstructor]
    public ActualPayment(
        [JsonProperty] DateTime paymentDate,
        [JsonProperty] Money actualAmount
    )
    {
        PaymentDate = paymentDate;
        ActualAmount = actualAmount;
    }

    /// <summary>
    ///     Создание объекта
    /// </summary>
    protected ActualPayment()
    {
        ActualAmount = Money.Rub(0);
    }

    /// <inheritdoc />
    public Money ActualAmount { get; internal set; }

    /// <inheritdoc />
    public DateTime PaymentDate { get; internal set; }
}