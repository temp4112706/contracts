using Some.Abstractions.Enums;

namespace Some.Domain.Common;

/// <summary>
///     Идентификаторы доп видов настроек
/// </summary>
public record AdditionalSettingsIdentifiers
{
    /// <summary>
    ///     ctor
    /// </summary>
    /// <param name="insurerId">СК</param>
    /// <param name="insuranceType">Тип доп трахования</param>
    public AdditionalSettingsIdentifiers(InsurerId insurerId, AdditionalInsuranceType insuranceType)
    {
        InsurerId = insurerId;
        InsuranceType = insuranceType;
    }

    /// <summary>
    ///     Тип доп трахования
    /// </summary>
    public AdditionalInsuranceType InsuranceType { get; }

    /// <summary>
    ///     СК
    /// </summary>
    public InsurerId InsurerId { get; }
}