using System.ComponentModel.DataAnnotations.Schema;
using Ct.Module.Domain.Entities;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Domain.Common;

/// <summary>
///     Базовый эвент
/// </summary>
public abstract class BaseEvent<TPayload> : BaseEntity<Guid>
    where TPayload : class
{
    /// <inheritdoc />
    protected BaseEvent()
    {
    }

    /// <inheritdoc />
    protected BaseEvent(Guid id) : base(id)
    {
    }

    /// <summary>
    ///     Базовое создание эвента
    /// </summary>
    protected BaseEvent(Guid id, TPayload payload, string? creator, DateTime? createdAt = null)
        : this(id)
    {
        Creator = creator;
        if (createdAt.HasValue)
        {
            CreatedAt = createdAt.Value;
        }

        Payload = payload;
    }

    /// <summary>
    ///     Дополнительная информация
    /// </summary>
    [Column("payload")]
    public virtual TPayload Payload { get; private set; } = null!;

    /// <summary>
    ///     Дата фиксации
    /// </summary>
    [Column("created_at")]
    public DateTime CreatedAt { get; private set; }

    /// <summary>
    ///     Ответственное лицо / Создатель эвента
    /// </summary>
    [Column("creator")]
    public string? Creator { get; private set; }
}