﻿using System.Globalization;
using Newtonsoft.Json.Linq;

namespace Some.Domain.ExtraProperties.Extensions;

/// <summary>
///     Расширения для <see cref="IHasExtraProperties" />
/// </summary>
public static class HasExtraPropertiesExtensions
{
    /// <summary>
    ///     Получить свойство
    /// </summary>
    public static object GetProperty(this IHasExtraProperties source, string name, object defaultValue = null!) =>
        source.ExtraProperties?.GetValueOrDefault(name, defaultValue) ?? defaultValue;

    /// <summary>
    ///     Проверка наличия свойства
    /// </summary>
    public static bool HasProperty(this IHasExtraProperties source, string name) =>
        source.ExtraProperties?.ContainsKey(name) ?? false;

    /// <summary>
    ///     Удаление свойства
    /// </summary>
    public static TSource RemoveProperty<TSource>(this TSource source, string name)
        where TSource : IHasExtraProperties
    {
        source.ExtraProperties?.Remove(name);
        return source;
    }

    /// <summary>
    ///     Добавление свойства
    /// </summary>
    public static TSource SetProperty<TSource>(
        this TSource source,
        string name,
        object value)
        where TSource : IHasExtraProperties
    {
        if (source.ExtraProperties == null)
        {
            return source;
        }

        source.ExtraProperties![name] = value;
        return source;
    }

    /// <summary>
    ///     Получить свойство с определенным типом
    /// </summary>
    public static TProperty? TryGetProperty<TProperty>(this IHasExtraProperties source, string name,
        TProperty defaultValue = default!)
    {
        var property = source.GetProperty(name);
        return property switch
        {
            null => defaultValue,
            JObject jObject => jObject.ToObject<TProperty>(),
            _ => (TProperty)Convert.ChangeType(property, typeof(TProperty), CultureInfo.InvariantCulture)
        };
    }
}