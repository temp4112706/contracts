﻿using Newtonsoft.Json;

namespace Some.Domain.ExtraProperties;

/// <summary>
///     Дополнительные свойства сущности
/// </summary>
[JsonDictionary]
public class ExtraPropertyDictionary : Dictionary<string, object>
{
    /// <summary>
    ///     Создание объекта
    /// </summary>
    public ExtraPropertyDictionary()
    {
    }
}