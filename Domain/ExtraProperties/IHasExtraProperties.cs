﻿namespace Some.Domain.ExtraProperties;

/// <summary>
///     Интерфейс сущности с динамически устанавливаемыми дополнительными свойствами
/// </summary>
public interface IHasExtraProperties
{
    /// <summary>
    ///     Дополнительные свойства сущности
    /// </summary>
    ExtraPropertyDictionary? ExtraProperties { get; }
}