using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Ct.Module.Domain.Entities;

namespace Some.Domain.GenNumbers;

/// <summary>
///     Последовательность генерации номеров
/// </summary>
[Table("gen_number_counter")]
public sealed class GenNumberCounter : BaseEntity<string>
{
    /// <summary>
    ///     Создание последовательности
    /// </summary>
    public GenNumberCounter(string id, int sequentialNumber)
    {
        Id = id;
        SequentialNumber = sequentialNumber;
    }

    // ReSharper disable once UnusedMember.Local
    private GenNumberCounter()
    {
    }

    /// <summary>
    ///     Порядковый номер
    /// </summary>
    [Column("sequential_number")]
    [Range(1, int.MaxValue)]
    public int SequentialNumber { get; private set; }

    /// <summary>
    ///     Инкремент порядкового номера
    /// </summary>
    public void NextSequentialNumber() => SequentialNumber++;
}