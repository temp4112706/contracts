﻿namespace Some.Domain.Countries;

/// <summary>
///     Страна
/// </summary>
public static class Country
{
    /// <summary>
    ///     Текущая страна (Россия)
    /// </summary>
    public const string Current = "RU";

    /// <summary>
    ///     Нормализация коа страны
    /// </summary>
    /// <param name="rawCountry">Какой-то код страны</param>
    /// <returns></returns>
    public static string Normalize(string? rawCountry)
    {
        switch (rawCountry?.ToUpperInvariant())
        {
            // Азербайджан
            case "AZ":
            case "AZE":
                return "AZ";
            // Армения
            case "AM":
            case "ARM":
                return "AM";
            // Белоруссия
            case "BY":
            case "BLR":
                return "BY";
            // Грузия
            case "GE":
            case "GEO":
                return "GE";
            // Казахстан
            case "KZ":
            case "KAZ":
                return "KZ";
            // Киргизия
            case "KG":
            case "KGZ":
                return "KG";
            // Молдавия
            case "MD":
            case "MDA":
                return "MD";
            // Российская Федерация
            case "RU":
            case "RUR":
            case "RUS":
                return Current;
            // Таджикистан
            case "TJ":
            case "TJK":
                return "TJ";
            // Узбекистан
            case "UZ":
            case "UZB":
                return "UZ";
            // Туркмения
            case "TM":
            case "TKM":
                return "TM";
            // Украина
            case "UA":
            case "UKR":
                return "UA";
            default:
                return rawCountry ?? Current;
        }
    }
}