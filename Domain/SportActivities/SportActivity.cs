﻿using Some.Abstractions.Constants;
using Some.Abstractions.SportActivity;

namespace Some.Domain.SportActivities;

/// <summary>
///     Спортивная деятельность
/// </summary>
public class SportActivity : ISportActivity
{
    /// <summary>
    ///     Спортивная деятельность
    /// </summary>
    public SportActivity(string name, bool isProfessional)
    {
        Name = name;
        IsProfessional= isProfessional;
    }
    
    /// <inheritdoc />
    public bool IsProfessional { get; private set; }
    
    /// <inheritdoc />
    public string Name { get; private set;  }
    
    /// <summary>
    ///     Изменить спортивную деятельность
    /// </summary>
    public void Change(ISportActivity sportActivity)
    {
        Name = sportActivity.Name;
        IsProfessional = sportActivity.IsProfessional;
    }
    
    /// <summary>
    ///     Отсутсвует спортивная деятельность
    /// </summary>
    public static SportActivity Default => new(SportActivityConstants.NoSportActivity, false);

 
}