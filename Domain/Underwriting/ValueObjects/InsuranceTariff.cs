﻿using Some.Abstractions.Common;
using Some.Abstractions.Monies;
using Some.Domain.Monies;
using Newtonsoft.Json;

namespace Some.Domain.Underwriting.ValueObjects;

/// <summary>
///     Тариф онлайн/офлайн андеррайтинга (за первый период)
/// </summary>
public class InsuranceTariff : IInsuranceTariff<Money>
{
    /// <summary>
    ///     Создание строки графика платежа
    /// </summary>
    [JsonConstructor]
    public InsuranceTariff(
        [JsonProperty] decimal rate,
        [JsonProperty] Money insuranceAmount,
        [JsonProperty] Money premiumAmount
    )
    {
        Rate = rate;
        InsuranceAmount = insuranceAmount;
        PremiumAmount = premiumAmount;
    }

    private InsuranceTariff()
    {
    }

    /// <summary>
    ///     Страховая сумма
    /// </summary>
    public Money InsuranceAmount { get; private set; } = null!;

    /// <summary>
    ///     Страховая премия
    /// </summary>
    public Money PremiumAmount { get; private set; } = null!;

    /// <summary>
    ///     Ставка тарифа страхования
    /// </summary>
    public decimal Rate { get; private set; }

    /// <summary>
    ///     Изменить страховую сумму
    /// </summary>
    public void ChangeInsuranceAmount(decimal amount) =>
        InsuranceAmount = Money.Create(amount, InsuranceAmount.Currency);

    /// <summary>
    ///     Изменить страховую премию
    /// </summary>
    public void ChangePremiumAmount<TMoney>(TMoney amount)
        where TMoney : class, IMoney =>
        PremiumAmount = Money.Create(amount.Amount, amount.Currency);

    internal void ChangeInsuranceAmount<TMoney>(TMoney amount)
        where TMoney : class, IMoney =>
        InsuranceAmount = Money.Create(amount.Amount, amount.Currency);

    internal void ChangePremiumAmount(decimal amount) => PremiumAmount = Money.Create(amount, PremiumAmount.Currency);

    internal void ChangeRate(decimal rate) => Rate = rate;

    /// <summary>
    ///     Создание результата андеррайтинга
    /// </summary>
    public static InsuranceTariff FromAmount(decimal rate, Money loanAmount, decimal multiplier)
    {
        var insuranceAmount = loanAmount.Multiply(multiplier);
        return new InsuranceTariff(
            rate,
            insuranceAmount,
            insuranceAmount.Multiply(rate)
        );
    }
    
    /// <summary>
    ///     Пустой тариф
    /// </summary>
    public static InsuranceTariff Empty()
    {
        return FromAmount(0, Money.ZeroRub(), 0);
    }
}