using Some.Abstractions.Underwriting;
using Newtonsoft.Json;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local

namespace Some.Domain.Underwriting.ValueObjects;

/// <summary>
///     Строка графика тарифов
/// </summary>
public class TariffScheduleRow : ITariffScheduleRow
{
    /// <summary>
    ///     Создание строки графика
    /// </summary>
    [Newtonsoft.Json.JsonConstructor]
    public TariffScheduleRow(
        [JsonProperty] int number,
        [JsonProperty] decimal rate)
    {
        Number = number;
        Rate = rate;
    }

    private TariffScheduleRow()
    {
    }

    /// <inheritdoc />
    public int Number { get; private set; }

    /// <inheritdoc />
    public decimal Rate { get; private set; }
}