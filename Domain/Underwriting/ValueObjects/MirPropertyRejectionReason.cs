using Some.Abstractions.Enums;
using Some.Abstractions.Underwriting;
using Newtonsoft.Json;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local

namespace Some.Domain.Underwriting.ValueObjects;

/// <summary>
///     Причина отказа при проведении андеррайтинга страхования имущества
/// </summary>
public class MirPropertyRejectionReason : IMirPropertyRejectionReason
{
    /// <summary>
    ///     Создание причины отказа страхования имущества
    /// </summary>
    /// <param name="code"></param>
    /// <param name="message"></param>
    [Newtonsoft.Json.JsonConstructor]
    public MirPropertyRejectionReason(
        [JsonProperty] PropertyRejectionReasonCode code,
        [JsonProperty] string message)
    {
        Code = code;
        Message = message;
    }

    private MirPropertyRejectionReason()
    {
    }

    /// <summary>
    ///     Код причины отказа
    /// </summary>
    public PropertyRejectionReasonCode Code { get; private set; }

    /// <summary>
    ///     Сообщение об ошибке
    /// </summary>
    public string Message { get; private set; } = null!;
}