using Some.Abstractions.Enums;
using Some.Abstractions.Underwriting;
using Newtonsoft.Json;

namespace Some.Domain.Underwriting.ValueObjects;

/// <summary>
///     Причина отказа при проведении андеррайтинга страхования титула
/// </summary>
public class MirTitleRejectionReason : IMirTitleRejectionReason
{
    /// <summary>
    ///     Создание причины отказа страхования титула
    /// </summary>
    [JsonConstructor]
    public MirTitleRejectionReason(
        [JsonProperty] TitleRejectionReasonCode code,
        [JsonProperty] string message)
    {
        Code = code;
        Message = message ?? throw new ArgumentNullException(nameof(message));
    }

    private MirTitleRejectionReason()
    {
    }

    /// <summary>
    ///     Код причины отказа
    /// </summary>
    public TitleRejectionReasonCode Code { get; }

    /// <summary>
    ///     Сообщение об ошибке
    /// </summary>
    public string Message { get; } = null!;
}