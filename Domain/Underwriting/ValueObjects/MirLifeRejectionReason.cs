﻿using Some.Abstractions.Enums;
using Some.Abstractions.Underwriting;
using Newtonsoft.Json;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local

namespace Some.Domain.Underwriting.ValueObjects;

/// <summary>
///     Причина отказа при проведении андеррайтинга страхования жизни
/// </summary>
public class MirLifeRejectionReason : IMirLifeRejectionReason
{
    /// <summary>
    ///     Создание причины отказа страхования жизни
    /// </summary>
    [Newtonsoft.Json.JsonConstructor]
    public MirLifeRejectionReason(
        [JsonProperty] LifeRejectionReasonCode code,
        [JsonProperty] string message)
    {
        Code = code;
        Message = message;
    }

    private MirLifeRejectionReason()
    {
    }

    /// <summary>
    ///     Код причины отказа
    /// </summary>
    public LifeRejectionReasonCode Code { get; private set; }

    /// <summary>
    ///     Сообщение об ошибке
    /// </summary>
    public string Message { get; private set; } = null!;
}