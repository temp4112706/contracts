using System.ComponentModel.DataAnnotations.Schema;
using Some.Abstractions.Enums;
using Some.Abstractions.Underwriting;
using Some.Domain.Mirs;
using Some.Domain.Monies;
using Some.Domain.Underwriting.ValueObjects;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local
// ReSharper disable UnusedAutoPropertyAccessor.Local

namespace Some.Domain.Underwriting;

/// <summary>
///     Запрос тарифа страхования имущества
/// </summary>
[Table("mir_property_underwriting")]
public sealed class MirPropertyUnderwriting : MirBaseUnderwriting,
    IMirPropertyUnderwriting<Money, InsuranceTariff, TariffScheduleRow, MirPropertyRejectionReason>
{
    // ReSharper disable once UnusedMember.Local
    private MirPropertyUnderwriting()
    {
    }

    /// <summary>
    ///     Создание андеррайтинга имущества
    /// </summary>
    private MirPropertyUnderwriting(Guid id, MirPropertyInsuranceRequest insRequest,
        MirUnderwritingState state, bool online, InsuranceTariff? tariff,
        MirPropertyRejectionReason[]? rejectionReasons = null) : base(id, state, tariff)
    {
        MirPropertyInsuranceRequest = insRequest;
        MirPropertyInsuranceRequestId = insRequest.Id;
        Online = online;
        RejectionReasons = rejectionReasons;
    }

    /// <summary>
    ///     Коды и сообщения отказа при проведении андеррайтинга страхования имущества
    /// </summary>
    [Column("rejection_reasons")]
    public MirPropertyRejectionReason[]? RejectionReasons { get; private set; }

    /// <summary>
    ///     Запрос страхования имущества
    /// </summary>
    private MirPropertyInsuranceRequest MirPropertyInsuranceRequest { get; } = null!;

    /// <summary>
    ///     ИД запроса страхования имущества
    /// </summary>
    [Column("mir_property_insurance_request_id")]
    private Guid MirPropertyInsuranceRequestId { get; set; }

    /// <inheritdoc />
    public override void OfflineProcessing(TariffScheduleRow[] customSchedule, InsuranceTariff tariff)
    {
        base.OfflineProcessing(customSchedule, tariff);
        RejectionReasons = null;
    }

    /// <inheritdoc />
    public override void OnlineProcessing(InsuranceTariff tariff)
    {
        base.OnlineProcessing(tariff);
        RejectionReasons = null;
    }


    /// <inheritdoc />
    public override void Reset()
    {
        base.Reset();
        RejectionReasons = null;
    }

    /// <summary>
    ///     Офлайн отклонение андеррайтинга страхования имущества
    /// </summary>
    /// <param name="rejectionReasons">Причины отклонения</param>
    public void OfflineRejecting(MirPropertyRejectionReason[] rejectionReasons)
    {
        State = MirUnderwritingState.Rejected;
        if (RejectionReasons != null)
        {
            var newRejectionReasons = RejectionReasons.ToList();
            newRejectionReasons.AddRange(rejectionReasons);
            RejectionReasons = newRejectionReasons.ToArray();
        }
        else
        {
            RejectionReasons = rejectionReasons;
        }
    }

    /// <summary>
    ///     Онлайн отклонение андеррайтинга страхования имущества
    /// </summary>
    /// <param name="rejectionReasons">Причины отклонения</param>
    public void OnlineRejecting(MirPropertyRejectionReason[]? rejectionReasons)
    {
        State = MirUnderwritingState.Rejected;
        RejectionReasons = rejectionReasons;
    }

    /// <summary>
    ///     Полное обновление данных андеррайтинга имущества без валидации
    /// </summary>
    public void Replace(MirUnderwritingState state, bool online, InsuranceTariff? tariff,
        TariffScheduleRow[]? customSchedule, MirPropertyRejectionReason[]? rejectionReasons,
        DateTime? updatedAt)
    {
        State = state;
        Online = online;
        Tariff = tariff;
        CustomSchedule = customSchedule;
        RejectionReasons = rejectionReasons;
        UpdatedAt = updatedAt;
    }

    /// <summary>
    ///     Изменить онлайн андеррайтинг на офлайн
    ///     <param name="rejectionReasons">Причины отказа</param>
    /// </summary>
    public void SwitchToOffline(MirPropertyRejectionReason[]? rejectionReasons = null)
    {
        Online = false;
        State = MirUnderwritingState.InProcess;
        if (rejectionReasons != null)
        {
            RejectionReasons = rejectionReasons;
        }

        Tariff = null;
    }

    /// <summary>
    ///     Создание кастомного андеррайтинга страхования имущества
    /// </summary>
    public static MirPropertyUnderwriting CreateCustom(Guid id, MirPropertyInsuranceRequest insRequest,
        MirUnderwritingState state, bool online, InsuranceTariff? tariff,
        MirPropertyRejectionReason[]? rejectionReasons = null) =>
        new(
            id,
            insRequest,
            state,
            online,
            tariff,
            rejectionReasons
        );

    /// <summary>
    ///     Создание офлайн андеррайтинга страхования имущества со стейтом "В процессе"
    /// </summary>
    public static MirPropertyUnderwriting CreateOffline(MirPropertyInsuranceRequest insRequest,
        TariffScheduleRow[]? customSchedule = null)
    {
        var underwriting = new MirPropertyUnderwriting(
            Guid.NewGuid(),
            insRequest,
            MirUnderwritingState.InProcess,
            false,
            null
        ) { CustomSchedule = customSchedule };
        return underwriting;
    }

    /// <summary>
    ///     Создание онлайн андеррайтинга страхования имущества со стейтом "В процессе"
    /// </summary>
    public static MirPropertyUnderwriting CreateOnline(MirPropertyInsuranceRequest insRequest) =>
        new(
            Guid.NewGuid(),
            insRequest,
            MirUnderwritingState.InProcess,
            true,
            null
        );
}