using System.ComponentModel.DataAnnotations.Schema;
using Some.Abstractions.Enums;
using Some.Abstractions.Underwriting;
using Some.Domain.Mirs;
using Some.Domain.Monies;
using Some.Domain.Underwriting.ValueObjects;

// ReSharper disable UnusedAutoPropertyAccessor.Local
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local

namespace Some.Domain.Underwriting;

/// <summary>
///     Запрос тарифа индивидуальных условий страхования жизни
/// </summary>
[Table("mir_life_underwriting")]
public sealed class MirLifeUnderwriting : MirBaseUnderwriting,
    IMirLifeUnderwriting<Money, InsuranceTariff, TariffScheduleRow, MirLifeRejectionReason>
{
    // ReSharper disable once UnusedMember.Local
    private MirLifeUnderwriting()
    {
    }

    /// <summary>
    ///     Создание андеррайтинга жизни
    /// </summary>
    private MirLifeUnderwriting(Guid id, MirLifeInsuranceRequest insRequest, MirUnderwritingState state,
        bool online, InsuranceTariff? tariff, MirLifeRejectionReason[]? rejectionReasons = null)
        : base(id, state, tariff)
    {
        MirLifeInsuranceRequest = insRequest;
        MirLifeInsuranceRequestId = insRequest.Id;
        Online = online;
        RejectionReasons = rejectionReasons;
    }

    /// <summary>
    ///     Коды и сообщения отказа при проведении андеррайтинга страхования жизни
    /// </summary>
    [Column("rejection_reasons")]
    public MirLifeRejectionReason[]? RejectionReasons { get; private set; }

    /// <summary>
    ///     Запрос страхования жизни
    /// </summary>
    private MirLifeInsuranceRequest MirLifeInsuranceRequest { get; } = null!;

    /// <summary>
    ///     ИД запроса страхования жизни жизни
    /// </summary>
    [Column("mir_life_insurance_request_id")]
    private Guid MirLifeInsuranceRequestId { get; set; }

    /// <inheritdoc />
    public override void OfflineProcessing(TariffScheduleRow[] customSchedule, InsuranceTariff tariff)
    {
        base.OfflineProcessing(customSchedule, tariff);
        RejectionReasons = null;
    }

    /// <inheritdoc />
    public override void OnlineProcessing(InsuranceTariff tariff)
    {
        base.OnlineProcessing(tariff);
        RejectionReasons = null;
    }

    /// <inheritdoc />
    public override void Reset()
    {
        base.Reset();
        RejectionReasons = null;
    }

    /// <summary>
    ///     Офлайн отклонение андеррайтинга страхования жизни
    /// </summary>
    /// <param name="rejectionReasons">Причины отказа</param>
    public void OfflineRejecting(MirLifeRejectionReason[] rejectionReasons)
    {
        State = MirUnderwritingState.Rejected;
        if (RejectionReasons != null)
        {
            var newRejectionReasons = RejectionReasons.ToList();
            newRejectionReasons.AddRange(rejectionReasons);
            RejectionReasons = newRejectionReasons.ToArray();
        }
        else
        {
            RejectionReasons = rejectionReasons;
        }
    }

    /// <summary>
    ///     Онлайн отклонение андеррайтинга страхования жизни
    /// </summary>
    /// <param name="rejectionReasons">Причины отказа</param>
    public void OnlineRejecting(MirLifeRejectionReason[]? rejectionReasons)
    {
        State = MirUnderwritingState.Rejected;
        RejectionReasons = rejectionReasons;
    }

    /// <summary>
    ///     Полное обновление данных андеррайтинга жизни без валидации
    /// </summary>
    public void Replace(MirUnderwritingState state, bool online, InsuranceTariff? tariff,
        TariffScheduleRow[]? customSchedule, MirLifeRejectionReason[]? rejectionReasons, DateTime? updatedAt)
    {
        State = state;
        Online = online;
        Tariff = tariff;
        CustomSchedule = customSchedule;
        RejectionReasons = rejectionReasons;
        UpdatedAt = updatedAt;
    }

    /// <summary>
    ///     Изменить онлайн андеррайтинг на офлайн
    ///     <param name="rejectionReasons">Причины отказа</param>
    /// </summary>
    public void SwitchToOffline(MirLifeRejectionReason[]? rejectionReasons = null)
    {
        Online = false;
        State = MirUnderwritingState.InProcess;
        if (rejectionReasons != null)
        {
            RejectionReasons = rejectionReasons;
        }

        Tariff = null;
    }

    /// <summary>
    ///     Создание кастомного андеррайтинга страхования жизни
    /// </summary>
    public static MirLifeUnderwriting CreateCustom(Guid id, MirLifeInsuranceRequest insRequest,
        MirUnderwritingState state, bool online, InsuranceTariff? tariff,
        MirLifeRejectionReason[]? rejectionReasons = null) =>
        new(
            id,
            insRequest,
            state,
            online,
            tariff,
            rejectionReasons
        );

    /// <summary>
    ///     Создание офлайн андеррайтинга страхования жизни со стейтом "В процессе"
    /// </summary>
    public static MirLifeUnderwriting CreateOffline(MirLifeInsuranceRequest insRequest,
        TariffScheduleRow[]? customSchedule = null)
    {
        var underwriting = new MirLifeUnderwriting(
            Guid.NewGuid(),
            insRequest,
            MirUnderwritingState.InProcess,
            false,
            null
        ) { CustomSchedule = customSchedule };
        return underwriting;
    }

    /// <summary>
    ///     Создание онлайн андеррайтинга страхования жизни со стейтом "В процессе"
    /// </summary>
    public static MirLifeUnderwriting CreateOnline(MirLifeInsuranceRequest insRequest) =>
        new(
            Guid.NewGuid(),
            insRequest,
            MirUnderwritingState.InProcess,
            true,
            null
        );
}