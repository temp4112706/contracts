using System.ComponentModel.DataAnnotations.Schema;
using Ct.Module.Domain.Entities;
using Some.Abstractions.Enums;
using Some.Abstractions.Underwriting;
using Some.Domain.Monies;
using Some.Domain.Underwriting.ValueObjects;

namespace Some.Domain.Underwriting;

/// <summary>
///     Базовая сущность запроса тарифа для страхового договора
/// </summary>
public abstract class MirBaseUnderwriting : TrackableEntity<Guid>,
    IBaseMirUnderwriting<Money, InsuranceTariff, TariffScheduleRow>
{
    // ReSharper disable once UnusedMember.Global
    /// <inheritdoc />
    protected MirBaseUnderwriting()
    {
    }

    /// <inheritdoc />
    protected MirBaseUnderwriting(Guid id, MirUnderwritingState state, InsuranceTariff? tariff)
        : base(id)
    {
        Tariff = tariff;
        State = state;
        CustomSchedule = null;
        Online = false;
    }

    /// <summary>
    ///     Обработан?
    /// </summary>
    [NotMapped]
    public bool IsProcessed => State != MirUnderwritingState.InProcess;

    /// <summary>
    ///     Пользовательский график андеррайтинга
    /// </summary>
    [Column("custom_schedule")]
    public TariffScheduleRow[]? CustomSchedule { get; protected set; }

    /// <summary>
    ///     Страховая премия
    /// </summary>
    [Column("tariff")]
    public InsuranceTariff? Tariff { get; protected set; }

    /// <summary>
    ///     Онлайн андеррайтинг?
    /// </summary>
    [Column("online")]
    public bool Online { get; protected set; }

    /// <summary>
    ///     Статус
    /// </summary>
    [Column("state")]
    public MirUnderwritingState State { get; protected set; }

    /// <summary>
    ///     Сбросить данные андеррайтинга
    /// </summary>
    public virtual void Reset()
    {
        State = MirUnderwritingState.InProcess;
        Tariff = null;
        CustomSchedule = null;
        // Online - оставляем, тк если заявка нетиповая, то нельзя провести онлайн андеррайтинг
    }

    /// <summary>
    ///     Офлайн обработка заявки
    /// </summary>
    /// <param name="customSchedule">Пользовательский график андеррайтинга</param>
    /// <param name="tariff">Тариф андеррайтинга за первый период</param>
    public virtual void OfflineProcessing(TariffScheduleRow[] customSchedule, InsuranceTariff tariff)
    {
        State = MirUnderwritingState.Processed;
        CustomSchedule = customSchedule;
        Tariff = tariff;
    }

    /// <summary>
    ///     Онлайн обработка заявки
    /// </summary>
    /// <param name="tariff">Тариф андеррайтинга за первый период</param>
    public virtual void OnlineProcessing(InsuranceTariff tariff)
    {
        State = MirUnderwritingState.Processed;
        Tariff = tariff;
        CustomSchedule = null;
    }
}