using System.ComponentModel.DataAnnotations.Schema;
using Some.Abstractions.Enums;
using Some.Abstractions.Underwriting;
using Some.Domain.Mirs;
using Some.Domain.Monies;
using Some.Domain.Underwriting.ValueObjects;

namespace Some.Domain.Underwriting;

/// <summary>
///     Запрос тарифа страхования титула
/// </summary>
[Table("mir_title_underwriting")]
public sealed class MirTitleUnderwriting : MirBaseUnderwriting,
    IMirTitleUnderwriting<Money, InsuranceTariff, TariffScheduleRow, MirTitleRejectionReason>
{
    // ReSharper disable once UnusedMember.Local
    private MirTitleUnderwriting()
    {
    }

    /// <summary>
    ///     Создание андеррайтинга титула
    /// </summary>
    private MirTitleUnderwriting(Guid id, MirTitleInsuranceRequest insRequest,
        MirUnderwritingState state, bool online, InsuranceTariff? tariff,
        MirTitleRejectionReason[]? rejectionReasons = null) : base(id, state, tariff)
    {
        MirTitleInsuranceRequest = insRequest;
        MirTitleInsuranceRequestId = insRequest.Id;
        Online = online;
        RejectionReasons = rejectionReasons;
    }

    /// <summary>
    ///     Коды и сообщения отказа при проведении андеррайтинга страхования титула
    /// </summary>
    [Column("rejection_reasons")]
    public MirTitleRejectionReason[]? RejectionReasons { get; private set; }

    /// <summary>
    ///     Запрос страхования титула
    /// </summary>
    private MirTitleInsuranceRequest MirTitleInsuranceRequest { get; } = null!;

    /// <summary>
    ///     ИД запроса страхования титула
    /// </summary>
    [Column("mir_title_insurance_request_id")]
    private Guid MirTitleInsuranceRequestId { get; }

    /// <inheritdoc />
    public override void OfflineProcessing(TariffScheduleRow[] customSchedule, InsuranceTariff tariff)
    {
        base.OfflineProcessing(customSchedule, tariff);
        RejectionReasons = null;
    }

    /// <inheritdoc />
    public override void OnlineProcessing(InsuranceTariff tariff)
    {
        base.OnlineProcessing(tariff);
        RejectionReasons = null;
    }

    /// <inheritdoc />
    public override void Reset()
    {
        base.Reset();
        RejectionReasons = null;
    }

    /// <summary>
    ///     Офлайн отклонение андеррайтинга страхования титула
    /// </summary>
    /// <param name="rejectionReasons">Причины отклонения</param>
    public void OfflineRejecting(MirTitleRejectionReason[] rejectionReasons)
    {
        State = MirUnderwritingState.Rejected;
        if (RejectionReasons != null)
        {
            var newRejectionReasons = RejectionReasons.ToList();
            newRejectionReasons.AddRange(rejectionReasons);
            RejectionReasons = newRejectionReasons.ToArray();
        }
        else
        {
            RejectionReasons = rejectionReasons;
        }
    }

    /// <summary>
    ///     Онлайн отклонение андеррайтинга страхования титула
    /// </summary>
    /// <param name="rejectionReasons">Причины отклонения</param>
    public void OnlineRejecting(MirTitleRejectionReason[]? rejectionReasons)
    {
        State = MirUnderwritingState.Rejected;
        RejectionReasons = rejectionReasons;
    }

    /// <summary>
    ///     Полное обновление данных андеррайтинга титула без валидации
    /// </summary>
    public void Replace(MirUnderwritingState state, bool online, InsuranceTariff? tariff,
        TariffScheduleRow[]? customSchedule, MirTitleRejectionReason[]? rejectionReasons,
        DateTime? updatedAt)
    {
        State = state;
        Online = online;
        Tariff = tariff;
        CustomSchedule = customSchedule;
        RejectionReasons = rejectionReasons;
        UpdatedAt = updatedAt;
    }

    /// <summary>
    ///     Изменить онлайн андеррайтинг на офлайн
    ///     <param name="rejectionReasons">Причины отказа</param>
    /// </summary>
    public void SwitchToOffline(MirTitleRejectionReason[]? rejectionReasons = null)
    {
        Online = false;
        State = MirUnderwritingState.InProcess;
        if (rejectionReasons != null)
        {
            RejectionReasons = rejectionReasons;
        }

        Tariff = null;
    }

    /// <summary>
    ///     Создание кастомного андеррайтинга страхования титула
    /// </summary>
    public static MirTitleUnderwriting CreateCustom(Guid id, MirTitleInsuranceRequest insRequest,
        MirUnderwritingState state, bool online, InsuranceTariff? tariff,
        MirTitleRejectionReason[]? rejectionReasons = null) =>
        new(
            id,
            insRequest,
            state,
            online,
            tariff,
            rejectionReasons
        );

    /// <summary>
    ///     Создание офлайн андеррайтинга страхования титула со стейтом "В процессе"
    /// </summary>
    public static MirTitleUnderwriting CreateOffline(MirTitleInsuranceRequest insRequest,
        TariffScheduleRow[]? customSchedule = null)
    {
        var underwriting = new MirTitleUnderwriting(
            Guid.NewGuid(),
            insRequest,
            MirUnderwritingState.InProcess,
            false,
            null
        ) { CustomSchedule = customSchedule };
        return underwriting;
    }

    /// <summary>
    ///     Создание онлайн андеррайтинга страхования титула со стейтом "В процессе"
    /// </summary>
    public static MirTitleUnderwriting CreateOnline(MirTitleInsuranceRequest insRequest) =>
        new(
            Guid.NewGuid(),
            insRequest,
            MirUnderwritingState.InProcess,
            true,
            null
        );
}