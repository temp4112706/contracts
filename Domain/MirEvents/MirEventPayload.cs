using Some.Abstractions.Enums;
using Some.Domain.Mirs.ValueObjects;
using Newtonsoft.Json;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Domain.MirEvents;

/// <summary>
///     Дополнительная информация эвента заявки
/// </summary>
public class MirEventPayload
{
    /// <summary>
    ///     Создание дополнительной информации
    /// </summary>
    [Newtonsoft.Json.JsonConstructor]
    public MirEventPayload(
        [JsonProperty] MirStatusChangesResult? statusChangesResult = null,
        [JsonProperty] string? comment = null
    )
    {
        StatusChangesResult = statusChangesResult;
        Comment = comment;
    }

    /// <summary>
    ///     Создание дополнительной информации
    /// </summary>
    public MirEventPayload(MirStatus currentStatus, MirStatus? previousStatus, DateTime changedAt,
        string? comment = null)
    {
        StatusChangesResult = new MirStatusChangesResult(currentStatus, previousStatus, changedAt);
        Comment = comment;
    }

    /// <summary>
    ///     Создание дополнительной информации
    /// </summary>
    private MirEventPayload()
    {
    }

    /// <summary>
    ///     Комментарий
    /// </summary>
    public string? Comment { get; private set; }

    /// <summary>
    ///     Результат перемещения статуса
    /// </summary>
    public MirStatusChangesResult? StatusChangesResult { get; private set; }
}