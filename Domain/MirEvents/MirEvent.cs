using System.ComponentModel.DataAnnotations.Schema;
using Some.Domain.Common;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Domain.MirEvents;

/// <summary>
///     Эвенты заявки
/// </summary>
[Table("mir_event")]
public sealed class MirEvent : BaseEvent<MirEventPayload>
{
    /// <summary>
    ///     Создание эвента заявки
    /// </summary>
    /// <param name="id">Ид существующего эвента заявки</param>
    /// <param name="mirId">Ид заявки</param>
    /// <param name="event">Название эвента</param>
    /// <param name="payload">Дополнительная информация</param>
    /// <param name="creator">Ответственное лицо / Создатель эвента</param>
    /// <param name="createdAt">Создана, если отличается от текущей даты</param>
    public MirEvent(Guid id, Guid mirId, string @event, MirEventPayload payload,
        string? creator = null, DateTime? createdAt = null)
        : base(id, payload, creator, createdAt)
    {
        MirId = mirId;
        Event = @event;
    }

    // ReSharper disable once UnusedMember.Local
    private MirEvent()
    {
    }

    /// <summary>
    ///     Название эвента
    /// </summary>
    [Column("event")]
    public string Event { get; private set; } = null!;

    /// <summary>
    ///     Ид заявки
    /// </summary>
    [Column("mir_id")]
    public Guid MirId { get; private set; }
}