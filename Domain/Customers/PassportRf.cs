using System.ComponentModel.DataAnnotations.Schema;
using Ct.Module.Domain.Entities;
using Some.Abstractions.Customers;
using Some.Abstractions.Enums;

// ReSharper disable UnusedAutoPropertyAccessor.Local
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local

namespace Some.Domain.Customers;

/// <summary>
///     Документ удостоверяющий личность (Паспорт РФ)
/// </summary>
[Table("customer_document")]
public sealed class PassportRf : BaseEntity<Guid>, IPassportRf
{
    /// <summary>
    ///     Создание документв
    /// </summary>
    public PassportRf(Guid id, Guid customerId, DocType docType, string series, string number,
        DateTime issueDate, string issuedBy, string? departmentCode) : base(id)
    {
        DocType = docType;
        CustomerId = customerId;
        Series = series;
        Number = number;
        IssuedBy = issuedBy;
        IssueDate = issueDate;
        DepartmentCode = departmentCode;
    }

    // ReSharper disable once UnusedMember.Local
    private PassportRf()
    {
    }

    /// <summary>
    ///     Код подразделения
    /// </summary>
    [Column("department_code")]
    public string? DepartmentCode { get; private set; }

    /// <summary>
    ///     Код типа документа (21 для Паспорта РФ)
    /// </summary>
    [Column("doc_type")]
    public DocType DocType { get; private set; }

    /// <summary>
    ///     Дата выдачи
    /// </summary>
    [Column("issue_date", TypeName = "date")]
    public DateTime IssueDate { get; private set; }

    /// <summary>
    ///     Документ выдан
    /// </summary>
    [Column("issued_by")]
    public string IssuedBy { get; private set; } = null!;

    /// <summary>
    ///     Номер документа (формат 999999)
    /// </summary>
    [Column("number")]
    public string Number { get; private set; } = null!;

    /// <summary>
    ///     Серия документа (формат 9999)
    /// </summary>
    [Column("series")]
    public string Series { get; private set; } = null!;

    /// <summary>
    ///     Ид клиента
    /// </summary>
    [Column("customer_id")]
    private Guid CustomerId { get; set; }

    /// <summary>
    ///     Обновить паспортные данные
    /// </summary>
    /// <param name="passportRf">Паспортные данные</param>
    public void Change(IPassportRf passportRf)
    {
        DocType = passportRf.DocType;
        Number = passportRf.Number;
        Series = passportRf.Series;
        IssueDate = passportRf.IssueDate;
        IssuedBy = passportRf.IssuedBy;
        DepartmentCode = passportRf.DepartmentCode;
    }
}