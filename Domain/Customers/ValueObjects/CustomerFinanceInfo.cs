﻿using Some.Abstractions.Customers;
using Newtonsoft.Json;

namespace Some.Domain.Customers.ValueObjects;

/// <inheritdoc cref="ICustomerFinanceInfo" />
public sealed class CustomerFinanceInfo : ICustomerFinanceInfo
{
    [Newtonsoft.Json.JsonConstructor]
    public CustomerFinanceInfo(
        [JsonProperty] int? annualIncome,
        [JsonProperty] int? differenceAssets,
        [JsonProperty] int? dependents)
    {
        AnnualIncome = annualIncome;
        DifferenceAssets = differenceAssets;
        Dependents = dependents;
    }

    private CustomerFinanceInfo()
    {
    }

    /// <inheritdoc />
    public int? AnnualIncome { get; }

    /// <inheritdoc />
    public int? Dependents { get; }

    /// <inheritdoc />
    public int? DifferenceAssets { get; }
}