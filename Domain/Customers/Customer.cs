using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using Ct.Module.Domain.Entities;
using Some.Abstractions.Customers;
using Some.Abstractions.Enums;
using Some.Abstractions.Extensions;
using Some.Domain.Addresses;
using Some.Domain.Customers.ValueObjects;
using Some.Domain.Occupations;
using Some.Domain.SportActivities;

// ReSharper disable UnusedAutoPropertyAccessor.Local

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local

namespace Some.Domain.Customers;

/// <summary>
///     Данные клиента
/// </summary>
[Table("customer")]
public sealed class Customer : TrackableEntity<Guid>, ICustomer<Address, PassportRf, Occupation, CustomerFinanceInfo, SportActivity>
{
    /// <summary>
    ///     Создание клиента
    /// </summary>
    public Customer(Guid id, string externalId, string lastName, string firstName, string? middleName,
        DateTime birthDate, string birthPlace, string? inn, string? snils,
        string citizenship, string residenceCountry, Gender gender, MaritalState maritalState, string cellPhone,
        string? homePhone, string? workPhone, string? email, Address registrationAddress, Address? currentAddress,
        bool isForeignPublicPerson, int? childCount) : base(id)
    {
        RegistrationAddress = registrationAddress;
        CurrentAddress = currentAddress;
        RegistrationAddressId = registrationAddress.Id;
        CurrentAddressId = currentAddress?.Id;
        SportActivity = SportActivity.Default;
        Change(
            externalId,
            lastName,
            firstName,
            middleName,
            birthDate,
            birthPlace,
            inn,
            snils,
            citizenship,
            residenceCountry,
            gender,
            maritalState,
            cellPhone,
            homePhone,
            workPhone,
            email,
            isForeignPublicPerson,
            childCount
        );
    }

    // ReSharper disable once UnusedMember.Local
    private Customer()
    {
    }

    /// <summary>
    ///     ФИО
    /// </summary>
    [NotMapped]
    [JsonIgnore]
    public string FullName => this.GetFullName();

    /// <inheritdoc />
    [Column("child_count")]
    public int? ChildCount { get; private set; }

    /// <inheritdoc />
    public Address? CurrentAddress { get; private set; }
    
    /// <inheritdoc />
    public SportActivity SportActivity { get; private set; } = null!;

    /// <inheritdoc />
    public PassportRf Document { get; private set; } = null!;

    /// <inheritdoc />
    [Column("finance_info")]
    public CustomerFinanceInfo? FinanceInfo { get; private set; }

    /// <summary>
    ///     Информация о занятости
    /// </summary>
    public Occupation Occupation { get; private set; }

    /// <inheritdoc />
    public Address RegistrationAddress { get; private set; } = null!;

    /// <inheritdoc />
    [Column("first_name")]
    public string FirstName { get; private set; } = null!;

    /// <inheritdoc />
    [Column("last_name")]
    public string LastName { get; private set; } = null!;

    /// <inheritdoc />
    [Column("middle_name")]
    public string? MiddleName { get; private set; }

    /// <inheritdoc />
    [Column("birth_date", TypeName = "date")]
    public DateTime BirthDate { get; private set; }

    /// <inheritdoc />
    [Column("birth_place")]
    public string BirthPlace { get; private set; } = null!;

    /// <inheritdoc />
    [Column("cell_phone")]
    public string CellPhone { get; private set; } = null!;

    /// <inheritdoc />
    [Column("citizenship")]
    public string Citizenship { get; private set; } = null!;

    /// <summary>
    ///     DS Id
    /// </summary>
    [Column("common_store_id")]
    public Guid? CommonStoreId { get; set; }

    /// <inheritdoc />
    [Column("email")]
    public string? Email { get; internal set; }

    /// <inheritdoc />
    [Column("external_id")]
    public string? ExternalId { get; private set; }

    /// <inheritdoc />
    [Column("gender")]
    public Gender Gender { get; private set; }

    /// <inheritdoc />
    [Column("home_phone")]
    public string? HomePhone { get; private set; }

    /// <inheritdoc />
    [Column("inn")]
    public string? Inn { get; private set; }

    /// <inheritdoc />
    [Column("is_foreign_public_person")]
    public bool IsForeignPublicPerson { get; private set; }

    /// <inheritdoc />
    [Column("marital_state")]
    public MaritalState MaritalState { get; private set; }

    /// <inheritdoc />
    [Column("residence_country")]
    public string ResidenceCountry { get; private set; } = null!;

    /// <inheritdoc />
    [Column("snils")]
    public string? Snils { get; private set; }

    /// <inheritdoc />
    [Column("work_phone")]
    public string? WorkPhone { get; private set; }

    /// <summary>
    ///     Ид фактического адреса
    /// </summary>
    [Column("current_address_id")]
    private Guid? CurrentAddressId { get; set; }

    /// <summary>
    ///     Ид адреса регистрации
    /// </summary>
    [Column("registration_address_id")]
    private Guid RegistrationAddressId { get; set; }

    /// <summary>
    ///     Изменить данные
    /// </summary>
    public void Change(string externalId, string lastName, string firstName, string? middleName,
        DateTime birthDate, string birthPlace, string? inn, string? snils,
        string citizenship, string residenceCountry, Gender gender, MaritalState maritalState, string cellPhone,
        string? homePhone, string? workPhone, string? email, bool isForeignPublicPerson, int? childCount)
    {
        ExternalId = externalId;
        LastName = lastName;
        FirstName = firstName;
        Citizenship = citizenship;
        ResidenceCountry = residenceCountry;
        CellPhone = cellPhone;
        MiddleName = middleName;
        BirthDate = birthDate;
        BirthPlace = birthPlace;
        Inn = inn;
        Snils = snils;
        Gender = gender;
        MaritalState = maritalState;
        HomePhone = homePhone;
        WorkPhone = workPhone;
        Email = email;
        IsForeignPublicPerson = isForeignPublicPerson;
        ChildCount = childCount;
    }

    /// <summary>
    ///     Установка DS Id
    /// </summary>
    /// <param name="commonStoreId">Id</param>
    public void ChangeCommonStoreId(Guid? commonStoreId)
    {
        if (commonStoreId != null && commonStoreId != Guid.Empty)
        {
            CommonStoreId = commonStoreId;
        }
    }

    /// <summary>
    ///     Изменить текущий адрес
    /// </summary>
    public void ChangeCurrentAddress(Address? currentAddress)
    {
        CurrentAddress = currentAddress;
        CurrentAddressId = currentAddress?.Id;
    }

    /// <summary>
    ///     Обновить документ
    /// </summary>
    public void ChangeDocument(PassportRf document) => Document = document;

    /// <summary>
    ///     Изменение информации о доходах и иждивенцах
    /// </summary>
    /// <param name="financeInfo">Информация о доходах и иждивенцах </param>
    public void ChangeFinanceInfo(ICustomerFinanceInfo? financeInfo) => FinanceInfo = financeInfo == null 
        || (!financeInfo.DifferenceAssets.HasValue && !financeInfo.Dependents.HasValue && !financeInfo.AnnualIncome.HasValue)
            ? null
            : new CustomerFinanceInfo(
                differenceAssets: financeInfo.DifferenceAssets,
                annualIncome: financeInfo.AnnualIncome,
                dependents: financeInfo.Dependents);

    /// <summary>
    ///     Изменить место работы
    /// </summary>
    public void ChangeOccupation(Occupation occupation) => Occupation = occupation;
}