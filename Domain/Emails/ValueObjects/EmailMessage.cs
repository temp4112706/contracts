namespace Some.Domain.Emails.ValueObjects;

/// <summary>
///     Email
/// </summary>
/// <param name="Subject">Тема</param>
/// <param name="Senders">Отправители</param>
/// <param name="Recipients">Получатели</param>
/// <param name="Texts">Тексты</param>
/// <param name="Files">Файлы</param>
public record EmailMessage(
    string Subject,
    string[] Senders,
    string[] Recipients,
    EmailMessageTextEntity[] Texts,
    EmailMessageFileEntity[] Files
);