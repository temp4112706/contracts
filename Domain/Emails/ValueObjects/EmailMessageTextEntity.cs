namespace Some.Domain.Emails.ValueObjects;

/// <summary>
///     Тест Email
/// </summary>
/// <param name="Text">Текст</param>
/// <param name="IsHtml">Это Html?</param>
/// <param name="Order">Порядок</param>
public record EmailMessageTextEntity(string Text, bool IsHtml, int Order);