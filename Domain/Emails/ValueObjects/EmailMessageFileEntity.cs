namespace Some.Domain.Emails.ValueObjects;

/// <summary>
///     Файл Email
/// </summary>
/// <param name="Name">Название</param>
/// <param name="FileId">Ид файла</param>
/// <param name="ContentType">тип контента</param>
/// <param name="Order">порядок</param>
public record EmailMessageFileEntity(string Name, string FileId, string ContentType, int Order);