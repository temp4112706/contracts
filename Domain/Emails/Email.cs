using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using Ct.Module.Domain.Entities;
using Some.Domain.Emails.ValueObjects;

namespace Some.Domain.Emails;

/// <summary>
///     Email
/// </summary>
[Table(Table)]
[SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local")]
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
public class Email : IEntity<Guid>, ICreationTrackable
{
    /// <summary>
    ///     Название таблицы
    /// </summary>
    public const string Table = "email";

    /// <summary>
    ///     Создание Email
    /// </summary>
    public Email(Guid id, EmailMessage message)
    {
        Id = id;
        Message = message;
        Processed = false;
    }

    // ReSharper disable once UnusedMember.Local
    private Email()
    {
    }

    /// <summary>
    ///     Будет удалено
    /// </summary>
    [Column("expires_at")]
    public DateTime? ExpiresAt { get; private set; }

    /// <summary>
    ///     Сообщение
    /// </summary>
    [Column("message")]
    public EmailMessage Message { get; private set; } = null!;

    /// <summary>
    ///     Обработано?
    /// </summary>
    [Column("processed")]
    public bool Processed { get; private set; }

    /// <summary>
    ///     Отправлено в
    /// </summary>
    [Column("sent_at")]
    public DateTime? SentAt { get; private set; }

    /// <summary>
    ///     Записано
    /// </summary>
    [Column("created_at")]
    // ReSharper disable once UnusedAutoPropertyAccessor.Local
    public DateTime CreatedAt { get; private set; }

    /// <summary>
    ///     ИД email
    /// </summary>
    [Column("id")]
    public Guid Id { get; private set; }

    /// <summary>
    ///     Отправлено
    /// </summary>
    public void Sent(DateTime sentAt)
    {
        Processed = true;
        SentAt = sentAt;
#if DEBUG
        ExpiresAt = sentAt.AddMinutes(7);
#else
            ExpiresAt = sentAt.AddDays(1);
#endif
    }
}