using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using Some.Abstractions.Monies;
using Newtonsoft.Json;

namespace Some.Domain.Monies;

/// <summary>
///     Сумма и валюта
/// </summary>
[ComplexType]
[SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local")]
[SuppressMessage("ReSharper", "NonReadonlyMemberInGetHashCode")]
public class Money : IMoney
{
    private Money()
    {
    }

    [Newtonsoft.Json.JsonConstructor]
    private Money([JsonProperty] decimal amount, [JsonProperty] string currency)
    {
        Amount = amount;
        Currency = currency;
    }

    /// <summary>
    ///     Сумма
    /// </summary>
    public decimal Amount { get; private set; }

    /// <summary>
    ///     Код валюты ISO (для рубля РФ - RUB)
    /// </summary>
    public string Currency { get; private set; } = null!;

    /// <summary>
    ///     Опредление эквивалентности
    /// </summary>
    public override bool Equals(object? obj)
    {
        if (ReferenceEquals(null!, obj))
        {
            return false;
        }

        if (ReferenceEquals(this, obj))
        {
            return true;
        }

        if (obj.GetType() != GetType())
        {
            return false;
        }

        return Equals((Money)obj);
    }

    /// <inheritdoc />
    public override int GetHashCode()
    {
        unchecked
        {
            return ((!string.IsNullOrEmpty(Currency) ? Currency.GetHashCode() : 0) * 397) ^ Amount.GetHashCode();
        }
    }

    /// <inheritdoc />
    public override string ToString() => Amount.ToString("C2");

    /// <summary>
    ///     Умножение суммы
    /// </summary>
    /// <param name="amount">На что умножаем</param>
    /// <returns>Результат</returns>
    public Money Multiply(decimal amount) => Create(Amount * amount, Currency);

    /// <summary>
    ///     Увеличение суммы
    /// </summary>
    /// <param name="amount">На что увеличиваем</param>
    /// <returns>Результат</returns>
    public Money Plus(decimal amount) => Create(Amount + amount, Currency);

    /// <summary>
    ///     Увеличение суммы
    /// </summary>
    /// <param name="amount">На что увеличиваем</param>
    /// <returns>Результат</returns>
    public Money Plus(Money amount)
    {
        if (amount.Currency != Currency)
        {
            throw new InvalidOperationException("Валюта не совпадает.");
        }

        return Create(Amount + amount.Amount, Currency);
    }

    /// <summary>
    ///     Опредление эквивалентности
    /// </summary>
    protected bool Equals(Money other) => string.Equals(Currency, other.Currency) && Amount == other.Amount;

    /// <summary>
    ///     Создание суммы с валютой
    /// </summary>
    public static Money Create(decimal amount, string currency)
    {
        amount = decimal.Round(amount, 2, MidpointRounding.AwayFromZero);
        return new Money(amount, MoneyCurrencies.Normalize(currency));
    }

    /// <summary>
    ///     Создание суммы с валютой
    /// </summary>
    public static Money Create<TMoney>(TMoney money)
        where TMoney : class, IMoney =>
        Create(money.Amount, money.Currency);

    /// <summary>
    ///     Оператор +
    /// </summary>
    public static Money operator +(Money left, Money right)
    {
        CheckCurrency(left, right);
        return Create(left.Amount + right.Amount, left.Currency);
    }

    /// <summary>
    ///     Оператор ==
    /// </summary>
    public static bool operator ==(Money? left, Money? right) => Equals(left, right);

    /// <summary>
    ///     Оператор больше
    /// </summary>
    public static bool operator >(Money left, Money right)
    {
        CheckCurrency(left, right);
        return left.Amount > right.Amount;
    }

    /// <summary>
    ///     Оператор !=
    /// </summary>
    public static bool operator !=(Money? left, Money? right) => !Equals(left, right);

    /// <summary>
    ///     Оператор меньше
    /// </summary>
    public static bool operator <(Money left, Money right)
    {
        CheckCurrency(left, right);
        return left.Amount < right.Amount;
    }

    /// <summary>
    ///     Сумма в рублях
    /// </summary>
    /// <param name="value">Кол-во денег</param>
    public static Money Rub(decimal value) => Create(value, MoneyCurrencies.Rub);

    /// <summary>
    ///     0 рублей
    /// </summary>
    /// <returns></returns>
    public static Money ZeroRub() => Rub(0);

    private static void CheckCurrency(IMoney? left, IMoney? right)
    {
        if (left?.Currency != right?.Currency)
        {
            throw new InvalidOperationException("The currency of money must be the same");
        }
    }
}