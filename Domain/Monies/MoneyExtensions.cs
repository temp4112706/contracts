﻿using Some.Abstractions.Monies;

namespace Some.Domain.Monies;

/// <summary>
///     Расширения для <see cref="Money" />
/// </summary>
public static class MoneyExtensions
{
    /// <summary>
    ///     Создать новый объект Суммы и валюты. Обязательно должен присутствовать конструктор (amount, currency)
    /// </summary>
    public static Money? Cast(this IMoney? money) => money == null ? null : Money.Create(money);

    /// <summary>
    ///     Создать новый объект Суммы и валюты в рублях.
    /// </summary>
    public static Money MoneyRub(this decimal amount) => Money.Rub(amount);

    /// <summary>
    ///     Создать новый объект Суммы и валюты в рублях.
    /// </summary>
    public static Money MoneyRub(this decimal amount, string currency) => Money.Create(amount, currency);
}