﻿using System.ComponentModel.DataAnnotations.Schema;
using Ct.Module.Domain.Entities;
using Some.Abstractions.MortgageInsuranceSettings;
using Some.Abstractions.MortgageUnderwritingCriteria;
using Some.Domain.Addresses;
using Some.Domain.Beneficiaries;
using Some.Domain.Insurers;
using Some.Domain.Insurers.ValueObjects;
using Some.Domain.MortgageUnderwritingRates;

// ReSharper disable ReplaceAutoPropertyWithComputedProperty
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local

namespace Some.Domain.MortgageInsuranceSettings;

/// <summary>
///     Настройки
/// </summary>
[Table("mortgage_insurance_settings")]
public sealed class MortgageInsuranceSettings : TrackableEntity<Guid>,
    IMortgageInsuranceSettings<Insurer, Beneficiary, Address, Address, BankAccount,
        Kid, Signer, SignerProxy, MortgageUnderwritingRatesData,
        MortgageUnderwritingLifeRate, MortgageUnderwritingPropertyRates,
        MortgageUnderwritingPropertyRatesByMaterial, MortgageUnderwritingPropertyRateByMaterial,
        InsuranceAmountMultiplier, MortgageUnderwritingTitleRates,
        MortgageUnderwritingTitleByPropertyTypeRates, MortgageUnderwritingTitleByAmountOfDeals,
        MortgageUnderwritingRatesRange>
{
    /// <inheritdoc />
    public MortgageInsuranceSettings(Guid id, Insurer? insurer, Beneficiary? beneficiary,
        MortgageUnderwritingRatesData? rates, MortgageUnderwritingCriteria? criteria) : base(id)
    {
        ChangeIdentifiers(insurer, beneficiary);
        ChangeRates(rates);
        ChangeCriteria(criteria);
    }

    private MortgageInsuranceSettings(Guid id, Insurer? insurer, Beneficiary? beneficiary) : base(id)
    {
        ChangeIdentifiers(insurer, beneficiary);
    }

    // ReSharper disable once UnusedMember.Local
    private MortgageInsuranceSettings()
    {
    }

    /// <summary>
    ///     ID выгодоприобретателя
    /// </summary>
    [Column("beneficiary_id")]
    public string? BeneficiaryId { get; private set; }

    /// <summary>
    ///     ID СК
    /// </summary>
    [Column("insurer_id")]
    public string? InsurerId { get; private set; }

    /// <summary>
    ///     Выгодоприобретатель
    /// </summary>
    public Beneficiary? Beneficiary { get; private set; }

    /// <summary>
    ///     СК
    /// </summary>
    public Insurer? Insurer { get; private set; }

    /// <summary>
    ///     Тарифы
    /// </summary>
    public MortgageUnderwritingRatesData? Rates { get; private set; }

    /// <inheritdoc />
    [Column("online_underwriting_criteria")]
    public MortgageUnderwritingCriteria? Criteria { get; private set; }

    /// <summary>
    ///     Установка/измененеи критериев онлайн андерайтинга
    /// </summary>
    /// <param name="criteria"></param>
    public void ChangeCriteria(MortgageUnderwritingCriteria? criteria) => Criteria = criteria;

    /// <summary>
    ///     Изменение /установка тарифов страхования жизни (<see cref="Rates" />)
    /// </summary>
    /// <param name="rates">Тарифы</param>
    public void ChangeRates(MortgageUnderwritingRatesData? rates) => Rates = rates;

    /// <summary>
    ///     Удаляет критериев онлайн андерайтинга (<see cref="Criteria" />)
    /// </summary>
    public void DiscardCriteria() => Criteria = null;


    /// <summary>
    ///     Удаляет тарифы страхования жизни (<see cref="Rates" />)
    /// </summary>
    public void DiscardRates() => Rates = null;

    private void ChangeIdentifiers(Insurer? insurer, Beneficiary? beneficiary)
    {
        Insurer = insurer;
        InsurerId = insurer?.Id;

        Beneficiary = beneficiary;
        BeneficiaryId = Beneficiary?.Id;
    }

    /// <summary>
    ///     Создание пустых настроек. NOTE : Только для миграции
    /// </summary>
    /// <returns></returns>
    public static MortgageInsuranceSettings Empty(Guid id) => new() { Id = id };

    /// <summary>
    /// </summary>
    /// <param name="id"></param>
    /// <param name="insurer"></param>
    /// <param name="beneficiary"></param>
    /// <returns></returns>
    public static MortgageInsuranceSettings Empty(Guid id, Insurer? insurer, Beneficiary? beneficiary)
    {
        var newRecord = new MortgageInsuranceSettings(id, insurer, beneficiary);
        return newRecord;
    }
}