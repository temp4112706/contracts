﻿using Some.Abstractions.Enums;
using Some.Abstractions.Mirs;
using Newtonsoft.Json;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local

namespace Some.Domain.Mirs.ValueObjects;

/// <summary>
///     Результат перехода статуса в заявке
/// </summary>
public class MirStatusChangesResult : IMirStatusChangesResult
{
    /// <summary>
    ///     Создание объекта
    /// </summary>
    [Newtonsoft.Json.JsonConstructor]
    public MirStatusChangesResult(
        [JsonProperty] MirStatus nextStatus,
        [JsonProperty] MirStatus? previousStatus,
        [JsonProperty] DateTime changedAt)
    {
        NextStatus = nextStatus;
        PreviousStatus = previousStatus;
        ChangedAt = changedAt;
    }

    private MirStatusChangesResult()
    {
    }

    /// <summary>
    ///     Дата и время перехода
    /// </summary>
    public DateTime ChangedAt { get; private set; }

    /// <inheritdoc />
    public MirStatus NextStatus { get; private set; }

    /// <inheritdoc />
    public MirStatus? PreviousStatus { get; private set; }
}