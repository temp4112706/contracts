using Some.Abstractions.Enums;

namespace Some.Domain.Mirs;

/// <summary>
///     Хелпер для <see cref="MirEventName" />
/// </summary>
public static class MirEventNameResolver
{
    /// <summary>
    ///     Название эвента на основе андеррайтинга жизни
    /// </summary>
    /// <param name="index">Индекс застрахованного лица</param>
    /// <param name="state">Стейт андеррайтинга</param>
    /// <returns>Название эвента</returns>
    public static MirEventName LifeUnderwriting(InsuredPersonIndex index, MirUnderwritingState state)
    {
        switch (state)
        {
            case MirUnderwritingState.InProcess:
                return index switch
                {
                    InsuredPersonIndex.First => MirEventName.LifeUnderwritingCreated1,
                    InsuredPersonIndex.Second => MirEventName.LifeUnderwritingCreated2,
                    InsuredPersonIndex.Third => MirEventName.LifeUnderwritingCreated3,
                    InsuredPersonIndex.Fourth => MirEventName.LifeUnderwritingCreated4,
                    _ => throw new ArgumentOutOfRangeException(nameof(index), index, null)
                };
            case MirUnderwritingState.Processed:
                return index switch
                {
                    InsuredPersonIndex.First => MirEventName.LifeUnderwritingProcessed1,
                    InsuredPersonIndex.Second => MirEventName.LifeUnderwritingProcessed2,
                    InsuredPersonIndex.Third => MirEventName.LifeUnderwritingProcessed3,
                    InsuredPersonIndex.Fourth => MirEventName.LifeUnderwritingProcessed4,
                    _ => throw new ArgumentOutOfRangeException(nameof(index), index, null)
                };
            case MirUnderwritingState.Rejected:
                return index switch
                {
                    InsuredPersonIndex.First => MirEventName.LifeUnderwritingRejected1,
                    InsuredPersonIndex.Second => MirEventName.LifeUnderwritingRejected2,
                    InsuredPersonIndex.Third => MirEventName.LifeUnderwritingRejected3,
                    InsuredPersonIndex.Fourth => MirEventName.LifeUnderwritingRejected4,
                    _ => throw new ArgumentOutOfRangeException(nameof(index), index, null)
                };
            default:
                throw new ArgumentOutOfRangeException(nameof(state), state, null);
        }
    }

    /// <summary>
    ///     Название эвента на основе андеррайтинга имущества
    /// </summary>
    /// <param name="state">Стейт андеррайтинга</param>
    /// <returns>Название эвента</returns>
    public static MirEventName PropertyUnderwriting(MirUnderwritingState state) =>
        state switch
        {
            MirUnderwritingState.InProcess => MirEventName.PropertyUnderwritingCreated,
            MirUnderwritingState.Processed => MirEventName.PropertyUnderwritingProcessed,
            MirUnderwritingState.Rejected => MirEventName.PropertyUnderwritingRejected,
            _ => throw new ArgumentOutOfRangeException(nameof(state), state, null)
        };

    /// <summary>
    ///     Название эвента на основе андеррайтинга титула
    /// </summary>
    /// <param name="state">Стейт андеррайтинга</param>
    /// <returns>Название эвента</returns>
    public static MirEventName TitleUnderwriting(MirUnderwritingState state) =>
        state switch
        {
            MirUnderwritingState.InProcess => MirEventName.TitleUnderwritingCreated,
            MirUnderwritingState.Processed => MirEventName.TitleUnderwritingProcessed,
            MirUnderwritingState.Rejected => MirEventName.TitleUnderwritingRejected,
            _ => throw new ArgumentOutOfRangeException(nameof(state), state, null)
        };
}