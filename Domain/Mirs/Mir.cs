using System.ComponentModel.DataAnnotations.Schema;
using Ct.Module.Domain.Entities;
using Some.Abstractions.Common;
using Some.Abstractions.Enums;
using Some.Abstractions.Extensions;
using Some.Abstractions.Mirs;
using Some.Abstractions.Policies;
using Some.Domain.Addresses;
using Some.Domain.Companies;
using Some.Domain.Customers;
using Some.Domain.Employees;
using Some.Domain.ExtraProperties;
using Some.Domain.InsuredPersons;
using Some.Domain.Insurers;
using Some.Domain.LoanAgreements;
using Some.Domain.MirEvents;
using Some.Domain.Mirs.ValueObjects;
using Some.Domain.Monies;
using Some.Domain.Policies.Mortgage;
using Some.Domain.Properties;
using Some.Domain.Underwriting;

// ReSharper disable UnusedMethodReturnValue.Global
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local
// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable UnusedAutoPropertyAccessor.Local

namespace Some.Domain.Mirs;

/// <summary>
///     Заявка
/// </summary>
[Table("mir")]
public sealed class Mir : TrackableEntity<Guid>,
    IMir<Money, Customer, Insurer, LoanAgreement, Property, Property, InsuredPerson,
        MirLifeUnderwriting, MirPropertyUnderwriting, MirTitleUnderwriting,
        MirLifeInsuranceRequest, MirPropertyInsuranceRequest, MirTitleInsuranceRequest,
        Employee, Company, Address>, IHasExtraProperties, IHasExternalId
{
    /// <summary>
    ///     Создание заявки
    /// </summary>
    public Mir(Guid id, Company? company, Employee? companyEmployee, MirType type, string number,
        MirStatus status, DateTime statusUpdatedAt, LoanAgreement loanAgreement, Customer insurant, Insurer insurer,
        Employee? insurerEmployee, bool discount, bool oneYear, InsuranceRisk[] risks,
        IPolicyDates policyDates, string? mortgageRegion, DateTime? createdAt) : base(id)
    {
        Type = type;
        Status = status;
        StatusUpdatedAt = statusUpdatedAt;
        Number = number;
        LoanAgreement = loanAgreement;
        Insurant = insurant;
        Insurer = insurer;
        LoanAgreementId = loanAgreement.Id;
        InsurantId = insurant.Id;
        InsurerId = insurer.Id;
        LifeInsuranceRequests = new List<MirLifeInsuranceRequest>();
        PropertyInsuranceRequests = new List<MirPropertyInsuranceRequest>();
        TitleInsuranceRequests = new List<MirTitleInsuranceRequest>();
        Discount = discount;
        OneYear = oneYear;
        Risks = risks;
        MortgageRegion = mortgageRegion;
        Company = company;
        CompanyId = company?.Id;
        Events = new List<MirEvent>();

        InsurerEmployee = insurerEmployee;
        InsurerEmployeeId = insurerEmployee?.Id;

        CompanyEmployee = companyEmployee;
        CompanyEmployeeId = companyEmployee?.Id;

        ChangePolicyDates(policyDates);

        if (createdAt.HasValue)
        {
            CreatedAt = createdAt.Value;
        }
    }

    /// <inheritdoc />
    // ReSharper disable once UnusedMember.Local
    private Mir()
    {
    }

    /// <summary>
    ///     Эвенты
    /// </summary>
    public ICollection<MirEvent> Events { get; private set; } = null!;

    /// <summary>
    ///     Есть ли запросы тарифов страхования?
    /// </summary>
    [NotMapped]
    public bool HasInsuranceRequests => HasLifeInsuranceRequests || HasPropertyInsuranceRequests ||
                                        HasTitleInsuranceRequests;

    /// <summary>
    ///     Есть ли запросы тарифов страхования жизни?
    /// </summary>
    [NotMapped]
    public bool HasLifeInsuranceRequests => LifeInsuranceRequests.Count > 0;

    /// <summary>
    ///     Есть ли запросы тарифов страхования имущества?
    /// </summary>
    [NotMapped]
    public bool HasPropertyInsuranceRequests => PropertyInsuranceRequests.NotNullOrEmpty();

    /// <summary>
    ///     Есть ли запросы тарифов страхования титула?
    /// </summary>
    [NotMapped]
    public bool HasTitleInsuranceRequests => TitleInsuranceRequests.NotNullOrEmpty();

    /// <summary>
    ///     Типовая заявка?
    /// </summary>
    [NotMapped]
    public bool IsOnline => Type == MirType.Online;

    /// <summary>
    ///     Связанные ДС
    /// </summary>
    public ICollection<MortgagePolicy> Policies { get; private set; } = null!;

    /// <inheritdoc />
    [Column("external_id")]
    public string? ExternalId { get; private set; }

    /// <inheritdoc />
    [Column("extra_properties")]
    public ExtraPropertyDictionary? ExtraProperties { get; private set; } = new();

    /// <inheritdoc />
    public Company? Company { get; private set; }

    /// <inheritdoc />
    public Employee? CompanyEmployee { get; private set; }

    /// <inheritdoc />
    [Column("discount")]
    public bool Discount { get; private set; }

    /// <inheritdoc />
    public Customer Insurant { get; private set; } = null!;

    /// <inheritdoc />
    public Insurer Insurer { get; private set; } = null!;

    /// <inheritdoc />
    public Employee? InsurerEmployee { get; private set; }

    /// <inheritdoc />
    public ICollection<MirLifeInsuranceRequest> LifeInsuranceRequests { get; private set; } = null!;

    /// <inheritdoc />
    public LoanAgreement LoanAgreement { get; private set; } = null!;

    /// <inheritdoc />
    [Column("mortgage_region")]
    public string? MortgageRegion { get; private set; }

    /// <inheritdoc />
    [Column("one_year")]
    public bool OneYear { get; private set; }

    /// <inheritdoc />
    [Column("policy_agreement_datetime")]
    public DateTime PolicyAgreementDateTime { get; private set; }

    /// <inheritdoc />
    [Column("policy_commencement_datetime")]
    public DateTime PolicyCommencementDateTime { get; private set; }

    /// <inheritdoc />
    [Column("policy_expiry_datetime")]
    public DateTime PolicyExpiryDateTime { get; private set; }

    /// <inheritdoc />
    public ICollection<MirPropertyInsuranceRequest> PropertyInsuranceRequests { get; private set; } = null!;

    /// <inheritdoc />
    [Column("risks")]
    public InsuranceRisk[] Risks { get; private set; } = null!;

    /// <inheritdoc />
    public ICollection<MirTitleInsuranceRequest> TitleInsuranceRequests { get; private set; } = null!;

    /// <inheritdoc />
    [Column("number")]
    public string Number { get; private set; } = null!;

    /// <inheritdoc />
    [Column("status")]
    public MirStatus Status { get; private set; }

    /// <inheritdoc />
    [Column("status_updated_at")]
    public DateTime StatusUpdatedAt { get; private set; }

    /// <inheritdoc />
    [Column("type")]
    public MirType Type { get; private set; }

    /// <summary>
    ///     ИД <see cref="CompanyEmployee" />
    /// </summary>
    [Column("company_employee_id")]
    private string? CompanyEmployeeId { get; set; }

    /// <summary>
    ///     ИД компании
    /// </summary>
    private string? CompanyId { get; set; }

    /// <summary>
    ///     Ид страхователя
    /// </summary>
    [Column("insurant_id")]
    private Guid InsurantId { get; set; }

    /// <summary>
    ///     ИД <see cref="InsurerEmployee" />
    /// </summary>
    [Column("insurer_employee_id")]
    private string? InsurerEmployeeId { get; set; }

    /// <summary>
    ///     Ид страховой компании
    /// </summary>
    [Column("insurer_id")]
    private string InsurerId { get; set; } = null!;

    /// <summary>
    ///     Ид кредитного договора
    /// </summary>
    [Column("loan_agreement_id")]
    private Guid LoanAgreementId { get; set; }

    /// <summary>
    ///     Изменить внешний ИД
    /// </summary>
    public void ChangeExternalId(string? externalId) => ExternalId = externalId;

    /// <summary>
    ///     Изменить менеджера
    /// </summary>
    public void ChangeInsurerEmployee(Employee? insurerEmployee)
    {
        InsurerEmployee = insurerEmployee;
        InsurerEmployeeId = insurerEmployee?.Id;
    }

    /// <summary>
    ///     Изменить регион предмета ипотеки
    /// </summary>
    public bool ChangeMortgageRegion(string? mortgageRegion)
    {
        if (MortgageRegion == mortgageRegion)
        {
            return false;
        }

        MortgageRegion = mortgageRegion;
        return true;
    }

    /// <summary>
    ///     Изменить страхование на один год
    /// </summary>
    /// <param name="oneYear"></param>
    public void ChangeOneYear(bool oneYear)
    {
        if (OneYear != oneYear)
        {
            OneYear = oneYear;
        }
    }

    /// <summary>
    ///     Изменить даты ДС
    /// </summary>
    public void ChangePolicyDates(IPolicyDates policyDates)
    {
        PolicyAgreementDateTime = policyDates.AgreementDateTime;
        PolicyCommencementDateTime = policyDates.CommencementDateTime;
        PolicyExpiryDateTime = policyDates.ExpiryDateTime;
    }

    /// <summary>
    ///     Изменить риски
    /// </summary>
    public void ChangeRisks(InsuranceRisk[] risks) => Risks = risks;

    /// <summary>
    ///     Изменение статуса
    /// </summary>
    public MirStatusChangesResult? ChangeStatus(MirStatus nextStatus, DateTime? changedAt = null)
    {
        if (Status == nextStatus)
        {
            return null;
        }

        var previousStatus = Status;
        Status = nextStatus;
        StatusUpdatedAt = changedAt ?? DateTime.UtcNow;
        return new MirStatusChangesResult(Status, previousStatus, StatusUpdatedAt);
    }

    /// <summary>
    ///     Изменить тип заявки
    /// </summary>
    public void ChangeType(MirType type) => Type = type;
}