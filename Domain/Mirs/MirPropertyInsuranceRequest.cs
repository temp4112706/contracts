using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using Ct.Module.Domain.Entities;
using Some.Abstractions.Mirs;
using Some.Domain.Addresses;
using Some.Domain.Monies;
using Some.Domain.Properties;
using Some.Domain.Underwriting;

// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Domain.Mirs;

/// <summary>
///     Запрос страхования имущества
/// </summary>
[Table("mir_property_insurance_request")]
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Local")]
[SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local")]
public sealed class MirPropertyInsuranceRequest : TrackableEntity<Guid>,
    IMirPropertyInsuranceRequest<Money, Property, MirPropertyUnderwriting, Address>
{
    /// <summary>
    ///     Создание запроса страхования имущества
    /// </summary>
    public MirPropertyInsuranceRequest(Guid id, IScalarMir mir, Property property, decimal? loanShare,
        MirPropertyUnderwriting? underwriting = null) : base(id)
    {
        MirId = mir.Id;
        Property = property;
        PropertyId = property.Id;
        LoanShare = loanShare ?? 1;
        Underwriting = underwriting;
    }

    // ReSharper disable once UnusedMember.Local
    private MirPropertyInsuranceRequest()
    {
    }

    /// <summary>
    ///     Ид заявки
    /// </summary>
    [Column("mir_id")]
    public Guid MirId { get; }

    /// <inheritdoc />
    [Column("loan_share")]
    public decimal? LoanShare { get; private set; }

    /// <summary>
    ///     Страхуемое имущество
    /// </summary>
    public Property? Property { get; private set; }

    /// <summary>
    ///     Андеррайтинг имущества
    /// </summary>
    public MirPropertyUnderwriting? Underwriting { get; private set; }

    /// <summary>
    ///     Ид страхуемого имущества
    ///     EF shadow
    /// </summary>
    // ReSharper disable once UnassignedGetOnlyAutoProperty
    [Column("property_id")]
    private Guid? PropertyId { get; }

    /// <summary>
    ///     Присвоить результаты андеррайтинга имущества
    /// </summary>
    /// <param name="underwriting">Результаты андеррайтинга имущества</param>
    /// <returns>Результат</returns>
    public void ChangeUnderwriting(MirPropertyUnderwriting? underwriting) => Underwriting = underwriting;

    /// <summary>
    ///     Сбросить андеррайтинг
    /// </summary>
    public void ResetUnderwriting() => Underwriting?.Reset();
}