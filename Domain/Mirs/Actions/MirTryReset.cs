namespace Some.Domain.Mirs.Actions;

/// <summary>
///     Данные для сброса андеррайтинга
/// </summary>
/// <param name="ResetUnderwriting">Сбросить андеррайтинг?</param>
public record MirTryReset(bool ResetUnderwriting);