﻿using Ct.Module.Domain.Json;
using Some.Domain.ExtraProperties.Extensions;

namespace Some.Domain.Mirs;

/// <summary>
///     Расширения для <see cref="Mir"/>
/// </summary>
public static class MirExtraPropertiesExtensions
{
    /// <summary>
    ///     Ключ коментария
    /// </summary>
    private const string UserComment = "requestDocumentsComment";

    /// <summary>
    ///     Удалить коментарий
    /// </summary>
    public static void RemoveRequestDocumentsComment(this Mir mir, IEntityJsonPropertyContext? jsonContext)
    {
        mir.RemoveProperty(UserComment);
        jsonContext?.TrackModify(mir, x => x.ExtraProperties);
    }


    /// <summary>
    ///     Установить коментария заявки
    /// </summary>
    public static void ChangeRequestDocumentsComment(this Mir mir, string comment, IEntityJsonPropertyContext? jsonContext)
    {
        mir.SetProperty(UserComment, comment);
        jsonContext?.TrackModify(mir, x => x.ExtraProperties);
    }

    /// <summary>
    ///     Получить коментарий
    /// </summary>
    public static string? TryGetRequestDocumentsComment(this Mir policy) =>
        policy.TryGetProperty<string>(UserComment);
}