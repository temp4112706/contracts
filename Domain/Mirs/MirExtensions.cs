using Ct.Module.Domain.Exceptions;
using Some.Abstractions.Enums;
using Some.Abstractions.Exceptions;
using Some.Abstractions.Underwriting;
using Some.Domain.Employees;
using Some.Domain.InsuredPersons;
using Some.Domain.Mirs.Actions;
using Some.Domain.Mirs.ValueObjects;
using Some.Domain.Properties;
using Some.Domain.Underwriting;

namespace Some.Domain.Mirs;

/// <summary>
///     Расширения для <see cref="Mir" />
/// </summary>
public static class MirExtensions
{
    /// <summary>
    ///     Созданы андеррайтинги для всех рисков
    /// </summary>
    public static bool HasAllUnderwriting(this Mir mir) =>
        mir.LifeInsuranceRequests.All(x => x.Underwriting != null) &&
        mir.PropertyInsuranceRequests.All(x => x.Underwriting != null) &&
        mir.TitleInsuranceRequests.All(x => x.Underwriting != null);

    /// <summary>
    ///     Переход с запроса андеррайтинга на андеррайтинг.
    ///     Допустим переход только с запрос андеррайтинга, запрос МЕДО.
    /// </summary>
    /// <param name="mir">Заявка</param>
    /// <param name="employee">Исполнитель из СК</param>
    /// <param name="result">Результат перехода статуса</param>
    /// <returns>Результат перехода</returns>
    public static bool MoveStatusToBeginUnderwriting(this Mir mir, Employee employee,
        out MirStatusChangesResult? result)
    {
        if (mir.Status != MirStatus.UnderwritingRequest)
        {
            result = null;
            return false;
        }

        mir.ChangeInsurerEmployee(employee);
        result = mir.ChangeStatus(MirStatus.Underwriting);
        return true;
    }

    /// <summary>
    ///     Переход с андеррайтинга на запрос МЕДО.
    ///     Допустим переход только с статуса андеррайтинг.
    /// </summary>
    /// <param name="mir">Заявка</param>
    /// <param name="result">Результат перехода статуса</param>
    /// <returns>Результат перехода</returns>
    public static bool MoveStatusToHealthSurvey(this Mir mir, out MirStatusChangesResult? result)
    {
        if (mir.Status != MirStatus.Underwriting)
        {
            result = null;
            return false;
        }

        result = mir.ChangeStatus(MirStatus.HealthSurvey);
        return true;
    }

    /// <summary>
    ///     Переход с андеррайтинг выполнен на ДС выпущен.
    ///     Допустим переход только с андерратйинг выполнен.
    /// </summary>
    /// <param name="mir">Заявка</param>
    /// <param name="result">Результат перехода статуса</param>
    /// <returns>Результат перехода</returns>
    public static bool MoveStatusToPolicyIssued(this Mir mir, out MirStatusChangesResult? result)
    {
        if (mir.Status != MirStatus.UnderwritingCompleted)
        {
            result = null;
            return false;
        }

        result = mir.ChangeStatus(MirStatus.PolicyIssued);
        return true;
    }

    /// <summary>
    ///     Переход на статус запрос документов
    /// </summary>
    /// <param name="mir">Заявка</param>
    /// <param name="result">Результат перехода статуса</param>
    /// <returns>Результат перехода</returns>
    public static bool MoveStatusToRequestDocuments(this Mir mir, out MirStatusChangesResult? result)
    {
        if (mir.Status != MirStatus.RequestDocuments &&
            mir.Status != MirStatus.UnderwritingRequest &&
            mir.Status != MirStatus.Underwriting &&
            mir.Status != MirStatus.Created)
        {
            result = null;
            return false;
        }

        result = mir.ChangeStatus(MirStatus.RequestDocuments);
        return true;
    }

    /// <summary>
    ///     Переход на статус андеррайтинг
    /// </summary>
    /// <param name="mir">Заявка</param>
    /// <param name="result">Результат перехода статуса</param>
    /// <returns>Результат перехода</returns>
    public static bool MoveStatusToUnderwriting(this Mir mir, out MirStatusChangesResult? result)
    {
        if (mir.Status != MirStatus.Underwriting &&
            mir.Status != MirStatus.RequestDocuments &&
            mir.Status != MirStatus.UnderwritingRequest)
        {
            result = null;
            return false;
        }

        result = mir.ChangeStatus(MirStatus.Underwriting);
        return true;
    }

    /// <summary>
    ///     Переход с запроса андеррайтинга, андеррайтинга в андеррайтинг завершен.
    ///     Допустим переход только с запрос андеррайтинга, андеррайтинг.
    /// </summary>
    /// <param name="mir">Заявка</param>
    /// <param name="result">Результат перехода статуса</param>
    /// <returns>Результат перехода</returns>
    public static bool MoveStatusToUnderwritingCompleted(this Mir mir, out MirStatusChangesResult? result)
    {
        // разрешены переводы со статусов начало андеррайтинга, ндеррайтинг, апрос МЕДО
        if (mir.Status != MirStatus.UnderwritingRequest &&
            mir.Status != MirStatus.Underwriting &&
            mir.Status != MirStatus.HealthSurvey)
        {
            result = null;
            return false;
        }

        // обязательно должны завершиться все андеррайтинги
        if (!mir.AllUnderwritingCompleted())
        {
            result = null;
            return false;
        }

        result = mir.ChangeStatus(MirStatus.UnderwritingCompleted);
        return true;
    }

    /// <summary>
    ///     Переход с созданной заявки на запрос андеррайтинга.
    ///     Допустим переход только с созданной заявки.
    /// </summary>
    /// <param name="mir">Заявка</param>
    /// <param name="result">Результат перехода статуса</param>
    /// <returns>Результат перехода</returns>
    public static bool MoveStatusToUnderwritingRequest(this Mir mir, out MirStatusChangesResult? result)
    {
        if (mir.Status != MirStatus.Created)
        {
            result = null;
            return false;
        }

        result = mir.ChangeStatus(MirStatus.UnderwritingRequest);
        return true;
    }

    /// <summary>
    ///     Сброс данных заявки
    ///     <example>
    ///         * - если флаг сброс андеррайтинга
    ///         # - если нет флага сброс андеррайтинга
    ///         Заявка создана
    ///         Запрос андеррайтинга
    ///         * - Оставляем статус
    ///         # - Оставляем статус
    ///         Андеррайтинг
    ///         Запрос МЕДО
    ///         * - Сбрасываем статус до «Запрос андеррайтинга» со сбросом тарифов
    ///         # - Оставляем статус
    ///         Андеррайтинг выполнен
    ///         Полис выпущен
    ///         * - Сбрасываем до статуса «Запрос андеррайтинга» со сбросом тарифов
    ///         # - Сбрасываем до статуса «Андеррайтинг выполнен» без сброса тарифов
    ///     </example>
    /// </summary>
    public static bool TryReset(this Mir mir, MirTryReset tryReset, out MirStatusChangesResult? result)
    {
        MirStatus? nextStatus = null;
        // ReSharper disable once SwitchStatementHandlesSomeKnownEnumValuesWithDefault
        switch (mir.Status)
        {
            case MirStatus.Created:
                break;
            case MirStatus.UnderwritingRequest:
            case MirStatus.Underwriting:
            case MirStatus.HealthSurvey:
            case MirStatus.RequestDocuments:
                if (tryReset.ResetUnderwriting)
                {
                    ResetUnderwriting();
                    nextStatus = MirStatus.UnderwritingRequest;
                    // делаем заявку типовой
                    mir.ChangeType(MirType.Online);
                }

                break;
            case MirStatus.UnderwritingCompleted:
            case MirStatus.PolicyIssued:
                if (tryReset.ResetUnderwriting)
                {
                    ResetUnderwriting();
                    nextStatus = MirStatus.UnderwritingRequest;
                }
                else
                {
                    nextStatus = MirStatus.UnderwritingCompleted;
                }

                break;
        }

        result = nextStatus == null ? null : mir.ChangeStatus(nextStatus.Value);
        return result != null;

        void ResetUnderwriting()
        {
            foreach (var lifeInsuranceRequest in mir.LifeInsuranceRequests)
            {
                lifeInsuranceRequest.ResetUnderwriting();
            }

            foreach (var propertyInsuranceRequest in mir.PropertyInsuranceRequests)
            {
                propertyInsuranceRequest.ResetUnderwriting();
            }

            foreach (var titleInsuranceRequest in mir.TitleInsuranceRequests)
            {
                titleInsuranceRequest.ResetUnderwriting();
            }
        }
    }

    /// <summary>
    ///     Изменить заявку на <see cref="MirType.Offline" />
    /// </summary>
    /// <returns>Результат операции</returns>
    public static bool TrySwitchToOffline(this Mir mir, bool force = false)
    {
        if (force)
        {
            return SwitchToOffline();
        }

        var allLifeUnderwriting = mir.LifeInsuranceRequests
            .Where(x => x.Underwriting != null)
            .Select(x => x.Underwriting as IScalarMirUnderwriting)
            .ToList();
        var allPropertyUnderwriting = mir.PropertyInsuranceRequests
            .Where(x => x.Underwriting != null)
            .Select(x => x.Underwriting as IScalarMirUnderwriting)
            .ToList();
        var allTitleUnderwriting = mir.TitleInsuranceRequests
            .Where(x => x.Underwriting != null)
            .Select(x => x.Underwriting as IScalarMirUnderwriting)
            .ToList();
        var allUnderwriting = allLifeUnderwriting.Concat(allPropertyUnderwriting).Concat(allTitleUnderwriting);

        // если среди андеррайтингов есть пользовательский со стейтом "В работе"
        var can = allUnderwriting.Any(x => x is { Online: false, State: MirUnderwritingState.InProcess });
        return can ? SwitchToOffline() : can;

        bool SwitchToOffline()
        {
            mir.ChangeType(MirType.Offline);

            // проставляем андеррайтинги по умолчанию если не проставлены
            foreach (var lifeInsuranceRequest in mir.LifeInsuranceRequests)
            {
                if (lifeInsuranceRequest.Underwriting != null)
                {
                    lifeInsuranceRequest.Underwriting.SwitchToOffline();
                }
                else
                {
                    lifeInsuranceRequest.ChangeUnderwriting(
                        MirLifeUnderwriting.CreateOffline(lifeInsuranceRequest));
                }
            }

            foreach (var propertyInsuranceRequest in mir.PropertyInsuranceRequests)
            {
                if (propertyInsuranceRequest.Underwriting != null)
                {
                    propertyInsuranceRequest.Underwriting.SwitchToOffline();
                }
                else
                {
                    propertyInsuranceRequest.ChangeUnderwriting(
                        MirPropertyUnderwriting.CreateOffline(propertyInsuranceRequest));
                }
            }

            foreach (var titleInsuranceRequest in mir.TitleInsuranceRequests)
            {
                if (titleInsuranceRequest.Underwriting != null)
                {
                    titleInsuranceRequest.Underwriting.SwitchToOffline();
                }
                else
                {
                    titleInsuranceRequest.ChangeUnderwriting(
                        MirTitleUnderwriting.CreateOffline(titleInsuranceRequest));
                }
            }

            return true;
        }
    }

    /// <summary>
    ///     Разрешаем редактировать если статус не договор выпущен
    /// </summary>
    public static void ValidateCanChangeOrThrow(this Mir mir)
    {
        if (mir.Status == MirStatus.PolicyIssued)
        {
            throw new BusinessException(BusinessErrorCode.MirCannotChange);
        }
    }

    #region Risks

    /// <summary>
    ///     Поддерживается риск
    /// </summary>
    public static bool RiskSupported(this Mir mir, InsuranceRisk risk) => mir.Risks.Contains(risk);

    /// <summary>
    ///     Обновить риски
    /// </summary>
    public static void UpdateRisks(this Mir mir)
    {
        var risks = new List<InsuranceRisk>();

        if (mir.LifeInsuranceRequests.Any())
        {
            risks.Add(InsuranceRisk.Life);
        }

        if (mir.PropertyInsuranceRequests.Any())
        {
            risks.Add(InsuranceRisk.Property);
        }

        if (mir.TitleInsuranceRequests.Any())
        {
            risks.Add(InsuranceRisk.Title);
        }

        mir.ChangeRisks(risks.ToArray());
    }

    #endregion

    #region Underwriting

    /// <summary>
    ///     Завершились все андеррайтинги?
    /// </summary>
    public static bool AllUnderwritingCompleted(this Mir mir)
    {
        var completed = true;

        if (mir.RiskSupported(InsuranceRisk.Life))
        {
            if (mir.LifeInsuranceRequests.Any(r => r.Underwriting is not { IsProcessed: true }))
            {
                completed = false;
            }
        }

        if (mir.RiskSupported(InsuranceRisk.Property))
        {
            if (mir.PropertyInsuranceRequests.Any(r => r.Underwriting is not { IsProcessed: true }))
            {
                completed = false;
            }
        }

        if (mir.RiskSupported(InsuranceRisk.Title))
        {
            if (mir.TitleInsuranceRequests.Any(r => r.Underwriting is not { IsProcessed: true }))
            {
                completed = false;
            }
        }

        return completed;
    }

    /// <summary>
    ///     Успешно обработались все андеррайтинги?
    /// </summary>
    public static bool AllUnderwritingProcessed(this Mir mir)
    {
        var completed = true;

        if (mir.RiskSupported(InsuranceRisk.Life))
        {
            if (mir.LifeInsuranceRequests.Any(r => r.Underwriting is not { State: MirUnderwritingState.Processed }))
            {
                completed = false;
            }
        }

        if (mir.RiskSupported(InsuranceRisk.Property))
        {
            if (mir.PropertyInsuranceRequests.Any(r => r.Underwriting is not { State: MirUnderwritingState.Processed }))
            {
                completed = false;
            }
        }

        if (mir.RiskSupported(InsuranceRisk.Title))
        {
            if (mir.TitleInsuranceRequests.Any(r => r.Underwriting is not { State: MirUnderwritingState.Processed }))
            {
                completed = false;
            }
        }

        return completed;
    }

    #endregion

    #region LifeInsurance

    /// <summary>
    ///     Найти запрос страхования жизни по ИД андеррайтинга
    /// </summary>
    public static MirLifeInsuranceRequest? FindLifeInsuranceRequest(this Mir mir, Guid underwritingId) =>
        mir.LifeInsuranceRequests.FirstOrDefault(x => x.Underwriting?.Id == underwritingId);

    /// <summary>
    ///     Найти андеррайтинг жизни по ИД андеррайтинга страхования жизни
    /// </summary>
    public static MirLifeUnderwriting? FindLifeUnderwriting(this Mir mir, Guid underwritingId)
    {
        var insuranceRequest = mir.FindLifeInsuranceRequest(underwritingId);
        return insuranceRequest?.Underwriting;
    }

    /// <summary>
    ///     Найти андеррайтинг жизни по Ид андеррайтинга
    /// </summary>
    public static MirLifeInsuranceRequest GetLifeInsuranceRequest(this Mir mir, Guid underwritingId)
    {
        var insuranceRequest = mir.FindLifeInsuranceRequest(underwritingId);
        if (insuranceRequest == null)
        {
            throw new EntityNotFoundException(typeof(MirLifeInsuranceRequest), underwritingId);
        }

        return insuranceRequest;
    }

    /// <summary>
    ///     Найти андеррайтинг жизни по ИД андеррайтинга страхования жизни
    /// </summary>
    public static MirLifeUnderwriting? GetLifeUnderwriting(this Mir mir, Guid underwritingId)
    {
        var insuranceRequest = mir.GetLifeInsuranceRequest(underwritingId);
        return insuranceRequest.Underwriting;
    }

    /// <summary>
    ///     Найти запрос страхования жизни по индексу
    /// </summary>
    public static MirLifeInsuranceRequest? FindLifeInsuranceRequest(this Mir mir, InsuredPersonIndex index) =>
        mir.LifeInsuranceRequests.FirstOrDefault(x => x.InsuredPerson.Index == index);

    /// <summary>
    ///     Найти застрахованное лицо по индексу
    /// </summary>
    public static InsuredPerson? FindInsuredPerson(this Mir mir, InsuredPersonIndex index)
    {
        var insuranceRequest = mir.FindLifeInsuranceRequest(index);
        return insuranceRequest?.InsuredPerson;
    }

    /// <summary>
    ///     Найти андеррайтинг жизни по индексу
    /// </summary>
    public static MirLifeInsuranceRequest GetLifeInsuranceRequest(this Mir mir, InsuredPersonIndex index)
    {
        var insuranceRequest = mir.FindLifeInsuranceRequest(index);
        if (insuranceRequest == null)
        {
            throw new EntityNotFoundException(typeof(MirLifeInsuranceRequest), index);
        }

        return insuranceRequest;
    }

    /// <summary>
    ///     Найти андеррайтинг жизни по индексу
    /// </summary>
    public static MirLifeUnderwriting? FindLifeUnderwriting(this Mir mir, InsuredPersonIndex index)
    {
        var insuranceRequest = mir.FindLifeInsuranceRequest(index);
        return insuranceRequest?.Underwriting;
    }

    /// <summary>
    ///     Найти андеррайтинг жизни по индексу
    /// </summary>
    public static MirLifeUnderwriting GetLifeUnderwriting(this Mir mir, InsuredPersonIndex index)
    {
        var underwriting = mir.FindLifeUnderwriting(index);
        if (underwriting == null)
        {
            throw new EntityNotFoundException(typeof(MirLifeUnderwriting), index);
        }

        return underwriting;
    }

    #endregion

    #region PropertyInsurance

    /// <summary>
    ///     Найти запрос страхования имущества по ИД
    /// </summary>
    public static MirPropertyInsuranceRequest? FindPropertyInsuranceRequest(this Mir mir, Guid requestId) =>
        mir.PropertyInsuranceRequests.FirstOrDefault(x => x.Id == requestId);

    /// <summary>
    ///     Найти запрос страхования имущества по ИД
    /// </summary>
    public static MirPropertyInsuranceRequest GetPropertyInsuranceRequest(this Mir mir, Guid requestId)
    {
        var request = mir.PropertyInsuranceRequests.FirstOrDefault(x => x.Id == requestId);
        if (request == null)
        {
            throw new EntityNotFoundException(typeof(MirPropertyInsuranceRequest), requestId);
        }

        return request;
    }

    /// <summary>
    ///     Найти единственный запрос страхования имущества
    /// </summary>
    public static MirPropertyInsuranceRequest? FindPropertyInsuranceRequest(this Mir mir) =>
        mir.PropertyInsuranceRequests.SingleOrDefault();


    /// <summary>
    ///     Найти запрос страхования имущества по ИД андеррайтинга
    /// </summary>
    public static MirPropertyInsuranceRequest? FindPropertyInsuranceRequestByUnderwritingId(this Mir mir,
        Guid underwritingId) =>
        mir.PropertyInsuranceRequests.FirstOrDefault(x => x.Underwriting?.Id == underwritingId);

    /// <summary>
    ///     Найти андеррайтинг имущества по ИД андеррайтинга страхования имущества
    /// </summary>
    public static MirPropertyUnderwriting? FindPropertyUnderwriting(this Mir mir, Guid underwritingId)
    {
        var insuranceRequest = mir.FindPropertyInsuranceRequestByUnderwritingId(underwritingId);
        return insuranceRequest?.Underwriting;
    }

    /// <summary>
    ///     Найти андеррайтинг имущества по Ид андеррайтинга
    /// </summary>
    public static MirPropertyInsuranceRequest GetPropertyInsuranceRequestByUnderwritingId(this Mir mir,
        Guid underwritingId)
    {
        var insuranceRequest = mir.FindPropertyInsuranceRequestByUnderwritingId(underwritingId);
        if (insuranceRequest == null)
        {
            throw new EntityNotFoundException(typeof(MirPropertyInsuranceRequest), underwritingId);
        }

        return insuranceRequest;
    }

    /// <summary>
    ///     Найти андеррайтинг имущества по ИД андеррайтинга страхования имущества
    /// </summary>
    public static MirPropertyUnderwriting GetPropertyUnderwriting(this Mir mir, Guid underwritingId)
    {
        var underwriting = mir.FindPropertyUnderwriting(underwritingId);
        if (underwriting == null)
        {
            throw new EntityNotFoundException(typeof(MirPropertyUnderwriting), underwritingId);
        }

        return underwriting;
    }

    #endregion

    #region TitleInsurance

    /// <summary>
    ///     Найти запрос страхования титула по ИД
    /// </summary>
    public static MirTitleInsuranceRequest? FindTitleInsuranceRequest(this Mir mir, Guid requestId) =>
        mir.TitleInsuranceRequests.FirstOrDefault(x => x.Id == requestId);

    /// <summary>
    ///     Найти единственный запрос страхования имущества
    /// </summary>
    public static MirTitleInsuranceRequest? FindTitleInsuranceRequest(this Mir mir) =>
        mir.TitleInsuranceRequests.SingleOrDefault();

    /// <summary>
    ///     Найти запрос страхования титула по ИД андеррайтинга
    /// </summary>
    public static MirTitleInsuranceRequest?
        FindTitleInsuranceRequestByUnderwritingId(this Mir mir, Guid underwritingId) =>
        mir.TitleInsuranceRequests.FirstOrDefault(x => x.Underwriting?.Id == underwritingId);

    /// <summary>
    ///     Найти андеррайтинг титула по ИД андеррайтинга страхования титула
    /// </summary>
    public static MirTitleUnderwriting? FindTitleUnderwriting(this Mir mir, Guid underwritingId)
    {
        var insuranceRequest = mir.FindTitleInsuranceRequestByUnderwritingId(underwritingId);
        return insuranceRequest?.Underwriting;
    }

    /// <summary>
    ///     Найти андеррайтинг титула по Ид андеррайтинга
    /// </summary>
    public static MirTitleInsuranceRequest GetTitleInsuranceRequestByUnderwritingId(this Mir mir, Guid underwritingId)
    {
        var insuranceRequest = mir.FindTitleInsuranceRequestByUnderwritingId(underwritingId);
        if (insuranceRequest == null)
        {
            throw new EntityNotFoundException(typeof(MirTitleInsuranceRequest), underwritingId);
        }

        return insuranceRequest;
    }

    /// <summary>
    ///     Найти андеррайтинг титула по ИД андеррайтинга страхования титула
    /// </summary>
    public static MirTitleUnderwriting GetTitleUnderwriting(this Mir mir, Guid underwritingId)
    {
        var underwriting = mir.FindTitleUnderwriting(underwritingId);
        if (underwriting == null)
        {
            throw new EntityNotFoundException(typeof(MirTitleUnderwriting), underwritingId);
        }

        return underwriting;
    }

    #endregion

    #region Property

    /// <summary>
    ///     Получение списка имущества в заявке
    /// </summary>
    public static IEnumerable<Property> GetAllProperty(this Mir mir) =>
        mir.PropertyInsuranceRequests.Select(x => x.Property)
            .Concat(mir.TitleInsuranceRequests.Select(x => x.Property))
            .Where(x => x != null)
            .DistinctBy(x => x!.Id)
            .ToList()!;

    /// <summary>
    ///     Получение имущества в заявке по ИД
    /// </summary>
    public static Property? GetProperty(this Mir mir, Guid propertyId) =>
        mir.GetAllProperty().FirstOrDefault(x => x.Id == propertyId);

    #endregion
}