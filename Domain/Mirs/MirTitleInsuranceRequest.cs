using System.ComponentModel.DataAnnotations.Schema;
using Ct.Module.Domain.Entities;
using Some.Abstractions.Mirs;
using Some.Domain.Addresses;
using Some.Domain.Monies;
using Some.Domain.Properties;
using Some.Domain.Underwriting;

namespace Some.Domain.Mirs;

/// <summary>
///     Запрос страхования титула
/// </summary>
[Table("mir_title_insurance_request")]
public sealed class MirTitleInsuranceRequest : TrackableEntity<Guid>,
    IMirTitleInsuranceRequest<Money, Property, MirTitleUnderwriting, Address>
{
    /// <summary>
    ///     Создание запроса страхования титула
    /// </summary>
    public MirTitleInsuranceRequest(Guid id, IScalarMir mir, Property? property, decimal? loanShare,
        int? insurancePeriod, MirTitleUnderwriting? underwriting = null) : base(id)
    {
        MirId = mir.Id;
        Property = property;
        PropertyId = property?.Id;
        LoanShare = loanShare ?? 1;
        InsurancePeriod = insurancePeriod;
        Underwriting = underwriting;
    }

    private MirTitleInsuranceRequest()
    {
    }

    /// <summary>
    ///     Ид заявки
    /// </summary>
    [Column("mir_id")]
    public Guid MirId { get; }

    /// <inheritdoc />
    [Column("insurance_period")]
    public int? InsurancePeriod { get; }

    /// <inheritdoc />
    [Column("loan_share")]
    public decimal? LoanShare { get; }

    /// <summary>
    ///     Страхуемое имущество
    /// </summary>
    public Property? Property { get; }

    /// <summary>
    ///     Андеррайтинг титула
    /// </summary>
    public MirTitleUnderwriting? Underwriting { get; private set; }

    /// <summary>
    ///     Ид страхуемого титула
    /// </summary>
    [Column("property_id")]
    private Guid? PropertyId { get; }

    /// <summary>
    ///     Присвоить результаты андеррайтинга титула
    /// </summary>
    /// <param name="underwriting">Результаты андеррайтинга титула</param>
    /// <returns>Результат</returns>
    public void ChangeUnderwriting(MirTitleUnderwriting? underwriting) => Underwriting = underwriting;

    /// <summary>
    ///     Сбросить андеррайтинг
    /// </summary>
    public void ResetUnderwriting() => Underwriting?.Reset();
}