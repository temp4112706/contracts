using System.ComponentModel.DataAnnotations.Schema;
using Ct.Module.Domain.Entities;
using Some.Abstractions.Mirs;
using Some.Domain.InsuredPersons;
using Some.Domain.Underwriting;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local
// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable UnusedAutoPropertyAccessor.Local

namespace Some.Domain.Mirs;

/// <summary>
///     Запрос страхования жизни
/// </summary>
[Table("mir_life_insurance_request")]
public sealed class MirLifeInsuranceRequest : TrackableEntity<Guid>,
    IMirLifeInsuranceRequest<InsuredPerson, MirLifeUnderwriting>
{
    /// <summary>
    ///     Создание запроса страхования жизни
    /// </summary>
    public MirLifeInsuranceRequest(Guid id, IScalarMir mir, InsuredPerson insuredPerson,
        MirLifeUnderwriting? underwriting = null) : base(id)
    {
        MirId = mir.Id;
        InsuredPerson = insuredPerson;
        InsuredPersonId = insuredPerson.Id;
        Underwriting = underwriting;
    }

    // ReSharper disable once UnusedMember.Local
    private MirLifeInsuranceRequest()
    {
    }

    /// <summary>
    ///     Ид заявки
    /// </summary>
    [Column("mir_id")]
    public Guid MirId { get; private set; }

    /// <summary>
    ///     Застрахованное лицо
    /// </summary>
    public InsuredPerson InsuredPerson { get; private set; } = null!;

    /// <summary>
    ///     Андеррайтинг жизни
    /// </summary>
    public MirLifeUnderwriting? Underwriting { get; private set; }

    /// <summary>
    ///     Ид застрахованного лица
    /// </summary>
    // ReSharper disable once UnassignedGetOnlyAutoProperty
    [Column("insured_person_id")]
    private Guid InsuredPersonId { get; set; }

    /// <summary>
    ///     Присвоить результаты андеррайтинга жизни
    /// </summary>
    /// <param name="underwriting">Результаты андеррайтинга жизни</param>
    /// <returns>Результат</returns>
    public void ChangeUnderwriting(MirLifeUnderwriting? underwriting) => Underwriting = underwriting;

    /// <summary>
    ///     Сбросить андеррайтинг
    /// </summary>
    public void ResetUnderwriting() => Underwriting?.Reset();
}