using System.ComponentModel.DataAnnotations.Schema;
using Ct.Module.Domain.Entities;
using Some.Abstractions.Addresses;
using Some.Domain.Countries;

namespace Some.Domain.Addresses;

/// <summary>
///     Адрес
/// </summary>
[Table("address")]
public class Address : BaseEntity<Guid>, IAddress
{
    /// <summary>
    ///     Создание адреса
    /// </summary>
    public Address(Guid id, IAddress address) : base(id)
    {
        Change(
            address.CountryCode,
            address.FullAddress,
            address.Postcode,
            address.Region,
            address.District,
            address.City,
            address.Locality,
            address.IntracityTerritory,
            address.Street,
            address.House,
            address.Building,
            address.Apartment,
            address.Room
        );
    }

    /// <summary>
    ///     Создание адреса
    /// </summary>
    public Address(Guid id, string fullAddress)
        : this(
            id,
            Country.Current,
            fullAddress
        )
    {
    }

    /// <summary>
    ///     Создание адреса
    /// </summary>
    public Address(Guid id,
        string countryCode,
        string fullAddress
    ) : base(id)
    {
        Change(
            countryCode,
            fullAddress,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null
        );
    }

    /// <summary>
    ///     Создание адреса
    /// </summary>
    public Address(Guid id,
        string countryCode,
        string fullAddress,
        string? postcode,
        string? region,
        string? district,
        string? city,
        string? locality,
        string? intracityTerritory,
        string? street,
        string? house,
        string? building,
        string? apartment,
        string? room) : base(id)
    {
        Change(
            countryCode,
            fullAddress,
            postcode,
            region,
            district,
            city,
            locality,
            intracityTerritory,
            street,
            house,
            building,
            apartment,
            room
        );
    }

    // ReSharper disable once UnusedMember.Local
    private Address()
    {
    }

    /// <summary>
    ///     Квартира
    /// </summary>
    [Column("apartment")]
    public string? Apartment { get; private set; }

    /// <summary>
    ///     Строение
    /// </summary>
    [Column("building")]
    public string? Building { get; private set; }

    /// <summary>
    ///     Город
    /// </summary>
    [Column("city")]
    public string? City { get; private set; }

    /// <summary>
    ///     Район
    /// </summary>
    [Column("district")]
    public string? District { get; private set; }

    /// <summary>
    ///     Дом
    /// </summary>
    [Column("house")]
    public string? House { get; private set; }

    /// <summary>
    ///     Внутригородская территория
    /// </summary>
    [Column("intracity_territory")]
    public string? IntracityTerritory { get; private set; }

    /// <summary>
    ///     Населенный пункт
    /// </summary>
    [Column("locality")]
    public string? Locality { get; private set; }

    /// <summary>
    ///     Почтовый индекс
    /// </summary>
    [Column("postcode")]
    public string? Postcode { get; private set; }

    /// <summary>
    ///     Регион
    /// </summary>
    [Column("region")]
    public string? Region { get; private set; }

    /// <summary>
    ///     Комната
    /// </summary>
    [Column("room")]
    public string? Room { get; private set; }

    /// <summary>
    ///     Улица
    /// </summary>
    [Column("street")]
    public string? Street { get; private set; }

    /// <summary>
    ///     Код страны ISO (например RU)
    /// </summary>
    [Column("country_code")]
    public string CountryCode { get; private set; } = null!;

    /// <summary>
    ///     Полная строка адреса
    /// </summary>
    [Column("full_address")]
    public string FullAddress { get; private set; } = null!;

    /// <summary>
    ///     Изменить адрес
    /// </summary>
    public void Change(
        string countryCode,
        string fullAddress,
        string? postcode,
        string? region,
        string? district,
        string? city,
        string? locality,
        string? intracityTerritory,
        string? street,
        string? house,
        string? building,
        string? apartment,
        string? room)
    {
        CountryCode = countryCode;
        FullAddress = fullAddress;
        Region = region;
        District = district;
        City = city;
        IntracityTerritory = intracityTerritory;
        Street = street;
        House = house;
        Building = building;
        Apartment = apartment;
        Postcode = postcode;
        Locality = locality;
        Room = room;
    }

    /// <summary>
    ///     Изменить адрес
    /// </summary>
    public void Change(IAddress address)
    {
        CountryCode = address.CountryCode;
        FullAddress = address.FullAddress;
        Region = address.Region;
        District = address.District;
        City = address.City;
        IntracityTerritory = address.IntracityTerritory;
        Street = address.Street;
        House = address.House;
        Building = address.Building;
        Apartment = address.Apartment;
        Postcode = address.Postcode;
        Locality = address.Locality;
        Room = address.Room;
    }

    /// <summary>
    ///     Изменить адрес
    /// </summary>
    /// <param name="fullAddress">Полностью адрес</param>
    public bool Change(string fullAddress)
    {
        if (FullAddress == fullAddress)
        {
            return false;
        }

        if (CountryCode != Country.Current)
        {
            CountryCode = Country.Current;
        }

        FullAddress = fullAddress;
        Region = null;
        District = null;
        City = null;
        IntracityTerritory = null;
        Street = null;
        House = null;
        Building = null;
        Apartment = null;
        Postcode = null;
        Locality = null;
        Room = null;

        return true;
    }
}