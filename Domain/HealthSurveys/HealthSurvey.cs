using System.ComponentModel.DataAnnotations.Schema;
using Ct.Module.Domain.Entities;
using Some.Abstractions.HealthSurveys;
using Some.Domain.HealthSurveys.ValueObjects;
using Some.Domain.InsuredPersons;

// ReSharper disable UnusedAutoPropertyAccessor.Local

namespace Some.Domain.HealthSurveys;

/// <summary>
///     Медицинское обследование
/// </summary>
[Table("health_survey")]
public class HealthSurvey : TrackableEntity<Guid>, IHealthSurvey<HealthFacility>
{
    /// <summary>
    ///     Создание медицинского обследования
    /// </summary>
    public HealthSurvey(Guid id, InsuredPerson insuredPerson, DateTime visitDate,
        HealthFacility? healthFacility) : base(id)
    {
        InsuredPersonId = insuredPerson.Id;
        HealthFacility = healthFacility;
        VisitDate = visitDate;
    }

    // ReSharper disable once UnusedMember.Local
    private HealthSurvey()
    {
    }

    /// <summary>
    ///     Медицинское учреждение
    /// </summary>
    [Column("health_facility")]
    public HealthFacility? HealthFacility { get; private set; }

    /// <summary>
    ///     Дата обследования
    /// </summary>
    [Column("visit_date", TypeName = "date")]
    public DateTime VisitDate { get; private set; }

    /// <summary>
    ///     Ид застрахованного лица
    /// </summary>
    [Column("insured_person_id")]
    private Guid InsuredPersonId { get; }

    /// <summary>
    ///     Изменение данных медицинского обследования
    /// </summary>
    public void Change<THealthFacility>(IHealthSurvey<THealthFacility> hs)
        where THealthFacility : class, IHealthFacility
    {
        VisitDate = hs.VisitDate;
        HealthFacility = hs.HealthFacility != null ? new HealthFacility(hs.HealthFacility) : null;
    }

    /// <summary>
    ///     Изменение данных медицинского обследования
    /// </summary>
    /// <param name="visitDate">Дата обследования</param>
    /// <param name="healthFacility">Медицинское учреждение</param>
    private void Change(DateTime visitDate, HealthFacility healthFacility)
    {
        VisitDate = visitDate;
        HealthFacility = healthFacility;
    }
}