using Some.Abstractions.HealthSurveys;
using Newtonsoft.Json;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local

namespace Some.Domain.HealthSurveys.ValueObjects;

/// <summary>
///     Медицинское учреждение
/// </summary>
public class HealthFacility : IHealthFacility
{
    /// <summary>
    ///     Создание мед. учреждения
    /// </summary>
    public HealthFacility(IHealthFacility healthFacility)
    {
        Name = healthFacility.Name;
        Phone = healthFacility.Phone;
        Address = healthFacility.Address;
    }

    /// <summary>
    ///     Создание мед. учреждения
    /// </summary>
    [Newtonsoft.Json.JsonConstructor]
    public HealthFacility([JsonProperty] string? name, [JsonProperty] string? phone, [JsonProperty] string? address)
    {
        Name = name;
        Phone = phone;
        Address = address;
    }

    private HealthFacility()
    {
    }

    /// <inheritdoc />
    public string? Address { get; private set; }

    /// <inheritdoc />
    public string? Name { get; private set; }

    /// <inheritdoc />
    public string? Phone { get; private set; }
}