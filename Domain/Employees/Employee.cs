using System.ComponentModel.DataAnnotations.Schema;
using Ct.Module.Domain.Entities;
using Some.Abstractions.Employees;
using Some.Abstractions.Enums;
using Some.Abstractions.Extensions;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local

namespace Some.Domain.Employees;

/// <summary>
///     Cотрудник
/// </summary>
[Table("employee")]
public class Employee : TrackableEntity<string>, IEmployee
{
    /// <inheritdoc />
    public Employee(string id, string email, string fullName, EmployeeType type, bool enabled = true) : base(id)
    {
        ChangeProps(email, fullName, type, enabled);
    }

    /// <inheritdoc />
    public Employee(IEmployee employee, EmployeeType type, bool enabled = true)
        : this(employee.Id, employee.Email, employee.FullName, type, enabled)
    {
    }

    /// <inheritdoc />
    // ReSharper disable once UnusedMember.Local
    protected Employee()
    {
    }

    /// <summary>
    ///     Активен?
    /// </summary>
    [Column("enabled")]
    public bool Enabled { get; private set; }

    /// <summary>
    ///     Тип сотрудника
    /// </summary>
    [Column("types")]
    public string[] Types { get; private set; } = null!;

    /// <inheritdoc />
    [Column("email")]
    public string Email { get; private set; } = null!;

    /// <inheritdoc />
    [Column("full_name")]
    public string FullName { get; private set; } = null!;

    /// <summary>
    ///     Изменить данные
    /// </summary>
    public void ChangeProps(string email, string fullName, EmployeeType type, bool enabled = true)
    {
        Email = email;
        FullName = fullName;
        Enabled = enabled;
        // ReSharper disable once NullCoalescingConditionIsAlwaysNotNullAccordingToAPIContract
        Types = new List<string>(Types ?? Array.Empty<string>()) { type.ToValue() }.Distinct().ToArray();
    }
}