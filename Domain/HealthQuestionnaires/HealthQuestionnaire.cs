using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using Ct.Module.Domain.Entities;
using Some.Abstractions.HealthQuestionnaires;
using Some.Domain.HealthQuestionnaires.ValueObjects;
using Some.Domain.InsuredPersons;

// ReSharper disable UnusedAutoPropertyAccessor.Local

namespace Some.Domain.HealthQuestionnaires;

/// <summary>
///     Медицинская анкета
/// </summary>
[Table("health_questionnaire")]
public class HealthQuestionnaire : TrackableEntity<Guid>, IHealthQuestionnaire<ArterialPressure, AnswerWithComments>
{
    /// <summary>
    ///     Создание медицинской анкеты
    /// </summary>
    public HealthQuestionnaire(Guid id, InsuredPerson insuredPerson, DateTime fillingDateTime,
        float height, float weight, bool isSmoking, string? smokingDetails,
        int? numberOfCigarettesPerDay, float? beerPerWeek, float? winePerWeek,
        float? strongAlcoholPerWeek, string? clinicRegistration, ArterialPressure arterialPressure,
        string? additionalInfo, ICollection<AnswerWithComments> answersWithComments) : base(id)
    {
        InsuredPersonId = insuredPerson.Id;
        Change(
            fillingDateTime,
            height,
            weight,
            isSmoking,
            smokingDetails,
            numberOfCigarettesPerDay,
            beerPerWeek,
            winePerWeek,
            strongAlcoholPerWeek,
            clinicRegistration,
            arterialPressure,
            additionalInfo,
            answersWithComments
        );
    }

    /// <summary>
    ///     Создание медицинской анкеты
    /// </summary>
    public HealthQuestionnaire(Guid id, InsuredPerson insuredPerson,
        IHealthQuestionnaire<ArterialPressure, AnswerWithComments> healthQuestionnaire) : this(
        id,
        insuredPerson,
        healthQuestionnaire.FillingDateTime,
        healthQuestionnaire.Height,
        healthQuestionnaire.Weight,
        healthQuestionnaire.IsSmoking,
        healthQuestionnaire.SmokingDetails,
        healthQuestionnaire.NumberOfCigarettesPerDay,
        healthQuestionnaire.BeerPerWeek,
        healthQuestionnaire.WinePerWeek,
        healthQuestionnaire.StrongAlcoholPerWeek,
        healthQuestionnaire.ClinicRegistration,
        healthQuestionnaire.ArterialPressure,
        healthQuestionnaire.AdditionalInfo,
        healthQuestionnaire.AnswersWithComments
    )
    {
    }

    // ReSharper disable once UnusedMember.Local
    private HealthQuestionnaire()
    {
    }

    /// <summary>
    ///     Ответ с комментарием. При формировании необходимо указать для значения для всех ключей из поля code, за исключением
    ///     вопросов для женщин, которые заполняются в зависимости от пола.
    /// </summary>
    [Column("answers_with_comments")]
    public ICollection<AnswerWithComments> AnswersWithComments { get; private set; } = null!;

    /// <summary>
    ///     Артериальное давление (последнее измерение, мм. рт. ст.)
    /// </summary>
    [Column("arterial_pressure")]
    public ArterialPressure ArterialPressure { get; private set; } = null!;

    /// <summary>
    ///     Дополнительная информация
    /// </summary>
    [Column("additional_info")]
    public string? AdditionalInfo { get; private set; }

    /// <summary>
    ///     Количество пива в неделю (мл)
    /// </summary>
    [Column("beer_per_week")]
    public float? BeerPerWeek { get; private set; }

    /// <summary>
    ///     Наименование и адрес медучреждения, в котором вы наблюдаетесь, стоите на учете, проходите лечение
    /// </summary>
    [Column("clinic_registration")]
    public string? ClinicRegistration { get; private set; }

    /// <summary>
    ///     Дата и время заполнения
    /// </summary>
    [Column("filling_datetime")]
    public DateTime FillingDateTime { get; private set; }

    /// <summary>
    ///     Рост
    /// </summary>
    [Column("height")]
    public float Height { get; private set; }

    /// <summary>
    ///     Курите ли вы?
    /// </summary>
    [Column("is_smoking")]
    public bool IsSmoking { get; private set; }

    /// <summary>
    ///     Количество сигарет в день
    /// </summary>
    [Column("number_of_cigarettes_per_day")]
    public int? NumberOfCigarettesPerDay { get; private set; }

    /// <summary>
    ///     Как давно курите или как давно бросили курить?
    /// </summary>
    [Column("smoking_details")]
    public string? SmokingDetails { get; private set; }

    /// <summary>
    ///     Количество крепкого спиртного в неделю (мл)
    /// </summary>
    [Column("strong_alcohol_per_week")]
    public float? StrongAlcoholPerWeek { get; private set; }

    /// <summary>
    ///     Вес
    /// </summary>
    [Column("weight")]
    public float Weight { get; private set; }

    /// <summary>
    ///     Количество вина в неделю (мл)
    /// </summary>
    [Column("wine_per_week")]
    public float? WinePerWeek { get; private set; }

    /// <summary>
    ///     Ид застрахованного лица
    /// </summary>
    [Column("insured_person_id")]
    private Guid InsuredPersonId { get; }

    /// <summary>
    ///     Изменение анкеты
    /// </summary>
    [SuppressMessage("ReSharper", "ArgumentsStyleOther")]
    public void Change<TArterialPressure, TAnswerWithComments>(
        IHealthQuestionnaire<TArterialPressure, TAnswerWithComments> hq)
        where TArterialPressure : IArterialPressure
        where TAnswerWithComments : IAnswerWithComments
    {
        static ArterialPressure MapArterialPressureFrom(TArterialPressure input)
        {
            return new ArterialPressure(
                input.Systolic,
                input.Diastolic
            );
        }

        static AnswerWithComments MapAnswersWithCommentsFrom(TAnswerWithComments input)
        {
            return new AnswerWithComments(
                input.Code,
                input.Answer,
                input.Comments
            );
        }

        Change(
            hq.FillingDateTime,
            hq.Height,
            hq.Weight,
            hq.IsSmoking,
            hq.SmokingDetails,
            hq.NumberOfCigarettesPerDay,
            hq.BeerPerWeek,
            hq.WinePerWeek,
            hq.StrongAlcoholPerWeek,
            hq.ClinicRegistration,
            arterialPressure: MapArterialPressureFrom(hq.ArterialPressure),
            hq.AdditionalInfo,
            answersWithComments: hq.AnswersWithComments.Select(MapAnswersWithCommentsFrom).ToArray()
        );
    }

    /// <summary>
    ///     Изменение анкеты
    /// </summary>
    private void Change(DateTime fillingDateTime,
        float height, float weight, bool isSmoking, string? smokingDetails,
        int? numberOfCigarettesPerDay, float? beerPerWeek, float? winePerWeek,
        float? strongAlcoholPerWeek, string? clinicRegistration, ArterialPressure arterialPressure,
        string? additionalInfo, ICollection<AnswerWithComments> answersWithComments)
    {
        Height = height;
        Weight = weight;
        IsSmoking = isSmoking;
        SmokingDetails = smokingDetails;
        NumberOfCigarettesPerDay = numberOfCigarettesPerDay;
        BeerPerWeek = beerPerWeek;
        WinePerWeek = winePerWeek;
        StrongAlcoholPerWeek = strongAlcoholPerWeek;
        ClinicRegistration = clinicRegistration;
        ArterialPressure = arterialPressure;
        AnswersWithComments = answersWithComments;
        AdditionalInfo = additionalInfo;
        FillingDateTime = fillingDateTime;
    }
}