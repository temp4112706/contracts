﻿using Some.Abstractions.Enums;
using Some.Abstractions.HealthQuestionnaires;
using Newtonsoft.Json;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local

namespace Some.Domain.HealthQuestionnaires.ValueObjects;

/// <summary>
///     Ответ с комментарием. При формировании необходимо указать для значения для всех ключей из поля code, за исключением
///     вопросов для женщин, которые заполняются в зависимости от пола.
/// </summary>
public class AnswerWithComments : IAnswerWithComments
{
    /// <summary>
    ///     Создание ответа с комментарием
    /// </summary>
    [Newtonsoft.Json.JsonConstructor]
    public AnswerWithComments(
        [JsonProperty] QuestionCode code,
        [JsonProperty] bool answer,
        [JsonProperty] string? comments)
    {
        Code = code;
        Answer = answer;
        Comments = comments;
    }

    private AnswerWithComments()
    {
    }

    /// <summary>
    ///     Ответ да или нет
    /// </summary>
    public bool Answer { get; private set; }

    /// <summary>
    ///     Код вопроса
    /// </summary>
    public QuestionCode Code { get; private set; }

    /// <summary>
    ///     Комментарий
    /// </summary>
    public string? Comments { get; private set; }
}