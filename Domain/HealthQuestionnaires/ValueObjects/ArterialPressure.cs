﻿using Some.Abstractions.HealthQuestionnaires;
using Newtonsoft.Json;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local

namespace Some.Domain.HealthQuestionnaires.ValueObjects;

/// <summary>
///     Артериальное давление (последнее измерение, мм. рт. ст.)
/// </summary>
public record ArterialPressure : IArterialPressure, IEquatable<IArterialPressure?>
{
    /// <summary>
    ///     Создание артериальное давление
    /// </summary>
    [Newtonsoft.Json.JsonConstructor]
    public ArterialPressure([JsonProperty] int systolic, [JsonProperty] int diastolic)
    {
        Systolic = systolic;
        Diastolic = diastolic;
    }

    private ArterialPressure()
    {
    }

    /// <summary>
    ///     Диастолическое
    /// </summary>
    public int Diastolic { get; private set; }

    /// <summary>
    ///     Систолическое
    /// </summary>
    public int Systolic { get; private set; }

    /// <inheritdoc />
    public bool Equals(IArterialPressure? other)
    {
        if (other == null)
        {
            return false;
        }

        return Systolic == other.Systolic && Diastolic == other.Diastolic;
    }
}