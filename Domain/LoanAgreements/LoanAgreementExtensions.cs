using Some.Domain.LoanAgreements.ValueObjects;

namespace Some.Domain.LoanAgreements;

/// <summary>
///     Расшириения для <see cref="LoanAgreement" />
/// </summary>
public static class LoanAgreementExtensions
{
    internal static void TryAppendInterestRateToSchedule(this LoanAgreement loanAgreement, decimal? startInterestRate,
        decimal endInterestRate, DateTime? updatedAt)
    {
        if (startInterestRate == null || updatedAt == null)
        {
            return;
        }

        var newInterestRateSchedule = new[]
        {
            new InterestRateRow(startInterestRate.Value, endInterestRate, updatedAt.Value)
        };

        if (loanAgreement.InterestRateSchedule == null)
        {
            loanAgreement.ChangeInterestRateSchedule(newInterestRateSchedule);
        }
        else
        {
            if (!loanAgreement.InterestRateSchedule.Any(x =>
                    x.StartInterestRate == startInterestRate &&
                    x.EndInterestRate == endInterestRate &&
                    x.UpdatedAt == updatedAt))
            {
                loanAgreement.ChangeInterestRateSchedule(loanAgreement.InterestRateSchedule
                    .Concat(newInterestRateSchedule)
                    .OrderBy(x => x.UpdatedAt)
                    .ToArray());
            }
        }
    }
}