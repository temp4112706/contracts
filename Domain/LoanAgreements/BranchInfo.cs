using System.ComponentModel.DataAnnotations.Schema;
using Ct.Module.Domain.Entities;
using Some.Abstractions.LoanAgreements;
using Some.Domain.Addresses;
using Some.Domain.Employees;

// ReSharper disable UnusedAutoPropertyAccessor.Local

namespace Some.Domain.LoanAgreements;

/// <summary>
///     Информация о месте выдачи кредита
/// </summary>
[Table("branch_info")]
public class BranchInfo : BaseEntity<Guid>, IBranchInfo<Employee, Address>
{
    /// <summary>
    ///     Название места выдачи по умолчанию
    /// </summary>
    public const string DefaultName = "Москва";

    /// <summary>
    ///     Код региона по умолчанию - Москва
    /// </summary>
    public const string DefaultRegionCode = "77";

    /// <summary>
    ///     Создание места выдачи кредита
    /// </summary>
    public BranchInfo(Guid id, string regionCode, string name, string? code,
        Address? address, Employee? employee) : base(id)
    {
        ChangeProps(regionCode, name, code);
        ChangeEmployee(employee);
        ChangeAddress(address);
    }

    // ReSharper disable once UnusedMember.Local
    private BranchInfo()
    {
    }

    /// <inheritdoc />
    [Column("address")]
    public Address? Address { get; private set; }

    /// <inheritdoc />
    [Column("code")]
    public string? Code { get; private set; }

    /// <inheritdoc />
    public Employee? Employee { get; private set; }

    /// <inheritdoc />
    [Column("name")]
    public string Name { get; private set; } = null!;

    /// <inheritdoc />
    [Column("region_code")]
    public string RegionCode { get; private set; } = null!;

    /// <summary>
    ///     Ид адреса
    /// </summary>
    [Column("address_id")]
    private Guid? AddressId { get; set; }

    /// <summary>
    ///     ИД <see cref="Employee" />
    /// </summary>
    [Column("employee_id")]
    private string? EmployeeId { get; set; }

    /// <summary>
    ///     Изменить данные
    /// </summary>
    public void ChangeAddress(Address? address)
    {
        Address = address;
        AddressId = address?.Id;
    }

    /// <summary>
    ///     Изменить сотрудника
    /// </summary>
    public void ChangeEmployee(Employee? employee)
    {
        if (Employee?.Id == employee?.Id)
        {
            return;
        }

        Employee = employee;
        EmployeeId = employee?.Id;
    }

    /// <summary>
    ///     Изменить данные
    /// </summary>
    public void ChangeProps(string regionCode, string name, string? code)
    {
        RegionCode = regionCode;
        Name = name;
        Code = code;
    }

    /// <summary>
    ///     Создание информация по умолчанию о месте выдачи кредита в Москве
    /// </summary>
    /// <returns>Место выдачи в Москве</returns>
    public static BranchInfo Default() =>
        new(
            Guid.NewGuid(),
            DefaultRegionCode,
            DefaultName,
            null,
            null,
            null);


    /// <summary>
    ///     Создание информация по умолчанию с пустыми данными
    /// </summary>
    public static BranchInfo Empty() =>
        new(
            Guid.NewGuid(),
            "",
            "",
            null,
            null,
            null);
}