using System.ComponentModel.DataAnnotations.Schema;
using Ct.Module.Domain.Entities;
using Some.Abstractions.Enums;
using Some.Abstractions.LoanAgreements;
using Some.Domain.Beneficiaries;
using Some.Domain.Customers;
using Some.Domain.LoanAgreements.ValueObjects;
using Some.Domain.Monies;
using Some.Domain.Policies.Mortgage;

// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable UnusedMember.Global
// ReSharper disable UnusedAutoPropertyAccessor.Local
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local

namespace Some.Domain.LoanAgreements;

/// <summary>
///     Кредитный договор
/// </summary>
[Table(Table)]
public sealed class LoanAgreement : TrackableEntity<Guid>,
    ILoanAgreement<Money, BranchInfo, MortgageInfo, OverdueInfo,
        InterestRateRow, RepaymentScheduleRow, RepaymentAmount>
{
    /// <summary>
    ///     Название таблицы
    /// </summary>
    public const string Table = "loan_agreement";

    /// <summary>
    ///     Создание кредитного договора
    /// </summary>
    public LoanAgreement(Guid id, BranchInfo branchInfo) : base(id)
    {
        BranchInfo = branchInfo;
        BranchInfoId = branchInfo.Id;
    }

    /// <summary>
    ///     Создание кредитного договора
    /// </summary>
    public LoanAgreement(Guid id, BranchInfo branchInfo, Beneficiary beneficiary, int agreementPeriod, Money loanAmount,
        DateTime agreementDateTime, string? agreementNumber, decimal? interestRate, decimal? extraInterestRate)
        : this(id, branchInfo)
    {
        ChangeMain(
            agreementPeriod,
            loanAmount,
            agreementDateTime,
            agreementNumber,
            interestRate,
            extraInterestRate
        );
        ChangeBeneficiary(beneficiary);
    }

    /// <summary>
    ///     Создание кредитного договора
    /// </summary>
    public LoanAgreement(Guid id, BranchInfo branchInfo, Beneficiary beneficiary, int agreementPeriod, Money loanAmount,
        DateTime agreementDateTime, string? agreementNumber, decimal? interestRate, decimal? extraInterestRate,
        string? loanType, RepaymentScheduleRow[]? repaymentSchedule, bool? insuranceAllBorrowers, string? externalId
    ) : this(id, branchInfo, beneficiary, agreementPeriod, loanAmount,
        agreementDateTime, agreementNumber, interestRate, extraInterestRate)
    {
        ExternalId = externalId;
        ChangeExtra(
            loanType,
            repaymentSchedule,
            insuranceAllBorrowers,
            agreementNumber,
            interestRate,
            extraInterestRate
        );
    }

    /// <summary>
    ///     Создание кредитного договора
    /// </summary>
    public LoanAgreement(Guid id, ILoanAgreement<Money, BranchInfo, MortgageInfo, OverdueInfo,
            InterestRateRow, RepaymentScheduleRow, RepaymentAmount> loanAgreement,
        Beneficiary? beneficiary, Customer? borrower)
        : this(id, loanAgreement.BranchInfo)
    {
        ExternalId = loanAgreement.ExternalId;
        LoanType = loanAgreement.LoanType;
        LoanAmount = loanAgreement.LoanAmount;
        InsuranceAllBorrowers = loanAgreement.InsuranceAllBorrowers;
        AgreementPeriod = loanAgreement.AgreementPeriod;
        AgreementNumber = loanAgreement.AgreementNumber;
        AgreementDateTime = loanAgreement.AgreementDateTime;
        InterestRate = loanAgreement.InterestRate;
        BeginInterestRate = loanAgreement.BeginInterestRate;
        RepaymentSchedule = loanAgreement.RepaymentSchedule;

        BeginDate = loanAgreement.BeginDate;
        EndDate = loanAgreement.EndDate;
        CloseDate = loanAgreement.CloseDate;

        Borrower = borrower;
        BorrowerId = borrower?.Id;

        Beneficiary = beneficiary;
        BeneficiaryId = beneficiary?.Id;

        Product = loanAgreement.Product;
        State = loanAgreement.State;
        MortgageInfo = loanAgreement.MortgageInfo;
        OverdueInfo = loanAgreement.OverdueInfo;
        SAccPay = loanAgreement.SAccPay;
        IdAccPay = loanAgreement.IdAccPay;
        MainDebtAmount = loanAgreement.MainDebtAmount;
        InterestRateSchedule = loanAgreement.InterestRateSchedule;
    }

    // ReSharper disable once UnusedMember.Local
    private LoanAgreement()
    {
    }

    /// <summary>
    ///     Выгодоприобретатель
    /// </summary>
    public Beneficiary? Beneficiary { get; private set; }

    /// <summary>
    ///     ID (code) выгодоприобретателя
    /// </summary>
    [Column("beneficiary_id")]
    public string? BeneficiaryId { get; private set; }

    /// <summary>
    ///     Заемщик
    /// </summary>
    public Customer? Borrower { get; private set; }

    /// <summary>
    ///     Можно ли провести онлайн андеррайтинг
    /// </summary>
    public bool CanOnlineUnderwriting => LoanAmount != null && Beneficiary?.Id != null;

    /// <summary>
    ///     Связанные ДС
    /// </summary>
    public ICollection<MortgagePolicy> Policies { get; private set; } = null!;

    /// <inheritdoc />
    [Column("agreement_number")]
    public string? AgreementNumber { get; private set; }

    /// <inheritdoc />
    [Column("begin_date")]
    public DateTime? BeginDate { get; private set; }

    /// <inheritdoc />
    [Column("begin_interest_rate")]
    public decimal? BeginInterestRate { get; private set; }

    /// <inheritdoc />
    public BranchInfo BranchInfo { get; private set; } = null!;

    /// <inheritdoc />
    [Column("close_date")]
    public DateTime? CloseDate { get; private set; }

    /// <inheritdoc />
    [Column("end_date")]
    public DateTime? EndDate { get; private set; }

    /// <inheritdoc />
    [Column("external_id")]
    public string? ExternalId { get; private set; }

    /// <inheritdoc />
    [Column("extra_interest_rate")]
    public decimal? ExtraInterestRate { get; private set; }

    /// <inheritdoc />
    [Column("id_acc_pay")]
    public string? IdAccPay { get; private set; }

    /// <inheritdoc />
    [Column("insurance_all_borrowers")]
    public bool? InsuranceAllBorrowers { get; private set; }

    /// <inheritdoc />
    [Column("interest_rate")]
    public decimal? InterestRate { get; private set; }

    /// <inheritdoc />
    [Column("interest_rate_change_date")]
    public DateTime? InterestRateChangeDate { get; private set; }

    /// <inheritdoc />
    [Column("interest_rate_schedule")]
    public InterestRateRow[]? InterestRateSchedule { get; private set; }

    /// <inheritdoc />
    [Column("main_debt_amount")]
    public Money? MainDebtAmount { get; private set; }

    /// <inheritdoc />
    [Column("mortgage_info")]
    public MortgageInfo? MortgageInfo { get; private set; }

    /// <inheritdoc />
    [Column("overdue_info")]
    public OverdueInfo? OverdueInfo { get; private set; }

    /// <summary>
    ///     Продукт
    /// </summary>
    [Column("product")]
    public string? Product { get; private set; }

    /// <inheritdoc />
    [Column("repayment_schedule")]
    public RepaymentScheduleRow[]? RepaymentSchedule { get; private set; }

    /// <inheritdoc />
    [Column("s_acc_pay")]
    public string? SAccPay { get; private set; }

    /// <inheritdoc />
    [Column("state")]
    public LoanAgreementState? State { get; private set; }

    /// <inheritdoc />
    [Column("sync_datetime")]
    public DateTime? SyncDateTime { get; private set; }

    /// <inheritdoc />
    [Column("agreement_datetime")]
    public DateTime AgreementDateTime { get; private set; }

    /// <inheritdoc />
    [Column("agreement_period")]
    public int AgreementPeriod { get; private set; }

    /// <inheritdoc />
    [Column("loan_amount")]
    public Money? LoanAmount { get; private set; }

    /// <inheritdoc />
    [Column("loan_type")]
    public string? LoanType { get; private set; }

    /// <summary>
    ///     Заемщик
    /// </summary>
    [Column("borrower_id")]
    private Guid? BorrowerId { get; set; }

    /// <summary>
    ///     Ид информации об отделении или месте выдачи кредита
    /// </summary>
    [Column("branch_info_id")]
    private Guid BranchInfoId { get; }

    /// <summary>
    ///     Изменить основные данные КД
    /// </summary>
    public void ChangeMain(
        int agreementPeriod,
        Money loanAmount,
        DateTime agreementDateTime,
        string? agreementNumber,
        decimal? interestRate,
        decimal? extraInterestRate
    )
    {
        LoanAmount = loanAmount;
        AgreementNumber = agreementNumber;
        AgreementPeriod = agreementPeriod;
        AgreementDateTime = agreementDateTime;
        InterestRate = interestRate;
        ExtraInterestRate = extraInterestRate;
    }

    /// <summary>
    ///     Изменить доп данные КД
    /// </summary>
    public void ChangeExtra(
        string? loanType,
        RepaymentScheduleRow[]? repaymentSchedule,
        bool? insuranceAllBorrowers,
        string? agreementNumber,
        decimal? interestRate,
        decimal? extraInterestRate,
        DateTime? syncDateTime = null)
    {
        LoanType = loanType;
        RepaymentSchedule = repaymentSchedule;
        InsuranceAllBorrowers = insuranceAllBorrowers;
        AgreementNumber = agreementNumber;
        InterestRate = interestRate;
        ExtraInterestRate = extraInterestRate;
        if (syncDateTime.HasValue)
        {
            SyncDateTime = syncDateTime;
        }
    }

    /// <summary>
    ///     Изменить данные КД из внешних источников
    /// </summary>
    public void ExternalChange(
        string? externalId,
        LoanAgreementState? state,
        int agreementPeriod,
        Money loanAmount,
        string agreementNumber,
        DateTime agreementDateTime,
        decimal beginInterestRate,
        decimal interestRate,
        DateTime? interestRateChangeDate,
        Money? mainDebtAmount,
        string? loanType,
        string? product,
        DateTime beginDate,
        DateTime endDate,
        DateTime? closeDate,
        string? idAccPay,
        string? sAccPay,
        RepaymentScheduleRow[]? repaymentSchedule,
        decimal? extraInterestRate,
        DateTime? syncDateTime = null)
    {
        Product = product;
        BeginDate = beginDate;
        EndDate = endDate;
        CloseDate = closeDate;
        State = state;
        IdAccPay = idAccPay;
        SAccPay = sAccPay;
        MainDebtAmount = mainDebtAmount;
        BeginInterestRate = beginInterestRate;
        InterestRateChangeDate = interestRateChangeDate;
        ExternalId = externalId;
        ChangeMain(
            agreementPeriod,
            loanAmount,
            agreementDateTime,
            agreementNumber,
            interestRate,
            extraInterestRate
        );
        ChangeExtra(
            loanType,
            repaymentSchedule,
            InsuranceAllBorrowers,
            agreementNumber,
            interestRate,
            extraInterestRate,
            syncDateTime
        );
    }

    /// <summary>
    ///     Изменение выгодоприобретателя
    /// </summary>
    /// <param name="beneficiary">выгодоприобретатель</param>
    public void ChangeBeneficiary(Beneficiary? beneficiary)
    {
        Beneficiary = beneficiary;
        BeneficiaryId = beneficiary?.Id;
    }

    /// <summary>
    ///     Обновить заемщика
    /// </summary>
    public void ChangeBorrower(Customer? borrower)
    {
        Borrower = borrower;
        BorrowerId = borrower?.Id;
    }

    /// <summary>
    ///     Обновить ИД КД с НА
    /// </summary>
    public void ChangeExternalId(string? externalId) => ExternalId = externalId;

    /// <summary>
    ///     Обновить ID счета для погашения
    /// </summary>
    public void ChangeIdAccPay(string? idAccPay) => IdAccPay = idAccPay;

    /// <summary>
    ///     Изменить процентную ставку
    /// </summary>
    /// <param name="interestRate">Новая проц ставка</param>
    /// <param name="interestRateChangeDate">Дата след изменения проц ставки</param>
    public void ChangeInterestRate(decimal interestRate, DateTime interestRateChangeDate)
    {
        InterestRate = interestRate;
        InterestRateChangeDate = interestRateChangeDate;
    }

    /// <summary>
    ///     Изменить график процентных ставок
    /// </summary>
    public void ChangeInterestRateSchedule(InterestRateRow[]? interestRateSchedule) =>
        InterestRateSchedule = interestRateSchedule;

    /// <summary>
    ///     Изменить остаток ссудной задол
    /// </summary>
    public void ChangeMainDebtAmount(Money? mainDebtAmount) => MainDebtAmount = mainDebtAmount;

    /// <summary>
    ///     Изменить данные об ипотеке
    /// </summary>
    public void ChangeMortgageInfo(MortgageInfo? mortgageInfo) => MortgageInfo = mortgageInfo;

    /// <summary>
    ///     Изменить информация о просроченном платеже
    /// </summary>
    public void ChangeOverdueInfo(OverdueInfo? overdueInfo) => OverdueInfo = overdueInfo;

    /// <summary>
    ///     Изменить график платежей
    /// </summary>
    public void ChangeRepaymentSchedule(RepaymentScheduleRow[]? schedule) => RepaymentSchedule = schedule;
}