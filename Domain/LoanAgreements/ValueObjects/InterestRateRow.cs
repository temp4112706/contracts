﻿using System.Diagnostics.CodeAnalysis;
using Some.Abstractions.LoanAgreements;
using Newtonsoft.Json;

namespace Some.Domain.LoanAgreements.ValueObjects;

/// <summary>
///     Строка измнения процентной ставки
/// </summary>
[SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local")]
public class InterestRateRow : IInterestRateRow
{
    /// <summary>
    ///     Создание строки изменения процентной ставки
    /// </summary>
    [Newtonsoft.Json.JsonConstructor]
    public InterestRateRow(
        [JsonProperty] decimal startInterestRate,
        [JsonProperty] decimal endInterestRate,
        [JsonProperty] DateTime updatedAt)
    {
        StartInterestRate = startInterestRate;
        EndInterestRate = endInterestRate;
        UpdatedAt = updatedAt;
    }

    private InterestRateRow()
    {
    }

    /// <inheritdoc />
    public decimal EndInterestRate { get; private set; }

    /// <inheritdoc />
    public decimal StartInterestRate { get; private set; }

    /// <inheritdoc />
    public DateTime UpdatedAt { get; private set; }
}