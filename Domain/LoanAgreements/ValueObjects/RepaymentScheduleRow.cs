using System.Diagnostics.CodeAnalysis;
using Some.Abstractions.LoanAgreements;
using Some.Domain.Monies;
using Newtonsoft.Json;

namespace Some.Domain.LoanAgreements.ValueObjects;

/// <summary>
///     Строка графика платежей по кредиту
/// </summary>
[SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local")]
public class RepaymentScheduleRow : IRepaymentScheduleRow<Money, RepaymentAmount>
{
    /// <summary>
    ///     Создание строки графика платежа
    /// </summary>
    [Newtonsoft.Json.JsonConstructor]
    public RepaymentScheduleRow(
        [JsonProperty] int order,
        [JsonProperty] DateTime repaymentDate,
        [JsonProperty] RepaymentAmount repaymentAmount,
        [JsonProperty] Money principalAmount
    )
    {
        Order = order;
        RepaymentDate = repaymentDate;
        RepaymentAmount = repaymentAmount;
        PrincipalAmount = principalAmount;
    }

    private RepaymentScheduleRow()
    {
    }

    /// <summary>
    ///     Порядковый номер платежа
    /// </summary>
    public int Order { get; private set; }

    /// <summary>
    ///     Сумма платежа по кредиту
    /// </summary>
    public RepaymentAmount? RepaymentAmount { get; private set; } = null!;

    /// <summary>
    ///     Остаток основного долга (Rest)
    /// </summary>
    public Money PrincipalAmount { get; private set; } = null!;

    /// <summary>
    ///     Дата платежа
    /// </summary>
    public DateTime RepaymentDate { get; private set; }
}