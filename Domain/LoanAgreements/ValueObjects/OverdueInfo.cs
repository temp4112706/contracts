﻿using System.Diagnostics.CodeAnalysis;
using Some.Abstractions.LoanAgreements;
using Some.Domain.Monies;
using Newtonsoft.Json;

namespace Some.Domain.LoanAgreements.ValueObjects;

/// <summary>
///     Информация о просроченном платеже
/// </summary>
[SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local")]
public class OverdueInfo : IOverdueInfo<Money>
{
    /// <summary>
    ///     Создание информации о просроченном платеже
    /// </summary>
    [Newtonsoft.Json.JsonConstructor]
    public OverdueInfo(
        [JsonProperty] int durationDays,
        [JsonProperty] Money overdueSum,
        [JsonProperty] Money overduePercentSum,
        [JsonProperty] Money percentPenaltySum,
        [JsonProperty] Money loanPenaltySum,
        [JsonProperty] Money fineSum,
        [JsonProperty] Money penaltySum)
    {
        DurationDays = durationDays;
        OverdueSum = overdueSum;
        OverduePercentSum = overduePercentSum;
        PercentPenaltySum = percentPenaltySum;
        LoanPenaltySum = loanPenaltySum;
        FineSum = fineSum;
        PenaltySum = penaltySum;
    }

    private OverdueInfo()
    {
    }

    /// <inheritdoc />
    public int DurationDays { get; private set; }

    /// <inheritdoc />
    public Money FineSum { get; private set; } = null!;

    /// <inheritdoc />
    public Money LoanPenaltySum { get; private set; } = null!;

    /// <inheritdoc />
    public Money OverduePercentSum { get; private set; } = null!;

    /// <inheritdoc />
    public Money OverdueSum { get; private set; } = null!;

    /// <inheritdoc />
    public Money PenaltySum { get; private set; } = null!;

    /// <inheritdoc />
    public Money PercentPenaltySum { get; private set; } = null!;
}