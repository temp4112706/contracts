using Some.Abstractions.LoanAgreements;
using Some.Domain.Monies;
using Newtonsoft.Json;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local

namespace Some.Domain.LoanAgreements.ValueObjects;

/// <summary>
///     Сумма платежа по кредиту
/// </summary>
public class RepaymentAmount : IRepaymentAmount<Money>
{
    /// <summary>
    ///     Создание суммы платежа по кредиту
    /// </summary>
    [Newtonsoft.Json.JsonConstructor]
    public RepaymentAmount(
        [JsonProperty] Money full,
        [JsonProperty] Money interest,
        [JsonProperty] Money principal,
        [JsonProperty] Money fee)
    {
        Full = full;
        Interest = interest;
        Principal = principal;
        Fee = fee;
    }

    private RepaymentAmount()
    {
    }

    /// <summary>
    ///     Комиссии и прочие выплаты (Commission)
    /// </summary>
    public Money Fee { get; private set; } = null!;

    /// <summary>
    ///     Полная сумма (Amount)
    /// </summary>
    public Money Full { get; private set; } = null!;

    /// <summary>
    ///     Проценты (Percent)
    /// </summary>
    public Money Interest { get; private set; } = null!;

    /// <summary>
    ///     Основной долг (Debt)
    /// </summary>
    public Money Principal { get; private set; } = null!;

    /// <summary>
    ///     0 рублей
    /// </summary>
    public static RepaymentAmount Default() =>
        new(
            Money.ZeroRub(),
            Money.ZeroRub(),
            Money.ZeroRub(),
            Money.ZeroRub());
}