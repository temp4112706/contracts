﻿using System.Diagnostics.CodeAnalysis;
using Some.Abstractions.LoanAgreements;
using Newtonsoft.Json;

namespace Some.Domain.LoanAgreements.ValueObjects;

/// <summary>
///     Данные об ипотеке
/// </summary>
[SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local")]
public class MortgageInfo : IMortgageInfo
{
    /// <summary>
    ///     Создание данных об ипотеке
    /// </summary>
    [Newtonsoft.Json.JsonConstructor]
    public MortgageInfo(
        [JsonProperty] string? number,
        [JsonProperty] string? poolNumber,
        [JsonProperty] DateTime? poolDate,
        [JsonProperty] string? registrationNumber,
        [JsonProperty] DateTime? registrationDate,
        [JsonProperty] string? holder,
        [JsonProperty] string? issuer)
    {
        Number = number;
        PoolNumber = poolNumber;
        PoolDate = poolDate;
        RegistrationNumber = registrationNumber;
        RegistrationDate = registrationDate;
        Holder = holder;
        Issuer = issuer;
    }

    private MortgageInfo()
    {
    }

    /// <inheritdoc />
    public string? Holder { get; private set; }

    /// <inheritdoc />
    public string? Issuer { get; private set; }

    /// <inheritdoc />
    public string? Number { get; private set; }

    /// <inheritdoc />
    public DateTime? PoolDate { get; private set; }

    /// <inheritdoc />
    public string? PoolNumber { get; private set; }

    /// <inheritdoc />
    public DateTime? RegistrationDate { get; private set; }

    /// <inheritdoc />
    public string? RegistrationNumber { get; private set; }
}