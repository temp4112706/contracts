﻿using System.ComponentModel.DataAnnotations.Schema;
using Ct.Module.Domain.Entities;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local
// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable UnusedType.Global

namespace Some.Domain.LoanAgreements;

/// <summary>
///     Продукт КД
/// </summary>
[Table("loan_agreement_product")]
public class LoanAgreementProduct : BaseEntity<string>
{
    /// <summary>
    ///     Создание объекта
    /// </summary>
    public LoanAgreementProduct(string id, string label) : base(id)
    {
        Label = label;
    }

    /// <inheritdoc />
    // ReSharper disable once UnusedMember.Local
    private LoanAgreementProduct()
    {
    }

    /// <summary>
    ///     Наименование продукта
    /// </summary>
    [Column("label")]
    public string Label { get; private set; } = null!;
}