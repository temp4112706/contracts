namespace Some.Dto.Dictionaries;

/// <summary>
///     DTO: Справочник
/// </summary>
public class DictionaryDto : Dictionary<string, string>
{
    /// <inheritdoc />
    public DictionaryDto(IDictionary<string, string> dict) : base(dict)
    {
    }
}