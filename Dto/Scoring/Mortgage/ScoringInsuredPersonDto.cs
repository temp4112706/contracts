﻿using Some.Abstractions.Scoring.Mortgage;
using Some.Dto.HealthQuestionnaires;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Dto.Scoring.Mortgage;

/// <summary>
///     Данные застрахованного лица
/// </summary>
public record ScoringInsuredPersonDto : ScoringCustomerDto,
    IScoringInsuredPerson<ScoringHealthQuestionnaireDto, ArterialPressureDto>
{
    /// <summary>
    ///     Мед анкета
    /// </summary>
    public ScoringHealthQuestionnaireDto HealthQuestionnaire { get; init; } = null!;

    /// <summary>
    ///     Доля созаемщика в процентах
    /// </summary>
    public decimal LoanShare { get; init; }
}