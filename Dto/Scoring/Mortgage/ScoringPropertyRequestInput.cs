﻿using System.ComponentModel;
using Some.Abstractions.Scoring.Mortgage;
using Some.Dto.Normalizers;

// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Dto.Scoring.Mortgage;

/// <summary>
///     Данные по риску имущества
/// </summary>
public record ScoringPropertyRequestInput : IScoringPropertyRequest<ScoringPropertyDataInput>
{
    /// <summary>
    ///     Данные о страхуемом объекте
    /// </summary>
    [DisplayName("Данные о страхуемом объекте")]
    public ScoringPropertyDataInput Data { get; init; } = null!;

    /// <summary>
    ///     % страхования по имуществу
    /// </summary>
    [DisplayName("% страхования по имуществу")]
    public decimal? LoanShare { get; set; }

    /// <summary>
    ///     Нормализация данных
    /// </summary>
    public void Normalize() =>
        LoanShare = RateNormalizer.Normalize(LoanShare);
}