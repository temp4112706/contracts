﻿using System.ComponentModel;
using Some.Abstractions.Enums;
using Some.Abstractions.Scoring.Mortgage;

// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Dto.Scoring.Mortgage;

/// <summary>
///     Данные страхуемого имущества
/// </summary>
public class
    ScoringInsuredPropertyInput : IScoringInsuredProperty<ScoringTitleRequestInput, ScoringPropertyRequestInput,
        ScoringPropertyDataInput>
{
    /// <summary>
    ///     Данные по риску имущества
    /// </summary>
    public ScoringPropertyRequestInput? PropertyRequest { get; init; }

    /// <summary>
    ///     Данные по риске титула
    /// </summary>
    public ScoringTitleRequestInput? TitleRequest { get; init; }

    /// <summary>
    ///     Тип имущества
    /// </summary>
    [DisplayName("Тип имущества")]
    public PropertyType Type { get; init; }

    /// <summary>
    ///     Нормализация данных
    /// </summary>
    public void Normalize()
    {
        TitleRequest?.Normalize();
        PropertyRequest?.Normalize();
    }
}