using Some.Abstractions.Policies;

// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Dto.Scoring.Mortgage;

/// <summary>
///     Даты ДС
/// </summary>
public record ScoringPolicyDatesDto : IPolicyDates
{
    /// <summary>
    ///     Дата заключения ДС
    /// </summary>
    public DateTime AgreementDateTime { get; init; }

    /// <summary>
    ///     Дата начала ДС
    /// </summary>
    public DateTime CommencementDateTime { get; init; }

    /// <summary>
    ///     Дата завершения ДС
    /// </summary>
    public DateTime ExpiryDateTime { get; init; }
}