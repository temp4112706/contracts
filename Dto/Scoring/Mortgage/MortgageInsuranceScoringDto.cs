﻿using Some.Abstractions.Enums;
using Some.Abstractions.Scoring.Mortgage;
using Some.Dto.Beneficiaries;
using Some.Dto.Common;
using Some.Dto.Companies;
using Some.Dto.HealthQuestionnaires;
using Some.Dto.Insurers;
using Some.Dto.Monies;

// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global

namespace Some.Dto.Scoring.Mortgage;

/// <summary>
///     Данные скоринга
/// </summary>
public record MortgageInsuranceScoringDto : BaseInsuranceScoringDto,
    IMortgageInsuranceScoring<ScoringPolicyDatesDto, ScoringLoanAgreementDto,
        IdentifierInsurerDto, FlatCompanyDto, MoneyDto, PremiumAmountInfoDto,
        MortgageScoringDataDto, ScoringInsuredPersonDto, ScoringCustomerDto,
        ScoringHealthQuestionnaireDto, ArterialPressureDto, ScoringInsuredPropertyDto,
        ScoringTitleRequestDto, ScoringPropertyRequestDto, ScoringPropertyDataDto>
{
    /// <summary>
    ///     Выгодоприобретатель
    /// </summary>
    public IdentifierBeneficiaryDto Beneficiary { get; set; } = null!;

    /// <summary>
    ///     Компания
    /// </summary>
    public FlatCompanyDto Company { get; set; } = null!;

    /// <summary>
    ///     Oбщая страховая сумма
    /// </summary>
    public MoneyDto InsuranceAmount { get; set; } = null!;

    /// <summary>
    ///     Данные скоринга
    /// </summary>
    public MortgageScoringDataDto InsuranceData { get; set; } = null!;

    /// <summary>
    ///     Cтраховая компания
    /// </summary>
    public IdentifierInsurerDto Insurer { get; set; } = null!;

    /// <summary>
    ///     Oбщее комиссионное вознаграждение
    /// </summary>
    public MoneyDto InsurerCommission { get; set; } = null!;

    /// <summary>
    ///     offline или online андерайтинг
    /// </summary>
    public bool IsOnlineUnderwriting { get; init; }

    /// <summary>
    ///     Данные КД
    /// </summary>
    public ScoringLoanAgreementDto LoanAgreement { get; set; } = null!;

    /// <summary>
    ///     Даты ДС
    /// </summary>
    public ScoringPolicyDatesDto PolicyDates { get; set; } = null!;

    /// <summary>
    ///     Oбщая страховая премия
    /// </summary>
    public MoneyDto PremiumAmount { get; set; } = null!;

    /// <summary>
    ///     Страховые премии в разрезе
    /// </summary>
    public PremiumAmountInfoDto? PremiumAmountInfo { get; set; } = null!;

    /// <summary>
    ///     Набор рисков
    /// </summary>
    public InsuranceRisk[] Risks { get; set; } = null!;
}