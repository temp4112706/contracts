﻿using System.ComponentModel;
using Some.Abstractions.Enums;
using Some.Abstractions.Scoring.Mortgage;

// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Dto.Scoring.Mortgage;

/// <summary>
///     Данные о страхуемом объекте
/// </summary>
public class ScoringPropertyDataInput : IScoringPropertyData
{
    /// <summary>
    ///     Материал конструкции дома
    /// </summary>
    [DisplayName("Материал конструкции дома")]
    public ConstructionMaterial ConstructionMaterial { get; init; }

    /// <summary>
    ///     Материал перекрытий
    /// </summary>
    [DisplayName("Материал перекрытий")]
    public InterfloorOverlapMaterial InterfloorOverlapMaterial { get; init; }

    /// <summary>
    ///     Год постройки
    /// </summary>
    [DisplayName("Год постройки")]
    public int YearOfConstruction { get; init; }
}