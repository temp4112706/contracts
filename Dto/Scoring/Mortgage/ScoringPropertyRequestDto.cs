﻿using Some.Abstractions.Scoring.Mortgage;

// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Dto.Scoring.Mortgage;

/// <summary>
///     Данные по риску имущества
/// </summary>
public record ScoringPropertyRequestDto : IScoringPropertyRequest<ScoringPropertyDataDto>
{
    /// <summary>
    ///     Данные о страхуемом объекте
    /// </summary>
    public ScoringPropertyDataDto Data { get; init; }

    /// <summary>
    ///     % страхования по имуществу
    /// </summary>
    public decimal? LoanShare { get; init; }
}