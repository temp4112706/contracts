﻿using Some.Abstractions.Scoring.Mortgage;
using Some.Dto.HealthQuestionnaires;
using Some.Dto.Normalizers;

// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Dto.Scoring.Mortgage;

/// <summary>
///     Данные застрахованного лица
/// </summary>
public record ScoringInsuredPersonInput : ScoringCustomerInput,
    IScoringInsuredPerson<ScoringHealthQuestionnaireInput, ArterialPressureInput>
{
    /// <summary>
    ///     Доля созаемщика в процентах
    /// </summary>
    public decimal LoanShare { get; set; }

    /// <summary>
    ///     Мед анкеты
    /// </summary>
    public ScoringHealthQuestionnaireInput HealthQuestionnaire { get; init; } = null!;

    /// <summary>
    ///     Нормализация данных
    /// </summary>
    public void Normalize()
    {
        LoanShare = RateNormalizer.Normalize(LoanShare);
    }
}