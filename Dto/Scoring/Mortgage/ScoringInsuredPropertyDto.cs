﻿using Some.Abstractions.Enums;
using Some.Abstractions.Scoring.Mortgage;

// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Dto.Scoring.Mortgage;

/// <summary>
///     Данные страхуемого имущества
/// </summary>
public record
    ScoringInsuredPropertyDto : IScoringInsuredProperty<ScoringTitleRequestDto, ScoringPropertyRequestDto,
        ScoringPropertyDataDto>
{
    /// <summary>
    ///     Данные по риску имущества
    /// </summary>
    public ScoringPropertyRequestDto? PropertyRequest { get; init; }

    /// <summary>
    ///     Данные по риску титула
    /// </summary>
    public ScoringTitleRequestDto? TitleRequest { get; init; }

    /// <summary>
    ///     Тип недвижимости
    /// </summary>
    public PropertyType Type { get; init; }
}