﻿using System.ComponentModel;
using Some.Abstractions.Enums;
using Some.Dto.Mirs;

namespace Some.Dto.Scoring.Mortgage;

/// <summary>
/// </summary>
/// <param name="InsurerIds">ИД СК</param>
/// <param name="Risks">Набор рисков</param>
/// <param name="Insurant">Страхователь</param>
/// <param name="InsuredPersons">Страхуемые лица</param>
/// <param name="InsuredProperties">Информация об объекте имущества</param>
/// <param name="LoanAgreement">Данные по КД</param>
public record MortgageInsuranceScoringInput(
    [property: DisplayName("ИД СК")] InsurerId[] InsurerIds,
    [property: DisplayName("Набор рисков")]
    InsuranceRisk[] Risks,
    [property: DisplayName("Даты ДС")]
    MirPolicyDatesInput PolicyDates,
    [property: DisplayName("Страхователь")]
    ScoringCustomerInput Insurant,
    [property: DisplayName("Страхуемые лица")]
    ScoringInsuredPersonInput[] InsuredPersons,
    [property: DisplayName("Информация об объекте имущества")]
    ScoringInsuredPropertyInput[] InsuredProperties,
    [property: DisplayName("Основные данные по КД")]
    ScoringLoanAgreementInput LoanAgreement)
{
    /// <summary>
    ///     Нормализация
    /// </summary>
    public void Normalize()
    {
        foreach (var insuredPersonInput in InsuredPersons)
        {
            insuredPersonInput.Normalize();
        }

        foreach (var insuredPropertyInput in InsuredProperties)
        {
            insuredPropertyInput.Normalize();
        }

        LoanAgreement.Normalize();
    }
}