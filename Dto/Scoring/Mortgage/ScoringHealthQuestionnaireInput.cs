using System.ComponentModel;
using Some.Abstractions.Scoring.Mortgage;
using Some.Dto.HealthQuestionnaires;

// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Dto.Scoring.Mortgage;

/// <summary>
///     Данные мед анкеты
/// </summary>
public record ScoringHealthQuestionnaireInput : IScoringHealthQuestionnaire<ArterialPressureInput>
{
    /// <summary>
    ///     Давление
    /// </summary>
    [DisplayName("Давление")]
    public ArterialPressureInput ArterialPressure { get; init; } = null!;

    /// <summary>
    ///     Рост
    /// </summary>
    [DisplayName("Рост")]
    public float Height { get; init; }

    /// <summary>
    ///     Вес
    /// </summary>
    [DisplayName("Вес")]
    public float Weight { get; init; }
}