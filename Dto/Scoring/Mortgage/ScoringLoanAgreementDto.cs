using Some.Abstractions.Scoring.Mortgage;
using Some.Dto.Monies;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global

// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Dto.Scoring.Mortgage;

/// <summary>
///     Данные КД в скоринге
/// </summary>
public record ScoringLoanAgreementDto : IScoringLoanAgreement<MoneyDto>
{
    /// <summary>
    ///     Cрок кредита
    /// </summary>
    public int AgreementPeriod { get; init; }

    /// <summary>
    ///     Ставка кредита
    /// </summary>
    public decimal? InterestRate { get; init; }

    /// <summary>
    ///     Oстаток по кредиту
    /// </summary>
    public MoneyDto LoanAmount { get; init; } = null!;
}