using Some.Abstractions.Enums;
using Some.Abstractions.Scoring.Mortgage;

// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Dto.Scoring.Mortgage;

/// <summary>
///     Данные клиента
/// </summary>
public record ScoringCustomerInput : IScoringCustomer
{
    /// <summary>
    ///     Дата рождения
    /// </summary>
    public DateTime BirthDate { get; init; }

    /// <summary>
    ///     Пол
    /// </summary>
    public Gender Gender { get; init; }
    
    /// <inheritdoc />
    public string OccupationPosition { get; init; } = null!;
}