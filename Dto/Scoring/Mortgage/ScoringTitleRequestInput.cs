﻿using System.ComponentModel;
using Some.Abstractions.Scoring.Mortgage;
using Some.Dto.Normalizers;

// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Dto.Scoring.Mortgage;

/// <summary>
///     Данные по риске титула
/// </summary>
public record ScoringTitleRequestInput : IScoringTitleRequest
{
    /// <summary>
    ///     % страхования по титулу
    /// </summary>
    [DisplayName("% страхования по титулу")]
    public decimal? LoanShare { get; set; }

    /// <summary>
    ///     Нормализация данных
    /// </summary>
    public void Normalize() =>
        LoanShare = RateNormalizer.Normalize(LoanShare);
}