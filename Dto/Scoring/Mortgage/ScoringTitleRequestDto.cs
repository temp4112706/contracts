﻿using Some.Abstractions.Scoring.Mortgage;

// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Dto.Scoring.Mortgage;

/// <summary>
///     Данные по риске титула
/// </summary>
public record ScoringTitleRequestDto : IScoringTitleRequest
{
    /// <summary>
    ///     % страхования по титулу
    /// </summary>
    public decimal? LoanShare { get; init; }
}