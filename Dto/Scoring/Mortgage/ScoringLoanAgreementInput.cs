﻿using System.ComponentModel;
using Some.Abstractions.Enums;
using Some.Abstractions.Scoring.Mortgage;
using Some.Dto.Monies;
using Some.Dto.Normalizers;

namespace Some.Dto.Scoring.Mortgage;

/// <param name="BeneficiaryId">ИД выгодоприобретателя</param>
/// <param name="LoanAmount">Oстаток по кредиту</param>
/// <param name="AgreementPeriod">Cрок кредита</param>
public record ScoringLoanAgreementInput(
    [property: DisplayName("ИД выгодоприобретателя")]
    BeneficiaryId BeneficiaryId,
    [property: DisplayName("Cрок кредита")]
    int AgreementPeriod,
    [property: DisplayName("Oстаток по кредиту")]
    MoneyInput LoanAmount
) : IScoringLoanAgreement<MoneyInput>
{
    /// <summary>
    ///     Cтавка по кредитному договору
    /// </summary>
    [DisplayName("Cтавка по кредитному договору")]
    public decimal? InterestRate { get; set; }

    /// <summary>
    ///     Нормализация
    /// </summary>
    public void Normalize()
    {
        if (InterestRate.HasValue)
        {
            InterestRate = RateNormalizer.Normalize(InterestRate.Value);
        }
    }
}