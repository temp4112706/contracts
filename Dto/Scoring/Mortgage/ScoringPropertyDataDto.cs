﻿using Some.Abstractions.Enums;
using Some.Abstractions.Scoring.Mortgage;

// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Dto.Scoring.Mortgage;

/// <summary>
///     Данные о страхуемом объекте
/// </summary>
public class ScoringPropertyDataDto : IScoringPropertyData
{
    /// <summary>
    ///     Материал конструкции дома
    /// </summary>
    public ConstructionMaterial ConstructionMaterial { get; init; }

    /// <summary>
    ///     Материал перекрытий
    /// </summary>
    public InterfloorOverlapMaterial InterfloorOverlapMaterial { get; init; }

    /// <summary>
    ///     Год постройки
    /// </summary>
    public int YearOfConstruction { get; init; }
}