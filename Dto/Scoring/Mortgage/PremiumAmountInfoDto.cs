﻿using Some.Abstractions.Scoring.Mortgage;
using Some.Dto.Monies;

namespace Some.Dto.Scoring.Mortgage;

/// <summary>
///     Страховая премия в скоринге
/// </summary>
public record PremiumAmountInfoDto : IPremiumAmountInfo<MoneyDto>
{
    /// <inheritdoc />
    public MoneyDto[]? LifeInsurance { get; set; }

    /// <inheritdoc />
    public MoneyDto[]? PropertyInsurance { get; set; }

    /// <inheritdoc />
    public MoneyDto[]? TitleInsurance { get; set; }
}