﻿using Some.Abstractions.Scoring.Mortgage;
using Some.Dto.HealthQuestionnaires;

// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global

namespace Some.Dto.Scoring.Mortgage;

/// <summary>
///     Данные скоринга
/// </summary>
public class MortgageScoringDataDto : IMortgageScoringData<ScoringInsuredPersonDto, ScoringHealthQuestionnaireDto,
    ScoringCustomerDto, ArterialPressureDto, ScoringInsuredPropertyDto, ScoringTitleRequestDto,
    ScoringPropertyRequestDto, ScoringPropertyDataDto>
{
    /// <summary>
    ///     Страхователь
    /// </summary>
    public ScoringCustomerDto Insurant { get; init; } = null!;

    /// <summary>
    ///     Застрахованные лица
    /// </summary>
    public ScoringInsuredPersonDto[] InsuredPersons { get; set; } = null!;

    /// <summary>
    ///     Застрахованное имущество
    /// </summary>
    public ScoringInsuredPropertyDto[] InsuredProperties { get; set; } = null!;
}