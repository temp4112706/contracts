using Some.Abstractions.Scoring.Mortgage;
using Some.Dto.HealthQuestionnaires;

// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Dto.Scoring.Mortgage;

/// <summary>
///     Данные мед анкеты
/// </summary>
public record ScoringHealthQuestionnaireDto : IScoringHealthQuestionnaire<ArterialPressureDto>
{
    /// <summary>
    ///     Давление
    /// </summary>
    public ArterialPressureDto ArterialPressure { get; init; } = null!;

    /// <summary>
    ///     Рост
    /// </summary>
    public float Height { get; init; }

    /// <summary>
    ///     Вес
    /// </summary>
    public float Weight { get; init; }
    
}