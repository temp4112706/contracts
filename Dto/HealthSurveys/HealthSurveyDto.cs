using Some.Abstractions.HealthSurveys;

namespace Some.Dto.HealthSurveys;

/// <summary>
///     DTO: Медицинское обследование
/// </summary>
public record HealthSurveyDto : EntityDto<Guid>, IHealthSurvey<HealthFacilityDto>
{
    /// <summary>
    ///     Дата обследования
    /// </summary>
    public DateTime VisitDate { get; init; }

    /// <summary>
    ///     Медицинское учреждение
    /// </summary>
    public HealthFacilityDto HealthFacility { get; init; } = null!;
}