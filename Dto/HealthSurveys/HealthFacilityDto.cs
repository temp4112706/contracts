using Some.Abstractions.HealthSurveys;

namespace Some.Dto.HealthSurveys;

/// <summary>
///     DTO: Медицинское учреждение
/// </summary>
public record HealthFacilityDto : IHealthFacility
{
    /// <summary>
    ///     Название медицинского учреждения
    /// </summary>
    public string? Name { get; init; }

    /// <summary>
    ///     Телефон медицинского учреждения
    /// </summary>
    public string? Phone { get; init; }

    /// <summary>
    ///     Адрес медицинского учреждения
    /// </summary>
    public string? Address { get; init; }
}