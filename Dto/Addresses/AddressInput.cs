using System.ComponentModel;
using Some.Abstractions.Addresses;

// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global

namespace Some.Dto.Addresses;

/// <summary>
///     Данные для добавления адреса
/// </summary>
public record AddressInput : IAddress
{
    /// <summary>
    ///     Квартира
    /// </summary>
    [DisplayName("Квартира")]
    public string? Apartment { get; init; }

    /// <summary>
    ///     Строение
    /// </summary>
    [DisplayName("Строение")]
    public string? Building { get; init; }

    /// <summary>
    ///     Город
    /// </summary>
    [DisplayName("Город")]
    public string? City { get; init; }

    /// <summary>
    ///     Район
    /// </summary>
    [DisplayName("Район")]
    public string? District { get; init; }

    /// <summary>
    ///     Дом
    /// </summary>
    [DisplayName("Дом")]
    public string? House { get; init; }

    /// <summary>
    ///     Внутригородская территория
    /// </summary>
    [DisplayName("Внутригородская территория")]
    public string? IntracityTerritory { get; init; }

    /// <summary>
    ///     Населенный пункт
    /// </summary>
    [DisplayName("Населенный пункт")]
    public string? Locality { get; init; }

    /// <summary>
    ///     Почтовый индекс
    /// </summary>
    [DisplayName("Почтовый индекс")]
    public string? Postcode { get; init; }

    /// <summary>
    ///     Регион
    /// </summary>
    [DisplayName("Регион")]
    public string? Region { get; init; }

    /// <summary>
    ///     Комната
    /// </summary>
    [DisplayName("Комната")]
    public string? Room { get; init; }

    /// <summary>
    ///     Улица
    /// </summary>
    [DisplayName("Улица")]
    public string? Street { get; init; }

    /// <summary>
    ///     Код страны ISO (например RU)
    /// </summary>
    [DisplayName("Код страны")]
    public string CountryCode { get; init; } = null!;

    /// <summary>
    ///     Полная строка адреса
    /// </summary>
    [DisplayName("Полная строка адреса")]
    public string FullAddress { get; init; } = null!;
}