using Some.Abstractions.Addresses;

// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Dto.Addresses;

/// <summary>
///     DTO: Данные адреса
/// </summary>
public record AddressDto : FlatAddressDto, IAddress
{
    /// <summary>
    ///     Квартира
    /// </summary>
    public string? Apartment { get; init; }

    /// <summary>
    ///     Строение
    /// </summary>
    public string? Building { get; init; }

    /// <summary>
    ///     Город
    /// </summary>
    public string? City { get; init; }

    /// <summary>
    ///     Район
    /// </summary>
    public string? District { get; init; }

    /// <summary>
    ///     Дом
    /// </summary>
    public string? House { get; init; }

    /// <summary>
    ///     Внутригородская территория
    /// </summary>
    public string? IntracityTerritory { get; init; }

    /// <summary>
    ///     Населенный пункт
    /// </summary>
    public string? Locality { get; init; }

    /// <summary>
    ///     Почтовый индекс
    /// </summary>
    public string? Postcode { get; init; }

    /// <summary>
    ///     Регион
    /// </summary>
    public string? Region { get; init; }

    /// <summary>
    ///     Комната
    /// </summary>
    public string? Room { get; init; }

    /// <summary>
    ///     Улица
    /// </summary>
    public string? Street { get; init; }
}