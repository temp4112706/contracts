using Some.Abstractions.Addresses;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global

namespace Some.Dto.Addresses;

/// <summary>
///     DTO: Данные адреса с основными полями
/// </summary>
public record FlatAddressDto : EntityDto<Guid>, IScalarAddress
{
    /// <summary>
    ///     Код страны ISO (например RU)
    /// </summary>
    public string CountryCode { get; init; } = null!;

    /// <summary>
    ///     Полная строка адреса
    /// </summary>
    public string FullAddress { get; init; } = null!;
}