
// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global

using Some.Abstractions.Attachments;
using Some.Abstractions.Enums;

namespace Some.Dto.Attachments;

/// <summary>
///     DTO: Прикрепленный файл к заявке
/// </summary>
public record AttachmentDto : FlatAttachmentDto, IAttachment
{
    /// <summary>
    ///     Добавлен
    /// </summary>
    public DateTime CreatedAt { get; init; }

    /// <summary>
    ///     Размер файла
    /// </summary>
    public string Size { get; init; } = null!;

    /// <summary>
    ///     Автор файла
    /// </summary>
    public string Author { get; init; } = null!;

    /// <summary>
    ///     Хеш файла
    /// </summary>
    public string Hash { get; init; } = null!;

    /// <summary>
    ///     Описание файла
    /// </summary>
    public string? Description { get; init; }

    /// <summary>
    ///     Действие при вложении
    /// </summary>
    public string? Action { get; init; }

    /// <summary>
    ///     Тип сущности
    /// </summary>
    public FileEntityType EntityType { get; init; }

    /// <summary>
    ///     ИД сущности
    /// </summary>
    public Guid EntityId { get; init; }

    /// <inheritdoc />
    public string ExternalId { get; init; } = null!;

    /// <summary>
    ///     Временный?
    /// </summary>
    public bool? Temp { get; init; }
        
    /// <summary>
    ///     Удаляемый?
    /// </summary>
    public bool? Removable { get; init; }
}