﻿using Some.Abstractions.Enums;
using Some.Abstractions.Attachments;

namespace Some.Dto.Attachments;

/// <summary>
///     DTO: Данные о вложении при удалении
/// </summary>
public record RemoveAttachmentInput : EntityDto<Guid>, IRemoveAttachment
{
    /// <summary>
    ///     Тип сущности
    /// </summary>
    public FileEntityType EntityType { get; init; }
        
    /// <summary>
    ///     Ид сущности
    /// </summary>
    public Guid EntityId { get; init; }
}