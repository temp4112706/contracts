namespace Some.Dto.Attachments;

/// <inheritdoc />
public record FileInput(string Name, Stream Stream) : IFileInput
{
    /// <inheritdoc />
    public Stream OpenReadStream() => Stream;
}