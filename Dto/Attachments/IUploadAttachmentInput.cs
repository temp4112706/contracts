namespace Some.Dto.Attachments;

public interface IUploadAttachmentInput
{
    /// <summary>
    ///     Данные о существующем вложении
    /// </summary>
    ExistingAttachmentInfoInput? ExistingFile { get; }

    /// <summary>
    ///     Данные файла
    /// </summary>
    UploadAttachmentInfoInput? FileInfo { get; }

    /// <summary>
    ///     Пользовательский файл
    /// </summary>
    IFileInput? CustomFile { get; }
}