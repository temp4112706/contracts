﻿using System.ComponentModel;
using Some.Abstractions.Enums;
using Some.Abstractions.Attachments;

namespace Some.Dto.Attachments;

/// <summary>
///     DTO: Данные о вложении при получении ссылки
/// </summary>
public record GetAttachmentLinkInput : EntityDto<Guid>, IGetAttachmentLink
{
    /// <summary>
    ///     Тип сущности
    /// </summary>
    [DisplayName("Тип сущности")]
    public FileEntityType EntityType { get; init; }
        
    /// <summary>
    ///     Ид сущности
    /// </summary>
    [DisplayName("Ид сущности")]
    public Guid EntityId { get; init; }
}