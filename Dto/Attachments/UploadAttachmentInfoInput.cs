﻿using System.ComponentModel;

namespace Some.Dto.Attachments;

/// <summary>
///     DTO: Данные о вложении при загрузке
/// </summary>
public record UploadAttachmentInfoInput
{
    /// <summary>
    ///     Описание
    /// </summary>
    [DisplayName("Описание")]
    public string? Description { get; init; }

    /// <summary>
    ///     Действие
    /// </summary>
    [DisplayName("Действие")]
    public string? Action { get; init; }
}