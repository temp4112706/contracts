namespace Some.Dto.Attachments;

/// <summary>
///     Данные для загрузки файлов
/// </summary>
public interface IFileInput
{
    /// <summary>
    ///     Название файла
    /// </summary>
    string Name { get; }

    /// <summary>
    ///     Данные файла
    /// </summary>
    /// <returns></returns>
    Stream OpenReadStream();
}