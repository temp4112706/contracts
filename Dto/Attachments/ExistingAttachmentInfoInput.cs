﻿using System.ComponentModel;

namespace Some.Dto.Attachments;

/// <summary>
///     DTO: Данные о существующем вложении при загрузке
/// </summary>
public record ExistingAttachmentInfoInput
{
    /// <summary>
    ///     Название файла
    /// </summary>
    [DisplayName("Название файла")]
    public string? FileName { get; init; }
}