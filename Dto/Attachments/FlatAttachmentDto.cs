﻿namespace Some.Dto.Attachments;

/// <summary>
///     DTO: Вложение
/// </summary>
public record FlatAttachmentDto : EntityDto<Guid>
{
        
    /// <summary>
    ///     Ссылка на скачивания вложения
    /// </summary>
    public string? DownloadUrl { get; set; }

    /// <summary>
    ///     Название файла
    /// </summary>
    public string Name { get; init; } = null!;
}