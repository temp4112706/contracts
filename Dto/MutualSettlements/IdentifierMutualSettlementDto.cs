using Some.Abstractions.MutualSettlements;

namespace Some.Dto.MutualSettlements;

/// <summary>
///     DTO: АС с основными полями
/// </summary>
public class IdentifierMutualSettlementDto : IIdentifierMutualSettlement
{
    /// <summary>
    ///     ИД АС
    /// </summary>
    public Guid Id { get; init; }

    /// <summary>
    ///     Номер Ас
    /// </summary>
    public string Number { get; init; } = null!;

    /// <summary>
    ///     Отчетный период АС
    /// </summary>
    public DateTime ReportDate { get; init; }
}