﻿using System.Diagnostics.CodeAnalysis;
using Some.Abstractions.Enums;
using Some.Abstractions.MutualSettlements;
using Some.Abstractions.Policies;
using Some.Dto.Common;
using Some.Dto.Monies;
using Some.Dto.Policies;

namespace Some.Dto.MutualSettlements;

/// <summary>
///     DTO: АС - ДС
/// </summary>
[SuppressMessage("ReSharper", "UnusedMember.Global")]
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
[SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Global")]
public record BaseMutualSettlementRowDto<TPolicy> : EntityDto<Guid>,
    IMutualSettlementRow<ActualPaymentDto, MoneyDto>
    where TPolicy : IScalarPolicy
{
    /// <summary>
    ///     Период ДС
    /// </summary>
    public InsurancePeriodDto Period { get; set; } = default!;

    /// <summary>
    ///     ДС
    /// </summary>
    public TPolicy Policy { get; set; } = default!;

    /// <summary>
    ///     Фактическая оплата страховой премии
    /// </summary>
    public ActualPaymentDto ActualPayment { get; set; } = null!;

    /// <summary>
    ///     Размер вознаграждения
    /// </summary>
    public MoneyDto AmountOfReward { get; set; } = null!;

    /// <summary>
    ///     Данные о страховой премии в СК
    /// </summary>
    public ActualPaymentDto? InsurerActualPayment { get; set; } = null;

    /// <summary>
    ///     Комиссионное вознаграждение, руб в СК
    /// </summary>
    public MoneyDto? InsurerAmountOfReward { get; set; } = null;

    /// <summary>
    ///     Комментарий Банк
    /// </summary>
    public string? CompanyComment { get; set; }

    /// <summary>
    ///     Статус Банк
    /// </summary>
    public MutualSettlementPolicyCompanyStatus CompanyStatus { get; set; }

    /// <summary>
    ///     Комментарий СК
    /// </summary>
    public string? InsurerComment { get; set; }

    /// <summary>
    ///     Комиссионное вознаграждение, % в СК
    /// </summary>
    public decimal? InsurerPercentOfReward { get; set; } = null;

    /// <summary>
    ///     Номер ДС в СК
    /// </summary>
    public string? InsurerPolicyNumber { get; set; } = null;

    /// <summary>
    ///     Статус СК
    /// </summary>
    public MutualSettlementPolicyInsurerStatus InsurerStatus { get; set; }

    /// <summary>
    ///     Партнерская сделка?
    /// </summary>
    public bool Partner { get; set; }

    /// <summary>
    ///     Процент вознаграждения
    /// </summary>
    public decimal PercentOfReward { get; set; }
}