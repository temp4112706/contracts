﻿using Some.Abstractions.Enums;

namespace Some.Dto.MutualSettlements;

/// <summary>
/// </summary>
/// <param name="InsurerId">СК Id</param>
/// <param name="Message">Сообщение о ошибке</param>
public record CreateMutualSettlementErrorDto(InsurerId InsurerId, string Message);