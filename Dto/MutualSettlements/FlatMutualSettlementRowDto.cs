﻿using Some.Dto.Policies;

namespace Some.Dto.MutualSettlements;

/// <summary>
///     DTO: АС - ДС
/// </summary>
public record FlatMutualSettlementRowDto : BaseMutualSettlementRowDto<MsPolicyDto>
{
        
}