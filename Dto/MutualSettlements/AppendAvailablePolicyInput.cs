using Some.Abstractions.MutualSettlements.Actions;

namespace Some.Dto.MutualSettlements;

/// <summary>
///     Данные для добавления ДС к АС
/// </summary>
public class AppendAvailablePolicyInput : IAppendAvailablePolicyInput
{
    /// <summary>
    ///     ИД ДС
    /// </summary>
    public Guid PolicyId { get; init; }

    /// <summary>
    ///     ИД Периода
    /// </summary>
    public Guid PolicyPeriodId { get; init; }

    /// <summary>
    ///     Оплаченный период
    /// </summary>
    public int? PaymentPeriod { get; init; }
}