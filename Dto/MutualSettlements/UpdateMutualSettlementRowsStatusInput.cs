﻿using Some.Abstractions.Enums;

namespace Some.Dto.MutualSettlements;

/// <summary>
///     Данные для обновления статуса Банка записей АС
/// </summary>
public record UpdateMutualSettlementRowsStatusInput : BaseUpdateMutualSettlementRowsStatusInput
{
    /// <summary>
    ///     Новый статус Банка
    /// </summary>
    public MutualSettlementPolicyCompanyStatus Status { get; set; }
}