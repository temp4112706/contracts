using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;
using Some.Dto.Beneficiaries;
using Some.Abstractions.Enums;
using Some.Dto.Insurers;

namespace Some.Dto.MutualSettlements;

/// <summary>
///     Данные ДС для добавления в АС
/// </summary>
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
public record AvailablePolicyDto : EntityDto<Guid>
{
    /// <summary>
    ///     Номер ДС
    /// </summary>
    public string Number { get; init; } = null!;

    /// <summary>
    ///     Риски ДС
    /// </summary>
    public InsuranceRisk[] Risks { get; init; } = null!;

    /// <summary>
    ///     СК
    /// </summary>
    public IdentifierInsurerDto Insurer { get; init; } = null!;

    /// <summary>
    ///     Номер КД
    /// </summary>
    public string LoanAgreementNumber { get; init; } = null!;

    /// <summary>
    ///     Выгодоприобретатель
    /// </summary>
    public IdentifierBeneficiaryDto? Beneficiary { get; init; } = null!;

    /// <summary>
    ///     Доступные периоды для выбора
    /// </summary>
    public ICollection<SelectPeriod> Periods { get; init; } = null!;

    /// <summary>
    ///     Ошибки
    /// </summary>
    public ICollection<ValidationError>? Errors { get; init; }

    /// <summary>
    ///     Оплаченный период для выбора
    /// </summary>
    public record SelectPeriod
    {
        /// <summary>
        ///     ИД периода
        /// </summary>
        public Guid Id { get; init; }

        /// <summary>
        ///     Номер периода
        /// </summary>
        public int Number { get; init; }
            
        /// <summary>
        ///     Начало периода
        /// </summary>
        public DateTime StartDate { get; init; }
            
        /// <summary>
        ///     Конец периода
        /// </summary>
        public DateTime EndDate { get; init; }
            
        /// <summary>
        ///     Тип оплаты
        /// </summary>
        public PolicyActualPremiumType Type { get;init; }

        /// <summary>
        ///     Фактические оплаты
        /// </summary>
        public ICollection<ActualPayment> Payments { get; init; } = null!;

        /// <summary>
        ///     Фактическая оплата
        /// </summary>
        public record ActualPayment
        {
            /// <summary>
            ///     Период оплаты
            /// </summary>
            public int? Period { get; init; }

            /// <summary>
            ///     Ошибки
            /// </summary>
            public ICollection<PeriodValidationError>? Errors { get; init; }
        }

        /// <summary>
        ///     Ошибки валидации при добавлении периода ДС к АС
        /// </summary>
        [Newtonsoft.Json.JsonConverter(typeof(Newtonsoft.Json.Converters.StringEnumConverter))]
        [JsonConverter(typeof(JsonStringEnumMemberConverter))]
        public enum PeriodValidationError
        {
            /// <summary>
            ///     Присутствует в другом АС
            /// </summary>
            [EnumMember(Value = "existsInAnyMs")]
            ExistsInAnyMs,
                
            /// <summary>
            ///     Присутствует в текущем АС
            /// </summary>
            [EnumMember(Value = "existsInCurrentMs")]
            ExistsInCurrentMs,
                
            /// <summary>
            ///     Не оплачен
            /// </summary>
            [EnumMember(Value = "notPaid")]
            NotPaid,
        }
    }

    /// <summary>
    ///     Ошибки валидации при добавлении ДС к АС
    /// </summary>
    [Newtonsoft.Json.JsonConverter(typeof(Newtonsoft.Json.Converters.StringEnumConverter))]
    [JsonConverter(typeof(JsonStringEnumMemberConverter))]
    public enum ValidationError
    {
        /// <summary>
        ///     Разные СК
        /// </summary>
        [EnumMember(Value = "differentInsurer")]
        DifferentInsurer,
    }
}