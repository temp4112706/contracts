﻿namespace Some.Dto.MutualSettlements;

/// <summary>
///     Базовый класс данных обновления статуса записей АС
/// </summary>
public abstract record BaseUpdateMutualSettlementRowsStatusInput
{
    /// <summary>
    ///     ИД записей
    /// </summary>
    public IEnumerable<Guid> MutualSettlementRowIds { get; set; } = null!;
}