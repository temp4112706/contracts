using Some.Abstractions.MutualSettlements.Actions;

namespace Some.Dto.MutualSettlements;

/// <summary>
///     Данные для добавления списка ДС к АС
/// </summary>
public record AppendAvailablePoliciesInput : IAppendAvailablePoliciesInput<AppendAvailablePolicyInput>
{
    /// <summary>
    ///     Список ДС для добавления к АС
    /// </summary>
    public ICollection<AppendAvailablePolicyInput> Policies { get; init; } = null!;
}