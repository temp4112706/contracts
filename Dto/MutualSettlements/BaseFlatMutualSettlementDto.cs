﻿using System.Diagnostics.CodeAnalysis;
using Some.Abstractions.MutualSettlements;
using Some.Abstractions.Enums;
using Some.Dto.Monies;

namespace Some.Dto.MutualSettlements;

/// <summary>
///     DTO: АС с основными полями
/// </summary>
[SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Global")]
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
public abstract record BaseFlatMutualSettlementDto<TInsurer> : TrackableEntityDto<Guid>,
    IScalarMutualSettlement<MoneyDto, TInsurer>
{
    /// <summary>
    ///     Количество записей
    /// </summary>
    public long RowsCount { get; set; }

    /// <summary>
    ///     Номер АС
    /// </summary>
    public string Number { get; set; } = null!;

    /// <summary>
    ///     Статус АС
    /// </summary>
    public MutualSettlementStatus Status { get; set; }

    /// <summary>
    ///     Отчетная дата
    /// </summary>
    public DateTime ReportDate { get; set; }

    /// <summary>
    ///     № агентского договора
    /// </summary>
    public string AgencyAgreementNumber { get; set; } = null!;

    /// <summary>
    ///     Дата агентского договора
    /// </summary>
    public DateTime AgencyAgreementDate { get; set; }

    /// <summary>
    ///     СК
    /// </summary>
    public TInsurer Insurer { get; set; } = default!;

    /// <summary>
    ///     Общий размер оплаченной страховой премии
    /// </summary>
    public MoneyDto ActualPaymentAmount { get; set; } = null!;

    /// <summary>
    ///     Общий размер вознаграждения
    /// </summary>
    public MoneyDto AmountOfReward { get; set; } = null!;
}