﻿namespace Some.Dto.MutualSettlements;

/// <summary>
///     Данные обновления комментария к записи АС
/// </summary>
public class UpdateMutualSettlementRowCommentInput
{
    /// <summary>
    ///     ИД записи
    /// </summary>
    public Guid MutualSettlementRowId { get; set; }

    /// <summary>
    ///     Комментарий
    /// </summary>
    public string? Comment { get; set; }
}