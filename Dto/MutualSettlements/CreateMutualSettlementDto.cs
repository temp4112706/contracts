﻿namespace Some.Dto.MutualSettlements;

/// <summary>
/// </summary>
/// <param name="Data">Список созданных АС с основными полями</param>
/// <param name="Errors">Не созданные АС </param>
public record CreateMutualSettlementDto(List<IdentifierMutualSettlementDto>? Data,
    List<CreateMutualSettlementErrorDto>? Errors);