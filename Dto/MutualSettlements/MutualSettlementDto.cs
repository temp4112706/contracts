﻿using Some.Dto.Insurers;

namespace Some.Dto.MutualSettlements;

/// <summary>
///     DTO: АС
/// </summary>
public record MutualSettlementDto : BaseMutualSettlementDto<IdentifierInsurerDto>
{
}