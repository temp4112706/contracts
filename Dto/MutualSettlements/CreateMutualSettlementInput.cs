﻿using System.ComponentModel;
using Some.Abstractions.Enums;

namespace Some.Dto.MutualSettlements;

/// <summary>
///     Input: Данные для создания АС
/// </summary>
public class CreateMutualSettlementInput
{
    /// <summary>
    ///     Код СК
    /// </summary>
    [Description("Страховая компания")]
    public InsurerId[] InsurerIds { get; init; } = null!;

    /// <summary>
    ///     Месяц
    /// </summary>
    [Description("Отчётный период")]
    public int Month { get; init; }

    /// <summary>
    ///     Год
    /// </summary>
    [Description("Отчётный период")]
    public int Year { get; init; }
}