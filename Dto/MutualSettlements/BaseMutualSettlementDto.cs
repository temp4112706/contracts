﻿using Some.Dto.Companies;
using Some.Dto.Employees;
using Some.Abstractions.Enums;
using Some.Abstractions.MutualSettlements;
using Some.Dto.Monies;

namespace Some.Dto.MutualSettlements;

/// <summary>
///     DTO: АС
/// </summary>
public abstract record BaseMutualSettlementDto<TInsurer> : TrackableEntityDto<Guid>,
    IMutualSettlement<MoneyDto, EmployeeDto, TInsurer, FlatCompanyDto>
{
    /// <summary>
    ///     Номер АС
    /// </summary>
    public string Number { get; set; } = null!;

    /// <summary>
    ///     Отчетная дата
    /// </summary>
    public DateTime ReportDate { get; set; }

    /// <summary>
    ///     Сотрудник банка
    /// </summary>
    public EmployeeDto? CompanyEmployee { get; set; }

    /// <inheritdoc />
    public FlatCompanyDto Company { get; set; } = null!;

    /// <summary>
    ///     Сотрудник СК
    /// </summary>
    public EmployeeDto? InsurerEmployee { get; set; }

    /// <summary>
    ///     Статус АС
    /// </summary>
    public MutualSettlementStatus Status { get; set; }

    /// <summary>
    ///     Общий размер оплаченной страховой премии
    /// </summary>
    public MoneyDto ActualPaymentAmount { get; set; } = null!;

    /// <summary>
    ///     Дата агентского договора
    /// </summary>
    public DateTime AgencyAgreementDate { get; set; }

    /// <summary>
    ///     № агентского договора
    /// </summary>
    public string AgencyAgreementNumber { get; set; } = null!;

    /// <summary>
    ///     Общий размер вознаграждения
    /// </summary>
    public MoneyDto AmountOfReward { get; set; } = null!;

    /// <summary>
    ///     СК
    /// </summary>
    public TInsurer Insurer { get; set; } = default!;

    /// <summary>
    ///     Кол-во записей
    /// </summary>
    public long RowsCount { get; set; }
}