﻿using Some.Dto.Insurers;

namespace Some.Dto.MutualSettlements;

/// <summary>
///     DTO: АС с основными полями
/// </summary>
public record FlatMutualSettlementDto : BaseFlatMutualSettlementDto<IdentifierInsurerDto>
{

}