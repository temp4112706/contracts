﻿namespace Some.Dto.MutualSettlements;

/// <summary>
///     Input : Aвтоматического согласования
/// </summary>
/// <param name="DifferenceAmountOfReward">Разница в размере вознаграждения</param>
/// <param name="PaymentDayDifference">Разница в днях оплаты</param>
/// <param name="AutomaticReconciliationEmptyRecords">
///     Согласовать автоматически заявки, по которым со стороны СК не
///     заполнены данные, но стоит статус <see cref="Some.Abstractions.Enums.MutualSettlementPolicyInsurerStatus.Agreed" />
/// </param>
public record AutoAgreeMutualSettlementInput(decimal DifferenceAmountOfReward, int PaymentDayDifference,
    bool AutomaticReconciliationEmptyRecords);