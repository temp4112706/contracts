﻿using Some.Abstractions.Enums;

namespace Some.Dto.MutualSettlements;

/// <summary>
///     Свод информации по статусам
/// </summary>
public record SummaryDto
{
    public Dictionary<MutualSettlementPolicyCompanyStatus, SummaryItemDto> CompanyStatuses { get; set; }
}