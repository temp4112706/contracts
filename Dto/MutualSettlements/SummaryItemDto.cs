﻿using Some.Abstractions.Enums;

namespace Some.Dto.MutualSettlements;

/// <summary>
///     Данные в разрезе одного статуса <see cref="MutualSettlementPolicyCompanyStatus" />
/// </summary>
/// <param name="Count">Кол-во договоров компании в определенном статусе </param>
/// <param name="Agreed">Согласованные</param>
/// <param name="Declined">Не согласованные</param>
public record SummaryItemDto(int Count, int Agreed, int Declined);