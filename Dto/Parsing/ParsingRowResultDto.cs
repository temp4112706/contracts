using Some.Abstractions.Result;

namespace Some.Dto.Parsing;

/// <summary>
///     Результат парсинга строки в определенном формате 
/// </summary>
public record ParsingRowResultDto<TResult> : IParsingRowResult<TResult>
    where TResult : class
{
    /// <summary>
    ///     Индекс строки
    /// </summary>
    public int Index { get; init; }

    /// <summary>
    ///     Ошибка парсинга
    /// </summary>
    public string? Error { get; init; }

    /// <summary>
    ///     Результат парсинга строки
    /// </summary>
    public TResult? Result { get; init; }

    /// <summary>
    ///     Валидная строка?
    /// </summary>
    public bool IsValid { get; init; }
}