using Some.Dto.LoanAgreements;

namespace Some.Dto.Parsing;

/// <summary>
///     DTO: Результат валидации графика платежей КД
/// </summary>
public class ValidateUpdateRepaymentScheduleResult
{
    /// <summary>
    ///     Валиден график платежей КД?
    /// </summary>
    public bool IsValid { get; init; }

    /// <summary>
    ///     Ошибки при парсинге графика платежей КД
    /// </summary>
    public IDictionary<string, string> Validation { get; set; } = null!;

    /// <summary>
    ///     Результат парсинга графика платежей КД
    /// </summary>
    public IReadOnlyCollection<ParsingRowResultDto<UpdateRepaymentScheduleRowDto>> Rows { get; init; } = null!;
}