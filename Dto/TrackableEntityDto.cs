using Ct.Module.Domain.Entities;

// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Dto;

/// <summary>
///     DTO: Базовая модель с данными о создании и обновлении
/// </summary>
/// <typeparam name="TPrimaryKey">Тип ИД модели</typeparam>
public record TrackableEntityDto<TPrimaryKey> : EntityDto<TPrimaryKey>, ITrackableEntity
{
    /// <summary>
    ///     Создан
    /// </summary>
    public DateTime CreatedAt { get; init; }

    /// <summary>
    ///     Обновлен
    /// </summary>
    public DateTime? UpdatedAt { get; init; }
}