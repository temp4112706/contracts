﻿using System.ComponentModel;
using Some.Abstractions.Employees;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global

namespace Some.Dto.Employees;

/// <summary>
///     Input: Сотрудник выгодоприобретателя
/// </summary>
public record EmployeeInput : IEmployee
{
    /// <summary>
    ///     Почта сотрудника
    /// </summary>
    [DisplayName("Email")]
    public string Email { get; init; } = null!;

    /// <summary>
    ///     ФИО сотрудника
    /// </summary>
    [DisplayName("ФИО сотрудника")]
    public string FullName { get; init; } = null!;

    /// <summary>
    ///     Ид сотрудника
    /// </summary>
    [DisplayName("Ид сотрудника")]
    public string Id { get; init; } = null!;
}