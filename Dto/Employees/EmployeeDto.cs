using Some.Abstractions.Employees;

namespace Some.Dto.Employees;

/// <summary>
///     Сотрудник
/// </summary>
/// <param name="Email">Email</param>
/// <param name="FullName">ФИО</param>
/// <param name="Id">ИД</param>
/// <param name="Enabled">Активен</param>
public record EmployeeDto(string Email, string FullName, string Id, bool Enabled) : IEmployee;