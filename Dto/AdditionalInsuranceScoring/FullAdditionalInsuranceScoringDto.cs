﻿using System.Text.Json;

namespace Some.Dto.AdditionalInsuranceScoring;

/// <summary>
///     Результат рассчёта по доп.видам страхования
/// </summary>
public record FullAdditionalInsuranceScoringDto : AdditionalInsuranceScoringDto
{
    /// <summary>
    ///     Данные по которым произведен рассчёт
    /// </summary>
    public JsonDocument Criteria { get; set; } = null!;
}