﻿using System.Text.Json;
using Some.Abstractions.AdditionalInsuranceScoring;
using Some.Abstractions.Enums;
using Some.Dto.Common;
using Some.Dto.Companies;
using Some.Dto.Insurers;
using Some.Dto.Monies;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Dto.AdditionalInsuranceScoring;

/// <summary>
///     Результат рассчёта по доп.видам страхования
/// </summary>
public record AdditionalInsuranceScoringDto : BaseInsuranceScoringDto,
    IAdditionalInsuranceScoring<IdentifierInsurerDto, FlatCompanyDto, MoneyDto>
{
    /// <summary>
    ///     Результат рассчёта по конкретному типу
    /// </summary>
    public JsonDocument Scoring { get; set; } = null!;

    /// <summary>
    ///     Компания
    /// </summary>
    public FlatCompanyDto? Company { get; init; } = null!;

    /// <summary>
    ///     Продолжительность в месяцах
    /// </summary>
    public int Duration { get; init; }

    /// <summary>
    ///     Страховая сумма
    /// </summary>
    public MoneyDto InsuranceAmount { get; init; } = null!;

    /// <summary>
    ///     Страховая
    /// </summary>
    public IdentifierInsurerDto Insurer { get; init; } = null!;

    /// <summary>
    ///     Цена полиса
    /// </summary>
    public MoneyDto PremiumAmount { get; init; } = null!;

    /// <summary>
    ///     Типы добровольного страхования
    /// </summary>
    public AdditionalInsuranceType Type { get; init; }
}