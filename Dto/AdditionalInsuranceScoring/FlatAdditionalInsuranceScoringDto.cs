﻿namespace Some.Dto.AdditionalInsuranceScoring;

/// <summary>
///     DTO : Ипотечное страхование - Страхование квартиры (доп вид страхования)
/// </summary>
public record FlatAdditionalInsuranceScoringDto
{
    /// <summary>
    ///     Гражданская ответственность
    /// </summary>
    public decimal? CivilResponsibility { get; set; }

    /// <summary>
    ///     Внутренняя отделка
    /// </summary>
    public decimal? InteriorDecoration { get; set; }

    /// <summary>
    ///     Движимое имущество
    /// </summary>
    public decimal? MovableProperty { get; set; }

    /// <summary>
    ///     Стены и перекрытия
    /// </summary>
    public decimal? WallsAndCeilings { get; set; }
}