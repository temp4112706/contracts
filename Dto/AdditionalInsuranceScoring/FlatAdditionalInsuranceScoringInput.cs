﻿namespace Some.Dto.AdditionalInsuranceScoring;

/// <summary>
///     Ипотечное страхование - Страхование квартиры (доп вид страхования)
/// </summary>
/// <param name="CivilResponsibility">Гражданская ответственность </param>
/// <param name="InteriorDecoration">Внутренняя отделка </param>
/// <param name="MovableProperty">Движимое имущество </param>
/// <param name="WallsAndCeilings">Стены и перекрытия </param>
public record FlatAdditionalInsuranceScoringInput(decimal? CivilResponsibility, decimal? InteriorDecoration,
    decimal? MovableProperty, decimal? WallsAndCeilings);