﻿using System.ComponentModel;
using System.Text.Json;
using Some.Abstractions.Enums;

namespace Some.Dto.AdditionalInsuranceScoring;

/// <summary>
///     Параметры расчёта
/// </summary>
/// <param name="InsuranceType">Тип добровольного страхования</param>
public record AdditionalInsuranceScoringInput(
    [property: DisplayName("Тип добровольного страхования")]
    AdditionalInsuranceType InsuranceType)
{
    /// <summary>
    ///     Прамаетры калькуляции по доп видам страхования
    /// </summary>
    public JsonDocument Scoring { get; init; } = null!;
    
    /// <summary>
    ///     ИД Компании
    /// </summary>
    public string? CompanyId { get; init; }
}