using Ct.Module.Errors;

namespace Some.Dto.Common;

/// <summary>
///     Базовый ДТО скоринга
/// </summary>
public record BaseInsuranceScoringDto : EntityDto<Guid>
{
    /// <summary>
    ///     Ошибка скоринга
    /// </summary>
    public ErrorInfoResult? Error { get; init; }
}