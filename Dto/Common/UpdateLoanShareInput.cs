using System.ComponentModel;
using Some.Abstractions.Enums;
using Some.Dto.Normalizers;

namespace Some.Dto.Common;

/// <summary>
///     DTO: Данные для обновления данных созаемщиков
/// </summary>
public record UpdateLoanShareInput
{
    /// <summary>
    ///     Доли созаемщика в процентах
    /// </summary>
    [DisplayName("Доли созаемщика")]
    public Dictionary<InsuredPersonIndex, decimal> LoanShare { get; set; } = null!;

    /// <summary>
    ///     Нормализация
    /// </summary>
    public void Normalize()
    {
        LoanShare = LoanShare.ToDictionary(x => x.Key, y => RateNormalizer.Normalize(y.Value));
    }
}