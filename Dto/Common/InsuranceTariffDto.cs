using Some.Abstractions.Common;
using Some.Dto.Monies;

namespace Some.Dto.Common;

/// <summary>
///     DTO: Результаты андеррайтинга
/// </summary>
public record InsuranceTariffDto : IInsuranceTariff<MoneyDto>
{
    /// <summary>
    ///     Страховая сумма
    /// </summary>
    public MoneyDto InsuranceAmount { get; init; } = null!;

    /// <summary>
    ///     Страховая премия
    /// </summary>
    public MoneyDto PremiumAmount { get; init; } = null!;

    /// <summary>
    ///     Страховой тариф (%)
    /// </summary>
    public decimal Rate { get; init; }
}