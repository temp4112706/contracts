﻿using Some.Abstractions.Common;
using Some.Dto.Monies;

namespace Some.Dto.Common;

/// <summary>
///     Фактическая оплата
/// </summary>
public record ActualPaymentDto : IActualPayment<MoneyDto>
{
    /// <summary>
    ///     Дата оплаты
    /// </summary>
    public DateTime PaymentDate { get; init; }

    /// <summary>
    ///     Сумма к оплате
    /// </summary>
    public MoneyDto ActualAmount { get; init; } = null!; 
}