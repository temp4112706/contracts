using System.ComponentModel.DataAnnotations;
using Some.Abstractions.Result;

namespace Some.Dto;

/// <summary>
///     Реализация <see cref="ILimitedResultInput" />
/// </summary>
public record LimitedResultInput : ILimitedResultInput
{
    /// <summary>
    ///     Максимальное кол-во элементов
    /// </summary>
    [Range(1, int.MaxValue)]
    public virtual int MaxResultCount { get; init; } = 10;
}