using System.ComponentModel;
using Some.Abstractions.Monies;

namespace Some.Dto.Monies;

/// <summary>
///     Input: Данные о сумме и валюте
/// </summary>
/// <param name="Amount">Сумма</param>
/// <param name="Currency">Код валюты ISO (для рубля РФ - RUB)</param>
public record MoneyInput(
    [property: DisplayName("Сумма")] decimal Amount,
    [property: DisplayName("Код валюты")] string Currency
) : IMoney
{
    /// <inheritdoc />
    public override string ToString() => Amount.ToString("C2");

    /// <summary>
    ///     Сумма в рублях
    /// </summary>
    /// <param name="value">Кол-во денег</param>
    public static MoneyInput Rub(decimal value) => new(value, MoneyCurrencies.Rub);
}