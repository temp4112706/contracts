using Some.Abstractions.Monies;

namespace Some.Dto.Monies;

/// <summary>
///     DTO: Сумма и валюта
/// </summary>
/// <param name="Amount">Сумма</param>
/// <param name="Currency">Код валюты ISO (для рубля РФ - RUB)</param>
public record MoneyDto(decimal Amount, string Currency) : IMoney
{
    /// <inheritdoc />
    public override string ToString() => Amount.ToString("C2");

    /// <summary>
    ///     В рубли
    /// </summary>
    public static MoneyDto Rub(decimal amount) => new(amount, MoneyCurrencies.Rub);
    
    /// <summary>
    ///     0 рублей
    /// </summary>
    /// <returns></returns>
    public static MoneyDto ZeroRub() => Rub(0);

    /// <summary>
    ///     из <see cref="IMoney"/>
    /// </summary>
    public static MoneyDto From(IMoney money) => new MoneyDto(money.Amount, money.Currency);
}