using System.ComponentModel;

namespace Some.Dto.HealthQuestionnaires;

/// <summary>
///     Данные для обновления медицинских анкет у группы застрахованых лиц
/// </summary>
/// <param name="Inputs">Данные для обновления медицинских анкет</param>
public record UpdateHealthQuestionnairesInput(
    [property: DisplayName("Данные для обновления медицинских анкет")]
    IEnumerable<UpdateHealthQuestionnaireInput> Inputs
);