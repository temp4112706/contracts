using System.Collections.Concurrent;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.Json;
using System.Text.Json.Serialization;
using Ct.Module.Domain.Entities;
using Some.Abstractions.Enums;
using Some.Abstractions.Extensions;
using Some.Abstractions.HealthQuestionnaires;

namespace Some.Dto.HealthQuestionnaires;

/// <summary>
///     DTO: Медицинская анкета
/// </summary>
public record HealthQuestionnaireDto : EntityDto<Guid>,
    IHealthQuestionnaire<ArterialPressureDto, AnswerWithCommentsDto>
{
    private HealthQuestionnaireSectionDto? _commonSection;
    private HealthQuestionnaireSectionDto? _declarationSection;
    private HealthQuestionnaireSectionDto? _femaleSection;
    private HealthQuestionnaireSectionDto? _healthSurveySection;
    private IDictionary<string, HealthQuestionnaireSectionDto>? _sections;

    [JsonConstructor]
    public HealthQuestionnaireDto()
    {
    }

    /// <summary>
    ///     Вопросы
    /// </summary>
    [JsonPropertyName("sections")]
    public IDictionary<string, HealthQuestionnaireSectionDto> Sections => _sections ??= CreateSections();

    /// <summary>
    ///     Ответы с комментариями
    /// </summary>
    [JsonIgnore]
    public ICollection<AnswerWithCommentsDto> AnswersWithComments { get; private set; } = null!;

    /// <summary>
    ///     Артериальное давление (последнее измерение, мм. рт. ст.).
    /// </summary>
    [Description("Артериальное давление (последнее измерение, мм. рт. ст.)?")]
    [JsonIgnore]
    public ArterialPressureDto ArterialPressure { get; private set; } = null!;

    /// <summary>
    ///     Дополнительная информация.
    /// </summary>
    [Description("Дополнительная информация.")]
    [JsonIgnore]
    public string? AdditionalInfo { get; private set; }

    /// <summary>
    ///     Употребляете ли Вы пиво? Если "Да", то укажите среднее количество, потребляемое в неделю? (мл.)
    /// </summary>
    [Description("Употребляете ли Вы пиво? Если 'Да', то укажите среднее количество, потребляемое в неделю? (мл.)")]
    [JsonIgnore]
    public float? BeerPerWeek { get; private set; }

    /// <summary>
    ///     Укажите, пожалуйста, наименование и адрес медучреждения, в котором вы наблюдаетесь, стоите на учете, проходите
    ///     лечение?
    /// </summary>
    [Description(
        "Укажите, пожалуйста, наименование и адрес медучреждения, в котором вы наблюдаетесь, стоите на учете, проходите лечение?")]
    [JsonIgnore]
    public string? ClinicRegistration { get; private set; }

    /// <summary>
    ///     Дата заполнения
    /// </summary>
    public DateTime FillingDateTime { get; private set; }

    /// <summary>
    ///     Рост (см).
    /// </summary>
    [Description("Рост (см).")]
    [JsonIgnore]
    public float Height { get; private set; }

    /// <summary>
    ///     Курите ли Вы сейчас?
    /// </summary>
    [Description("Курите ли Вы сейчас?")]
    [JsonIgnore]
    public bool IsSmoking { get; private set; }

    /// <summary>
    ///     Если «Курите», укажите количество сигарет в день? (шт.)
    /// </summary>
    [Description("Если «Курите», укажите количество сигарет в день? (шт.)")]
    [JsonIgnore]
    public int? NumberOfCigarettesPerDay { get; private set; }

    /// <summary>
    ///     Как давно курите или как давно прекратили курить? (лет)
    /// </summary>
    [Description("Как давно курите или как давно прекратили курить? (лет)?")]
    [JsonIgnore]
    public string? SmokingDetails { get; private set; }

    /// <summary>
    ///     Употребляете ли Вы крепкие напитки (> 40 град) ? Если "Да", то укажите среднее количество, потребляемое в неделю?
    ///     (мл.)
    /// </summary>
    [Description(
        "Употребляете ли Вы крепкие напитки (> 40 град) ? Если 'Да', то укажите среднее количество, потребляемое в неделю? (мл.)")]
    [JsonIgnore]
    public float? StrongAlcoholPerWeek { get; private set; }

    /// <summary>
    ///     Вес (кг).
    /// </summary>
    [Description("Вес (кг).")]
    [JsonIgnore]
    public float Weight { get; private set; }

    /// <summary>
    ///     Употребляете ли Вы вино? Если "Да", то укажите среднее количество, потребляемое в неделю? (мл.)
    /// </summary>
    [Description("Употребляете ли Вы вино? Если 'Да', то укажите среднее количество, потребляемое в неделю? (мл.)")]
    [JsonIgnore]
    public float? WinePerWeek { get; private set; }

    [JsonIgnore]
    private bool HasBeerPerWeek => BeerPerWeek > 0;

    [JsonIgnore]
    private bool HasNumberOfCigarettesPerDay => NumberOfCigarettesPerDay > 0;

    [JsonIgnore]
    private bool HasStrongAlcoholPerWeek => StrongAlcoholPerWeek > 0;

    [JsonIgnore]
    private bool HasWinePerWeek => WinePerWeek > 0;

    private IDictionary<string, HealthQuestionnaireSectionDto> CreateSections()
    {
        var commonSection = new HealthQuestionnaireSectionDto("Общая информация",
            GetCommonQuestions());
        var declarationSection = new HealthQuestionnaireSectionDto("Декларация о состоянии здоровья",
            GetDeclarationQuestions());
        var femaleSection = new HealthQuestionnaireSectionDto("Дополнительные вопросы для женщин",
            GetFemaleQuestions());

        var sections = new Dictionary<string, HealthQuestionnaireSectionDto>
        {
            { "common", commonSection },
            { "declaration", declarationSection }
        };

        if (femaleSection.Questions.NotNullOrEmpty())
        {
            sections.Add("female", femaleSection);
        }

        return sections;
    }

    [SuppressMessage("ReSharper", "ConstantConditionalAccessQualifier")]
    private HealthQuestionnaireItemDto? GetAnswerByQuestionCode(QuestionCode questionCode)
    {
        var answerWithComments = AnswersWithComments?.FirstOrDefault(x => x.Code == questionCode);
        // ReSharper disable once ConditionIsAlwaysTrueOrFalse
        return answerWithComments != null
            ? new HealthQuestionnaireItemDto(answerWithComments)
            : null;
    }

    private IList<HealthQuestionnaireItemDto> GetCommonQuestions()
    {
        var type = GetType();
        return new List<HealthQuestionnaireItemDto>
        {
            // QUESTION_1
            GetAnswerByQuestionCode(QuestionCode.ExistingLifeInsurances)!,
            // QUESTION_2
            GetAnswerByQuestionCode(QuestionCode.LifeInsuranceDenial)!,
            // QUESTION_3
            GetAnswerByQuestionCode(QuestionCode.UnusualWorkingConditions)!,
            // QUESTION_8
            new(type, nameof(IsSmoking),
                IsSmoking, HasNumberOfCigarettesPerDay ? $"{NumberOfCigarettesPerDay} шт." : null),
            // QUESTION_42
            new(type, nameof(SmokingDetails),
                !string.IsNullOrEmpty(SmokingDetails), SmokingDetails),
            // QUESTION_9
            new(type, nameof(BeerPerWeek),
                HasBeerPerWeek, HasBeerPerWeek ? $"{BeerPerWeek} мл." : null),
            // QUESTION_36
            new(type, nameof(WinePerWeek),
                HasWinePerWeek, HasWinePerWeek ? $"{WinePerWeek} мл." : null),
            // QUESTION_37
            new(type, nameof(StrongAlcoholPerWeek),
                HasStrongAlcoholPerWeek, HasStrongAlcoholPerWeek ? $"{StrongAlcoholPerWeek} мл." : null),
            // QUESTION_10
            new(type, nameof(ClinicRegistration),
                !string.IsNullOrEmpty(ClinicRegistration), ClinicRegistration),
        }.Where(x => x != null).ToList();
    }

    [SuppressMessage("ReSharper", "ConstantConditionalAccessQualifier")]
    private IList<HealthQuestionnaireItemDto> GetDeclarationQuestions()
    {
        var type = GetType();
        return new List<HealthQuestionnaireItemDto>
        {
            // QUESTION_11
            new(type, nameof(Height),
                true, $"{Height} см."),
            // QUESTION_12
            new(type, nameof(Weight),
                true, $"{Weight} кг."),
            // QUESTION_13
            GetAnswerByQuestionCode(QuestionCode.RapidWeightChange)!,
            // QUESTION_38, QUESTION_14
            new(type, nameof(ArterialPressure),
                true, ArterialPressure?.ToString()),
            // QUESTION_15
            GetAnswerByQuestionCode(QuestionCode.Heart)!,
            // QUESTION_16
            GetAnswerByQuestionCode(QuestionCode.Blood)!,
            // QUESTION_17
            GetAnswerByQuestionCode(QuestionCode.RespiratorySystem)!,
            // QUESTION_18
            GetAnswerByQuestionCode(QuestionCode.DigestionSystem)!,
            // QUESTION_19
            GetAnswerByQuestionCode(QuestionCode.RenalSystem)!,
            // QUESTION_20
            GetAnswerByQuestionCode(QuestionCode.NervousSystem)!,
            // QUESTION_21
            GetAnswerByQuestionCode(QuestionCode.PsychiatricRegistration)!,
            // QUESTION_22
            GetAnswerByQuestionCode(QuestionCode.Endocrine)!,
            // QUESTION_23
            GetAnswerByQuestionCode(QuestionCode.VisionAndHearing)!,
            // QUESTION_24
            GetAnswerByQuestionCode(QuestionCode.MusculoskeletalSystem)!,
            // QUESTION_25
            GetAnswerByQuestionCode(QuestionCode.Oncology)!,
            // QUESTION_26
            GetAnswerByQuestionCode(QuestionCode.OrganDefects)!,
            // QUESTION_27
            GetAnswerByQuestionCode(QuestionCode.Aids)!,
            // QUESTION_28
            GetAnswerByQuestionCode(QuestionCode.Surgery)!,
            // QUESTION_29
            GetAnswerByQuestionCode(QuestionCode.DispensaryRegistration)!,
            // QUESTION_30
            GetAnswerByQuestionCode(QuestionCode.DangerousRelativeIllness)!,
            // QUESTION_31
            GetAnswerByQuestionCode(QuestionCode.Disability)!,
            // QUESTION_32
            GetAnswerByQuestionCode(QuestionCode.SpecialExamination)!,

            GetAnswerByQuestionCode(QuestionCode.Allergy)!,

            GetAnswerByQuestionCode(QuestionCode.HospitalTreatment)!,
            // QUESTION_33
            GetAnswerByQuestionCode(QuestionCode.Other)!,
            // QUESTION_34
            new(type, nameof(AdditionalInfo),
                AdditionalInfo.NotNullOrEmpty(), AdditionalInfo)
        }.Where(x => x != null).ToList();
    }

    private IList<HealthQuestionnaireItemDto> GetFemaleQuestions() =>
        new List<HealthQuestionnaireItemDto>
        {
            // QUESTION_39
            GetAnswerByQuestionCode(QuestionCode.FemaleBody)!,
            // QUESTION_40
            GetAnswerByQuestionCode(QuestionCode.Pregnancy)!,
            // QUESTION_41
            GetAnswerByQuestionCode(QuestionCode.PregnancyComplications)!
        }.Where(x => x != null).ToList();

    /// <summary>
    ///     Элемент вопроса мед. анкеты
    /// </summary>
    public class HealthQuestionnaireItemDto
    {
        [JsonConstructor]
        internal HealthQuestionnaireItemDto(
            QuestionCode questionCode,
            bool answer = false,
            string? comments = null)
        {
            var (code, name) = HealthQuestionnaireDescriptionCache.GetDescription(questionCode);
            Code = code;
            Name = name;
            Answer = answer;
            Comments = comments;
        }

        internal HealthQuestionnaireItemDto(Type type, string propertyName, bool answer, string? comments = null)
        {
            var (code, name) = HealthQuestionnaireDescriptionCache.GetDescription(type, propertyName);
            Code = code;
            Name = name;
            Answer = answer;
            Comments = comments;
        }

        internal HealthQuestionnaireItemDto(AnswerWithCommentsDto dto)
        {
            var (code, name) = HealthQuestionnaireDescriptionCache.GetDescription(dto.Code);
            Code = code;
            Name = name;
            Answer = dto.Answer;
            Comments = dto.Comments;
        }

        /// <summary>
        ///     Ответ true &#x3D; да, false &#x3D; нет.
        /// </summary>
        public bool Answer { get; private set; }

        /// <summary>
        ///     Код вопроса
        /// </summary>
        public string Code { get; }

        /// <summary>
        ///     Комментарий
        /// </summary>
        public string? Comments { get; private set; }

        /// <summary>
        ///     Название вопроса
        /// </summary>
        public string Name { get; }

        internal void UpdateAnswer(bool answer, string? comments = null)
        {
            Answer = answer;
            Comments = comments;
        }
    }

    /// <summary>
    ///     Секция с вопросами мед. анкеты
    /// </summary>
    public class HealthQuestionnaireSectionDto
    {
        [JsonConstructor]
        internal HealthQuestionnaireSectionDto(
            string title,
            IList<HealthQuestionnaireItemDto> questions)
        {
            Title = title;
            Questions = questions;
        }

        /// <summary>
        ///     Список вопросов
        /// </summary>
        public IList<HealthQuestionnaireItemDto> Questions { get; }

        /// <summary>
        ///     Название
        /// </summary>
        public string Title { get; }
    }

    /// <summary>
    ///     Кеширование описаний вопросов
    /// </summary>
    private static class HealthQuestionnaireDescriptionCache
    {
        private static readonly ConcurrentDictionary<string, string> DescriptionCache = new();

        private static readonly ConcurrentDictionary<QuestionCode, string> QuestionCodeCache = new();

        public static (string, string) GetDescription(QuestionCode questionCode)
        {
            var code = QuestionCodeCache.GetOrAdd(questionCode, GetEnumMember);
            var value = DescriptionCache.GetOrAdd(code, key => GetEnumDescription(questionCode));
            return (code, value);
        }

        public static (string, string) GetDescription(Type type, string propertyName)
        {
            var codeStr = JsonNamingPolicy.CamelCase.ConvertName(propertyName);
            var value = DescriptionCache.GetOrAdd(codeStr, key => GetPropertyDescription(type, propertyName));
            return (codeStr, value);
        }

        private static string GetEnumDescription(QuestionCode code)
        {
            var codeStr = code.ToString();
            var attribute = code.GetType().GetMember(codeStr)
                .FirstOrDefault()?.GetCustomAttribute<DescriptionAttribute>();
            return attribute?.Description ?? codeStr;
        }

        private static string GetEnumMember(QuestionCode code)
        {
            var codeStr = code.ToString();
            var attribute = code.GetType().GetMember(codeStr)
                .FirstOrDefault()?.GetCustomAttribute<EnumMemberAttribute>();
            return attribute?.Value ?? codeStr;
        }

        private static string GetPropertyDescription(Type type, string propertyName)
        {
            var attribute = type.GetProperty(propertyName,
                    BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance)?
                .GetCustomAttribute<DescriptionAttribute>();
            return attribute?.Description ?? propertyName;
        }
    }

    /// <summary>
    ///     Создать из объекта
    /// </summary>
    public static HealthQuestionnaireDto MapFrom<THealthQuestionnaire, TArterialPressure, TAnswerWithComments>(
        THealthQuestionnaire source,
        bool isForeignPublicPerson)
        where TArterialPressure : IArterialPressure
        where TAnswerWithComments : IAnswerWithComments
        where THealthQuestionnaire : IHealthQuestionnaire<TArterialPressure, TAnswerWithComments>, IEntity<Guid> =>
        new()
        {
            Id = source.Id,
            FillingDateTime = source.FillingDateTime,
            Height = source.Height,
            Weight = source.Weight,
            IsSmoking = source.IsSmoking,
            SmokingDetails = source.SmokingDetails,
            NumberOfCigarettesPerDay = source.NumberOfCigarettesPerDay,
            BeerPerWeek = source.BeerPerWeek,
            WinePerWeek = source.WinePerWeek,
            StrongAlcoholPerWeek = source.StrongAlcoholPerWeek,
            ClinicRegistration = source.ClinicRegistration,
            AdditionalInfo = source.AdditionalInfo,
            ArterialPressure = new ArterialPressureDto
            {
                Diastolic = source.ArterialPressure.Diastolic,
                Systolic = source.ArterialPressure.Systolic
            },
            AnswersWithComments = source.AnswersWithComments.Select(x => new AnswerWithCommentsDto
            {
                Code = x.Code,
                Answer = x.Answer,
                Comments = x.Comments
            }).ToArray()
        };
}