using System.ComponentModel;
using Some.Abstractions.Enums;
using Some.Abstractions.HealthQuestionnaires;

// ReSharper disable PropertyCanBeMadeInitOnly.Global

namespace Some.Dto.HealthQuestionnaires;

/// <summary>
///     Ответ с комментарием. При формировании необходимо указать для значения для всех ключей из поля code, за исключением
///     вопросов для женщин, которые заполняются в зависимости от пола.
/// </summary>
public record AnswerWithCommentsInput : IAnswerWithComments
{
    /// <summary>
    ///     Ответ да или нет
    /// </summary>
    [DisplayName("Ответ")]
    public bool Answer { get; set; }

    /// <summary>
    ///     Код вопроса
    /// </summary>
    [DisplayName("Код вопроса")]
    public QuestionCode Code { get; set; }

    /// <summary>
    ///     Комментарий
    /// </summary>
    [DisplayName("Комментарий")]
    public string? Comments { get; set; }
}