using System.Diagnostics.CodeAnalysis;
using Some.Abstractions.HealthQuestionnaires;

namespace Some.Dto.HealthQuestionnaires;

/// <summary>
///     DTO: Медицинская анкета с основными полями
/// </summary>
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
public record FlatHealthQuestionnaireDto : EntityDto<Guid>, IScalarHealthQuestionnaire
{
    /// <summary>
    ///     Дата заполнения
    /// </summary>
    public DateTime FillingDateTime { get; set; }

    /// <summary>
    ///     Рост
    /// </summary>
    public float Height { get; set; }

    /// <summary>
    ///     Вес
    /// </summary>
    public float Weight { get; set; }

    /// <summary>
    ///     Курите ли вы?
    /// </summary>
    public bool IsSmoking { get; set; }

    /// <summary>
    ///     Как давно курите или как давно бросили курить?
    /// </summary>
    public string? SmokingDetails { get; set; } = null!;

    /// <summary>
    ///     Количество сигарет в день
    /// </summary>
    public int? NumberOfCigarettesPerDay { get; set; }

    /// <summary>
    ///     Количество пива в неделю (мл)
    /// </summary>
    public float? BeerPerWeek { get; set; }

    /// <summary>
    ///     Количество вина в неделю (мл)
    /// </summary>
    public float? WinePerWeek { get; set; }

    /// <summary>
    ///     Количество крепкого спиртного в неделю (мл)
    /// </summary>
    public float? StrongAlcoholPerWeek { get; set; }

    /// <summary>
    ///     Наименование и адрес медучреждения, в котором вы наблюдаетесь, стоите на учете, проходите лечение
    /// </summary>
    public string? ClinicRegistration { get; set; }

    /// <summary>
    ///     Дополнительная информация
    /// </summary>
    public string? AdditionalInfo { get; set; }
}