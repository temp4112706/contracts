using System.ComponentModel;
using Some.Abstractions.HealthQuestionnaires;

namespace Some.Dto.HealthQuestionnaires;

/// <summary>
///     DTO: Медицинская анкета
/// </summary>
public record HealthQuestionnaireInput : IHealthQuestionnaire<ArterialPressureInput, AnswerWithCommentsInput>
{
    /// <summary>
    ///     Ответы с комментариями
    /// </summary>
    [DisplayName("Ответы с комментариями")]
    public ICollection<AnswerWithCommentsInput> AnswersWithComments { get; set; } = null!;

    /// <summary>
    ///     Артериальное давление
    /// </summary>
    [DisplayName("Артериальное давление")]
    public ArterialPressureInput ArterialPressure { get; set; } = null!;

    /// <summary>
    ///     Дата заполнения
    /// </summary>
    [DisplayName("Дата заполнения")]
    public virtual DateTime FillingDateTime { get; set; }

    /// <summary>
    ///     Дополнительная информация
    /// </summary>
    [DisplayName("Дополнительная информация")]
    public string? AdditionalInfo { get; set; }

    /// <summary>
    ///     Количество пива в неделю (мл)
    /// </summary>
    [DisplayName("Кол-во пива в неделю")]
    public float? BeerPerWeek { get; set; }

    /// <summary>
    ///     Наименование и адрес медучреждения, в котором вы наблюдаетесь, стоите на учете, проходите лечение
    /// </summary>
    [DisplayName("Наименование и адрес медучреждения, в котором вы наблюдаетесь...")]
    public string? ClinicRegistration { get; set; }

    /// <summary>
    ///     Рост
    /// </summary>
    [DisplayName("Рост")]
    public float Height { get; set; }

    /// <summary>
    ///     Курите ли вы?
    /// </summary>
    [DisplayName("Курите")]
    public bool IsSmoking { get; set; }

    /// <summary>
    ///     Количество сигарет в день
    /// </summary>
    [DisplayName("Кол-во сигарет в день")]
    public int? NumberOfCigarettesPerDay { get; set; }

    /// <summary>
    ///     Как давно курите или как давно бросили курить?
    /// </summary>
    [DisplayName("Как давно курите")]
    public string? SmokingDetails { get; set; }

    /// <summary>
    ///     Количество крепкого спиртного в неделю (мл)
    /// </summary>
    [DisplayName("Кол-во крепкого спиртного в неделю")]
    public float? StrongAlcoholPerWeek { get; set; }

    /// <summary>
    ///     Вес
    /// </summary>
    [DisplayName("Вес")]
    public float Weight { get; set; }

    /// <summary>
    ///     Количество вина в неделю (мл)
    /// </summary>
    [DisplayName("Кол-во вина в неделю")]
    public float? WinePerWeek { get; set; }
}