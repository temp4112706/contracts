using System.Diagnostics.CodeAnalysis;
using Some.Abstractions.HealthQuestionnaires;

namespace Some.Dto.HealthQuestionnaires;

/// <summary>
///     DTO: Артериальное давление (последнее измерение, мм. рт. ст.)
/// </summary>
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
public record ArterialPressureDto : IArterialPressure
{
    /// <summary>
    ///     Диастолическое
    /// </summary>
    public int Diastolic { get; set; }

    /// <summary>
    ///     Систолическое
    /// </summary>
    public int Systolic { get; set; }

    /// <inheritdoc />
    public override string ToString()
    {
        return $"{Systolic}/{Diastolic}";
    }
}