using System.ComponentModel;
using Some.Abstractions.Enums;

namespace Some.Dto.HealthQuestionnaires;

/// <summary>
///     Данные для изменения медицинской анкеты застрахованого лица
/// </summary>
/// <param name="Index">Индекс застрахованного лица</param>
/// <param name="Input">Данные для изменения</param>
public record UpdateHealthQuestionnaireInput(
    [property: DisplayName("Индекс застрахованного лица")]
    InsuredPersonIndex Index,
    [property: DisplayName("Данные для изменения")]
    HealthQuestionnaireInput Input
);