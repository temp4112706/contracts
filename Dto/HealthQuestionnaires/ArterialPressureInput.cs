using System.ComponentModel;
using Some.Abstractions.HealthQuestionnaires;

// ReSharper disable PropertyCanBeMadeInitOnly.Global

namespace Some.Dto.HealthQuestionnaires;

/// <summary>
///     Артериальное давление (последнее измерение, мм. рт. ст.)
/// </summary>
public record ArterialPressureInput : IArterialPressure
{
    /// <summary>
    ///     Диастолическое
    /// </summary>
    [DisplayName("Диастолическое давление")]
    public int Diastolic { get; set; }

    /// <summary>
    ///     Систолическое
    /// </summary>
    [DisplayName("Систолическое давление")]
    public int Systolic { get; set; }
}