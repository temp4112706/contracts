using System.Diagnostics.CodeAnalysis;
using Some.Abstractions.Enums;
using Some.Abstractions.HealthQuestionnaires;

namespace Some.Dto.HealthQuestionnaires;

/// <summary>
///     DTO: Ответ с комментарием. При формировании необходимо указать для значения для всех ключей из поля code, за исключением
///     вопросов для женщин, которые заполняются в зависимости от пола.
/// </summary>
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
public record AnswerWithCommentsDto : IAnswerWithComments
{
    /// <summary>
    ///     Код вопроса
    /// </summary>
    public QuestionCode Code { get; set; }

    /// <summary>
    ///     Ответ true &#x3D; да, false &#x3D; нет.
    /// </summary>
    public bool Answer { get; set; }

    /// <summary>
    ///     Комментарий
    /// </summary>
    public string? Comments { get; set; }
}