﻿using Some.Abstractions.SportActivity;

namespace Some.Dto.SportActivities;

/// <inheritdoc />
public record SportActivityDto(string Name, bool IsProfessional) : ISportActivity;