﻿using Some.Abstractions.SportActivity;

namespace Some.Dto.SportActivities;

/// <inheritdoc />
public record SportActivityInput(string Name, bool IsProfessional)
 : ISportActivity;