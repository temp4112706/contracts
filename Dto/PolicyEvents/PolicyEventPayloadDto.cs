using Some.Abstractions.Policies;

namespace Some.Dto.PolicyEvents;

/// <summary>
///     DTO: Дополнительная информация в эвенте ДС
/// </summary>
public record PolicyEventPayloadDto : IPolicyEventPayload<PolicyStatusChangesResultDto>
{
    /// <summary>
    ///     Результат перемещения статуса
    /// </summary>
    public PolicyStatusChangesResultDto? StatusChangesResult { get; init; }

    /// <summary>
    ///     Комментарий
    /// </summary>
    public string? Comment { get; init; }
}