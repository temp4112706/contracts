﻿namespace Some.Dto.PolicyEvents;

/// <summary>
///     DTO: Результат перехода статуса в ДС <see href="IPolicyStatusMoveResult"/>
/// </summary>
public record PolicyStatusChangesResultDto
{
    /// <summary>
    ///     Предыдущий статус
    /// </summary>
    public string? PreviousStatus { get; init; }

    /// <summary>
    ///  Новый статус
    /// </summary>
    public string NextStatus { get; init; } = null!;
        
    /// <summary>
    ///     Дата и время перехода
    /// </summary>
    public DateTime ChangedAt { get; init; }
}