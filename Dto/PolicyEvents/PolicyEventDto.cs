using Some.Abstractions.Events;

namespace Some.Dto.PolicyEvents;

/// <summary>
///     DTO: Эвент ДС
/// </summary>
public record PolicyEventDto : EntityDto<Guid>, IEvent
{
    /// <summary>
    ///     Добавлен
    /// </summary>
    public DateTime CreatedAt { get; init; }

    /// <summary>
    ///     Эвент
    /// </summary>
    public string Event { get; init; } = null!;

    /// <summary>
    ///     Создатель эвента
    /// </summary>
    public string? Creator { get; init; }

    /// <summary>
    ///     Дополнительная информация
    /// </summary>
    public PolicyEventPayloadDto Payload { get; init; } = null!;
}