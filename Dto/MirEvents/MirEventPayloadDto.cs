using Some.Abstractions.Mirs;

namespace Some.Dto.MirEvents;

/// <summary>
///     DTO: Дополнительная информация в эвенте заявки
/// </summary>
public record MirEventPayloadDto : IMirEventPayload<MirStatusChangesResultDto>
{
    /// <summary>
    ///     Результат перемещения статуса
    /// </summary>
    public MirStatusChangesResultDto? StatusChangesResult { get; init; }

    /// <summary>
    ///     Комментарий
    /// </summary>
    public string? Comment { get; init; }
}