﻿namespace Some.Dto.MirEvents;

/// <summary>
///     DTO: Результат перехода статуса в заявке  <see href="IMirStatusMoveResult"/>
/// </summary>
public record MirStatusChangesResultDto
{
    /// <summary>
    ///     Предыдущий статус
    /// </summary>
    public string? PreviousStatus { get; init; }

    /// <summary>
    ///  Новый статус
    /// </summary>
    public string NextStatus { get; init; } = null!;
        
    /// <summary>
    ///     Дата и время перехода
    /// </summary>
    public DateTime ChangedAt { get; init; }
}