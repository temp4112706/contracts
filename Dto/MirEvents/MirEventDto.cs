using Some.Abstractions.Events;

// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global

namespace Some.Dto.MirEvents;

/// <summary>
///     DTO: Эвент заявки
/// </summary>
public record MirEventDto : EntityDto<Guid>, IEvent
{
    /// <summary>
    ///     Добавлен
    /// </summary>
    public DateTime CreatedAt { get; init; }

    /// <summary>
    ///     Эвент
    /// </summary>
    public string Event { get; init; } = null!;

    /// <summary>
    ///     Создатель эвента
    /// </summary>
    public string? Creator { get; init; }

    /// <summary>
    ///     Дополнительная информация
    /// </summary>
    public MirEventPayloadDto Payload { get; init; } = null!;
}