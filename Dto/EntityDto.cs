using System.Text.Json.Serialization;
using Some.Abstractions;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global

namespace Some.Dto;

/// <summary>
///     Реализация <see cref="EntityDto{TPrimaryKey}" />.
/// </summary>
[Serializable]
public record EntityDto : EntityDto<int>, IEntityDto
{
    /// <inheritdoc />
    public EntityDto()
    {
    }

    /// <summary>
    /// </summary>
    /// <param name="id"></param>
    public EntityDto(int id) : base(id)
    {
    }
}

/// <summary>
///     Реализация <see cref="EntityDto{TPrimaryKey}" />.
/// </summary>
[Serializable]
public record EntityDto<TPrimaryKey> : IEntityDto<TPrimaryKey>
{
    /// <summary>
    ///     <see cref="EntityDto" />
    /// </summary>
    public EntityDto()
    {
    }

    /// <summary>
    ///     <see cref="EntityDto" />
    /// </summary>
    public EntityDto(TPrimaryKey id)
    {
        Id = id;
    }

    /// <summary>
    ///     Ид элемента.
    /// </summary>
    [JsonPropertyOrder(1)]
    public virtual TPrimaryKey Id { get; init; } = default!;
}