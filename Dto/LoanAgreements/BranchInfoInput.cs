﻿using System.ComponentModel;
using Some.Dto.Employees;
using Some.Abstractions.LoanAgreements;
using Some.Dto.Addresses;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global

namespace Some.Dto.LoanAgreements;

/// <summary>
///     Информация об отделении или месте выдачи кредита
/// </summary>
public record BranchInfoInput : IBranchInfo<EmployeeInput, AddressInput>
{
    /// <summary>
    ///     Адрес
    /// </summary>
    [DisplayName("Адрес")]
    public AddressInput? Address { get; init; } = null!;

    /// <summary>
    ///     Код по классификации банка
    /// </summary>
    [DisplayName("Код по классификации банка")]
    public string? Code { get; init; }

    /// <summary>
    ///     Сотрудник банка
    /// </summary>
    [DisplayName("Сотрудник банка")]
    public EmployeeInput? Employee { get; init; }

    /// <summary>
    ///     Наименование
    /// </summary>
    [DisplayName("Наименование")]
    public string Name { get; init; } = null!;

    /// <summary>
    ///     Код региона по ФИАС
    /// </summary>
    [DisplayName("Код региона")]
    public string RegionCode { get; init; } = null!;
}