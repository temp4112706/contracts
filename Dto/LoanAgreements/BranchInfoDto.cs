﻿using Some.Dto.Employees;
using Some.Abstractions.LoanAgreements;
using Some.Dto.Addresses;

namespace Some.Dto.LoanAgreements;

/// <summary>
///     DTO: Информация об отделении или месте выдачи кредита
/// </summary>
public record BranchInfoDto : FlatBranchInfoDto, IBranchInfo<EmployeeDto, AddressDto>
{
    /// <summary>
    ///     Код по классификации банка
    /// </summary>
    public string? Code { get; init; }

    /// <summary>
    ///     Адрес
    /// </summary>
    public AddressDto? Address { get; init; }
}