using System.ComponentModel;
using Some.Dto.Normalizers;

namespace Some.Dto.LoanAgreements;

/// <summary>
///     Информация по кредитному договору
/// </summary>
/// <param name="LoanType">Тип кредита</param>
/// <param name="RepaymentSchedule">График платежей</param>
/// <param name="InsuranceAllBorrowers">Наличие личного страхования у всех заемщиков</param>
/// <param name="BranchInfo">Информация об отделении или месте выдачи кредита</param>
/// <param name="AgreementNumber">Номер КД</param>
public record ExtraLoanAgreementInput(
    [property: DisplayName("Тип кредита")] string LoanType,
    [property: DisplayName("График платежей")]
    List<RepaymentScheduleRowInput>? RepaymentSchedule,
    [property: DisplayName("Наличие личного страхования у всех заемщиков")]
    bool InsuranceAllBorrowers,
    [property: DisplayName("Место выдачи кредита")]
    BranchInfoInput BranchInfo,
    [property: DisplayName("Номер КД")] string? AgreementNumber)
{
    /// <summary>
    ///     Надбавка при отказе страхования
    /// </summary>
    [DisplayName("Надбавка при отказе страхования")]
    public decimal? ExtraInterestRate { get; set; }

    /// <summary>
    ///     Процентная ставка
    /// </summary>
    [DisplayName("Процентная ставка")]
    public decimal? InterestRate { get; set; }

    /// <summary>
    ///     Нормализация входных данных
    /// </summary>
    public void Normalize()
    {
        // https://stackoverflow.com/a/15400942
        InterestRate = InterestRate.HasValue ? RateNormalizer.Normalize(InterestRate.Value) : null;
        ExtraInterestRate = ExtraInterestRate.HasValue ? RateNormalizer.Normalize(ExtraInterestRate.Value) : null;
    }
}