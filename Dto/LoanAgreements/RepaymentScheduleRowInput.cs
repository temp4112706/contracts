using System.ComponentModel;
using Some.Abstractions.LoanAgreements;
using Some.Dto.Monies;

namespace Some.Dto.LoanAgreements;

/// <summary>
///     Строка графика платежей по кредиту
/// </summary>
public record RepaymentScheduleRowInput : IRepaymentScheduleRow<MoneyInput, RepaymentAmountInput>
{
    /// <summary>
    ///     Порядковый номер платежа
    /// </summary>
    [DisplayName("Порядковый номер платежа")]
    public int Order { get; set; }

    /// <summary>
    ///     Сумма платежа по кредиту
    /// </summary>
    [DisplayName("Сумма платежа по кредиту")]
    public RepaymentAmountInput? RepaymentAmount { get; set; }

    /// <summary>
    ///     Остаток основного долга (Rest)
    /// </summary>
    [DisplayName("Остаток основного долга")]
    public MoneyInput PrincipalAmount { get; set; } = null!;

    /// <summary>
    ///     Дата платежа
    /// </summary>
    [DisplayName("Дата платежа")]
    public DateTime RepaymentDate { get; set; }
}