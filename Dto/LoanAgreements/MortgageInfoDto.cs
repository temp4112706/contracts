﻿using Some.Abstractions.LoanAgreements;
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Dto.LoanAgreements;

/// <summary>
///     DTO: Данные об ипотеке
/// </summary>
public record MortgageInfoDto : IMortgageInfo
{
    /// <summary>
    ///     Номер закладной
    /// </summary>
    public string? Number { get; init; } = null!;

    /// <summary>
    ///     Номер пула
    /// </summary>
    public string? PoolNumber { get; init; } = null!;

    /// <summary>
    ///     Дата включения в ИП
    /// </summary>
    public DateTime? PoolDate { get; init; }

    /// <summary>
    ///     Регистрационный номер ипотеки
    /// </summary>
    public string? RegistrationNumber { get; init; } = null!;

    /// <summary>
    ///     Дата регистрации ипотеки
    /// </summary>
    public DateTime? RegistrationDate { get; init; }

    /// <summary>
    ///     Ипотечный держатель
    /// </summary>
    public string? Holder { get; init; } = null!;

    /// <summary>
    ///     Ипотечный эмитент
    /// </summary>
    public string? Issuer { get; init; } = null!;
}