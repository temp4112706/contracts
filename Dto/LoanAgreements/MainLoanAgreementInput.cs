using System.ComponentModel;
using Some.Abstractions.Enums;
using Some.Dto.Monies;
using Some.Dto.Normalizers;

namespace Some.Dto.LoanAgreements;

/// <summary>
///     Основные данные по КД
/// </summary>
/// <param name="AgreementDateTime">Дата КД</param>
/// <param name="LoanAmount">Сумма кредита</param>
/// <param name="AgreementNumber">Номер КД</param>
/// <param name="AgreementPeriod">Срок кредитования</param>
/// <param name="BeneficiaryId">ИД выгодоприобретателя</param>
public record MainLoanAgreementInput(
    [property: DisplayName("ИД выгодоприобретателя")]
    BeneficiaryId BeneficiaryId,
    [property: DisplayName("Дата КД")] DateTime AgreementDateTime,
    [property: DisplayName("Сумма кредита")]
    MoneyInput LoanAmount,
    [property: DisplayName("Срок кредитования")]
    int AgreementPeriod,
    [property: DisplayName("Номер КД")] string? AgreementNumber
)
{
    /// <summary>
    ///     Надбавка при отказе страхования
    /// </summary>
    [DisplayName("Надбавка при отказе страхования")]
    public decimal? ExtraInterestRate { get; set; }

    /// <summary>
    ///     Процентная ставка
    /// </summary>
    [DisplayName("Процентная ставка")]
    public decimal? InterestRate { get; set; }

    /// <summary>
    ///     Нормализация входных данных
    /// </summary>
    public void Normalize()
    {
        // https://stackoverflow.com/a/15400942
        InterestRate = InterestRate.HasValue ? RateNormalizer.Normalize(InterestRate.Value) : null;
        ExtraInterestRate = ExtraInterestRate.HasValue ? RateNormalizer.Normalize(ExtraInterestRate.Value) : null;
    }
}