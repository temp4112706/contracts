using Some.Abstractions.Enums;

namespace Some.Dto.LoanAgreements;

/// <summary>
///     DTO: Найденный КД
/// </summary>
public record FindExtLoanAgreementDto : EntityDto<string>
{
    /// <summary>
    ///     Дата КД
    /// </summary>
    public DateTime AgreementDate { get; init; }

    /// <summary>
    ///     Длительность в месяцах
    /// </summary>
    public int AgreementPeriod { get; init; }

    /// <summary>
    ///     Дата начала
    /// </summary>
    public DateTime BeginDate { get; init; }

    /// <summary>
    ///     Замещики
    /// </summary>
    public IReadOnlyCollection<FindExtLoanAgreementBorrowerDto> Borrowers { get; set; } = null!;

    /// <summary>
    ///     Дата закрытия КД
    /// </summary>
    public DateTime? CloseDate { get; init; }

    /// <summary>
    ///     Валюта КД
    /// </summary>
    public string? Currency { get; init; }

    /// <summary>
    ///     Дата завершения
    /// </summary>
    public DateTime EndDate { get; init; }

    /// <summary>
    ///     ID счёта для погашения
    /// </summary>
    public string? IdAccPay { get; init; }

    /// <summary>
    ///     Текущая процентная ставка
    /// </summary>
    public decimal InterestRate { get; init; }

    /// <summary>
    ///     Общая сумма
    /// </summary>
    public decimal LoanAmount { get; init; }

    /// <summary>
    ///     Тип
    /// </summary>
    public string? LoanType { get; init; }

    /// <summary>
    ///     ОСЗ
    /// </summary>
    public decimal MainDebtAmount { get; init; }

    /// <summary>
    ///     Номер
    /// </summary>
    public string Number { get; init; } = null!;

    /// <summary>
    ///     Продукт
    /// </summary>
    public string? Product { get; init; }

    /// <summary>
    ///     Счёт для погашения кредита
    /// </summary>
    public string? SAccPay { get; init; }

    /// <summary>
    ///     Статус КД
    /// </summary>
    public LoanAgreementState? State { get; init; }
}