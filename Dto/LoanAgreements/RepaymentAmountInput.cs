using System.ComponentModel;
using Some.Abstractions.LoanAgreements;
using Some.Dto.Monies;

namespace Some.Dto.LoanAgreements;

/// <summary>
///     Сумма платежа по кредиту
/// </summary>
public record RepaymentAmountInput : IRepaymentAmount<MoneyInput>
{
    /// <summary>
    ///     Комиссии и прочие выплаты (Commission)
    /// </summary>
    [DisplayName("Комиссии и прочие выплаты")]
    public MoneyInput Fee { get; set; } = null!;

    /// <summary>
    ///     Полная сумма (Amount)
    /// </summary>
    [DisplayName("Полная сумма")]
    public MoneyInput Full { get; set; } = null!;

    /// <summary>
    ///     Проценты (Percent)
    /// </summary>
    [DisplayName("Проценты")]
    public MoneyInput Interest { get; set; } = null!;

    /// <summary>
    ///     Основной долг (Debt)
    /// </summary>
    [DisplayName("Основной долг")]
    public MoneyInput Principal { get; set; } = null!;
}