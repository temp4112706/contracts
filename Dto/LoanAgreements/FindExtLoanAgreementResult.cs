// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Dto.LoanAgreements;

/// <summary>
///     Результат поиска КД
/// </summary>
public class FindExtLoanAgreementResult
{
    /// <summary>
    ///     Найденный КД
    /// </summary>
    public FindExtLoanAgreementDto LoanAgreement { get; init; } = null!;

    /// <summary>
    ///     Связанные с этим КД ДС
    /// </summary>
    public ICollection<Guid> RelatedPolicies { get; init; } = null!;
}