﻿using System.Text.Json.Serialization;
using Some.Abstractions.LoanAgreements;
using Some.Dto.Monies;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global
namespace Some.Dto.LoanAgreements;

/// <summary>
///     DTO: Кредитный договор с графиком платежей
/// </summary>
public record LoanAgreementDto : FlatLoanAgreementDto,
    ILoanAgreement<MoneyDto, BranchInfoDto, MortgageInfoDto, OverdueInfoDto,
        InterestRateRowDto, RepaymentScheduleRowDto, RepaymentAmountDto>
{
    /// <summary>
    ///     График платежей
    /// </summary>
    [JsonIgnore]
    public RepaymentScheduleRowDto[]? RepaymentSchedule { get; init; }

    /// <summary>
    ///    Есть график платежей
    /// </summary>
    public bool HasRepaymentSchedule { get; set; }

    /// <summary>
    ///     Наличие личного страхования у всех заемщиков
    /// </summary>
    public bool? InsuranceAllBorrowers { get; init; }

    /// <inheritdoc />
    public decimal? ExtraInterestRate { get; init; }

    /// <summary>
    ///     Дата возможной изменения ставки
    /// </summary>
    public DateTime? InterestRateChangeDate { get; init; }
    
    /// <summary>
    ///     Информация об отделении или месте выдачи кредита
    /// </summary>
    public new BranchInfoDto BranchInfo { get; init; } = null!;

    /// <summary>
    ///     Начальная процентная ставка
    /// </summary>
    public decimal? BeginInterestRate { get; init; }

    /// <summary>
    ///     Данные об ипотеке
    /// </summary>
    public MortgageInfoDto? MortgageInfo { get; init; }

    /// <summary>
    ///     Информация о просроченном платеже
    /// </summary>
    public OverdueInfoDto? OverdueInfo { get; init; }

    /// <summary>
    ///     График изменений процентных ставок
    /// </summary>
    public InterestRateRowDto[]? InterestRateSchedule { get; init; }

    /// <summary>
    ///     Дата открытия договора
    /// </summary>
    public DateTime? BeginDate { get; init; }

    /// <summary>
    ///     Дата окончания договора
    /// </summary>
    public DateTime? EndDate { get; init; }

    /// <summary>
    ///     ID счёта для погашения
    /// </summary>
    public string? IdAccPay { get; init; }

    /// <summary>
    ///     Счёт для погашения кредита
    /// </summary>
    public string? SAccPay { get; init; }

    /// <summary>
    ///     Продукт
    /// </summary>
    public string? Product { get; init; }

    /// <summary>
    ///     Дата и время синхронизации
    /// </summary>
    public DateTime? SyncDateTime { get; init; }
}