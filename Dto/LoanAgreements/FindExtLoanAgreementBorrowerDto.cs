using Some.Abstractions.Enums;
using Some.Dto.Passports;

namespace Some.Dto.LoanAgreements;

/// <summary>
///     Данные заемщика
/// </summary>
/// <param name="FirstName">Имя</param>
/// <param name="LastName">Фамилия</param>
/// <param name="MiddleName">Отчество</param>
/// <param name="Document">Документ заемщика</param>
/// <param name="BirthDate">Дата рождения</param>
/// <param name="BirthPlace">Место рождения</param>
/// <param name="Inn">ИНН</param>
/// <param name="Snils">Снилс</param>
/// <param name="Citizenship">Гражданство</param>
/// <param name="ResidenceCountry">Страна проживания</param>
/// <param name="Gender">Пол</param>
/// <param name="MaritalState">Положение</param>
/// <param name="CellPhone">Номер телефона</param>
/// <param name="HomePhone">Домашний номер телефона</param>
/// <param name="WorkPhone">Рабочий номер телефона</param>
/// <param name="Email">Email</param>
/// <param name="RegistrationAddress">Адрес регистрации</param>
/// <param name="CurrentAddress">Фактический адрес проживания</param>
/// <param name="IsForeignPublicPerson">Является иностранным представителем</param>
public record FindExtLoanAgreementBorrowerDto(string FirstName, string LastName, string? MiddleName,
    PassportRfDto Document, DateTime BirthDate, string BirthPlace, string? Inn,
    string? Snils, string Citizenship, string ResidenceCountry, Gender Gender,
    MaritalState MaritalState, string CellPhone, string? HomePhone, string? WorkPhone, string? Email,
    string RegistrationAddress, string? CurrentAddress, bool IsForeignPublicPerson);