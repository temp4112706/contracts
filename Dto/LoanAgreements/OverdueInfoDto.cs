﻿using Some.Abstractions.LoanAgreements;
using Some.Dto.Monies;

// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global

namespace Some.Dto.LoanAgreements;

/// <summary>
///     DTO: Информация о просроченном платеже
/// </summary>
public record OverdueInfoDto : IOverdueInfo<MoneyDto>
{
    /// <summary>
    ///     Количество просроченных дней
    /// </summary>
    public int DurationDays { get; init; }

    /// <summary>
    ///     Просроченная ссудная задолженность
    /// </summary>
    public MoneyDto OverdueSum { get; init; } = null!;

    /// <summary>
    ///     Просроченные проценты
    /// </summary>
    public MoneyDto OverduePercentSum { get; init; } = null!;

    /// <summary>
    ///     Начисленные проценты
    /// </summary>
    public MoneyDto PercentPenaltySum { get; init; } = null!;

    /// <summary>
    ///     Начисленные проценты на просроченную ссудную задолженность
    /// </summary>
    public MoneyDto LoanPenaltySum { get; init; } = null!;

    /// <summary>
    ///     Пени/штрафы по просроченным процентам
    /// </summary>
    public MoneyDto FineSum { get; init; } = null!;

    /// <summary>
    ///     Пени/штрафы по просроченной ссудной задолженности
    /// </summary>
    public MoneyDto PenaltySum { get; init; } = null!;
}