﻿using Some.Abstractions.LoanAgreements;
using Some.Dto.Monies;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global

namespace Some.Dto.LoanAgreements;

/// <summary>
///     DTO: Сумма платежа по кредиту
/// </summary>
public record RepaymentAmountDto : IRepaymentAmount<MoneyDto>
{
    /// <summary>
    ///     Полная сумма
    /// </summary>
    public MoneyDto Full { get; init; } = null!;

    /// <summary>
    ///     Проценты
    /// </summary>
    public MoneyDto Interest { get; init; } = null!;

    /// <summary>
    ///     Основной долг
    /// </summary>
    public MoneyDto Principal { get; init; } = null!;

    /// <summary>
    ///     Комиссии и прочие выплаты
    /// </summary>
    public MoneyDto Fee { get; init; } = null!;
}