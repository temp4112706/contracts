using Some.Abstractions.LoanAgreements;

namespace Some.Dto.LoanAgreements;

/// <summary>
///     DTO: <inheritdoc />
/// </summary>
public record UpdateRepaymentScheduleRowDto(int Order, decimal PrincipalAmount, decimal Full, decimal Interest,
    decimal Principal, decimal Fee, DateTime RepaymentDate) : IUpdateRepaymentScheduleRow;