using Some.Dto.Beneficiaries;
using Some.Dto.Customers;
using Some.Abstractions.Enums;
using Some.Abstractions.LoanAgreements;
using Some.Dto.Monies;

// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global

namespace Some.Dto.LoanAgreements;

/// <summary>
///     DTO: Кредитный договор с основными полями
/// </summary>
public record FlatLoanAgreementDto : TrackableEntityDto<Guid>, IRequiredLoanAgreement<MoneyDto>
{
    /// <summary>
    ///     Номер КД
    /// </summary>
    public string? AgreementNumber { get; init; }

    /// <summary>
    ///     Выгодоприобретатель
    /// </summary>
    public IdentifierBeneficiaryDto? Beneficiary { get; init; }

    /// <summary>
    ///     Заемщик
    /// </summary>
    public FlatCustomerDto? Borrower { get; init; }

    /// <summary>
    ///     Информация об отделении или месте выдачи кредита
    /// </summary>
    public FlatBranchInfoDto BranchInfo { get; init; } = null!;

    /// <summary>
    ///     Дата закрытия КД
    /// </summary>
    public DateTime? CloseDate { get; init; }

    /// <summary>
    ///     Идентификатор кредитного договора в Новой Афине
    /// </summary>
    public string? ExternalId { get; init; }

    /// <summary>
    ///     Процентная ставка
    /// </summary>
    public decimal? InterestRate { get; init; }

    /// <summary>
    ///     Остаток ссудной задолженности
    /// </summary>
    public MoneyDto? MainDebtAmount { get; init; }

    /// <summary>
    ///     Статус
    /// </summary>
    public LoanAgreementState? State { get; init; }

    /// <summary>
    ///     Дата и время КД
    /// </summary>
    public DateTime AgreementDateTime { get; init; }

    /// <summary>
    ///     Срок кредитования (в месяцах)
    /// </summary>
    public int AgreementPeriod { get; init; }

    /// <summary>
    ///     Сумма ипотечного кредита
    /// </summary>
    public MoneyDto? LoanAmount { get; init; }

    /// <summary>
    ///     Вид кредита
    /// </summary>
    public string? LoanType { get; init; }
}