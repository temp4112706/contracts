﻿using System.ComponentModel;
using Some.Abstractions.LoanAgreements;

// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Dto.LoanAgreements;

/// <summary>
///     Данные для изменения ставки КД
/// </summary>
public record LoanAgreementUpdateRateInput : ILoanAgreementUpdateRate
{
    /// <summary>
    ///     Комментарий
    /// </summary>
    [DisplayName("Комментарий")]
    public string? Description { get; init; }

    /// <summary>
    ///     Дата изменения процентной ставки
    /// </summary>
    [DisplayName("Дата изменения процентной ставки")]
    public DateTime InterestRateDate { get; init; }

    /// <summary>
    ///     Новая ставка КД
    /// </summary>
    [DisplayName("Новая ставка КД")]
    public decimal InterestRate { get; set; }
}