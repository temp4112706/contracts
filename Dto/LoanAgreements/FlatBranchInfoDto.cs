﻿using Some.Dto.Employees;
using Some.Abstractions.LoanAgreements;

// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global

namespace Some.Dto.LoanAgreements;

/// <summary>
///     DTO: Информация об отделении или месте выдачи кредита с основными полями
/// </summary>
public record FlatBranchInfoDto : EntityDto<Guid>, IScalarBranchInfo
{
    /// <summary>
    ///     Код региона по ФИАС
    /// </summary>
    public string RegionCode { get; init; } = null!;

    /// <summary>
    ///     Наименование
    /// </summary>
    public string Name { get; init; } = null!;

    /// <summary>
    ///     Сотрудник банка
    /// </summary>
    public EmployeeDto? Employee { get; init; }
}