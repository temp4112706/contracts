﻿using Some.Abstractions.LoanAgreements;
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Dto.LoanAgreements;

/// <summary>
///     DTO: Строка измнения процентной ставки
/// </summary>
public record InterestRateRowDto : IInterestRateRow
{
    /// <summary>
    ///     Начальная процентная ставка
    /// </summary>
    public decimal StartInterestRate { get; init; }

    /// <summary>
    ///     Конечная процентная ставка
    /// </summary>
    public decimal EndInterestRate { get; init; }

    /// <summary>
    ///     Дата последнего изменения ставки
    /// </summary>
    public DateTime UpdatedAt { get; init; }
}