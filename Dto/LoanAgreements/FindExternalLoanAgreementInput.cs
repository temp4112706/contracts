// ReSharper disable UnusedAutoPropertyAccessor.Global

using System.ComponentModel;

namespace Some.Dto.LoanAgreements;

/// <summary>
///     Параметры поиска КД
/// </summary>
public record FindExternalLoanAgreementInput
{
    /// <summary>
    ///     Дата заключения КД
    /// </summary>
    [DisplayName("Дата заключения КД")]
    public DateTime? AgreementDate { get; init; }

    /// <summary>
    ///     Номер КД
    /// </summary>
    [DisplayName("Номер КД")]
    public string? AgreementNumber { get; init; }

    /// <summary>
    ///     Ид КД
    /// </summary>
    [DisplayName("Ид КД")]
    public string? Id { get; init; }
}