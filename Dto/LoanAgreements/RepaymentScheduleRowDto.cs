﻿using Some.Abstractions.LoanAgreements;
using Some.Dto.Monies;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Dto.LoanAgreements;

/// <summary>
///     DTO: Строка графика платежей по кредиту
/// </summary>
public record RepaymentScheduleRowDto : IRepaymentScheduleRow<MoneyDto, RepaymentAmountDto>
{
    /// <summary>
    ///     Порядковый номер платежа
    /// </summary>
    public int Order { get; init; }

    /// <summary>
    ///     Дата платежа
    /// </summary>
    public DateTime RepaymentDate { get; init; }

    /// <summary>
    ///     Сумма платежа по кредиту
    /// </summary>
    public RepaymentAmountDto? RepaymentAmount { get; init; } = null!;

    /// <summary>
    ///     Остаток основного долга
    /// </summary>
    public MoneyDto PrincipalAmount { get; init; } = null!;
}