﻿using System.ComponentModel;
using Some.Abstractions.Underwriting;
using Some.Dto.Normalizers;

namespace Some.Dto.Underwriting;

/// <summary>
///     Input: Строка графика тарифов
/// </summary>
public record TariffScheduleRowInput : ITariffScheduleRow
{
    /// <summary>
    ///     № Периода
    /// </summary>
    [DisplayName("№ Периода")]
    public int Number { get; set; }

    /// <summary>
    ///     Ставка тарифа страхования (в процентах)
    /// </summary>
    [DisplayName("Ставка тарифа страхования")]
    public decimal Rate { get; set; }

    /// <summary>
    ///     Нормализация входных данных
    /// </summary>
    public void Normalize() => Rate = RateNormalizer.Normalize(Rate);
}