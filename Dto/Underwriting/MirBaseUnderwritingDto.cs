using Some.Abstractions.Underwriting;
using Some.Abstractions.Enums;
using Some.Dto.Common;

namespace Some.Dto.Underwriting;

/// <summary>
///     DTO: Базовая модель запроcа тарифа с основными полями
/// </summary>
public abstract record MirBaseUnderwritingDto : EntityDto<Guid>, IScalarMirUnderwriting
{
    /// <summary>
    ///     Стейт запроса (в обработке, обработан)
    ///     Стейт "В процессе" возможен только в пользовательском андеррайтинге
    /// </summary>
    public MirUnderwritingState State { get; set; }

    /// <summary>
    ///     Пользовательский график андеррайтинга
    /// </summary>
    public TariffScheduleRowDto[]? CustomSchedule { get; set; }

    /// <summary>
    ///     Тариф страхования жизни
    /// </summary>
    public InsuranceTariffDto? Tariff { get; set; }

    /// <summary>
    ///     Онлайн андеррайтинг?
    /// </summary>
    public bool Online { get; set; }
}