using Some.Abstractions.Enums;
using Some.Abstractions.Underwriting;

namespace Some.Dto.Underwriting;

/// <summary>
///     DTO: Причина отказа
/// </summary>
public record MirTitleRejectionReasonDto : IMirTitleRejectionReason
{
    /// <summary>
    ///     Код причины отказа
    /// </summary>
    public TitleRejectionReasonCode Code { get; set; }

    /// <summary>
    ///     Сообщение об ошибке
    /// </summary>
    public string Message { get; set; } = null!;
}