using Some.Dto.Common;
using Some.Abstractions.Underwriting;
using Some.Dto.Monies;

namespace Some.Dto.Underwriting;

/// <summary>
///     DTO: Результат андеррайтинга страхования титула
/// </summary>
public record MirTitleUnderwritingDto : MirBaseUnderwritingDto, IMirTitleUnderwriting<MoneyDto,
    InsuranceTariffDto, TariffScheduleRowDto, MirTitleRejectionReasonDto>
{
    /// <summary>
    ///     Коды и сообщения отказа при проведении андеррайтинга страхования титула
    /// </summary>
    public MirTitleRejectionReasonDto[]? RejectionReasons { get; set; }
}