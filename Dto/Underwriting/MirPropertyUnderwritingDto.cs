using Some.Abstractions.Underwriting;
using Some.Dto.Common;
using Some.Dto.Monies;

namespace Some.Dto.Underwriting;

/// <summary>
///     DTO: Результат андеррайтинга страхования имущества
/// </summary>
public record MirPropertyUnderwritingDto : MirBaseUnderwritingDto, IMirPropertyUnderwriting<MoneyDto,
    InsuranceTariffDto, TariffScheduleRowDto, MirPropertyRejectionReasonDto>
{
    /// <summary>
    ///     Коды и сообщения отказа при проведении андеррайтинга страхования имущества
    /// </summary>
    public MirPropertyRejectionReasonDto[]? RejectionReasons { get; set; }
}