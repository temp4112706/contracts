using System.ComponentModel;

namespace Some.Dto.Underwriting;

/// <summary>
///     DTO: Данные для андеррайтинга страхования титула
/// </summary>
public record CreateMirTitleUnderwritingInput
{
    /// <summary>
    ///     ИД запроса страхования титула
    /// </summary>
    [DisplayName("ИД запроса страхования титула")]
    public Guid TitleInsuranceRequestId { get; set; }
}