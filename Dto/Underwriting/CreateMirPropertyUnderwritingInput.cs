using System.ComponentModel;

namespace Some.Dto.Underwriting;

/// <summary>
///     DTO: Данные для андеррайтинга страхования имущества
/// </summary>
public record CreateMirPropertyUnderwritingInput
{
    /// <summary>
    ///     ИД запроса страхования имущества
    /// </summary>
    [DisplayName("ИД запроса страхования имущества")]
    public Guid PropertyInsuranceRequestId { get; set; }
}