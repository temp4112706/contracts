using System.ComponentModel;

namespace Some.Dto.Underwriting;

/// <summary>
///     DTO: Данные при пользовательском одобении андеррайтинга
/// </summary>
public record ProcessingOfflineUnderwritingInput
{
    /// <summary>
    ///     Пользовательский график тарифов
    /// </summary>
    [DisplayName("График тарифов")]
    public TariffScheduleRowInput[] CustomSchedule { get; set; } = null!;
}