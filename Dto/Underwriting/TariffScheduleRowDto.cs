using System.Diagnostics.CodeAnalysis;
using Some.Abstractions.Underwriting;

namespace Some.Dto.Underwriting;

/// <summary>
///     DTO: Строка графика тарифов
/// </summary>
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
public record TariffScheduleRowDto : ITariffScheduleRow
{
    /// <inheritdoc />
    public int Number { get; set; }

    /// <inheritdoc />
    public decimal Rate { get; set; }
}