using System.Diagnostics.CodeAnalysis;
using Some.Abstractions.Underwriting;
using Some.Abstractions.Enums;

namespace Some.Dto.Underwriting;

/// <summary>
///     DTO: Причина отказа
/// </summary>
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
public record MirPropertyRejectionReasonDto : IMirPropertyRejectionReason
{
    /// <summary>
    ///     Код причины отказа
    /// </summary>
    public PropertyRejectionReasonCode Code { get; set; }

    /// <summary>
    ///     Сообщение об ошибке
    /// </summary>
    public string Message { get; set; } = null!;
}