using Some.Abstractions.Underwriting;
using Some.Dto.Common;
using Some.Dto.Monies;

namespace Some.Dto.Underwriting;

/// <summary>
///     DTO: Результат андеррайтинга страхования жизни
/// </summary>
public record MirLifeUnderwritingDto : MirBaseUnderwritingDto, IMirLifeUnderwriting<MoneyDto,
    InsuranceTariffDto, TariffScheduleRowDto, MirLifeRejectionReasonDto>
{
    /// <summary>
    ///     Коды и сообщения отказа при проведении андеррайтинга страхования жизни
    /// </summary>
    public MirLifeRejectionReasonDto[]? RejectionReasons { get; set; }
}