using System.ComponentModel;

namespace Some.Dto.Underwriting;

/// <summary>
///     DTO: Данные при пользовательском отклонении андеррайтинга
/// </summary>
public record RejectingOfflineUnderwritingInput
{
    /// <summary>
    ///     Комментарий при отклонении андеррайтинга
    /// </summary>
    [DisplayName("Комментарий")]
    public string Message { get; set; } = null!;
}