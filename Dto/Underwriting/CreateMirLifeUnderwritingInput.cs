using System.ComponentModel;
using Some.Abstractions.Enums;

namespace Some.Dto.Underwriting;

/// <summary>
///     DTO: Данные для андеррайтинга страхования жизни.
/// </summary>
public record CreateMirLifeUnderwritingInput
{
    /// <summary>
    ///     Индекс застрахованного лица
    /// </summary>
    [DisplayName("Индекс застрахованного лица")]
    public InsuredPersonIndex InsuredPersonIndex { get; set; }
}