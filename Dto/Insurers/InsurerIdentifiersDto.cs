using Some.Abstractions.Enums;

namespace Some.Dto.Insurers;

/// <summary>
///     Информация о СК
/// </summary>
/// <param name="Id">ИД</param>
/// <param name="Name">Название</param>
/// <param name="Inn">ИНН</param>
public record InsurerIdentifiersDto(InsurerId Id, string Name, string? Inn);