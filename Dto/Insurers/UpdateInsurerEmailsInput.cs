﻿using System.ComponentModel;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Dto.Insurers;

/// <summary>
///     Данные для обновления Emails СК
/// </summary>
public record UpdateInsurerEmailsInput
{
    /// <summary>
    ///     Почта для оффлайн андеррайтинга
    /// </summary>
    [DisplayName("Почта для оффлайн андеррайтинга")]
    public string? OfflineUnderwritingEmail { get; init; }

    /// <summary>
    ///     Почта для отчетов
    /// </summary>
    [DisplayName("Почта для отчетов")]
    public string? ReportEmail { get; init; }
}