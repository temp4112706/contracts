﻿using Some.Abstractions.Enums;

namespace Some.Dto.Insurers;

/// <summary>
///     Список КВ по доп видам страхования
/// </summary>
/// <param name="Commissions">Комиисионное вознаграждение</param>
/// <param name="InsuranceType">Тип страхования</param>
public record UpsertAdditionalInsuranceCommissionsInput(
    InsurerAdditionalInsuranceCommissionInput[] Commissions,
    AdditionalInsuranceType InsuranceType
);