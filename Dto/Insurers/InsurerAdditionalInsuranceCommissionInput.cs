﻿using System.ComponentModel;
using Some.Abstractions.Insurers;
using Some.Dto.Normalizers;

namespace Some.Dto.Insurers;

/// <summary>
///     Комиисионное вознаграждение по доп видам страхования
/// </summary>
/// <param name="PolicyFrom">Дата ДС от</param>
/// <param name="PolicyTo">Дата ДС до</param>
public record InsurerAdditionalInsuranceCommissionInput(
    [property: DisplayName("Дата ДС от")] DateTime? PolicyFrom,
    [property: DisplayName("Дата ДС до")] DateTime? PolicyTo
) : IScalarInsurerAdditionalInsuranceCommission
{
    /// <summary>
    ///     Процент
    /// </summary>
    [DisplayName("Процент")]
    public decimal Percent { get; set; }

    /// <summary>
    ///     Нормализация входных данных
    /// </summary>
    public void Normalize() => Percent = RateNormalizer.Normalize(Percent);
}