namespace Some.Dto.Insurers;

/// <summary>
///     Данные для сброса пароля
/// </summary>
/// <param name="Id">ИД пользователя</param>
/// <param name="NewPassword">Новый пароль</param>
public record ResetPasswordInsurerUserInput(string NewPassword);