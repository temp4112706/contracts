﻿using Some.Abstractions.Insurers;

namespace Some.Dto.Insurers;

/// <summary>
///     DTO : Комиисионное вознаграждение по доп видам страхования
/// </summary>
/// <param name="PolicyFrom">Дата ДС от</param>
/// <param name="PolicyTo">Дата ДС до</param>
/// <param name="Percent">Процент</param>
/// <param name="Order">Порядковый номер</param>
public record InsurerAdditionalInsuranceCommissionDto(
    DateTime? PolicyFrom,
    DateTime? PolicyTo,
    decimal Percent,
    int Order
) : IScalarInsurerAdditionalInsuranceCommission;