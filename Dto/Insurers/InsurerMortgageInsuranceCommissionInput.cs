﻿using System.ComponentModel;
using Some.Abstractions.Insurers;
using Some.Dto.Normalizers;

namespace Some.Dto.Insurers;

/// <summary>
///     Input: Комиисионное вознаграждение
/// </summary>
public record InsurerMortgageInsuranceCommissionInput : IScalarInsurerMortgageInsuranceCommission
{
    /// <summary>
    ///     Дата КД от
    /// </summary>
    [DisplayName("Дата КД от")]
    public DateTime? LoanAgreementFrom { get; init; }

    /// <summary>
    ///     Дата КД до
    /// </summary>
    [DisplayName("Дата КД до")]
    public DateTime? LoanAgreementTo { get; init; }

    /// <summary>
    ///     Процент
    /// </summary>
    [DisplayName("Процент")]
    public decimal Percent { get; set; }

    /// <summary>
    ///     Дата ДС от
    /// </summary>
    [DisplayName("Дата ДС от")]
    public DateTime? PolicyFrom { get; init; }

    /// <summary>
    ///     Дата ДС до
    /// </summary>
    [DisplayName("Дата ДС до")]
    public DateTime? PolicyTo { get; init; }

    /// <summary>
    ///     Нормализация входных данных
    /// </summary>
    public void Normalize() => Percent = RateNormalizer.Normalize(Percent);
}