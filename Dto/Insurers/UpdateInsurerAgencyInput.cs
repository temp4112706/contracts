using System.ComponentModel;

namespace Some.Dto.Insurers;

/// <summary>
///     Данные агентских данных
/// </summary>
public record UpdateInsurerAgencyInput
{
    /// <summary>
    ///     Дата агентского договора
    /// </summary>
    [DisplayName("Дата агентского договора")]
    public DateTime AgencyAgreementDate { get; init; }

    /// <summary>
    ///     № агентского договора
    /// </summary>
    [DisplayName("№ агентского договора")]
    public string AgencyAgreementNumber { get; init; } = null!;
}