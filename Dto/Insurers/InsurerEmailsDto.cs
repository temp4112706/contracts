using Some.Abstractions.Enums;

namespace Some.Dto.Insurers;

/// <summary>
///     Информация о Emails СК
/// </summary>
/// <param name="Id">ИД</param>
/// <param name="ReportEmail">Почта для отчетов</param>
/// <param name="OfflineUnderwritingEmail">Почта для офлайн андер</param>
public record InsurerEmailsDto(InsurerId Id, string? ReportEmail, string? OfflineUnderwritingEmail);