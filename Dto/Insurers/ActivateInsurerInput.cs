namespace Some.Dto.Insurers;

/// <summary>
///     Данные для активации СК
/// </summary>
public record ActivateInsurerInput : UpdateInsurerAgencyInput;