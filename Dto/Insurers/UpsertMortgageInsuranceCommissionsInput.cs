﻿using System.ComponentModel;
using Some.Abstractions.Enums;

namespace Some.Dto.Insurers;

/// <summary>
///     КВ по ипотечному страхованию
/// </summary>
/// <param name="BeneficiaryId">Выгодоприобретатель</param>
/// <param name="Commissions">КВ</param>
public record UpsertMortgageInsuranceCommissionsInput(
    [property: DisplayName("Выгодоприобретатель")] BeneficiaryId BeneficiaryId,
    [property: DisplayName("КВ")] InsurerMortgageInsuranceCommissionInput[] Commissions
);