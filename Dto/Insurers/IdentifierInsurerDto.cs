using Some.Abstractions.Organizations;

namespace Some.Dto.Insurers;

/// <summary>
///     DTO: Страховая компания с основными полями
/// </summary>
public record IdentifierInsurerDto : EntityDto<string>, IFlatOrganization
{
    /// <summary>
    ///     Код страховой компании
    /// </summary>
    public override string Id { get; init; } = null!;

    /// <summary>
    ///     Наименование
    /// </summary>
    public string Name { get; init; } = null!;
}