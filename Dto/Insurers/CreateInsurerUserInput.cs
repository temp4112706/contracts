namespace Some.Dto.Insurers;

/// <summary>
///     Данные для создание пользователя СК
/// </summary>
/// <param name="FullName">ФИО</param>
/// <param name="Email">Email</param>
/// <param name="Password">Пароль</param>
/// <param name="Enabled">Включен/отключен</param>
public record CreateInsurerUserInput(string FullName, string Email, string Password, bool Enabled);