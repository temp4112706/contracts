﻿// ReSharper disable UnusedAutoPropertyAccessor.Global

using Some.Abstractions.Insurers;

namespace Some.Dto.Insurers;

/// <summary>
///     DTO: Комиисионное вознаграждение
/// </summary>
public record InsurerMortgageInsuranceCommissionDto : IScalarInsurerMortgageInsuranceCommission
{
    /// <summary>
    ///     Дата КД от
    /// </summary>
    public DateTime? LoanAgreementFrom { get; init; }

    /// <summary>
    ///     Дата КД до
    /// </summary>
    public DateTime? LoanAgreementTo { get; init; }

    /// <summary>
    ///     Процент
    /// </summary>
    public decimal Percent { get; init; }

    /// <summary>
    ///     Дата ДС от
    /// </summary>
    public DateTime? PolicyFrom { get; init; }

    /// <summary>
    ///     Дата ДС до
    /// </summary>
    public DateTime? PolicyTo { get; init; }
}