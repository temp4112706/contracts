namespace Some.Dto.Insurers;

/// <summary>
///     Пользователь СК
/// </summary>
/// <param name="Id">ИД</param>
/// <param name="FullName">ФИО</param>
/// <param name="Email">Email</param>
/// <param name="Enabled">Включен/отключен</param>
/// <param name="CreatedAt">Дата создания</param>
public record InsurerUserDto(Guid Id, string FullName, string Email, bool Enabled, DateTime CreatedAt);