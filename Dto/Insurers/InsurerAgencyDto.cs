using Some.Abstractions.Enums;

namespace Some.Dto.Insurers;

/// <summary>
///     Информация о агентском договоре СК
/// </summary>
/// <param name="Id">ИД СК</param>
/// <param name="AgencyAgreementDate">Дата агентского договора</param>
/// <param name="AgencyAgreementNumber">№ агентского договора</param>
public record InsurerAgencyDto(InsurerId Id, DateTime AgencyAgreementDate, string AgencyAgreementNumber);