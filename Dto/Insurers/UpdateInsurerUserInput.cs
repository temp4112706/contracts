namespace Some.Dto.Insurers;

/// <summary>
///     Данные для изменения пользователя СК
/// </summary>
/// <param name="FullName">ФИО</param>
/// <param name="Enabled">Включен/отключен</param>
public record UpdateInsurerUserInput(string FullName, bool Enabled);