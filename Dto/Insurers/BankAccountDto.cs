using Some.Abstractions.Insurers;
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global

namespace Some.Dto.Insurers;

/// <summary>
///     DTO: Банковский аккаунт
/// </summary>
public record BankAccountDto : IBankAccount
{
    /// <summary>
    ///     20-значный номер счета
    /// </summary>
    public string AccountNumber { get; init; } = null!;

    /// <summary>
    ///     БИК Банка
    /// </summary>
    public string Bic { get; init; } = null!;

    /// <summary>
    ///     Корреспондентский счет
    /// </summary>
    public string CorrespondentAccount { get; init; } = null!;

    /// <summary>
    ///     Название банка
    /// </summary>
    public string Name { get; init; } = null!;
}