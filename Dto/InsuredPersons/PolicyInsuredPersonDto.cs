﻿using Some.Dto.Customers;
using Some.Abstractions.Enums;
using Some.Abstractions.InsuredPersons;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Dto.InsuredPersons;

/// <summary>
///     DTO: Застрахованное лицо для ДС
/// </summary>
public record PolicyInsuredPersonDto : EntityDto<Guid>, IScalarInsuredPerson
{
    /// <summary>
    ///     Персональные данные застрахованного лица
    /// </summary>
    public CustomerDto Customer { get; init; } = null!;

    /// <summary>
    ///     Доля созаемщика в процентах
    /// </summary>
    public decimal LoanShare { get; init; }

    /// <summary>
    ///     Позиция
    /// </summary>
    public InsuredPersonIndex Index { get; init; }
}