using Some.Dto.Customers;
using Some.Abstractions.Enums;
using Some.Abstractions.InsuredPersons;

// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable UnusedMember.Global

namespace Some.Dto.InsuredPersons;

/// <summary>
///     DTO: Застрахованное лицо с основными полями
/// </summary>
public record FlatInsuredPersonDto : EntityDto<Guid>, IScalarInsuredPerson
{
    /// <summary>
    ///     Персональные данные застрахованного лица
    /// </summary>
    public FlatCustomerDto Customer { get; init; } = null!;

    /// <summary>
    ///     Доля созаемщика в процентах
    /// </summary>
    public decimal LoanShare { get; init; }

    /// <summary>
    ///     Позиция
    /// </summary>
    public InsuredPersonIndex Index { get; init; }
}