using Some.Abstractions.InsuredPersons;
using Some.Dto.Customers;
using Some.Dto.HealthQuestionnaires;
using Some.Dto.HealthSurveys;
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Dto.InsuredPersons;

/// <summary>
///     DTO: Застрахованное лицо
/// </summary>
public record InsuredPersonDto : PolicyInsuredPersonDto,
    IInsuredPerson<CustomerDto, HealthQuestionnaireDto, HealthSurveyDto>
{
    /// <summary>
    ///     Медицинское обследование
    /// </summary>
    public HealthSurveyDto? HealthSurvey { get; init; }

    /// <summary>
    ///     Медицинская анкета
    /// </summary>
    public HealthQuestionnaireDto? HealthQuestionnaire { get; set; }
}