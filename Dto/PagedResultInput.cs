using System.ComponentModel.DataAnnotations;
using Some.Abstractions.Result;

namespace Some.Dto;

/// <summary>
///     Реализация <see cref="IPagedResultInput" />.
/// </summary>
[Serializable]
public record PagedResultInput : LimitedResultInput, IPagedResultInput
{
    /// <summary>
    ///     Пропустить кол-во
    /// </summary>
    [Range(0, int.MaxValue)]
    public virtual int SkipCount { get; init; }
}