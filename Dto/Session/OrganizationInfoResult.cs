namespace Some.Dto.Session;

/// <summary>
///     Данные об организации
/// </summary>
/// <param name="Id">ИД</param>
/// <param name="Name">Название</param>
public record OrganizationInfoResult(string Id, string Name);