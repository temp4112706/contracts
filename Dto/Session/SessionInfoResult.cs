namespace Some.Dto.Session;

/// <summary>
///     DTO: Данные текущей сессии
/// </summary>
/// <param name="Id">ИД</param>
/// <param name="Username">Логин</param>
/// <param name="Email">Email</param>
/// <param name="FullName">ФИО</param>
/// <param name="Organization">Организация</param>
/// <param name="Enabled">Активен</param>
/// <param name="EmailVerified">Email подтвержден</param>
/// <param name="Permissions">Доступные права</param>
public record SessionInfoResult(string Id, string Username, string? Email, string FullName, 
    OrganizationInfoResult Organization, bool Enabled, bool EmailVerified, string[] Permissions);