using System.ComponentModel;
using Some.Abstractions.Enums;
using Some.Abstractions.Occupations;
using Some.Dto.Employers;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Dto.Occupations;

/// <summary>
///     Добавление информация о занятости клиента
/// </summary>
public record OccupationInput : IOccupation<EmployerInput>
{
    /// <summary>
    ///     Данные о работодателе
    /// </summary>
    [DisplayName("Данные о работодателе")]
    public EmployerInput? Employer { get; init; }

    /// <inheritdoc />
    [DisplayName("Тип занятости")]
    public EmploymentKind EmploymentKind { get; init; }

    /// <inheritdoc />
    [DisplayName("Должность связана с повышенным риском")]
    public bool IsIncreasedRisk { get; init; }

    /// <inheritdoc />
    [DisplayName(
        "Должность связана с работой в районах с неблагоприятным климатом, с вооруженными конфликтами / другая (с риском для здоровья)")]
    public bool IsWorkingInDangerousPlace { get; init; }

    /// <inheritdoc />
    [DisplayName(
        "Деятельность связана с работой с горючими, взрывчатыми, опасными химическими, радиоактивными веществами")]
    public bool IsWorkingWithDangerousSubstances { get; init; }

    /// <inheritdoc />
    [DisplayName("Должность")]
    public string Position { get; init; } = null!;
}