using Some.Abstractions.Enums;
using Some.Abstractions.Occupations;
using Some.Dto.Employers;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Dto.Occupations;

/// <summary>
///     DTO: Информация о занятости клиента
/// </summary>
public record OccupationDto : EntityDto<Guid>, IOccupation<EmployerDto>
{
    /// <summary>
    ///     Данные о работодателе
    /// </summary>
    public EmployerDto? Employer { get; init; }

    /// <inheritdoc />
    public EmploymentKind EmploymentKind { get; init; }

    /// <inheritdoc />
    public bool IsIncreasedRisk { get; init; }

    /// <inheritdoc />
    public bool IsWorkingInDangerousPlace { get; init; }

    /// <inheritdoc />
    public bool IsWorkingWithDangerousSubstances { get; init; }

    /// <inheritdoc />
    public string Position { get; init; } = null!;
}