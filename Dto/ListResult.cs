using Some.Abstractions.Result;

namespace Some.Dto;

/// <summary>
///     Реализация <see cref="IListResult{T}" />.
/// </summary>
[Serializable]
public record ListResult<T> : IListResult<T>
{
    private IReadOnlyList<T>? _items;

    /// <summary>
    ///     <see cref="ListResult{T}" />
    /// </summary>
    public ListResult()
    {
    }

    /// <summary>
    ///     <see cref="ListResult{T}" />
    /// </summary>
    public ListResult(IReadOnlyList<T> items)
    {
        Items = items;
    }

    /// <summary>
    ///     Элементы
    /// </summary>
    public IReadOnlyList<T> Items
    {
        get => _items ??= new List<T>();
        init => _items = value;
    }
}