﻿using Some.Abstractions;
using Some.Abstractions.Companies;
using Some.Dto.Addresses;

namespace Some.Dto.Companies;

public record CompanyDto(string Id, string Name, string FullName, string? Email, AddressDto Address)
    : IEntityDto<string>, ICompany<AddressDto>;