﻿using Some.Abstractions;
using Some.Abstractions.Companies;

namespace Some.Dto.Companies;

/// <summary>
///     Компания
/// </summary>
/// <param name="Id">ID компании</param>
/// <param name="Name">Название компании</param>
public record FlatCompanyDto(string Id, string Name) : IEntityDto<string>, IScalarCompany;