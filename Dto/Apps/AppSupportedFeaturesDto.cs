namespace Some.Dto.Apps;

/// <summary>
///     DTO: Поддерживаемые функции приложения
/// </summary>
/// <param name="AdapterOfLoans">Адаптер кредитных договоров</param>
/// <param name="MutualSettlements">Поддерживает АС</param>
/// <param name="PlatformUrl">Адрес Плафтормы, если Страховка часть Платформы</param>
/// <param name="DaData">Данные для работы с сервисом DaData</param>
public record AppSupportedFeaturesDto(bool AdapterOfLoans, bool MutualSettlements, string? PlatformUrl,
    DaDataFeaturesDto? DaData);