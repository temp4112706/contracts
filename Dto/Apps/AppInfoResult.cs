using Some.Abstractions.Enums;

namespace Some.Dto.Apps;

/// <summary>
///     DTO: Информация о сервисе
/// </summary>
/// <param name="Version">Версия приложения</param>
/// <param name="Mode">Вид приложения</param>
/// <param name="SupportedFeatures">Поддерживаемые функции</param>
public record AppInfoResult(string Version, AppMode Mode, AppSupportedFeaturesDto SupportedFeatures);