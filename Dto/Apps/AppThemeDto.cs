namespace Some.Dto.Apps;

/// <summary>
///     Тема приложения
/// </summary>
/// <param name="PrimaryColor">Главный цвет</param>
/// <param name="SecondaryColor">Второстепенный цвет</param>
/// <param name="LogoUrl">Логотоип компании</param>
public record AppThemeDto(string PrimaryColor, string? SecondaryColor, string? LogoUrl);