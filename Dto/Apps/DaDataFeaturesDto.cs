﻿namespace Some.Dto.Apps;

/// <summary>
///     DTO: поддержки DaData
/// </summary>
/// <param name="Token">Токен</param>
/// <param name="Url">Ссылка на сервис</param>
public record DaDataFeaturesDto(string? Token, string Url);