using Some.Abstractions.Result;

namespace Some.Dto;

/// <summary>
///     Реализация <see cref="IPagedResult{T}" />.
/// </summary>
[Serializable]
public record PagedResult<T> : ListResult<T>, IPagedResult<T>
{
    /// <inheritdoc />
    public PagedResult()
    {
    }

    /// <inheritdoc />
    public PagedResult(int totalCount, IReadOnlyList<T> items)
        : base(items)
    {
        TotalCount = totalCount;
    }

    /// <summary>
    ///     Количество элементов.
    /// </summary>
    public int TotalCount { get; init; }
}