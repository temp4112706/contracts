using Some.Abstractions.Enums;

namespace Some.Dto.BeneficiaryInsurerMatrix;

/// <summary>
///     Данные для включения/отключения Выгодоприобретатель/СК
/// </summary>
/// <param name="BeneficiaryIds">Список Выгодоприобретателей</param>
/// <param name="InsurerId"СК</param>
/// <param name="Active">Активна?</param>
public record ToggleBeneficiaryInsurerMatrixCellInput(BeneficiaryId[] BeneficiaryIds, InsurerId InsurerId, bool Active);