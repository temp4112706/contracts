using Some.Abstractions.Enums;

namespace Some.Dto.BeneficiaryInsurerMatrix;

/// <summary>
///     Ячейка матрицы Выгодоприобретатель-СК
/// </summary>
/// <param name="InsurerId">СК</param>
/// <param name="Active">Активна</param>
/// <param name="Info">Информация об онлайн условиях и тарифах </param>
/// <param name="Disabled">Отключена?</param>
public record BeneficiaryInsurerMatrixCellDto(
    InsurerId InsurerId,
    bool Active,
    BeneficiaryInsurerMatrixCellInfoDto Info,
    bool Disabled
);