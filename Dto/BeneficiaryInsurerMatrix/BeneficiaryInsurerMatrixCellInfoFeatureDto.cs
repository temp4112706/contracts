namespace Some.Dto.BeneficiaryInsurerMatrix;

/// <summary>
///     Информация об онлайн условиях/тарифах
/// </summary>
/// <param name="Filled">Заполнен?</param>
/// <param name="CanFilled">Можно заполнить?</param>
public record BeneficiaryInsurerMatrixCellInfoFeatureDto(bool Filled, bool CanFilled)
{
    /// <summary>
    ///     Можно заполнять
    /// </summary>
    public static BeneficiaryInsurerMatrixCellInfoFeatureDto CanFill(bool filled) => new(filled, true);

    /// <summary>
    ///     Нельзя заполнять
    /// </summary>
    public static BeneficiaryInsurerMatrixCellInfoFeatureDto CanNotFill() => new(false, false);
}