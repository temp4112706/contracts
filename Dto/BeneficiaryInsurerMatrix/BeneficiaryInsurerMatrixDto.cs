using Some.Abstractions.Enums;

// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Dto.BeneficiaryInsurerMatrix;

/// <summary>
///     Матрица Выгодоприобретатель-СК
/// </summary>
public record BeneficiaryInsurerMatrixDto
{
    /// <summary>
    ///     Ячейки матрицы
    /// </summary>
    public Dictionary<BeneficiaryId, List<BeneficiaryInsurerMatrixCellDto>> Cells { get; set; } = null!;

    /// <summary>
    ///     Заголовки матрицы
    /// </summary>
    public BeneficiaryInsurerMatrixHeadersDto Headers { get; set; } = null!;
}