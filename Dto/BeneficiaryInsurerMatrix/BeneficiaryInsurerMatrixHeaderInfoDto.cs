namespace Some.Dto.BeneficiaryInsurerMatrix;

/// <summary>
///     Заголовки матрицы Выгодоприобретатель-СК
/// </summary>
/// <param name="Name">Название</param>
/// <param name="Active">Активна</param>
/// <param name="Disabled">Отключена?</param>
public record BeneficiaryInsurerMatrixHeaderInfoDto(string Name, bool Active, bool Disabled);