namespace Some.Dto.BeneficiaryInsurerMatrix;

/// <summary>
///     Информация об онлайн условиях и тарифах 
/// </summary>
/// <param name="OnlineUnderwritingCriteria">Условия онлайн андер</param>
/// <param name="OnlineUnderwritingRates">Тарифы онлайн андер</param>
public record BeneficiaryInsurerMatrixCellInfoDto(
    BeneficiaryInsurerMatrixCellInfoFeatureDto OnlineUnderwritingCriteria,
    BeneficiaryInsurerMatrixCellInfoFeatureDto OnlineUnderwritingRates
)
{
    /// <summary>
    ///     Нельзя заполнять
    /// </summary>
    public static BeneficiaryInsurerMatrixCellInfoDto CanNotFill() => new(
        BeneficiaryInsurerMatrixCellInfoFeatureDto.CanNotFill(),
        BeneficiaryInsurerMatrixCellInfoFeatureDto.CanNotFill());
}