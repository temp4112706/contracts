using Some.Abstractions.Enums;

namespace Some.Dto.BeneficiaryInsurerMatrix;

/// <summary>
///     Ячейка матрицы Выгодоприобретатель-СК
/// </summary>
public record BeneficiaryInsurerMatrixHeadersDto
{
    /// <summary>
    ///     Параметры СК
    /// </summary>
    public Dictionary<InsurerId, BeneficiaryInsurerMatrixHeaderInfoDto> Insurers { get; set; } = null!;
    
    /// <summary>
    ///     Параметры Выгодоприобретателей
    /// </summary>
    public Dictionary<BeneficiaryId, BeneficiaryInsurerMatrixHeaderInfoDto> Beneficiaries { get; set; } = null!;
}