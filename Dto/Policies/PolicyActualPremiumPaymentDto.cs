﻿using System.Diagnostics.CodeAnalysis;
using Some.Abstractions.Policies;
using Some.Dto.Attachments;
using Some.Dto.Monies;

namespace Some.Dto.Policies;

/// <summary>
///     DTO: Оплата периода в ДС
/// </summary>
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
[SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Global")]
public class PolicyActualPremiumPaymentDto : IPolicyActualPremiumPayment<MoneyDto, FlatAttachmentDto?,
    PolicyPlannedPaymentDto, PolicyActualPaymentDto>
{
    /// <summary>
    ///     Период
    /// </summary>
    public int? Period { get; set; }

    /// <summary>
    ///     Данные по плановой оплате
    /// </summary>
    public PolicyPlannedPaymentDto? PlannedPayment { get; set; }

    /// <summary>
    ///     Данные по фактической оплате
    /// </summary>
    public PolicyActualPaymentDto? ActualPayment { get; set; }
}