﻿using System.ComponentModel;
using Some.Abstractions.Policies.Actions;

// ReSharper disable PropertyCanBeMadeInitOnly.Global

namespace Some.Dto.Policies;

/// <summary>
///     DTO: Данные для изменения комментария в ДС
/// </summary>
public record PolicyUpdateCommentInput : IPolicyUpdateComment
{
    /// <summary>
    ///     Комментарий
    /// </summary>
    [DisplayName("Комментарий")]
    public string? Comment { get; set; }
}