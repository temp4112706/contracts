using System.ComponentModel;

namespace Some.Dto.Policies;

/// <summary>
///     DTO: Данные для изменения места выдачи ДС
/// </summary>
public record PolicyUpdateDetentionPlaceInput(
    [property: DisplayName("Место выдачи")]
    string? DetentionPlace
);