﻿using System.Diagnostics.CodeAnalysis;
using Some.Abstractions.Policies;
using Some.Dto.Attachments;
using Some.Dto.Common;
using Some.Dto.Monies;

namespace Some.Dto.Policies;

/// <summary>
///     DTO: Текущая страховая премия
/// </summary>
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
public record PolicyActualPaymentDto : ActualPaymentDto, IPolicyActualPayment<MoneyDto, FlatAttachmentDto?>
{
    /// <summary>
    ///     Ид квитанции
    /// </summary>
    public FlatAttachmentDto? Attachment { get; set; }
}