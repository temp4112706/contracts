using System.Diagnostics.CodeAnalysis;
using Some.Abstractions.Policies;
using Some.Abstractions.Enums;
using Some.Dto.Common;
using Some.Dto.Monies;

namespace Some.Dto.Policies;

/// <summary>
///     DTO: Строка графика страхования
/// </summary>
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
public class InsuranceScheduleRowDto : IInsuranceScheduleRow<MoneyDto, InsurancePeriodDto, InsuranceTariffDto>
{
    /// <summary>
    ///     Период (год) страхования
    /// </summary>
    public InsurancePeriodDto Period { get; set; } = null!;

    /// <summary>
    ///     Тариф
    /// </summary>
    public InsuranceTariffDto Tariff { get; set; } = null!;
        
    /// <summary>
    ///     Статус
    /// </summary>
    public PolicyInsurancePeriodState State { get; set; }

    /// <summary>
    ///     Оплата периода
    /// </summary>
    public InsuranceScheduleRowPaymentDto Payment { get; set; } = null!;
}