﻿using System.ComponentModel;
using Some.Abstractions.Enums;

// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Dto.Policies.Accidents;

/// <summary>
///     DTO: Данные для создания страхового случая
/// </summary>
public record PolicyInsuranceAccidentInput
{
    /// <summary>
    ///     Дата страхового случая
    /// </summary>
    [DisplayName("Дата страхового случая")]
    public DateTime AccidentDate { get; set; }

    /// <summary>
    ///     Ид файла
    /// </summary>
    [DisplayName("Ид файла")]
    public Guid AttachmentId { get; set; }

    /// <summary>
    ///     Комментарий
    /// </summary>
    [DisplayName("Комментарий")]
    public string? Comment { get; set; }

    /// <summary>
    ///     Ид застрахованого лица
    /// </summary>
    [DisplayName("Ид застрахованого лица")]
    public Guid CustomerId { get; set; }

    /// <summary>
    ///     Номер счета для зачисления страхового случая
    /// </summary>
    [DisplayName("Номер счета")]
    public string? NAcc { get; set; }

    /// <summary>
    ///     Дата обращения
    /// </summary>
    [DisplayName("Дата обращения")]
    public DateTime RequestDate { get; set; }

    /// <summary>
    ///     Причина обращения
    /// </summary>
    [DisplayName("Причина обращения")]
    public PolicyInsuranceAccidentReason RequestReason { get; set; }

    /// <summary>
    ///     Риск
    /// </summary>
    [DisplayName("Риск")]
    public InsuranceRisk? Risk { get; set; }
}