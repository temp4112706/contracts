using Some.Dto.Monies;

namespace Some.Dto.Policies.Accidents;

/// <summary>
///     DTO: Информация по выплате по страховому случаю
/// </summary>
public record PolicyInsuranceAccidentPaymentDto
{
    /// <summary>
    ///     Последняя Дата перечисления
    /// </summary>
    public DateTime LastPaymentDate { get; init; }

    /// <summary>
    ///     Общая сумма выплат
    /// </summary>
    public MoneyDto TotalPaymentAmount { get; init; } = null!;
}