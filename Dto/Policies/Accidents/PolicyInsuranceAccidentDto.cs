﻿using Some.Dto.Attachments;
using Some.Dto.Customers;
using Some.Abstractions.Enums;
using Some.Abstractions.Policies;
using Newtonsoft.Json;

// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Dto.Policies.Accidents;

/// <summary>
///     DTO: Страховой случай
/// </summary>
public record PolicyInsuranceAccidentDto : TrackableEntityDto<Guid>,
    IPolicyInsuranceAccident<PolicyInsuranceAccidentPaymentOperationDto>
{
    /// <summary>
    ///     Файл
    /// </summary>
    public FlatAttachmentDto? Attachment { get; set; } = null!;

    /// <summary>
    ///     Заастрахованное лицо
    /// </summary>
    public FlatCustomerDto Customer { get; set; } = null!;

    /// <summary>
    ///     Оплата
    /// </summary>
    public PolicyInsuranceAccidentPaymentDto? Payment { get; set; }

    /// <summary>
    ///     Комментарий
    /// </summary>
    public string? Comment { get; set; }

    /// <summary>
    ///     Операции оплаты
    /// </summary>
    [JsonIgnore]
    public PolicyInsuranceAccidentPaymentOperationDto[]? PaymentOperations { get; set; }

    /// <summary>
    ///     Дата обращения
    /// </summary>
    public DateTime RequestDate { get; set; }

    /// <summary>
    ///     Причина обращения
    /// </summary>
    public PolicyInsuranceAccidentReason RequestReason { get; set; }

    /// <summary>
    ///     Риск
    /// </summary>
    public InsuranceRisk? Risk { get; set; }

    /// <summary>
    ///     Дата страхового случая
    /// </summary>
    public DateTime AccidentDate { get; set; }

    /// <summary>
    ///     Номер счета для зачисления
    /// </summary>
    public string? NAcc { get; set; }

    /// <summary>
    ///     Статус
    /// </summary>
    public InsuranceAccidentState State { get; set; }
}