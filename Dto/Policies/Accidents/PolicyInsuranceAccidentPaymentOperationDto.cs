using Some.Abstractions.Policies;

namespace Some.Dto.Policies.Accidents;

/// DTO: <inheritdoc />
public record PolicyInsuranceAccidentPaymentOperationDto : IPolicyInsuranceAccidentPaymentOperation
{
    /// <summary>
    ///     Тип аккаунта
    /// </summary>
    public string AccountType { get; set; } = null!;

    /// <summary>
    ///     Код валюты транзакции
    /// </summary>
    public int CurrencyCode { get; set; }

    /// <summary>
    ///     Сумма транзакции
    /// </summary>
    public decimal TransactionSum { get; set; }

    /// <summary>
    ///     Баланс
    /// </summary>
    public decimal Balance { get; set; }

    /// <summary>
    ///     Дата операции
    /// </summary>
    public DateTime OperationDate { get; set; }

    /// <summary>
    ///     Тип операции
    /// </summary>
    public string OperationType { get; set; } = null!;
}