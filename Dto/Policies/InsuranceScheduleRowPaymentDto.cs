using System.Diagnostics.CodeAnalysis;

namespace Some.Dto.Policies;

/// <summary>
///     DTO: Результат оплаты периода
/// </summary>
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
public class InsuranceScheduleRowPaymentDto
{
        
    /// <summary>
    ///     Планируемая дата оплаты
    /// </summary>
    public DateTime PlannedDate { get; set; }

    /// <summary>
    ///     По умолчанию
    /// </summary>
    public static InsuranceScheduleRowPaymentDto Default => new InsuranceScheduleRowPaymentDto
    {
        PlannedDate = DateTime.MinValue
    };
}