﻿namespace Some.Dto.Policies.EditRisks;

/// <summary>
///     Данные для изменения риска
/// </summary>
public abstract class BasePolicyEditRiskInputItemData
{
    /// <summary>
    ///     График страхования
    /// </summary>
    public ICollection<PolicyEditRisksScheduleInput> Schedule { get; set; } = null!;
}