﻿namespace Some.Dto.Policies.EditRisks;

/// <summary>
///     Операция изменения данных о риске титула
/// </summary>
public class PolicyEditTitleRiskInputItem : BasePolicyEditRiskInputItem<PolicyEditTitleRiskInputItemData>
{
    /// <summary>
    ///     ИД риска
    /// </summary>
    public Guid RiskId { get; set; }
}