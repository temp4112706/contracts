﻿namespace Some.Dto.Policies.EditRisks;

/// <summary>
///     Операция изменения данных по риску имущества
/// </summary>
public class PolicyEditPropertyRiskInputItem : BasePolicyEditRiskInputItem<PolicyEditPropertyRiskInputItemData>
{
    /// <summary>
    ///     ИД риска
    /// </summary>
    public Guid RiskId { get; set; }
}