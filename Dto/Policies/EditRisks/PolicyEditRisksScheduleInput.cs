﻿using System.ComponentModel;
using Some.Abstractions.Underwriting;

// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Dto.Policies.EditRisks;

/// <summary>
///     Input: Строка графика тарифов
/// </summary>
public record PolicyEditRisksScheduleInput : ITariffScheduleRow
{
    /// <summary>
    ///     Страховая сумма
    /// </summary>
    [DisplayName("Страховая сумма")]
    public decimal InsuranceAmount { get; set; }

    /// <summary>
    ///     № Периода
    /// </summary>
    [DisplayName("№ Периода")]
    public int Number { get; set; }

    /// <summary>
    ///     Ставка тарифа страхования (в процентах)
    /// </summary>
    [DisplayName("Ставка тарифа страхования")]
    public decimal Rate { get; set; }
}