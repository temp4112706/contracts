﻿using Some.Abstractions.Enums;

namespace Some.Dto.Policies.EditRisks;

/// <summary>
///     Операция изменения данных о риске жизни
/// </summary>
public class PolicyEditLifeRiskInputItem : BasePolicyEditRiskInputItem<PolicyEditLifeRiskInputItemData>
{
    /// <summary>
    ///     Индекс застрахованного лица
    /// </summary>
    public InsuredPersonIndex Index { get; set; }
}