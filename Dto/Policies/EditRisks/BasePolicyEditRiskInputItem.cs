﻿using Some.Abstractions.Enums;

namespace Some.Dto.Policies.EditRisks;

/// <summary>
///     Базовая операция изменения данных о рисках
/// </summary>
public abstract class BasePolicyEditRiskInputItem<TData> where TData: class
{
    /// <summary>
    ///     Вид операции
    /// </summary>
    public CdcOperation Operation { get; set; }

    /// <summary>
    ///     Данные для изменения
    /// </summary>
    public TData? Data { get; set; }
}