﻿namespace Some.Dto.Policies.EditRisks;

/// <summary>
///     Данные для изменения риска жизни
/// </summary>
public class PolicyEditLifeRiskInputItemData : BasePolicyEditRiskInputItemData
{
    /// <summary>
    ///     ИД клиента
    /// </summary>
    public Guid CustomerId { get; set; }

    /// <summary>
    ///     Доля созаемщика
    /// </summary>
    public decimal LoanShare { get; set; }
}