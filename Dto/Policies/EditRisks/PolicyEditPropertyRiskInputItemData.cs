﻿using Some.Dto.Properties;

namespace Some.Dto.Policies.EditRisks;

/// <summary>
///     Данные для изменения риска имущества
/// </summary>
public class PolicyEditPropertyRiskInputItemData : BasePolicyEditRiskInputItemData
{
    /// <summary>
    ///     Доля страхования
    /// </summary>
    public decimal LoanShare { get; set; }

    //TODO
    ///// <summary>
    /////     Имущество
    ///// </summary>
    //public FullPropertyInput Property { get; set; } = null!;

    /// <summary>
    ///     ИД имущества
    /// </summary>
    public Guid? PropertyId { get; set; }
}