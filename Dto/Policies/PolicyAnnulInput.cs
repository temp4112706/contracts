﻿using System.ComponentModel;
using Some.Abstractions.Policies.Actions;
using Some.Abstractions.Enums;
using Newtonsoft.Json;

namespace Some.Dto.Policies;

/// <summary>
///     DTO: Данные для аннулирования ДС
/// </summary>
public record PolicyAnnulInput : IPolicyAnnul
{
    /// <summary>
    ///     Ид вложения
    /// </summary>
    [DisplayName("Ид вложения")]
    public Guid? Attachment { get; set; }

    /// <summary>
    ///     Дата аннулирования
    /// </summary>
    [DisplayName("Дата аннулирования")]
    public DateTime AnnulledAt { get; set; }

    /// <summary>
    ///     Комментарий
    /// </summary>
    [DisplayName("Комментарий")]
    public string? Comment { get; set; }

    /// <summary>
    ///     Удалить ДС
    /// </summary>
    [DisplayName("Удалить ДС")]
    public DateTime? DeletedAt { get; set; }

    /// <inheritdoc />
    [JsonIgnore]
    public bool Force { get; set; }

    /// <summary>
    ///     Причина аннулирования
    /// </summary>
    [DisplayName("Причина аннулирования")]
    public PolicyCompleteReason Reason { get; set; }
}