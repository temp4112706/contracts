﻿using System.Diagnostics.CodeAnalysis;
using Some.Abstractions.Policies;
using Some.Dto.Monies;

namespace Some.Dto.Policies;

/// <summary>
///     DTO: Плановая страховая премия
/// </summary>
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
public class PolicyPlannedPaymentDto : IPolicyPlannedPayment<MoneyDto>
{
    /// <summary>
    ///     Запланированная дата оплаты
    /// </summary>
    public DateTime PlannedDate { get; set; }
        
    /// <summary>
    ///     Запланированная сумма оплаты
    /// </summary>
    public MoneyDto PlannedAmount { get; set; } = null!;
}