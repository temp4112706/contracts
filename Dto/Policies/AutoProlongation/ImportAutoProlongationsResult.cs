﻿using Some.Abstractions.Policies.AutoProlongation;
using Some.Abstractions.Result;

namespace Some.Dto.Policies.AutoProlongation;

/// <summary>
///     DTO: Результат загрузки автопролонгаций
/// </summary>
public record ImportAutoProlongationsResult
{
    /// <summary>
    ///     ИД загрузки автопролонгаций
    /// </summary>
    public Guid? ImportId { get; set; }

    /// <summary>
    ///     Валиден список автопролонгаций?
    /// </summary>
    public bool IsValid => Rows.All(x => x.IsValid);

    /// <summary>
    ///     Результат парсинга автопролонгаций
    /// </summary>
    public IReadOnlyCollection<IParsingRowResult<IAutoProlongationRow>> Rows { get; init; } = null!;
}