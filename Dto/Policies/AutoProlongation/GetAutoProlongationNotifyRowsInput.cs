using System.ComponentModel;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global

namespace Some.Dto.Policies.AutoProlongation;

/// <summary>
///     DTO: Данные для получения списка автопролонгаций на уведомление клиента
/// </summary>
public record GetAutoProlongationNotifyRowsInput
{
    /// <summary>
    ///     Список ИД ДС
    /// </summary>
    [DisplayName("Список ИД ДС")]
    public IEnumerable<Guid> PolicyIds { get; set; } = null!;
}