﻿using Some.Abstractions.Policies.AutoProlongation;

namespace Some.Dto.Policies.AutoProlongation;

/// <inheritdoc />
public record AutoProlongationRowDto : IAutoProlongationRow
{
    /// <inheritdoc />
    public string LoanAgreementId { get; set; } = null!;

    /// <inheritdoc />
    public string LoanAgreementNumber { get; set; } = null!;

    /// <inheritdoc />
    public DateTime LoanAgreementDate { get; set; }

    /// <inheritdoc />
    public string Insurer { get; set; } = null!;

    /// <inheritdoc />
    public string PolicyNumber { get; set; } = null!;

    /// <inheritdoc />
    public DateTime PolicyDate { get; set; }

    /// <inheritdoc />
    public string Insurant { get; set; } = null!;

    /// <inheritdoc />
    public decimal MainDebtAmount { get; set; }

    /// <inheritdoc />
    public decimal InsuranceAmount { get; set; }

    /// <inheritdoc />
    public decimal Tariff { get; set; }

    /// <inheritdoc />
    public decimal PremiumAmount { get; set; }

    /// <inheritdoc />
    public DateTime PaymentDate { get; set; }

    /// <inheritdoc />
    public string PhoneNumber { get; set; } = null!;

    /// <inheritdoc />
    public string Email { get; set; } = null!;
}