using Some.Abstractions.Enums;
using Some.Abstractions.Policies.AutoProlongation;

namespace Some.Dto.Policies.AutoProlongation;

/// <summary>
///     DTO: <inheritdoc cref="IAutoProlongationSms"/>
/// </summary>
public record AutoProlongationSmsDto : TrackableEntityDto<Guid>, IAutoProlongationSms
{
    /// <inheritdoc />
    public AutoProlongationSmsStatus Status { get; set; }

    /// <inheritdoc />
    public DateTime? SentAt { get; set; }

    /// <inheritdoc />
    public string PaymentOrderUrl { get; set; } = null!;
}