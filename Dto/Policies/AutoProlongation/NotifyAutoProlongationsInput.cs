using System.ComponentModel;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global

namespace Some.Dto.Policies.AutoProlongation;

/// <summary>
///     DTO: Данные для уведомления клиента об автопролонгации по списку ДС
/// </summary>
public record NotifyAutoProlongationsInput
{
    /// <summary>
    ///     Список ИД ДС
    /// </summary>
    [DisplayName("Список ИД ДС")]
    public IEnumerable<Guid> PolicyIds { get; set; } = null!;
}