using System.ComponentModel;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Dto.Policies.AutoProlongation;

/// <summary>
///     DTO: Данные для создания автопролонгаций
/// </summary>
public record CreateAutoProlongationsInput
{
    /// <summary>
    ///     Кол-во дней просрочки пролонгации - До
    /// </summary>
    [DisplayName("Кол-во дней просрочки пролонгации (До)")]
    public int? EndOverdueDays { get; set; }

    /// <summary>
    ///     Плановая сумма пролонгации - До
    /// </summary>
    [DisplayName("Плановая сумма пролонгации (До)")]
    public decimal? EndPlannedAmount { get; set; }

    /// <summary>
    ///     Плановая дата пролонгации - Конец
    /// </summary>
    [DisplayName("Конец плановой даты пролонгации")]
    public DateTime EndPlannedDate { get; set; }

    /// <summary>
    ///     Коды СК
    /// </summary>
    [DisplayName("Коды СК")]
    public string[] InsurerIds { get; set; } = null!;

    /// <summary>
    ///     Кол-во дней просрочки пролонгации - От
    /// </summary>
    [DisplayName("Кол-во дней просрочки пролонгации (От)")]
    public int? StartOverdueDays { get; set; }

    /// <summary>
    ///     Плановая сумма пролонгации - От
    /// </summary>
    [DisplayName("Плановая сумма пролонгации (От)")]
    public decimal? StartPlannedAmount { get; set; }

    /// <summary>
    ///     Плановая дата пролонгации - Начало
    /// </summary>
    [DisplayName("Начало плановой даты пролонгации")]
    public DateTime StartPlannedDate { get; set; }
}