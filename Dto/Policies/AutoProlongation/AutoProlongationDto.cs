using Some.Abstractions.Enums;
using Some.Abstractions.Policies.AutoProlongation;

namespace Some.Dto.Policies.AutoProlongation;

/// <summary>
///     DTO: <inheritdoc cref="IAutoProlongation{TAutoProlongationSmsMessage}"/>
/// </summary>
public record AutoProlongationDto : EntityDto<string>, IAutoProlongation<AutoProlongationSmsDto>
{
    /// <inheritdoc />
    public AutoProlongationStatus? Status { get; set; }

    /// <inheritdoc />
    public ICollection<AutoProlongationSmsDto>? SmsMessages { get; set; }

    /// <summary>
    ///     Номер периода
    /// </summary>
    public int? PeriodNumber { get; set; }
}