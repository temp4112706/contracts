using System.ComponentModel;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global

namespace Some.Dto.Policies.AutoProlongation;

/// <summary>
///     DTO: данные для экспорта уведомлений клиентов об автопролонгациях
/// </summary>
public record ExportNotifyAutoProlongationsInput
{
    /// <summary>
    ///     Строки для экспорта
    /// </summary>
    [DisplayName("Строки для экспорта")]
    public IEnumerable<NotifyAutoProlongationRowDto> NotifyRows { get; set; } = null!;
}