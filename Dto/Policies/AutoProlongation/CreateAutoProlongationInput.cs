using System.ComponentModel;

namespace Some.Dto.Policies.AutoProlongation;

/// <summary>
///     DTO: Данные для создания автопролонгации
/// </summary>
public record CreateAutoProlongationInput
{
    /// <summary>
    ///     Номер периода страхования
    /// </summary>
    [DisplayName("Номер периода страхования")]
    public int PeriodNumber { get; set; }
}