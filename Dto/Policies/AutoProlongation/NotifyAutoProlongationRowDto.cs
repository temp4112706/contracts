using Some.Abstractions.Policies;
using Some.Abstractions.Enums;

namespace Some.Dto.Policies.AutoProlongation;

/// <summary>
///      DTO: <inheritdoc cref="INotifyAutoProlongationRow"/>
/// </summary>
public class NotifyAutoProlongationRowDto : INotifyAutoProlongationRow
{
    /// <summary>
    ///     ИД ДС
    /// </summary>
    public Guid PolicyId { get; set; }

    /// <inheritdoc />
    public string? LoanAgreementExternalId { get; set; } = null!;

    /// <inheritdoc />
    public string? LoanAgreementNumber { get; set; } = null!;

    /// <inheritdoc />
    public DateTime? LoanAgreementDate { get; set; }

    /// <inheritdoc />
    public LoanAgreementState? LoanAgreementState { get; set; }

    /// <inheritdoc />
    public string? InsurerName { get; set; } = null!;

    /// <inheritdoc />
    public string? PolicyNumber { get; set; } = null!;

    /// <inheritdoc />
    public DateTime? PolicyDate { get; set; }

    /// <inheritdoc />
    public decimal? MainDebtAmount { get; set; }

    /// <inheritdoc />
    public decimal? InsuranceAmount { get; set; }

    /// <inheritdoc />
    public decimal? PlannedPaymentAmount { get; set; }

    /// <inheritdoc />
    public DateTime? PlannedPaymentDate { get; set; }

    /// <inheritdoc />
    public decimal? ActualPaymentAmount { get; set; }

    /// <inheritdoc />
    public DateTime? ActualPaymentDate { get; set; }

    /// <inheritdoc />
    public AutoProlongationSmsStatus? SmsStatus { get; set; }

    /// <inheritdoc />
    public int? SmsCount { get; set; }

    /// <summary>
    ///     Ошибка валидации
    /// </summary>
    public string? Error { get; set; }
}