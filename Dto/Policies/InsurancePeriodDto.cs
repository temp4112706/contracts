using System.Diagnostics.CodeAnalysis;
using Some.Abstractions.Common;

namespace Some.Dto.Policies;

/// <summary>
///     DTO: Период оплаты
/// </summary>
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
public class InsurancePeriodDto : IInsurancePeriod
{
    /// <summary>
    ///     Позиция
    /// </summary>
    public int Number { get; set; }

    /// <summary>
    ///     Дата начала
    /// </summary>
    public DateTime StartDate { get; set; }

    /// <summary>
    ///     Дата завершения
    /// </summary>
    public DateTime EndDate { get; set; }

    /// <summary>
    ///     Период
    /// </summary>
    public int Duration { get; set; }
}