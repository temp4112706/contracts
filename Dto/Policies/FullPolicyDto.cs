﻿using Some.Dto.Customers;
using Some.Dto.Insurers;

namespace Some.Dto.Policies;

/// <summary>
///     DTO: ДС
/// </summary>
public record FullPolicyDto : BaseFullPolicyDto<IdentifierInsurerDto>
{
    /// <summary>
    ///     Дополнительные свойства
    /// </summary>
    public Dictionary<string, object> ExtraProperties { get; set; } = new();

    /// <summary>
    ///     Клиенты, не участвующие в ДС
    /// </summary>
    public CustomerDto[]? NoneTypeCustomers { get; set; }
}