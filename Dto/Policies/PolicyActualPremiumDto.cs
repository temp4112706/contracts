﻿using System.Diagnostics.CodeAnalysis;
using Some.Abstractions.Policies;
using Some.Abstractions.Enums;
using Some.Dto.Attachments;
using Some.Dto.Monies;

namespace Some.Dto.Policies;

/// <summary>
///     DTO: Фактичская оплата периода в ДС
/// </summary>
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
[SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Global")]
public class PolicyActualPremiumDto : IPolicyActualPremium<MoneyDto, FlatAttachmentDto?, PolicyPlannedPaymentDto,
    PolicyActualPaymentDto, PolicyActualPremiumPaymentDto>
{
    /// <summary>
    ///     Тип оплаты
    /// </summary>
    public PolicyActualPremiumType Type { get; set; }
        
    /// <summary>
    ///     Оплаты
    /// </summary>
    public ICollection<PolicyActualPremiumPaymentDto> Payments { get; set; } = null!;
}