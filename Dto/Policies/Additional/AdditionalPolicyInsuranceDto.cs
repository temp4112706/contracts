using Some.Abstractions.Enums;
using Some.Abstractions.Policies.Additional;
using Some.Dto.Monies;

namespace Some.Dto.Policies.Additional;

/// <summary>
///     Условия доп вида страхования
/// </summary>
/// <param name="Id">ИД</param>
/// <param name="Data">Страховые данные</param>
/// <param name="Type">Типы добровольного страхования</param>
/// <param name="InsuranceAmount">Страховая сумма</param>
/// <param name="PremiumAmount">Плановая страховая премия</param>
public record AdditionalPolicyInsuranceDto(Guid Id, string Data, AdditionalInsuranceType Type,
        MoneyDto InsuranceAmount, MoneyDto PremiumAmount)
    : IAdditionalPolicyInsurance<MoneyDto>;