using Some.Abstractions.Enums;

namespace Some.Dto.Policies.Additional;

/// <summary>
///     Условия доп вида страхования с базовыми полями
/// </summary>
/// <param name="Id">ИД</param>
/// <param name="Type">Типы добровольного страхования</param>
public record FlatAdditionalPolicyInsuranceDto(Guid Id, AdditionalInsuranceType Type);