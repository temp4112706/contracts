using Some.Dto.Monies;

namespace Some.Dto.Policies.Flat;

/// <summary>
///     DTO: Данные пролонгации
/// </summary>
public class FlatPolicyMetaResult
{
    /// <summary>
    ///     Текущая пролонгация
    /// </summary>
    public CurrentProlongationResult? CurrentProlongation { get; set; }

    /// <summary>
    ///     Плановая пролонгация
    /// </summary>
    public PlannedProlongationResult? PlannedProlongation { get; set; }

    /// <summary>
    ///     Базовая инфо о пролонгации
    /// </summary>
    public abstract class BaseProlongationResult
    {
        /// <summary>
        ///     Период
        /// </summary>
        public InsurancePeriodDto Period { get; set; } = null!;
    }

    /// <summary>
    ///     Плановая пролонгация
    /// </summary>
    public class PlannedProlongationResult : BaseProlongationResult
    {
        /// <summary>
        ///     Плановая дата пролонгации
        /// </summary>
        public DateTime? PaymentDate { get; set; }

        /// <summary>
        ///     Сумму пролонгации договора
        /// </summary>
        public MoneyDto PlannedAmount { get; set; } = null!;
    }

    /// <summary>
    ///     Текущая пролонгация
    /// </summary>
    public class CurrentProlongationResult : BaseProlongationResult
    {
        /// <summary>
        ///     Текущая страховая премия
        /// </summary>
        public MoneyDto? PremiumAmount { get; set; }
            
        /// <summary>
        ///     Текущая страховая сумма
        /// </summary>
        public MoneyDto? InsuranceAmount { get; set; }

        /// <summary>
        ///     Фактическая сумма оплаты 
        /// </summary>
        public MoneyDto? PaymentAmount { get; set; }

        /// <summary>
        ///     Фактическая дата оплаты
        /// </summary>
        public DateTime? PaymentDate { get; set; }
    }
}