using Some.Abstractions.Enums;
using Some.Abstractions.Policies.Additional;
using Some.Abstractions.Policies.Mortgage;
using Some.Dto.Customers;
using Some.Dto.Employees;
using Some.Dto.LoanAgreements;
using Some.Dto.Mirs;
using Some.Dto.Monies;
using Some.Dto.MutualSettlements;
using Some.Dto.Policies.Additional;
using Some.Dto.Policies.AutoProlongation;

#pragma warning disable CS8766

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Dto.Policies.Flat;

/// <summary>
///     DTO: ДС с основными полями
/// </summary>
public abstract record BaseFlatPolicyDto : TrackableEntityDto<Guid>,
    IFlatMortgagePolicy<MoneyDto, FlatLoanAgreementDto>, IFlatAdditionalPolicy
{
    /// <summary>
    ///     Тип LC
    /// </summary>
    public string Type { get; init; } = null!;
    
    /// <summary>
    ///     Комментарий
    /// </summary>
    public string? Comment { get; set; }

    /// <summary>
    ///     Исполнитель компании
    /// </summary>
    public EmployeeDto? CompanyEmployee { get; init; }

    /// <summary>
    ///     Причина расторжения
    /// </summary>
    public PolicyCompleteReason? CompleteReason { get; set; }

    /// <summary>
    ///     Страхователь
    /// </summary>
    public FlatCustomerDto Insurant { get; set; } = null!;

    /// <summary>
    ///     Последняя автопролонгация
    /// </summary>
    public AutoProlongationDto? LastAutoProlongation { get; set; }

    /// <summary>
    ///     Крайний АС
    /// </summary>
    public IdentifierMutualSettlementDto? LastMutualSettlement { get; set; }

    /// <summary>
    ///     Мета
    /// </summary>
    public FlatPolicyMetaResult Meta { get; set; } = null!;
    
    /// <summary>
    ///     Данные по доп виду страхования
    /// </summary>
    public FlatAdditionalPolicyInsuranceDto? AdditionalInsurance { get; init; }

    /// <summary>
    ///     Заявка
    /// </summary>
    public IdentifierMirDto? Mir { get; set; } = null!;

    /// <summary>
    ///     Регион предмета ипотеки
    /// </summary>
    public string? MortgageRegion { get; set; }

    /// <summary>
    ///     Набор рисков
    /// </summary>
    public InsuranceRisk[]? Risks { get; set; } = null!;

    /// <summary>
    ///     Кредитный договор
    /// </summary>
    public FlatLoanAgreementDto? LoanAgreement { get; set; } = null!;

    /// <summary>
    ///     Дата и время заключения договора страхования
    /// </summary>
    public DateTime AgreementDateTime { get; set; }

    /// <summary>
    ///     Дата и время начала действия договора
    /// </summary>
    public DateTime CommencementDateTime { get; set; }

    /// <summary>
    ///     Дата и время изменения статуса в Завершен или Аннулирован
    /// </summary>
    public DateTime? CompletedAt { get; set; }

    /// <summary>
    ///     Дата и время истечения договора
    /// </summary>
    public DateTime ExpiryDateTime { get; set; }

    /// <summary>
    ///     Ручное создание?
    /// </summary>
    public bool Manual { get; set; }

    /// <summary>
    ///     Черновик?
    /// </summary>
    public bool Draft { get; set; }

    /// <summary>
    ///     Номер договора страхования
    /// </summary>
    public string Number { get; set; } = null!;

    /// <summary>
    ///     Статус ДС
    /// </summary>
    public PolicyStatus Status { get; set; }

    /// <summary>
    ///     Дата и время изменения статуса
    /// </summary>
    public DateTime StatusUpdatedAt { get; set; }
}