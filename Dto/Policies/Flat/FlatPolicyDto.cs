﻿using Some.Dto.Insurers;

namespace Some.Dto.Policies.Flat;

/// <summary>
///     DTO: ДС с основными полями
/// </summary>
public record FlatPolicyDto : BaseFlatPolicyDto
{
    /// <summary>
    ///     Страховая компания
    /// </summary>
    public IdentifierInsurerDto Insurer { get; set; } = default!;
}