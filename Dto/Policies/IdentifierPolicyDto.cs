using Some.Abstractions.Enums;

namespace Some.Dto.Policies;

/// <summary>
///     DTO: Идентификатор ДС
/// </summary>
public record IdentifierPolicyDto : EntityDto<Guid>
{
    /// <summary>
    ///     Дата создания ДС
    /// </summary>
    public DateTime CreatedAt { get; init; }

    /// <summary>
    ///     Номер ДС
    /// </summary>
    public string Number { get; init; } = null!;

    /// <summary>
    ///     Статус ДС
    /// </summary>
    public PolicyStatus Status { get; init; }
}