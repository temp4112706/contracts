using Some.Dto.Customers;

// ReSharper disable UnusedAutoPropertyAccessor.Global

// ReSharper disable PropertyCanBeMadeInitOnly.Global

namespace Some.Dto.Policies;

/// <summary>
///     Данные для выпуска ДС по доп видах страхования
/// </summary>
public record IssueAdditionalPolicyInput
{
    /// <summary>
    ///     Дата ДС
    /// </summary>
    public DateTime AgreementDateTime { get; set; }

    /// <summary>
    ///     Страхователь
    /// </summary>
    public UpsertCustomerInput Insurant { get; set; } = null!;

    /// <summary>
    ///     ИД рассчета
    /// </summary>
    public Guid ScoringId { get; init; }
}