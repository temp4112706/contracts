using Some.Dto.Companies;
using Some.Dto.Customers;
using Some.Dto.Employees;
using Some.Dto.LoanAgreements;
using Some.Dto.Mirs;
using Some.Dto.Policies.Accidents;
using Some.Dto.Policies.Additional;
using Some.Dto.Policies.Mortgage;
using Some.Abstractions.Enums;
using Some.Abstractions.Policies.Additional;
using Some.Abstractions.Policies.Mortgage;
using Some.Dto.Monies;

#pragma warning disable CS8766

// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global

namespace Some.Dto.Policies;

/// <summary>
///     DTO: ДС
/// </summary>
public abstract record BaseFullPolicyDto<TInsurer> : BasePolicyDto<TInsurer>,
    IMortgagePolicy<MoneyDto, CustomerDto, TInsurer, LoanAgreementDto, MortgagePolicyLifeInsuranceDto,
        MortgagePolicyPropertyInsuranceDto, MortgagePolicyTitleInsuranceDto, PolicyPeriodDto, EmployeeDto,
        PolicyInsuranceAccidentDto, FlatCompanyDto>,
    IAdditionalPolicy<MoneyDto, CustomerDto, TInsurer, AdditionalPolicyInsuranceDto, PolicyPeriodDto, EmployeeDto,
        PolicyInsuranceAccidentDto, FlatCompanyDto>
{
    /// <summary>
    ///     Заявка
    /// </summary>
    public IdentifierMirDto? Mir { get; set; } = null!;

    /// <summary>
    ///     Тип ДС
    /// </summary>
    public string Type { get; init; } = null!;

    /// <summary>
    ///     Страховаые данные по доп видам
    /// </summary>
    public AdditionalPolicyInsuranceDto? AdditionalInsurance { get; set; } = null!;
    
    /// <summary>
    ///     Место выдачи ДС
    /// </summary>
    public string? DetentionPlace { get; set; }


    /// <summary>
    ///     Условия страхования созаемщиков
    /// </summary>
    public ICollection<MortgagePolicyLifeInsuranceDto>? LifeInsurances { get; set; } = null!;

    /// <summary>
    ///     Кредитный договор
    /// </summary>
    public LoanAgreementDto? LoanAgreement { get; set; } = null!;

    /// <summary>
    ///     Регион предмета ипотеки
    /// </summary>
    public string? MortgageRegion { get; set; }

    /// <summary>
    ///     Условия страхования имущества
    /// </summary>
    public ICollection<MortgagePolicyPropertyInsuranceDto>? PropertyInsurances { get; set; } = null!;

    /// <summary>
    ///     Набор рисков
    /// </summary>
    public InsuranceRisk[]? Risks { get; set; } = null!;

    /// <summary>
    ///     Условия страхования титула
    /// </summary>
    public ICollection<MortgagePolicyTitleInsuranceDto>? TitleInsurances { get; set; } = null!;
}