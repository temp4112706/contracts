﻿using System.Diagnostics.CodeAnalysis;
using Some.Abstractions;
using Some.Abstractions.Policies;
using Some.Abstractions.Enums;
using Some.Dto.Attachments;
using Some.Dto.MutualSettlements;
using Some.Dto.Policies.AutoProlongation;
using Some.Dto.Monies;

namespace Some.Dto.Policies;

/// <summary>
///     DTO: Период страхования в ДС
/// </summary>
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
[SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Global")]
public class PolicyPeriodDto : InsurancePeriodDto, IPolicyPeriod<MoneyDto, FlatAttachmentDto?,
    PolicyPlannedPaymentDto, PolicyActualPaymentDto, PolicyActualPremiumPaymentDto, PolicyPlannedPremiumDto,
    PolicyActualPremiumDto>, IEntityDto<Guid>
{
    /// <summary>
    ///     ИД
    /// </summary>
    public Guid Id { get; set; }

    /// <summary>
    ///     Статус
    /// </summary>
    public PolicyInsurancePeriodState State { get; set; }

    /// <summary>
    ///     Ссылка на оплату
    /// </summary>
    public string? PaymentUrl { get; set; }

    /// <summary>
    ///     Плановая страховая премия
    /// </summary>
    public PolicyPlannedPremiumDto PlannedPremium { get; set; } = null!;

    /// <summary>
    ///     Фактическая страховая премия
    /// </summary>
    public PolicyActualPremiumDto? ActualPremium { get; set; }
        
    /// <summary>
    ///     Просроченная премия, факт
    /// </summary>
    public MoneyDto? OverdueAmount
    {
        get
        {
            if (ActualPremium == null) return null;
            var actualPremium = ActualPremium.Payments.Sum(x => x.ActualPayment?.ActualAmount.Amount ?? 0);
            var overduePremium = PlannedPremium.PlannedAmount.Amount - actualPremium;
            return MoneyDto.Rub(overduePremium);
        }
    }

    /// <summary>
    ///     Страховая сумма
    /// </summary>
    public MoneyDto InsuranceAmount { get; set; } = null!;

    /// <summary>
    ///     АС
    /// </summary>
    public ICollection<IdentifierMutualSettlementDto> MutualSettlements { get; set; } = null!;

    /// <summary>
    ///     Автопролонгация
    /// </summary>
    public AutoProlongationDto? AutoProlongation { get; set; }

    /// <summary>
    ///     Дополнительные свойства
    /// </summary>
    public Dictionary<string, object> ExtraProperties { get; set; } = new();
}