using Some.Dto.Companies;
using Some.Dto.Customers;
using Some.Dto.Employees;
using Some.Dto.Policies.Accidents;
using Some.Dto.Policies.AutoProlongation;
using Some.Abstractions.Enums;
using Some.Abstractions.Policies;
using Some.Dto.Monies;

#pragma warning disable CS8766

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Dto.Policies;

/// <summary>
///     Базовый ДС
/// </summary>
public abstract record BasePolicyDto<TInsurer> : TrackableEntityDto<Guid>,
    IPolicy<MoneyDto, CustomerDto, TInsurer, PolicyPeriodDto, EmployeeDto,
        PolicyInsuranceAccidentDto, FlatCompanyDto>
{
    /// <summary>
    ///     Крайняя автопролонгация
    /// </summary>
    public AutoProlongationDto? LastAutoProlongation { get; set; }

    /// <summary>
    ///     Страховые случаи
    /// </summary>
    public ICollection<PolicyInsuranceAccidentDto>? Accidents { get; set; }

    /// <summary>
    ///     Комментарий
    /// </summary>
    public string? Comment { get; set; }

    /// <summary>
    ///     Компания
    /// </summary>
    public FlatCompanyDto? Company { get; init; }

    /// <summary>
    ///     Исполнитель компании
    /// </summary>
    public EmployeeDto? CompanyEmployee { get; init; }

    /// <summary>
    ///     Причина расторжения
    /// </summary>
    public PolicyCompleteReason? CompleteReason { get; set; }

    /// <summary>
    ///     Страхователь
    /// </summary>
    public CustomerDto Insurant { get; set; } = null!;

    /// <summary>
    ///     Страховщик
    /// </summary>
    public TInsurer Insurer { get; set; } = default!;

    /// <summary>
    ///     Сотрудник СК
    /// </summary>
    public EmployeeDto? InsurerEmployee { get; set; }

    /// <summary>
    ///     Дата и время последней оплаты
    /// </summary>
    public DateTime? LastPaymentDateTime { get; set; }

    /// <summary>
    ///     Общий график страхования
    /// </summary>
    public ICollection<PolicyPeriodDto> Periods { get; set; } = null!;

    /// <summary>
    ///     Дата и время заключения договора страхования
    /// </summary>
    public DateTime AgreementDateTime { get; set; }

    /// <summary>
    ///     Дата и время начала действия договора
    /// </summary>
    public DateTime CommencementDateTime { get; set; }

    /// <summary>
    ///     Дата и время изменения статуса в Завершен или Аннулирован
    /// </summary>
    public DateTime? CompletedAt { get; set; }

    /// <summary>
    ///     Дата и время истечения договора
    /// </summary>
    public DateTime ExpiryDateTime { get; set; }

    /// <summary>
    ///     Ручное создание?
    /// </summary>
    public bool Manual { get; set; }

    /// <summary>
    ///     Черновик?
    /// </summary>
    public bool Draft { get; set; }

    /// <summary>
    ///     Номер договора страхования
    /// </summary>
    public string Number { get; set; } = null!;

    /// <summary>
    ///     Статус ДС
    /// </summary>
    public PolicyStatus Status { get; set; }

    /// <summary>
    ///     Дата и время изменения статуса
    /// </summary>
    public DateTime StatusUpdatedAt { get; set; }
}