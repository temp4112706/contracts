using Some.Dto.Customers;
using Some.Dto.LoanAgreements;
using Some.Dto.Mirs;
using Some.Abstractions.Enums;
using Some.Abstractions.Policies;

// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global

namespace Some.Dto.Policies;

/// <summary>
///     DTO: ДС в АС
/// </summary>
public record MsPolicyDto : TrackableEntityDto<Guid>, IScalarPolicy
{
    /// <summary>
    ///     Страхователь
    /// </summary>
    public FlatCustomerDto Insurant { get; set; } = null!;

    /// <summary>
    ///     Кредитный договор
    /// </summary>
    public FlatLoanAgreementDto? LoanAgreement { get; set; } = null!;

    /// <summary>
    ///     Заявка
    /// </summary>
    public IdentifierMirDto? Mir { get; set; } = null!;

    /// <summary>
    ///     Набор рисков
    /// </summary>
    public InsuranceRisk[]? Risks { get; set; } = null!;

    /// <summary>
    ///     Дата и время заключения договора страхования
    /// </summary>
    public DateTime AgreementDateTime { get; set; }

    /// <summary>
    ///     Дата и время начала действия договора
    /// </summary>
    public DateTime CommencementDateTime { get; set; }

    /// <summary>
    ///     Дата и время изменения статуса в Завершен или Аннулирован
    /// </summary>
    public DateTime? CompletedAt { get; set; }

    /// <summary>
    ///     Дата и время истечения договора
    /// </summary>
    public DateTime ExpiryDateTime { get; set; }

    /// <summary>
    ///     Ручное создание?
    /// </summary>
    public bool Manual { get; set; }

    /// <summary>
    ///     Черновик?
    /// </summary>
    public bool Draft { get; set; }

    /// <summary>
    ///     Номер договора страхования
    /// </summary>
    public string Number { get; set; } = null!;

    /// <summary>
    ///     Статус ДС
    /// </summary>
    public PolicyStatus Status { get; set; }

    /// <summary>
    ///     Дата и время изменения статуса
    /// </summary>
    public DateTime StatusUpdatedAt { get; set; }
}