﻿using Some.Abstractions.Policies;
using Some.Dto.Common;
using Some.Dto.Monies;

namespace Some.Dto.Policies;

/// <summary>
///     DTO: График страхования
/// </summary>
public class InsuranceScheduleDto : List<InsuranceScheduleRowDto>,
    IInsuranceSchedule<MoneyDto, InsurancePeriodDto, InsuranceTariffDto, InsuranceScheduleRowDto>
{

}