using System.Diagnostics.CodeAnalysis;
using Some.Abstractions.Policies.Mortgage;
using Some.Dto.Addresses;
using Some.Dto.Common;
using Some.Dto.Monies;
using Some.Dto.Properties;

namespace Some.Dto.Policies.Mortgage;

/// <summary>
///     DTO: Индивидуальные условия титульного страхования
/// </summary>
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
[SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Global")]
public record MortgagePolicyTitleInsuranceDto : EntityDto<Guid>,
    IMortgagePolicyTitleInsurance<MoneyDto, TitlePropertyDto,
        InsurancePeriodDto, InsuranceTariffDto, InsuranceScheduleRowDto, InsuranceScheduleDto, FlatAddressDto>
{
    /// <summary>
    ///     Доля страхования в процентах
    /// </summary>
    public decimal? LoanShare { get; set; }

    /// <summary>
    ///     Застрахованное имущество
    /// </summary>
    public TitlePropertyDto? Property { get; set; }

    /// <summary>
    ///     График страхования
    /// </summary>
    public InsuranceScheduleDto Schedule { get; set; } = null!;
}