using System.Diagnostics.CodeAnalysis;
using Some.Dto.Common;
using Some.Dto.InsuredPersons;
using Some.Abstractions.Policies.Mortgage;
using Some.Dto.Monies;

namespace Some.Dto.Policies.Mortgage;

/// <summary>
///     DTO: Индивидуальные условия страхования жизни
/// </summary>
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
public record MortgagePolicyLifeInsuranceDto : EntityDto<Guid>, IMortgagePolicyLifeInsurance<MoneyDto, PolicyInsuredPersonDto, 
    InsurancePeriodDto, InsuranceTariffDto, InsuranceScheduleRowDto, InsuranceScheduleDto>
{
    /// <summary>
    ///     График страхования
    /// </summary>
    public InsuranceScheduleDto Schedule { get; set; } = null!;

    /// <summary>
    ///     Застрахованное лицо
    /// </summary>
    public PolicyInsuredPersonDto InsuredPerson { get; set; } = null!;
}