﻿using System.Diagnostics.CodeAnalysis;
using Ct.Module.Errors;
using Some.Dto.Parsing;

namespace Some.Dto.Policies.Mortgage.Manual;

/// <summary>
///     DTO: Результат валидации ручного графика страхования
/// </summary>
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
public class ManualInsuranceScheduleResult
{
    /// <summary>
    ///     Ошибка при парсинге графика страхования
    /// </summary>
    public ErrorInfoResult? Error { get; set; }

    /// <summary>
    ///     Валиден график страхования?
    /// </summary>
    public bool IsValid { get; set; }

    /// <summary>
    ///     Результат парсинга графика страхования
    /// </summary>
    public IReadOnlyCollection<ParsingRowResultDto<ManualInsuranceScheduleRowDto>>? Rows { get; set; }
}