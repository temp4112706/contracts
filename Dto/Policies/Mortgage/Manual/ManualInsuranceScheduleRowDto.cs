﻿using System.Diagnostics.CodeAnalysis;

namespace Some.Dto.Policies.Mortgage.Manual;

/// <summary>
///     DTO: Строка ручного графика страхования
/// </summary>
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
public class ManualInsuranceScheduleRowDto : InsuranceScheduleRowDto
{
}