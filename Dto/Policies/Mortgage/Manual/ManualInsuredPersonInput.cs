using System.ComponentModel;
using Some.Dto.Normalizers;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable PropertyCanBeMadeInitOnly.Global

namespace Some.Dto.Policies.Mortgage.Manual;

/// <summary>
///     Данные для создания ДС из НА
/// </summary>
public class ManualInsuredPersonInput
{
    /// <summary>
    ///     Доля созаемщика в процентах
    /// </summary>
    [DisplayName("Доля созаемщика")]
    public decimal LoanShare { get; set; }

    /// <summary>
    ///     ИД застрахованного лица
    /// </summary>
    [DisplayName("ИД застрахованного лица")]
    public string NaCustomerId { get; set; } = null!;

    /// <summary>
    ///     Нормализация входных данных
    /// </summary>
    public void Normalize() => LoanShare = RateNormalizer.Normalize(LoanShare);
}