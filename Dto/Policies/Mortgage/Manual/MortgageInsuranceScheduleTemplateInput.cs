using System.ComponentModel;
using Some.Abstractions.Enums;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable PropertyCanBeMadeInitOnly.Global

namespace Some.Dto.Policies.Mortgage.Manual;

/// <summary>
///     DTO: Данные для получения ручного шаблона графика страхования
/// </summary>
public record MortgageInsuranceScheduleTemplateInput
{
    /// <summary>
    ///     Дата начала действия ДС
    /// </summary>
    [DisplayName("Дата начала действия ДС")]
    public DateTime CommencementDateTime { get; set; }

    /// <summary>
    ///     Дата завершения действия ДС
    /// </summary>
    [DisplayName("Дата завершения действия ДС")]
    public DateTime ExpiryDateTime { get; set; }

    /// <summary>
    ///     ИД КД НА
    /// </summary>
    [DisplayName("ИД КД")]
    public string NaLoanAgreementId { get; set; } = null!;

    /// <summary>
    ///     Название файла в результате
    /// </summary>
    [DisplayName("Название файла в результате")]
    public string? ResultFileName { get; set; } = null!;

    /// <summary>
    ///     Риск
    /// </summary>
    [DisplayName("Риск")]
    public InsuranceRisk Risk { get; set; }
}