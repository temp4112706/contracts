﻿using System.Diagnostics.CodeAnalysis;
using System.Text.Json.Serialization;
using Ct.Module.Domain.Entities;
using Some.Dto.LoanAgreements;
using Some.Abstractions.Enums;
using Some.Abstractions.Policies;

namespace Some.Dto.Policies.Mortgage.Manual;

/// <summary>
///     Данные для создания ДС
/// </summary>
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
[SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Global")]
public class ManualPolicyInput : IPolicyDates
{
    /// <summary>
    ///     Номер ДС
    /// </summary>
    public string Number { get; set; } = null!;

    /// <summary>
    ///     Статус
    /// </summary>
    public PolicyStatus? Status { get; set; }

    /// <summary>
    ///     Дата и время заключения договора страхования
    /// </summary>
    public DateTime AgreementDateTime { get; set; }

    /// <summary>
    ///     Дата и время начала действия ДС
    /// </summary>
    public DateTime CommencementDateTime { get; set; }

    /// <summary>
    ///     Дата и время истечения ДС
    /// </summary>
    public DateTime ExpiryDateTime { get; set; }

    /// <summary>
    ///     Код СК
    /// </summary>
    public InsurerId InsurerId { get; set; }

    /// <summary>
    ///     ИД Страхователя
    /// </summary>
    public string ExtInsurantId { get; set; } = null!;

    /// <summary>
    ///     ИД КД НА
    /// </summary>
    public string ExtLoanAgreementId { get; set; } = null!;

    /// <summary>
    ///     Дата КД
    /// </summary>
    public DateTime LoanAgreementDateTime { get; set; }

    /// <summary>
    ///     Список данных для страхования жизни
    /// </summary>
    public IDictionary<InsuredPersonIndex, ManualPolicyLifeInsuranceInput>? LifeInsurances { get; set; }

    /// <summary>
    ///     Данные о страховании имущества/титула
    /// </summary>
    public IEnumerable<ManualPolicyPropertyOrTitleInsuranceInput>? PropertyOrTitleInsurances { get; set; }

    /// <summary>
    ///     Дата и время завершения страхования титула
    /// </summary>
    public DateTime? TitleInsuranceEndDateTime { get; set; }

    /// <summary>
    ///     Информация об отделении или месте выдачи кредита
    /// </summary>
    public BranchInfoInput? BranchInfo { get; set; }


    /// <inheritdoc />
    [Newtonsoft.Json.JsonIgnore, JsonIgnore]
    public DateTime CreatedAt => CommencementDateTime;
}