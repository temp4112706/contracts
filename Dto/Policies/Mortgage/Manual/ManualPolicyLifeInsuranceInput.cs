﻿using Some.Dto.Underwriting;

namespace Some.Dto.Policies.Mortgage.Manual;

/// <summary>
///     Данные страхования жизни
/// </summary>
public class ManualPolicyLifeInsuranceInput
{
    /// <summary>
    ///     Данные застрахованного лица
    /// </summary>
    public ManualInsuredPersonInput InsuredPerson { get; set; } = null!;

    /// <summary>
    ///     График страхования
    /// </summary>
    public ICollection<TariffScheduleRowInput> InsuranceSchedule { get; set; } = null!;

    /// <summary>
    ///     Нормализация входных данных
    /// </summary>
    public void Normalize()
    {
        InsuredPerson.Normalize();
        foreach (var row in InsuranceSchedule) row.Normalize();
    }
}