﻿using System.ComponentModel;
using Some.Abstractions.Common;
using Some.Abstractions.Enums;
using Some.Abstractions.Policies;

// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable PropertyCanBeMadeInitOnly.Global

namespace Some.Dto.Policies.Mortgage.Manual;

/// <summary>
///     DTO: Данные для валидации ручного графика страхования
/// </summary>
public record ManualInsuranceScheduleInput : IMortgageSettingsIdentifiers, IPolicyDates
{
    /// <summary>
    ///     Дата заключения ДС
    /// </summary>
    [DisplayName("Дата заключения ДС")]
    public DateTime AgreementDateTime { get; set; }

    /// <summary>
    ///     Дата начала ДС
    /// </summary>
    [DisplayName("Дата начала ДС")]
    public DateTime CommencementDateTime { get; set; }

    /// <summary>
    ///     Дата окончания ДС
    /// </summary>
    [DisplayName("Дата окончания ДС")]
    public DateTime ExpiryDateTime { get; set; }

    /// <summary>
    ///     Доля созаемщика
    /// </summary>
    public decimal? LoanShare { get; set; }

    /// <summary>
    ///     ИД КД
    /// </summary>
    public string ExtLoanAgreementId { get; set; } = null!;
    
    /// <summary>
    ///     ИД СК 
    /// </summary>
    public InsurerId? InsurerId { get; init; }
    
    /// <summary>
    ///     Выгодоприобретатель
    /// </summary>
    public BeneficiaryId? BeneficiaryId { get; init; }

    /// <summary>
    ///     Валидируемый риск
    /// </summary>
    [DisplayName("Валидируемый риск")]
    public InsuranceRisk Risk { get; set; }
}