﻿using System.Diagnostics.CodeAnalysis;
using Some.Dto.Normalizers;
using Some.Dto.Properties;
using Some.Dto.Underwriting;

namespace Some.Dto.Policies.Mortgage.Manual;

/// <summary>
///     Данные страхования имущества
/// </summary>
[SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Global")]
public class ManualPolicyPropertyOrTitleInsuranceInput
{
    /// <summary>
    ///     Данные о страхуемом объекте
    /// </summary>
    public FullPropertyInput Property { get; set; } = null!;

    /// <summary>
    ///     Доля страхования имущества
    /// </summary>
    public decimal? PropertyInsuranceLoanShare { get; set; }

    /// <summary>
    ///     График страхования
    /// </summary>
    public ICollection<TariffScheduleRowInput>? PropertyInsuranceSchedule { get; set; }

    /// <summary>
    ///     Доля страхования титула
    /// </summary>
    public decimal? TitleInsuranceLoanShare { get; set; }

    /// <summary>
    ///     График страхования титула
    /// </summary>
    public ICollection<TariffScheduleRowInput>? TitleInsuranceSchedule { get; set; }

    /// <summary>
    ///     Нормализация входных данных
    /// </summary>
    public void Normalize()
    {
        if (PropertyInsuranceLoanShare != null)
        {
            PropertyInsuranceLoanShare = RateNormalizer.Normalize(PropertyInsuranceLoanShare.Value);
        }

        if (TitleInsuranceLoanShare != null)
        {
            TitleInsuranceLoanShare = RateNormalizer.Normalize(TitleInsuranceLoanShare.Value);
        }

        if (PropertyInsuranceSchedule != null)
        {
            foreach (var row in PropertyInsuranceSchedule)
            {
                row.Normalize();
            }
        }

        if (TitleInsuranceSchedule != null)
        {
            foreach (var row in TitleInsuranceSchedule)
            {
                row.Normalize();
            }
        }
    }
}