using Some.Abstractions.Policies.Mortgage;
using Some.Dto.Addresses;
using Some.Dto.Common;
using Some.Dto.Monies;
using Some.Dto.Properties;

namespace Some.Dto.Policies.Mortgage;

/// <summary>
///     DTO: Условия страхования имущества
/// </summary>
public record MortgagePolicyPropertyInsuranceDto : EntityDto<Guid>,
    IMortgagePolicyPropertyInsurance<MoneyDto, MortgagePropertyDto,
        InsurancePeriodDto, InsuranceTariffDto, InsuranceScheduleRowDto, InsuranceScheduleDto, FlatAddressDto>
{
    /// <summary>
    ///     Доля страхования в процентах
    /// </summary>
    public decimal? LoanShare { get; set; }

    /// <summary>
    ///     Застрахованное имущество
    /// </summary>
    public MortgagePropertyDto Property { get; set; } = null!;

    /// <summary>
    ///     График страхования
    /// </summary>
    public InsuranceScheduleDto Schedule { get; set; } = null!;
}