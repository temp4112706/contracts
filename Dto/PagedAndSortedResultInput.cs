using Some.Abstractions.Result;

namespace Some.Dto;

/// <summary>
///     Реализация <see cref="IPagedAndSortedResultInput" />.
/// </summary>
[Serializable]
public record PagedAndSortedResultInput : PagedResultInput, IPagedAndSortedResultInput
{
    /// <summary>
    ///     Информация о сортировке.
    /// </summary>
    public virtual string? Sorting { get; init; }
}