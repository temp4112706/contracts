using Some.Abstractions.Occupations;
using Some.Dto.Addresses;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Dto.Employers;

/// <summary>
///     DTO: Данные о работодателе
/// </summary>
public record EmployerDto : EntityDto<Guid>, IEmployer<AddressDto>
{
    /// <summary>
    ///     Наименование работодателя
    /// </summary>
    public string Name { get; init; } = null!;

    /// <summary>
    ///     Юридический адрес
    /// </summary>
    public AddressDto? LegalAddress { get; init; }

    /// <summary>
    ///     Контактный телефон
    /// </summary>
    public string? ContactPhone { get; init; } = null!;
}