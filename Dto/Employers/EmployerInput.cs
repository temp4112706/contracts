using System.ComponentModel;
using Some.Abstractions.Occupations;
using Some.Dto.Addresses;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Dto.Employers;

/// <summary>
///     Данные о работодателе
/// </summary>
public record EmployerInput : IEmployer<AddressInput>
{
    /// <summary>
    ///     Юридический адрес
    /// </summary>
    [DisplayName("Юридический адрес")]
    public AddressInput? LegalAddress { get; init; }

    /// <summary>
    ///     Контактный телефон
    /// </summary>
    [DisplayName("Контактный телефон")]
    public string? ContactPhone { get; init; } = null!;

    /// <summary>
    ///     Наименование работодателя
    /// </summary>
    [DisplayName("Наименование работодателя")]
    public string Name { get; init; } = null!;
}