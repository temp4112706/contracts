using Some.Abstractions.Mirs;
using Some.Dto.Addresses;
using Some.Dto.Monies;
using Some.Dto.Properties;
using Some.Dto.Underwriting;

namespace Some.Dto.Mirs;

/// <summary>
///     DTO: Запрос страхования имущества
/// </summary>
public record MirPropertyInsuranceRequestDto : EntityDto<Guid>,
    IMirPropertyInsuranceRequest<MoneyDto, MortgagePropertyDto, MirPropertyUnderwritingDto, FlatAddressDto>
{
    /// <inheritdoc />
    public decimal? LoanShare { get; set; }

    /// <summary>
    ///     Страхуемое имущество
    /// </summary>
    public MortgagePropertyDto Property { get; init; } = null!;

    /// <summary>
    ///     Андеррайтинг имущества
    /// </summary>
    public MirPropertyUnderwritingDto? Underwriting { get; init; }
}