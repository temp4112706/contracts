using Some.Abstractions.Mirs;
using Some.Dto.InsuredPersons;
using Some.Dto.Underwriting;

namespace Some.Dto.Mirs;

/// <summary>
///     DTO: Запрос страхования жизни
/// </summary>
public record MirLifeInsuranceRequestDto : EntityDto<Guid>,
    IMirLifeInsuranceRequest<InsuredPersonDto, MirLifeUnderwritingDto>
{
    /// <summary>
    ///     Застрахованное лицо
    /// </summary>
    public InsuredPersonDto InsuredPerson { get; set; }

    /// <summary>
    ///     Андеррайтинг жизни
    /// </summary>
    public MirLifeUnderwritingDto? Underwriting { get; set; }
}