﻿namespace Some.Dto.Mirs.CheckOnline;

public record CheckOnlineUnderwritingDto
{
    /// <summary>
    ///     Информация почему заявка нетиповая
    /// </summary>
    public List<CheckOnlineUnderwritingDetailsDto>? Details { get; init; }

    /// <summary>
    ///     типовая / не типовая
    /// </summary>
    public bool Online { get; init; }


    public static CheckOnlineUnderwritingDto Ok() =>
        new()
        {
            Online = true
        };

    public static CheckOnlineUnderwritingDto Reject(List<CheckOnlineUnderwritingDetailsDto> details) =>
        new()
        {
            Online = false,
            Details = details
        };
}