﻿using Some.Abstractions.Enums;

namespace Some.Dto.Mirs.CheckOnline;

public record CheckOnlineUnderwritingDetailsDto
{
    /// <summary>
    ///     Поле для подсветки ошибки
    /// </summary>
    public OnlineUnderwritingRejectionReasonCode? Criteria { get; init; }

    /// <summary>
    ///     Id объекта
    /// </summary>
    public string Id { get; init; } = null!;

    /// <summary>
    ///     Сообщение почему не попадает под критерии не типовой заявки
    /// </summary>
    public string Message { get; init; } = null!;


    public static CheckOnlineUnderwritingDetailsDto Reject(string id, string message, OnlineUnderwritingRejectionReasonCode? criteria) =>
        new()
        {
            Id = id,
            Message = message,
            Criteria = criteria
        };

    public static CheckOnlineUnderwritingDetailsDto Reject(string id, string message) =>
        new()
        {
            Id = id,
            Message = message
        };
}