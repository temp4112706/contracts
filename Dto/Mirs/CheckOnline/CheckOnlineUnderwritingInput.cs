﻿using System.ComponentModel;
using Some.Abstractions.Enums;
using Some.Dto.Monies;

namespace Some.Dto.Mirs.CheckOnline;

public record CheckOnlineUnderwritingInput
{
    /// <summary>
    ///     Выгодоприобретатель
    /// </summary>
    public BeneficiaryId? BeneficiaryId { get; init; }

    /// <summary>
    ///     Застрахованные лица
    /// </summary>
    public List<CheckOnlineUnderwritingInsuredPersonInput>? InsuredPersons { get; init; } = null;

    /// <summary>
    ///     Имущество
    /// </summary>
    public List<CheckOnlineUnderwritingInsuredProperty>? InsuredProperties { get; init; } = null;

    /// <summary>
    ///     ИД СК
    /// </summary>
    public InsurerId? InsurerId { get; init; }

    /// <summary>
    ///     Сумма кредита
    /// </summary>
    public MoneyDto? LoanAmount { get; init; } = null;

    /// <summary>
    ///     Данные ДС
    /// </summary>
    [DisplayName("Данные ДС")]
    public CheckOnlineUnderwritingPolicyDates? PolicyDates { get; set; } = null;
}