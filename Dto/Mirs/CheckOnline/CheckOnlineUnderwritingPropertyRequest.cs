﻿using System.ComponentModel;
using Some.Abstractions.Enums;
using Some.Dto.Normalizers;

namespace Some.Dto.Mirs.CheckOnline;

public class CheckOnlineUnderwritingPropertyRequest
{
    /// <summary>
    ///     Материал конструкции дома
    /// </summary>
    [DisplayName("Материал конструкции дома")]
    public ConstructionMaterial? ConstructionMaterial { get; init; } = null;

    /// <summary>
    ///     Фундамент
    /// </summary>
    [DisplayName("Фундамент")]
    public FoundationMaterial? Foundation { get; init; } = null;

    /// <summary>
    ///     Материал межэтажных перекрытий
    /// </summary>
    [DisplayName("Материал межэтажных перекрытий")]
    public InterfloorOverlapMaterial? InterfloorOverlapMaterial { get; init; } = null;

    /// <summary>
    ///     Доля созаемщика в процентах
    /// </summary>
    [DisplayName("Доля созаемщика в процентах")]
    public decimal? LoanShare { get; set; } = null;

    /// <summary>
    ///     Этажность
    /// </summary>
    [DisplayName("Этажность")]
    public int? NumberOfFloors { get; init; } = null;

    /// <summary>
    ///     Степень износа
    /// </summary>
    [DisplayName("Степень износа")]
    public PropertyPercentWear? PercentWear { get; init; } = null;

    /// <summary>
    ///     Год постройки
    /// </summary>
    [DisplayName("Год постройки")]
    public int? YearOfConstruction { get; init; } = null;


    public void Normalize() => LoanShare = RateNormalizer.Normalize(LoanShare);
}