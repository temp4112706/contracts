﻿using System.ComponentModel;
using Some.Abstractions.Enums;

namespace Some.Dto.Mirs.CheckOnline;

public class CheckOnlineUnderwritingInsuredProperty
{
    /// <summary>
    ///     Id
    /// </summary>
    public string Id { get; init; } = null!;

    /// <summary>
    ///     Имя - псевдоним
    /// </summary>
    public string Name { get; init; } = null!;

    /// <summary>
    ///     Данные по имуществу
    /// </summary>
    public CheckOnlineUnderwritingPropertyRequest? PropertyRequest { get; init; } = null;

    /// <summary>
    ///     Страхуется титул
    /// </summary>
    public CheckOnlineUnderwritingTitleRequest? TitleRequest { get; init; } = null;

    /// <summary>
    ///     Тип недвижимости
    /// </summary>
    [DisplayName("Тип недвижимости")]
    public PropertyType? Type { get; init; }


    public void Normalize()
    {
        PropertyRequest?.Normalize();
        TitleRequest?.Normalize();
    }
}