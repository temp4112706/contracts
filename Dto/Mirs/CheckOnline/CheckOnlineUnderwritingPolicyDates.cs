﻿using System.ComponentModel;

namespace Some.Dto.Mirs.CheckOnline;

/// <summary>
///     Даты Дс в заявке
/// </summary>
/// <param name="AgreementDateTime">Дата заключения ДС</param>
/// <param name="CommencementDateTime">Дата начала ДС</param>
/// <param name="ExpiryDateTime">Дата завершения ДС</param>
public record CheckOnlineUnderwritingPolicyDates(
    [property: DisplayName("Дата заключения ДС")]
    DateTime? AgreementDateTime,
    [property: DisplayName("Дата начала ДС")]
    DateTime? CommencementDateTime,
    [property: DisplayName("Дата завершения ДС")]
    DateTime? ExpiryDateTime);