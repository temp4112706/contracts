﻿namespace Some.Dto.Mirs.CheckOnline;

public class CheckOnlineUnderwritingInsuredPersonInput
{
    /// <summary>
    ///     Кастомер
    /// </summary>
    public CheckOnlineUnderwritingCustomerInput Customer { get; init; } = null!;

    /// <summary>
    ///     Id
    /// </summary>
    public string Id { get; init; } = null!;

    /// <summary>
    ///     Имя - кастомера
    /// </summary>
    /// <remarks>Используется как часть строки в сообщение почему не типовая заявка</remarks>
    public string Name { get; init; } = null!;


    public void Normalize() => Customer.Normalize();
}