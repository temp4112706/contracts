﻿using System.ComponentModel;

namespace Some.Dto.Mirs.CheckOnline;

/// <summary>
///     Артериальное давление (последнее измерение, мм. рт. ст.)
/// </summary>
/// <param name="Diastolic">Диастолическое</param>
/// <param name="Systolic">Систолическое</param>
public record CheckOnlineUnderwritingArterialPressure(
    [property: DisplayName("Диастолическое давление")]
    int? Diastolic,
    [property: DisplayName("Систолическое давление")]
    int? Systolic);