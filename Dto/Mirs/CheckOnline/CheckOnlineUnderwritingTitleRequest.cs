﻿using System.ComponentModel;
using Some.Dto.Normalizers;

namespace Some.Dto.Mirs.CheckOnline;

public class CheckOnlineUnderwritingTitleRequest
{
    /// <summary>
    ///     Доля созаемщика в процентах
    /// </summary>
    [DisplayName("Доля созаемщика в процентах")]
    public decimal? LoanShare { get; set; }

    public void Normalize() => LoanShare = RateNormalizer.Normalize(LoanShare);
}