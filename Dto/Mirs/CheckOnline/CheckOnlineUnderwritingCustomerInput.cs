﻿using System.ComponentModel;
using Some.Dto.HealthQuestionnaires;
using Some.Dto.Normalizers;

namespace Some.Dto.Mirs.CheckOnline;

public class CheckOnlineUnderwritingCustomerInput
{
    /// <summary>
    ///     Ответы с комментариями
    /// </summary>
    [DisplayName("Ответы с комментариями")]
    public ICollection<AnswerWithCommentsInput>? AnswerWithComments { get; init; } = null;

    /// <summary>
    ///     Артериальное давление
    /// </summary>
    [DisplayName("Артериальное давление")]
    public CheckOnlineUnderwritingArterialPressure? ArterialPressure { get; init; } = null;

    /// <summary>
    ///     Дата рождения
    /// </summary>
    [DisplayName("Дата рождения")]
    public DateTime? BirthDate { get; init; }

    /// <summary>
    ///     Рост
    /// </summary>
    [DisplayName("Рост")]
    public float? Height { get; init; } = null;


    /// <summary>
    ///     Доля созаемщика в процентах
    /// </summary>
    [DisplayName("Доля созаемщика в процентах")]
    public decimal? LoanShare { get; set; }

    /// <summary>
    ///     Вес
    /// </summary>
    [DisplayName("Вес")]
    public float? Weight { get; init; } = null;

    public void Normalize() => LoanShare = RateNormalizer.Normalize(LoanShare);
}