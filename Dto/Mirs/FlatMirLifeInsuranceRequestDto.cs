using Some.Abstractions.Mirs;
using Some.Dto.InsuredPersons;

namespace Some.Dto.Mirs;

/// <summary>
///     DTO: Запрос страхования жизни с основными полями
/// </summary>
public record FlatMirLifeInsuranceRequestDto : EntityDto<Guid>,
    IMirLifeInsuranceRequest<FlatInsuredPersonDto, FlatMirUnderwritingDto>
{
    /// <summary>
    ///     Застрахованное лицо
    /// </summary>
    public FlatInsuredPersonDto InsuredPerson { get; init; }

    /// <summary>
    ///     Андеррайтинг жизни
    /// </summary>
    public FlatMirUnderwritingDto? Underwriting { get; init; }
}