using Some.Abstractions.Underwriting;
using Some.Abstractions.Enums;

namespace Some.Dto.Mirs;

/// <summary>
///     DTO: Результат андеррайтинга с основными полями
/// </summary>
public record FlatMirUnderwritingDto : EntityDto<Guid>, IScalarMirUnderwriting
{
    /// <summary>
    ///     Статус запроса тарифа
    /// </summary>
    public MirUnderwritingState State { get; init; }

    /// <summary>
    ///     Онлайн андеррайтинг?
    /// </summary>
    public bool Online { get; init; }
}