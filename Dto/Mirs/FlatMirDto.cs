﻿using Some.Dto.Insurers;

namespace Some.Dto.Mirs;

/// <summary>
///     DTO: Заявка с основными полями
/// </summary>
public record FlatMirDto : BaseFlatMirDto<IdentifierInsurerDto>
{
        
}