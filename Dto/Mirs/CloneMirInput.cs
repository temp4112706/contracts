using Some.Abstractions.Enums;

namespace Some.Dto.Mirs;

/// <summary>
///     Данные для клонирования заявки
/// </summary>
/// <param name="SourceId">ИД клонируемой заявки</param>
/// <param name="BeneficiaryId">Выгодоприобретатель</param>
/// <param name="InsurerId">СК</param>
public record CloneMirInput(Guid SourceId, BeneficiaryId? BeneficiaryId, InsurerId? InsurerId);