using Some.Dto.Monies;

namespace Some.Dto.Mirs;

/// <summary>
///     DTO: Результат андеррайтинга
/// </summary>
public record MirUnderwritingResult
{
    /// <summary>
    ///     Комиссионное вознаграждение
    /// </summary>
    public MoneyDto InsurerCommission { get; init; } = null!;

    /// <summary>
    ///     Общая страховая премия - это сумма страховой премии по всем рискам и застрахованным лицам.
    /// </summary>
    public MoneyDto TotalPremiumAmount { get; init; } = null!;
}