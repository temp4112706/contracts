using Some.Abstractions.Mirs;
using Some.Dto.Addresses;
using Some.Dto.Monies;
using Some.Dto.Properties;
using Some.Dto.Underwriting;

namespace Some.Dto.Mirs;

/// <summary>
///     DTO: Запрос страхования титула
/// </summary>
public record MirTitleInsuranceRequestDto : EntityDto<Guid>,
    IMirTitleInsuranceRequest<MoneyDto, TitlePropertyDto, MirTitleUnderwritingDto, FlatAddressDto>
{
    /// <inheritdoc />
    public int? InsurancePeriod { get; set; }

    /// <inheritdoc />
    public decimal? LoanShare { get; set; }

    /// <inheritdoc />
    public TitlePropertyDto Property { get; set; } = null!;

    /// <inheritdoc />
    public MirTitleUnderwritingDto? Underwriting { get; set; }
}