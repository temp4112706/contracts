﻿using Some.Dto.Insurers;

namespace Some.Dto.Mirs;

/// <summary>
///     DTO: Заявка
/// </summary>
public record MirDto : BaseMirDto<IdentifierInsurerDto>;