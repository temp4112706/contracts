using Some.Abstractions.Enums;
using Some.Abstractions.Mirs;
using Some.Dto.Addresses;
using Some.Dto.Companies;
using Some.Dto.Customers;
using Some.Dto.Employees;
using Some.Dto.InsuredPersons;
using Some.Dto.LoanAgreements;
using Some.Dto.Monies;
using Some.Dto.Policies;
using Some.Dto.Properties;
using Some.Dto.Underwriting;

// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global

namespace Some.Dto.Mirs;

/// <summary>
///     DTO: Заявка
/// </summary>
public abstract record BaseMirDto<TInsurer> : TrackableEntityDto<Guid>,
    IMir<MoneyDto, CustomerDto, TInsurer, LoanAgreementDto, MortgagePropertyDto, TitlePropertyDto, InsuredPersonDto,
        MirLifeUnderwritingDto, MirPropertyUnderwritingDto, MirTitleUnderwritingDto,
        MirLifeInsuranceRequestDto, MirPropertyInsuranceRequestDto, MirTitleInsuranceRequestDto,
        EmployeeDto, FlatCompanyDto, FlatAddressDto>
{
    /// <summary>
    ///     Дополнительные свойства
    /// </summary>
    public Dictionary<string, object> ExtraProperties { get; set; } = new();

    /// <summary>
    ///     Связанные ДС
    /// </summary>
    public ICollection<IdentifierPolicyDto> Policies { get; init; } = null!;

    /// <summary>
    ///     Предварительное кол-во периодов в ДС
    /// </summary>
    public int PolicyNumberOfPeriods { get; init; }

    /// <summary>
    ///     Результат андеррайтинга страхования жизни, имущества и титула
    /// </summary>
    public MirUnderwritingResult? UnderwritingResult { get; set; }

    /// <summary>
    ///     Компания
    /// </summary>
    public FlatCompanyDto? Company { get; init; }

    /// <inheritdoc />
    public EmployeeDto? CompanyEmployee { get; init; }

    /// <summary>
    ///     Скидка
    /// </summary>
    public bool Discount { get; init; }

    /// <summary>
    ///     Страхователь
    /// </summary>
    public CustomerDto Insurant { get; init; } = null!;

    /// <summary>
    ///     Страховая компания (СК)
    /// </summary>
    public TInsurer Insurer { get; init; } = default!;

    /// <summary>
    ///     Исполнитель из СК
    /// </summary>
    public EmployeeDto? InsurerEmployee { get; init; }

    /// <inheritdoc />
    public ICollection<MirLifeInsuranceRequestDto> LifeInsuranceRequests { get; init; } = null!;

    /// <summary>
    ///     Кредитный договор
    /// </summary>
    public LoanAgreementDto LoanAgreement { get; init; } = null!;

    /// <summary>
    ///     Регион предмета ипотеки
    /// </summary>
    public string? MortgageRegion { get; init; }

    /// <summary>
    ///     ДС на один год
    /// </summary>
    public bool OneYear { get; init; }

    /// <summary>
    ///     Дата заключения ДС
    /// </summary>
    public DateTime PolicyAgreementDateTime { get; init; }

    /// <summary>
    ///     Дата начала ДС
    /// </summary>
    public DateTime PolicyCommencementDateTime { get; init; }

    /// <summary>
    ///     Дата завершения ДС
    /// </summary>
    public DateTime PolicyExpiryDateTime { get; init; }

    /// <inheritdoc />
    public ICollection<MirPropertyInsuranceRequestDto> PropertyInsuranceRequests { get; set; } = null!;

    /// <summary>
    ///     Набор рисков
    /// </summary>
    public InsuranceRisk[] Risks { get; init; } = null!;

    /// <inheritdoc />
    public ICollection<MirTitleInsuranceRequestDto> TitleInsuranceRequests { get; set; } = null!;

    /// <summary>
    ///     Номер заявки
    /// </summary>
    public string Number { get; init; } = null!;

    /// <summary>
    ///     Статус заявки
    /// </summary>
    public MirStatus Status { get; init; }

    /// <summary>
    ///     Дата и время изменения статуса
    /// </summary>
    public DateTime StatusUpdatedAt { get; init; }

    /// <summary>
    ///     Тип заявки
    /// </summary>
    public MirType Type { get; init; }
}