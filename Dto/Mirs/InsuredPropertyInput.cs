using System.ComponentModel;
using Some.Abstractions.Enums;
using Some.Dto.Addresses;
using Some.Dto.Monies;

namespace Some.Dto.Mirs;

/// <summary>
///     Застрахованное имущество по риску титула и имущества
/// </summary>
public record InsuredPropertyInput
{
    /// <summary>
    ///     Действительная (рыночная) стоимость
    /// </summary>
    [DisplayName("Действительная (рыночная) стоимость")]
    public MoneyInput ActualPrice { get; set; } = null!;

    /// <summary>
    ///     Адрес
    /// </summary>
    [DisplayName("Адрес")]
    public AddressInput Address { get; set; } = null!;

    /// <summary>
    ///     Кадастровый номер
    /// </summary>
    [DisplayName("Кадастровый номер")]
    public string CadastralNumber { get; init; } = null!;

    /// <summary>
    ///     Данные по риску имущества
    /// </summary>
    [DisplayName("Риск имущества")]
    public MirPropertyRequestInput? PropertyRequest { get; init; }

    /// <summary>
    ///     Данные по риску титула
    /// </summary>
    [DisplayName("Риск титула")]
    public MirTitleRequestInput? TitleRequest { get; init; }

    /// <summary>
    ///     Тип недвижимости
    /// </summary>
    [DisplayName("Тип недвижимости")]
    public PropertyType Type { get; set; }
}