using System.ComponentModel;
using Some.Abstractions.Enums;
using Some.Dto.Customers;
using Some.Dto.HealthQuestionnaires;
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Dto.Mirs;

/// <summary>
///     Участник договора
/// </summary>
/// <param name="ExistsCustomerId">Существующий ИД</param>
/// <param name="Customer">Данные по клиенту</param>
public record InsuredPersonInput(string? ExistsCustomerId, CustomerInput? Customer, bool IsBorrower)
    : MortgageUpsertCustomerInput(ExistsCustomerId, Customer, IsBorrower)
{
    /// <summary>
    ///     Данные мед анкеты
    /// </summary>
    [DisplayName("Данные мед анкеты")]
    public HealthQuestionnaireInput? HealthQuestionnaire { get; init; }

    /// <summary>
    ///     Индекс застрахованного лица
    /// </summary>
    [DisplayName("Индекс застрахованного лица")]
    public InsuredPersonIndex InsuredPersonIndex { get; set; }

    /// <summary>
    ///     Доля созаемщика в процентах
    /// </summary>
    [DisplayName("Доля созаемщика")]
    public decimal LoanShare { get; set; }

    /// <summary>
    ///     Является страхователем?
    /// </summary>
    [DisplayName("Страхователь")]
    public bool IsInsurant { get; init; }
}