using Some.Dto.Customers;
using Some.Dto.Employees;
using Some.Dto.LoanAgreements;
using Some.Abstractions.Enums;
using Some.Abstractions.Mirs;

namespace Some.Dto.Mirs;

/// <summary>
///     DTO: Заявка с основными полями
/// </summary>
public abstract record BaseFlatMirDto<TInsurer> : TrackableEntityDto<Guid>, IScalarMir
{
    /// <summary>
    ///     Исполнитель из СК
    /// </summary>
    public EmployeeDto? InsurerEmployee { get; init; }

    /// <summary>
    ///     Исполнитель компании
    /// </summary>
    public EmployeeDto? CompanyEmployee { get; init; }
    
    /// <summary>
    ///     Страхователь
    /// </summary>
    public FlatCustomerDto Insurant { get; init; } = null!;

    /// <summary>
    ///     Страховая компания (СК)
    /// </summary>
    public TInsurer Insurer { get; init; } = default!;

    /// <summary>
    ///     Кредитный договор
    /// </summary>
    public FlatLoanAgreementDto LoanAgreement { get; init; } = null!;

    /// <summary>
    ///     Список запросов страхования жизни
    /// </summary>
    public ICollection<FlatMirLifeInsuranceRequestDto> LifeInsuranceRequests { get; init; } = null!;

    /// <summary>
    ///     Запросы страхования имущества
    /// </summary>
    public ICollection<FlatMirPropertyInsuranceRequestDto> PropertyInsuranceRequests { get; set; } = null!;

    /// <summary>
    ///     Запросы страхования титула
    /// </summary>
    public ICollection<FlatMirTitleInsuranceRequestDto> TitleInsuranceRequests { get; set; } = null!;

    /// <summary>
    ///     Набор рисков
    /// </summary>
    public InsuranceRisk[] Risks { get; init; } = null!;

    /// <summary>
    ///     Номер заявки
    /// </summary>
    public string Number { get; init; } = null!;

    /// <summary>
    ///     Тип заявки
    /// </summary>
    public MirType Type { get; init; }

    /// <summary>
    ///     Статус заявки
    /// </summary>
    public MirStatus Status { get; init; }

    /// <summary>
    ///     Дата и время изменения статуса
    /// </summary>
    public DateTime StatusUpdatedAt { get; init; }
        
    /// <summary>
    ///     Регион предмета ипотеки
    /// </summary>
    public string? MortgageRegion { get; init; }
}