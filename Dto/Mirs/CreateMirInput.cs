using System.ComponentModel;
using Some.Abstractions.Enums;
using Some.Dto.Customers;
using Some.Dto.Employees;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Dto.Mirs;

/// <summary>
///     Заявка на ипотечное страхование
/// </summary>
public record CreateMirInput : MirUpdateMainInput
{
    /// <summary>
    ///     Сотрудник компании
    /// </summary>
    [DisplayName("Сотрудник компании")]
    public EmployeeInput? CompanyEmployee { get; init; }

    /// <summary>
    ///     Выбранная  компания
    /// </summary>
    [DisplayName("ИД компании")]
    public string? CompanyId { get; init; }
    
    /// <summary>
    ///     Страхователь
    /// </summary>
    [DisplayName("Страхователь")]
    public MortgageUpsertCustomerInput Insurant { get; init; } = null!;

    /// <summary>
    ///     Участники договора
    /// </summary>
    [DisplayName("Застрахованные лица")]
    public ICollection<InsuredPersonInput>? InsuredPersons { get; init; }

    /// <summary>
    ///     Скидка
    /// </summary>
    [DisplayName("Скидка")]
    public bool Discount { get; set; } = false;

    /// <summary>
    ///     Застрахованное имущество
    /// </summary>
    [DisplayName("Застрахованное имущество")]
    public ICollection<InsuredPropertyInput>? InsuredProperties { get; init; }

    /// <summary>
    ///     Сотрудник СК
    /// </summary>
    [DisplayName("Сотрудник СК")]
    public EmployeeInput? InsurerEmployee { get; init; }

    /// <summary>
    ///     ИД СК
    /// </summary>
    [DisplayName("ИД СК")]
    public InsurerId InsurerId { get; init; }
}