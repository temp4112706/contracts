using Ct.Module.Domain.Entities;
using Some.Abstractions.Enums;

namespace Some.Dto.Mirs;

/// <summary>
///     DTO: Идентификатор заявки
/// </summary>
public record IdentifierMirDto : EntityDto<Guid>, ICreationTrackable
{
    /// <summary>
    ///     Номер заявки
    /// </summary>
    public string Number { get; init; }

    /// <summary>
    ///     Статус заявки
    /// </summary>
    public MirStatus Status { get; init; }

    /// <summary>
    ///     Дата создания заявки
    /// </summary>
    public DateTime CreatedAt { get; init; }
}