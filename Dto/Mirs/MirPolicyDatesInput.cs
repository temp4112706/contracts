using System.ComponentModel;
using Some.Abstractions.Policies;

namespace Some.Dto.Mirs;

/// <summary>
///     Даты ДС
/// </summary>
/// <param name="AgreementDateTime">Дата заключения ДС</param>
/// <param name="CommencementDateTime">Дата начала ДС</param>
/// <param name="ExpiryDateTime">Дата завершения ДС</param>
public record MirPolicyDatesInput(
    [property: DisplayName("Дата заключения ДС")]
    DateTime AgreementDateTime,
    [property: DisplayName("Дата начала ДС")]
    DateTime CommencementDateTime,
    [property: DisplayName("Дата завершения ДС")]
    DateTime ExpiryDateTime) : IPolicyDates;