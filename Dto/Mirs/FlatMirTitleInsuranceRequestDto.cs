using Some.Abstractions.Mirs;
using Some.Dto.Addresses;
using Some.Dto.Monies;
using Some.Dto.Properties;

namespace Some.Dto.Mirs;

/// <summary>
///     DTO: Запрос страхования титула с основными полями
/// </summary>
public record FlatMirTitleInsuranceRequestDto : EntityDto<Guid>,
    IMirTitleInsuranceRequest<MoneyDto, FlatPropertyDto, FlatMirUnderwritingDto, FlatAddressDto>
{
    /// <inheritdoc />
    public int? InsurancePeriod { get; set; }

    /// <inheritdoc />
    public decimal? LoanShare { get; set; }

    /// <inheritdoc />
    public FlatPropertyDto? Property { get; set; } = null!;

    /// <inheritdoc />
    public FlatMirUnderwritingDto? Underwriting { get; set; }
}