using Some.Abstractions.Mirs;
using Some.Dto.Addresses;
using Some.Dto.Monies;
using Some.Dto.Properties;

namespace Some.Dto.Mirs;

/// <summary>
///     DTO: Запрос страхования имущества с основными полями
/// </summary>
public record FlatMirPropertyInsuranceRequestDto : EntityDto<Guid>,
    IMirPropertyInsuranceRequest<MoneyDto, FlatPropertyDto, FlatMirUnderwritingDto, FlatAddressDto>
{
    /// <inheritdoc />
    public decimal? LoanShare { get; set; }

    /// <inheritdoc />
    public FlatPropertyDto Property { get; set; } = null!;

    /// <summary>
    ///     Андеррайтинг имущества
    /// </summary>
    public FlatMirUnderwritingDto? Underwriting { get; init; }
}