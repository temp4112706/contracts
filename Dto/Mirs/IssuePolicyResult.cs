namespace Some.Dto.Mirs;

/// <summary>
///     DTO: Ответ на запрос выпуска ДС - идентификатор для опроса состояния
/// </summary>
/// <param name="PolicyId">Идентификатор запроса на выпуск ДС</param>
public record IssuePolicyResult(Guid PolicyId);