namespace Some.Dto.Mirs;

/// <summary>
///     DTO: Данные для выпуска ДС
/// </summary>
/// <param name="PolicyNumber">Номер ДС</param>
/// <param name="DetentionPlace">Место выдачи ДС</param>
public record IssuePolicyInput(string? PolicyNumber, string? DetentionPlace);