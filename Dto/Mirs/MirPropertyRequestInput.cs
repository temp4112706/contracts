﻿using System.ComponentModel;
using Some.Dto.Properties;

namespace Some.Dto.Mirs;

/// <summary>
///     Данные по риску имущества
/// </summary>
public record MirPropertyRequestInput
{
    /// <summary>
    ///     Информация о страхуемом объекте по риску имущества
    /// </summary>
    [DisplayName("Страхуемый объект по риску имущества")]
    public MortgagePropertyDataInput? Data { get; init; }

    /// <summary>
    ///     Доля страхования имущества в процентах
    /// </summary>
    [DisplayName("Доля страхования имущества в процентах")]
    public decimal? LoanShare { get; set; }
}