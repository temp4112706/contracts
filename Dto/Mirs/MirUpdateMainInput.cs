using System.ComponentModel;
using Some.Dto.LoanAgreements;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global

namespace Some.Dto.Mirs;

/// <summary>
///     Данные для обновление региона предмета ипотеки
/// </summary>
public record MirUpdateMainInput
{
    /// <summary>
    ///     Данные по КД
    /// </summary>
    [DisplayName("Данные по КД")]
    public MainLoanAgreementInput LoanAgreement { get; init; } = null!;

    /// <summary>
    ///     Регион предмета ипотеки
    /// </summary>
    [DisplayName("Регион предмета ипотеки")]
    public string? MortgageRegion { get; set; } = null!;

    /// <summary>
    ///     Данные ДС
    /// </summary>
    [DisplayName("Данные ДС")]
    public MirPolicyDatesInput Policy { get; set; } = null!;
}