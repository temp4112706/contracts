﻿namespace Some.Dto.Mirs;

/// <summary>
///     Результат обновления заявки
/// </summary>
public record MirUpdateResult
{
    /// <summary>
    ///     Заявка
    /// </summary>
    public MirDto Mir { get; init; } = null!;

    /// <summary>
    ///     Сброшены данные пндеррайтинга
    /// </summary>
    public bool ResetUnderwriting { get; init; }
}