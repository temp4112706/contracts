namespace Some.Dto.Mirs;

/// <summary>
///     Данные для сдвига дат ДС в заявке
/// </summary>
/// <param name="ShiftPolicyDays">кол-во дней для сдвига (если не указано, то сдвиг до текущей даты)</param>
public record ShiftPolicyDatesMirInput(int? ShiftPolicyDays);