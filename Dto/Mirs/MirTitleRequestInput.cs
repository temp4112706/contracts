﻿using System.ComponentModel;
using Some.Dto.Properties;

namespace Some.Dto.Mirs;

/// <summary>
///     Данные по риску титула
/// </summary>
public record MirTitleRequestInput
{
    /// <summary>
    ///     Страхуемый объект по риску титула
    /// </summary>
    [DisplayName("Страхуемый объект по риску титула")]
    public TitlePropertyDataInput? Data { get; init; }

    /// <summary>
    ///     Срок страхования титула (в месяцах)
    /// </summary>
    [DisplayName("Срок страхования титула в месяцах")]
    public int? InsurancePeriod { get; set; }

    /// <summary>
    ///     Доля страхования титула в процентах
    /// </summary>
    [DisplayName("Доля страхования титула в процентах")]
    public decimal? LoanShare { get; set; }
}