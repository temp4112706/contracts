﻿using Some.Abstractions.Customers;

namespace Some.Dto.Customers;

/// <summary>
///     DTO: <inheritdoc cref="ICustomerFinanceInfo" />
/// </summary>
public record CustomerFinanceInfoDto(int? AnnualIncome, int? DifferenceAssets, int? Dependents)
    : ICustomerFinanceInfo
{
}