using System.ComponentModel;

namespace Some.Dto.Customers;

/// <summary>
///     Данные по застрахованному клиенту
/// </summary>
/// <param name="ExistsCustomerId">Существующий ИД</param>
/// <param name="Customer">Данные по клиенту</param>
/// <param name="IsBorrower">Заемщик</param>
public record MortgageUpsertCustomerInput(
    string? ExistsCustomerId, 
    CustomerInput? Customer,
    [property: DisplayName("Заемщик")] bool IsBorrower) 
    : UpsertCustomerInput(ExistsCustomerId, Customer);