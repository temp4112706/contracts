using Some.Abstractions.Customers;
using Some.Abstractions.Enums;
using Some.Dto.Addresses;
using Some.Dto.Occupations;
using Some.Dto.Passports;
using Some.Dto.SportActivities;

// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global

namespace Some.Dto.Customers;

/// <summary>
///     DTO: Данные клиента
/// </summary>
public record CustomerDto : FlatCustomerDto, ICustomer<AddressDto, PassportRfDto, OccupationDto, CustomerFinanceInfoDto, SportActivityDto>
{
    /// <summary>
    ///     Кол-во детей
    /// </summary>
    public int? ChildCount { get; init; }

    /// <summary>
    ///     Фактический адрес
    /// </summary>
    public AddressDto? CurrentAddress { get; init; }

    /// <summary>
    ///     Документ, подтверждающий личность
    /// </summary>
    public PassportRfDto Document { get; init; } = null!;

    /// <inheritdoc />
    public CustomerFinanceInfoDto? FinanceInfo { get; init; }

    /// <summary>
    ///     Информация о занятости
    /// </summary>
    public OccupationDto Occupation { get; init; } = null!;

    /// <summary>
    ///     Адрес регистрации
    /// </summary>
    public AddressDto RegistrationAddress { get; init; } = null!;
    
    /// <inheritdoc />
    public SportActivityDto SportActivity { get; init; } = null!;

    /// <summary>
    ///     Дата рождения
    /// </summary>
    public DateTime BirthDate { get; init; }

    /// <summary>
    ///     Место рождения
    /// </summary>
    public string BirthPlace { get; init; } = null!;

    /// <summary>
    ///     Мобильный телефон в формате 79999999999
    /// </summary>
    public string CellPhone { get; init; } = null!;

    /// <summary>
    ///     Страна гражданства (RU)
    /// </summary>
    public string Citizenship { get; init; } = null!;

    /// <summary>
    ///     Id в DS
    /// </summary>
    public Guid? CommonStoreId { get; set; }

    /// <summary>
    ///     Электронная почта
    /// </summary>
    public string? Email { get; init; }

    /// <summary>
    ///     Уникальный идентификатор клиента в системе Банка
    /// </summary>
    public string ExternalId { get; init; } = null!;

    /// <summary>
    ///     Пол
    /// </summary>
    public Gender Gender { get; init; }

    /// <summary>
    ///     Домашний телефон в формате 79999999999
    /// </summary>
    public string? HomePhone { get; init; }

    /// <summary>
    ///     ИНН (формат 999999999999)
    /// </summary>
    public string? Inn { get; init; }

    /// <summary>
    ///     Являетесь ли вы иностранным публичным должностным лицом?
    /// </summary>
    public bool IsForeignPublicPerson { get; init; }

    /// <summary>
    ///     Семейное положение (холост, женат, разведен)
    /// </summary>
    public MaritalState MaritalState { get; init; }

    /// <summary>
    ///     Страна резиденства (RU)
    /// </summary>
    public string ResidenceCountry { get; init; } = null!;

    /// <summary>
    ///     СНИЛС (формат 99999999999)
    /// </summary>
    public string? Snils { get; init; }

    /// <summary>
    ///     Рабочий телефон в формате 79999999999
    /// </summary>
    public string? WorkPhone { get; init; }
}