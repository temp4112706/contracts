﻿namespace Some.Dto.Customers;

/// <summary>
///     Фильтр для списка <see cref="Customer" />
/// </summary>
public record SearchCustomerFilter(SearchDocumentFilter? Document, string? CellPhone, string? Inn, string? Snils)
    : LimitedResultInput;