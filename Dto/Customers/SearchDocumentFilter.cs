﻿namespace Some.Dto.Customers;

/// <summary>
///     Поиск по паспарту
/// </summary>
/// <param name="Series">Серия</param>
/// <param name="Number">Номер</param>
public record SearchDocumentFilter(string Series, string Number);