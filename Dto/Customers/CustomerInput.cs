using System.ComponentModel;
using System.Text.Json.Serialization;
using Some.Abstractions.Customers;
using Some.Abstractions.Enums;
using Some.Dto.Addresses;
using Some.Dto.Occupations;
using Some.Dto.Passports;
using Some.Dto.SportActivities;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Dto.Customers;

/// <summary>
///     Данные клиента для заполнения
/// </summary>
public record CustomerInput : IScalarCustomer
{
    /// <summary>
    ///     Кол-во детей
    /// </summary>
    [DisplayName("Кол-во детей")]
    public int? ChildCount { get; set; }

    /// <summary>
    ///     Фактический адрес
    /// </summary>
    [DisplayName("Фактический адрес")]
    public AddressInput? CurrentAddress { get; set; }

    /// <summary>
    ///     ФИО клиента
    /// </summary>
    [DisplayName("ФИО клиента")]
    public CustomerNameInput CustomerName { get; set; } = null!;

    /// <summary>
    ///     Документ, подтверждающий личность
    /// </summary>
    [DisplayName("ФИО клиента")]
    public PassportRfInput Document { get; set; } = null!;

    /// <summary>
    ///     Информация о доходах и иждивенцах
    /// </summary>
    [DisplayName("Финансовая информация")]
    public CustomerFinanceInfoInput? FinanceInfo { get; init; }

    /// <summary>
    ///     Информация о занятости
    /// </summary>
    [DisplayName("Информация о занятости")]
    public OccupationInput Occupation { get; set; } = null!;
    
    /// <summary>
    ///     Информация о спортивной деятельности
    /// </summary>
    [DisplayName("Информация о спортивной деятельности")]
    public SportActivityInput SportActivity { get; set; } = null!;

    /// <summary>
    ///     Адрес регистрации
    /// </summary>
    [DisplayName("Адрес регистрации")]
    public AddressInput RegistrationAddress { get; set; } = null!;

    /// <summary>
    ///     Дата рождения
    /// </summary>
    [DisplayName("Дата рождения")]
    public DateTime BirthDate { get; set; }

    /// <summary>
    ///     Место рождения
    /// </summary>
    [DisplayName("Место рождения")]
    public string BirthPlace { get; set; } = null!;

    /// <summary>
    ///     Мобильный телефон в формате 79999999999
    /// </summary>
    [DisplayName("Мобильный телефон")]
    public string CellPhone { get; set; } = null!;

    /// <summary>
    ///     Страна гражданства (RU)
    /// </summary>
    [DisplayName("Страна гражданства")]
    public string Citizenship { get; set; } = null!;

    /// <summary>
    ///     Id в DS
    /// </summary>
    [DisplayName("Id в DS")]
    public Guid? CommonStoreId { get; set; }

    /// <summary>
    ///     Электронная почта
    /// </summary>
    [DisplayName("Электронная почта")]
    public string? Email { get; set; }

    /// <summary>
    ///     Уникальный идентификатор клиента в системе Банка
    /// </summary>
    [JsonIgnore]
    public string ExternalId { get; init; } = string.Empty;

    /// <summary>
    ///     Пол
    /// </summary>
    [DisplayName("Пол")]
    public Gender Gender { get; set; }

    /// <summary>
    ///     Домашний телефон в формате 79999999999
    /// </summary>
    [DisplayName("Домашний телефон")]
    public string? HomePhone { get; set; }

    /// <summary>
    ///     ИНН (формат 999999999999)
    /// </summary>
    [DisplayName("ИНН")]
    public string? Inn { get; set; }

    /// <summary>
    ///     Являетесь ли вы иностранным публичным должностным лицом?
    /// </summary>
    [DisplayName("Иностранная публичная должность")]
    public bool IsForeignPublicPerson { get; init; }

    /// <summary>
    ///     Семейное положение (холост, женат, разведен)
    /// </summary>
    [DisplayName("Семейное положение")]
    public MaritalState MaritalState { get; set; }

    /// <summary>
    ///     Страна резиденства (RU)
    /// </summary>
    [DisplayName("Страна резиденства")]
    public string ResidenceCountry { get; set; } = null!;

    /// <summary>
    ///     СНИЛС (формат 99999999999)
    /// </summary>
    [DisplayName("СНИЛС")]
    public string? Snils { get; set; }

    /// <summary>
    ///     Рабочий телефон в формате 79999999999
    /// </summary>
    [DisplayName("Рабочий телефон")]
    public string? WorkPhone { get; set; }
}