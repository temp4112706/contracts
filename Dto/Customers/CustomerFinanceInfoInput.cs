﻿using System.ComponentModel;
using Some.Abstractions.Customers;

namespace Some.Dto.Customers;

/// <summary>
///     Input: <inheritdoc cref="ICustomerFinanceInfo" />
/// </summary>
public record CustomerFinanceInfoInput(
        [property: DisplayName("Годовой доход")]
        int? AnnualIncome,
        [property: DisplayName("Разница активов")]
        int? DifferenceAssets,
        [property: DisplayName("Количество иждивенцев")]
        int? Dependents)
    : ICustomerFinanceInfo
{
}