using Some.Abstractions.Customers;

namespace Some.Dto.Customers;

/// <summary>
///     DTO: Данные клиента с основными полями
/// </summary>
public record FlatCustomerDto : EntityDto<Guid>, ICustomerName
{
    /// <summary>
    ///     Фамилия
    /// </summary>
    public string LastName { get; init; } = null!;

    /// <summary>
    ///     Имя
    /// </summary>
    public string FirstName { get; init; } = null!;

    /// <summary>
    ///     Отчество
    /// </summary>
    public string? MiddleName { get; init; } = null!;
}