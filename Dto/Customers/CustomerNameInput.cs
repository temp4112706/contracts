using System.ComponentModel;
using Some.Abstractions.Customers;

namespace Some.Dto.Customers;

/// <summary>
///     ФИО клиента
/// </summary>
public record CustomerNameInput : ICustomerName
{
    /// <summary>
    ///     Фамилия
    /// </summary>
    [DisplayName("Фамилия")]
    public string LastName { get; init; } = null!;

    /// <summary>
    ///     Имя
    /// </summary>
    [DisplayName("Имя")]
    public string FirstName { get; init; } = null!;

    /// <summary>
    ///     Отчество
    /// </summary>
    [DisplayName("Отчество")]
    public string? MiddleName { get; init; }
}