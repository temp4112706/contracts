using System.ComponentModel;

namespace Some.Dto.Customers;

/// <summary>
///     Данные по застрахованному клиенту
/// </summary>
/// <param name="ExistsCustomerId">Существующий ИД</param>
/// <param name="Customer">Данные по клиенту</param>
public record UpsertCustomerInput(
    [property: DisplayName("ИД существующего застрахованного клиента")]
    string? ExistsCustomerId,
    [property: DisplayName("Данные по застрахованному клиенту")]
    CustomerInput? Customer);