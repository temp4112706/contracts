﻿using Some.Abstractions.Result;

namespace Some.Dto.Builders;

public static class ListResultBuilder
{
    public static ListResult<T> Create<T>(T item) => new ListResult<T>(new List<T>(1) { item });

    public static ListResult<T> Create<T>(T[] items) => new ListResult<T>(items);
}