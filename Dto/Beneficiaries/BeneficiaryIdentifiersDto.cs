using Some.Abstractions.Enums;

namespace Some.Dto.Beneficiaries;

/// <summary>
///     Информация о выгодоприобретателе
/// </summary>
/// <param name="Id">ИД</param>
/// <param name="Name">Название</param>
/// <param name="Bic">БИК</param>
public record BeneficiaryIdentifiersDto(BeneficiaryId Id, string Name, string? Bic);