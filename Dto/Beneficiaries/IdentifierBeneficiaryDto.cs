namespace Some.Dto.Beneficiaries;

/// <summary>
///     Выгодоприобретатель
/// </summary>
/// <param name="Id">Код</param>
/// <param name="Name">Наименование</param>
public record IdentifierBeneficiaryDto(string Id, string Name);