﻿using System.Diagnostics.CodeAnalysis;
using Some.Dto.Webhooks.Payloads;

namespace Some.Dto.Webhooks;

/// <summary>
///     Объект вебхука при добавлении/изменении МЕДО в заявке
/// </summary>
[SuppressMessage("ReSharper", "UnusedMember.Global")]
public class HealthSurveyUpsertedWebhook: BaseMirWebhook<HealthSurveyUpsertedPayload>
{
        
}