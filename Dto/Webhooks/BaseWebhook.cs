﻿using System.Diagnostics.CodeAnalysis;

namespace Some.Dto.Webhooks;

/// <summary>
///     Базовый объект вебхука
/// </summary>
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
public class BaseWebhook
{
}