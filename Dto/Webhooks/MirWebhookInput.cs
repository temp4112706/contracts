﻿using System.ComponentModel;
using Some.Abstractions.Enums;

// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global

namespace Some.Dto.Webhooks;

/// <summary>
///     Input: Данные для создания вебхука
/// </summary>
public record MirWebhookInput
{
    /// <summary>
    ///     Заголовки
    /// </summary>
    [DisplayName("Заголовки")]
    public IDictionary<string, string>? Headers { get; set; }

    /// <summary>
    ///     Секретный ключ
    /// </summary>
    [DisplayName("Секретный ключ")]
    public string Secret { get; set; } = null!;

    /// <summary>
    ///     Тип подписки
    /// </summary>
    [DisplayName("Тип подписки")]
    public MirWebhookSubscription Subscription { get; set; }

    /// <summary>
    ///     Адрес для отправки
    /// </summary>
    [DisplayName("Адрес")]
    public string Url { get; set; } = null!;
}