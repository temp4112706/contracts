﻿using System.Diagnostics.CodeAnalysis;
using Some.Dto.Webhooks.Payloads;

namespace Some.Dto.Webhooks;

/// <summary>
///     Объект вебхука при завершении андеррайтинга жизни в заявке
/// </summary>
[SuppressMessage("ReSharper", "UnusedMember.Global")]
public class LifeUnderwritingCompletedWebhook : BaseMirWebhook<
    LifeUnderwritingCompletedPayload>
{

}