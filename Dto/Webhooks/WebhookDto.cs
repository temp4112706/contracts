﻿namespace Some.Dto.Webhooks;

/// <summary>
///     DTO: Вебхук
/// </summary>
public record WebhookDto : EntityDto<Guid>
{
    /// <summary>
    ///     Ид сущности
    /// </summary>
    public Guid EntityId { get; init; }

    /// <summary>
    ///     Заголовки
    /// </summary>
    public IDictionary<string, string>? Headers { get; init; }

    /// <summary>
    ///     Закрытый ключ
    /// </summary>
    public string Secret { get; init; } = null!;

    /// <summary>
    ///     Тип подписки
    /// </summary>
    public string Subscription { get; init; } = null!;

    /// <summary>
    ///     Адрес для отправки
    /// </summary>
    public string Url { get; init; } = null!;
}