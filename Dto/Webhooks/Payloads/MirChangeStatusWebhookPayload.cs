﻿using Some.Abstractions.Mirs;
using Some.Abstractions.Enums;

namespace Some.Dto.Webhooks.Payloads;

/// <summary>
///     Данные при изменении статуса
/// </summary>
public class MirChangeStatusWebhookPayload : IMirStatusChangesResult
{
    /// <inheritdoc />
    public MirStatus? PreviousStatus { get; set; }
        
    /// <inheritdoc />
    public MirStatus NextStatus { get; set; }

    /// <inheritdoc />
    public DateTime ChangedAt { get; set; }
}