﻿using System.Diagnostics.CodeAnalysis;
using Some.Dto.Underwriting;

namespace Some.Dto.Webhooks.Payloads;

/// <summary>
///     Данные при завершении андеррайтинга жизни
/// </summary>
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
public class LifeUnderwritingCompletedPayload
{
    /// <summary>
    ///     Застрахованоое лицо
    /// </summary>
    public InsuredPersonPayload InsuredPerson { get; set; } = null!;

    /// <summary>
    ///     Результаты андерратйинга
    /// </summary>
    public MirLifeUnderwritingDto Underwriting { get; set; } = null!;
}