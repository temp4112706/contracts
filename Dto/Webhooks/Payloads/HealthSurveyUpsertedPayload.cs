﻿using System.Diagnostics.CodeAnalysis;
using Some.Abstractions.Enums;
using Some.Dto.HealthSurveys;

namespace Some.Dto.Webhooks.Payloads;

/// <summary>
///     Данные при добавлении/изменении МЕДО в заявке
/// </summary>
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
public class HealthSurveyUpsertedPayload
{
    /// <summary>
    ///     Операция
    /// </summary>
    public CdcOperation Operation { get; set; }

    /// <summary>
    ///     Застрахованоое лицо
    /// </summary>
    public InsuredPersonPayload InsuredPerson { get; set; } = null!;

    /// <summary>
    ///     Данные МЕДО
    /// </summary>
    public HealthSurveyDto HealthSurvey { get; set; } = null!;
}