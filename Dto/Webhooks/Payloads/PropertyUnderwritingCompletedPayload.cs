﻿using System.Diagnostics.CodeAnalysis;
using Some.Dto.Underwriting;

namespace Some.Dto.Webhooks.Payloads;

/// <summary>
///     Данные при завершении андеррайтинга имущества
/// </summary>
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
public class PropertyUnderwritingCompletedPayload
{
    /// <summary>
    ///     Результаты андерратйинга
    /// </summary>
    public MirPropertyUnderwritingDto Underwriting { get; set; } = null!;
}