﻿using System.Diagnostics.CodeAnalysis;
using Some.Abstractions.Enums;

namespace Some.Dto.Webhooks.Payloads;

/// <summary>
///     DTO: Застрахованоое лицо
/// </summary>
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
public class InsuredPersonPayload
{
    /// <summary>
    ///     Индекс застрахованного лица
    /// </summary>
    public InsuredPersonIndex Index { get; set; }
}