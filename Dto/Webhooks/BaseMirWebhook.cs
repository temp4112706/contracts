﻿using System.Diagnostics.CodeAnalysis;
using Some.Dto.Employees;
using Some.Abstractions.Enums;

namespace Some.Dto.Webhooks;

/// <summary>
///     Базовый объект вебхука в заявке
/// </summary>
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
public abstract class BaseMirWebhook<TPayload> : BaseWebhook
    where TPayload : class
{
    /// <summary>
    ///     Подписка
    /// </summary>
    public MirWebhookSubscription Subscription { get; set; }

    /// <summary>
    ///     ИД заявки
    /// </summary>
    public Guid MirId { get; set; }

    /// <summary>
    ///     Сотрудник СК
    /// </summary>
    public EmployeeDto? InsurerEmployee { get; set; }

    /// <summary>
    ///     Сотрудник компании
    /// </summary>
    public EmployeeDto? CompanyEmployee { get; set; }

    /// <summary>
    ///     Данные
    /// </summary>
    public TPayload Data { get; set; } = null!;
}