using Some.Abstractions.Customers;
using Some.Abstractions.Enums;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Dto.Passports;

/// <summary>
///     DTO: Документ удостоверяющий личность (Паспорт РФ)
/// </summary>
public record PassportRfDto : EntityDto<Guid>, IPassportRf
{
    /// <summary>
    ///     Код подразделения
    /// </summary>
    public string? DepartmentCode { get; init; }

    /// <summary>
    ///     Код типа документа (21 для Паспорта РФ)
    /// </summary>
    public DocType DocType { get; init; }

    /// <summary>
    ///     Дата выдачи
    /// </summary>
    public DateTime IssueDate { get; init; }

    /// <summary>
    ///     Документ выдан
    /// </summary>
    public string IssuedBy { get; init; } = null!;

    /// <summary>
    ///     Номер документа (формат 999999)
    /// </summary>
    public string Number { get; init; } = null!;

    /// <summary>
    ///     Серия документа (формат 9999)
    /// </summary>
    public string Series { get; init; } = null!;
}