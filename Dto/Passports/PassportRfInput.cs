using System.ComponentModel;
using Some.Abstractions.Customers;
using Some.Abstractions.Enums;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Dto.Passports;

/// <summary>
///     Данные о документе, удостоверяющий личность (Паспорт РФ)
/// </summary>
public record PassportRfInput : IPassportRf
{
    /// <summary>
    ///     Код подразделения
    /// </summary>
    [DisplayName("Код подразделения")]
    public string? DepartmentCode { get; init; }

    /// <summary>
    ///     Код типа документа (21 для Паспорта РФ)
    /// </summary>
    [DisplayName("Тип документа")]
    public DocType DocType { get; init; }

    /// <summary>
    ///     Дата выдачи
    /// </summary>
    [DisplayName("Дата выдачи")]
    public DateTime IssueDate { get; init; }

    /// <summary>
    ///     Кем выдан
    /// </summary>
    [DisplayName("Кем выдан")]
    public string IssuedBy { get; init; } = null!;

    /// <summary>
    ///     Номер документа (формат 999999)
    /// </summary>
    [DisplayName("Номер документа")]
    public string Number { get; init; } = null!;

    /// <summary>
    ///     Серия документа (формат 9999)
    /// </summary>
    [DisplayName("Серия документа")]
    public string Series { get; init; } = null!;
}