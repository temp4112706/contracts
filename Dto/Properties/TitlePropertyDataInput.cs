﻿using System.ComponentModel;
using Some.Abstractions.Properties.Title;

namespace Some.Dto.Properties;

/// <summary>
///     Страхуемый объект по риску титула
/// </summary>
public record TitlePropertyDataInput : ITitleProperty<TitlePropertyAnswerWithCommentsInput>
{
    /// <inheritdoc />
    [Description("Количество сделок")]
    public int? AmountOfDeals { get; init; }

    /// <inheritdoc />
    [Description("Анкета по титулу")]
    public ICollection<TitlePropertyAnswerWithCommentsInput>? AnswerWithComments { get; init; }

    /// <inheritdoc />
    [Description("Документ, подтверждающий стоимость недвижимого имущества")]
    public string? DocumentOfPrice { get; init; }

    /// <inheritdoc />
    [Description("Основание последнего перехода права собственности")]
    public string? DocumentRegistration { get; init; }

    /// <inheritdoc />
    [Description("Количество переходов в течение последних 12 месяцев")]
    public int? HopCount { get; init; }

    /// <inheritdoc />
    [Description("Тип собственника (ФЛ/ЮЛ).")]
    public bool? IsLegalEntity { get; init; }

    /// <inheritdoc />
    [Description("Рынок недвижимости: первичный/вторичный")]
    public bool? IsPrimary { get; init; }

    /// <inheritdoc />
    [Description("Основание приобретения")]
    public bool? Pif { get; init; }

    /// <inheritdoc />
    [Description("Сделка совершается по доверенности")]
    public bool? PowerOfAttorney { get; init; }

    /// <inheritdoc />
    [Description("Дата регистрации права")]
    public DateTime? RegistrationDate { get; init; }
}