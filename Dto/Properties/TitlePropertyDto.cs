﻿using Some.Abstractions.Properties;
using Some.Abstractions.Properties.Title;
using Some.Dto.Addresses;
using Some.Dto.Monies;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Dto.Properties;

/// <summary>
///     Страхуемый объект по риску титула
/// </summary>
public record TitlePropertyDto : FlatPropertyDto, IScalarProperty<FlatAddressDto, MoneyDto>,
    ITitleProperty<TitlePropertyAnswerWithCommentsDto>
{
    /// <summary>
    ///     Количество сделок
    /// </summary>
    public int? AmountOfDeals { get; init; }

    /// <summary>
    ///     Анкета по титулу
    /// </summary>
    public ICollection<TitlePropertyAnswerWithCommentsDto>? AnswerWithComments { get; init; }

    /// <summary>
    ///     Документ, подтверждающий стоимость недвижимого имущества
    /// </summary>
    public string? DocumentOfPrice { get; init; }

    /// <summary>
    ///     Основание последнего перехода права собственности
    /// </summary>
    public string? DocumentRegistration { get; init; }

    /// <summary>
    ///     Количество переходов в течение последних 12 месяцев
    /// </summary>
    public int? HopCount { get; init; }

    /// <summary>
    ///     Тип собственника (ФЛ/ЮЛ). Если ЮЛ, то true
    /// </summary>
    public bool? IsLegalEntity { get; init; }

    /// <summary>
    ///     Рынок недвижимости: первичный/вторичный
    /// </summary>
    public bool? IsPrimary { get; init; }

    /// <summary>
    ///     Основание приобретения - ПИФ
    /// </summary>
    public bool? Pif { get; init; }

    /// <summary>
    ///     Сделка совершается по доверенности
    /// </summary>
    public bool? PowerOfAttorney { get; init; }

    /// <summary>
    ///     Дата регистрации права
    /// </summary>
    public DateTime? RegistrationDate { get; init; }
}