using Some.Abstractions.Enums;
using Some.Abstractions.Properties;
using Some.Abstractions.Properties.Mortgage;
using Some.Dto.Addresses;
using Some.Dto.Monies;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Dto.Properties;

/// <summary>
///     DTO: Страхуемый объект
/// </summary>
public record MortgagePropertyDto : FlatPropertyDto, IScalarProperty<FlatAddressDto, MoneyDto>, IMortgageProperty<MoneyDto>
{
    /// <summary>
    ///     Материал конструкции дома
    /// </summary>
    public ConstructionMaterial? ConstructionMaterial { get; init; }

    /// <summary>
    ///     Этаж
    /// </summary>
    public int? FloorNumber { get; init; }

    /// <summary>
    ///     Материал фундамента
    /// </summary>
    public FoundationMaterial? Foundation { get; init; }

    /// <summary>
    ///     Материал межэтажных перекрытий
    /// </summary>
    public InterfloorOverlapMaterial? InterfloorOverlapMaterial { get; init; }

    /// <summary>
    ///     Имеются внутренние и/или внешние повреждения/дефекты имущества
    /// </summary>
    public bool? IsDamaged { get; init; }

    /// <summary>
    ///     Средства противопожарной защиты
    /// </summary>
    public bool? IsFireProtected { get; init; }

    /// <summary>
    ///     Находится аварийном состоянии или нет
    /// </summary>
    public bool? IsInEmergencyCondition { get; init; }

    /// <summary>
    ///     Наличие источников повышенной пожарной опасности
    /// </summary>
    public bool? IsInFireDangerZone { get; init; }

    /// <summary>
    ///     Находится в зоне опасных природных явлений
    /// </summary>
    public bool? IsInNaturalHazardsZone { get; init; }

    /// <summary>
    ///     Соседство с объектами повышенной опасности
    /// </summary>
    public bool? IsInNeighborhoodDangerZone { get; init; }

    /// <summary>
    ///     Является объектом незавершенного строительства
    /// </summary>
    public bool? IsInProgressConstruction { get; init; }

    /// <summary>
    ///     Застраховано ли в настоящее время недвижимое имущество в другой в компании?
    /// </summary>
    public bool? IsInsuredByOtherInsurer { get; init; }

    /// <summary>
    ///     Несоответствие режима использования объекта недвижимого имущества
    /// </summary>
    public bool? IsInUseModeMismatch { get; init; }

    /// <summary>
    ///     Проводилась перепланировка недвижимого имущества
    /// </summary>
    public bool? IsRedeveloped { get; init; }

    /// <summary>
    ///     Средства, системы безопасности и охраны
    /// </summary>
    public bool? IsSecuritySystem { get; init; }

    /// <summary>
    ///     Датчики протечки воды, блокирующие подачу воды в квартире
    /// </summary>
    public bool? IsWaterLeakageSensors { get; init; }

    /// <summary>
    ///     Решетки либо рольставни на окнах (для первого этажа)
    /// </summary>
    public bool? IsWindowsProtected { get; init; }

    /// <summary>
    ///     Жилая площадь, кв.м.
    /// </summary>
    public float? LivingArea { get; init; }

    /// <summary>
    ///     Этажность
    /// </summary>
    public int? NumberOfFloors { get; init; }

    /// <summary>
    ///     Число комнат
    /// </summary>
    public int? NumberOfRooms { get; init; }

    /// <summary>
    ///     % износа
    /// </summary>
    public PropertyPercentWear? PercentWear { get; init; }

    /// <summary>
    ///     Фактическая цена приобретения недвижимости
    /// </summary>
    public MoneyDto PurchasePrice { get; init; }

    /// <summary>
    ///     Общая площадь, кв.м.
    /// </summary>
    public float? TotalArea { get; init; }

    /// <summary>
    ///     Год постройки
    /// </summary>
    public int? YearOfConstruction { get; init; }
}