using Some.Abstractions.Enums;
using Some.Abstractions.Properties;
using Some.Dto.Addresses;
using Some.Dto.Monies;

// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Dto.Properties;

/// <summary>
///     DTO: Страхуемый объект
/// </summary>
public record FlatPropertyDto : EntityDto<Guid>, IScalarProperty<FlatAddressDto, MoneyDto>
{
    /// <summary>
    ///     Действительная (рыночная) стоимость
    /// </summary>
    public MoneyDto ActualPrice { get; init; } = null!;

    /// <summary>
    ///     Адрес
    /// </summary>
    public FlatAddressDto Address { get; init; } = null!;

    /// <summary>
    ///     Кадастровый номер
    /// </summary>
    public string CadastralNumber { get; init; } = null!;

    /// <summary>
    ///     Тип недвижимости
    /// </summary>
    public PropertyType Type { get; init; }
}