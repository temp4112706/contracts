﻿using System.ComponentModel;
using Some.Abstractions.Enums;
using Some.Abstractions.Properties.Title;

namespace Some.Dto.Properties;

/// <summary>
///     Ответ с комментарием, для анкеты по титулу.
/// </summary>
public record TitlePropertyAnswerWithCommentsInput : ITitlePropertyAnswerWithComments
{
    /// <summary>
    ///     Ответ да или нет
    /// </summary>
    [DisplayName("Ответ")]
    public bool Answer { get; init; }

    /// <summary>
    ///     Код вопроса
    /// </summary>
    [DisplayName("Код вопроса")]
    public TitlePropertyQuestionCode Code { get; init; }

    /// <summary>
    ///     Комментарий
    /// </summary>
    [DisplayName("Комментарий")]
    public string? Comments { get; init; }
}