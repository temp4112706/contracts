﻿using System.ComponentModel;
using Some.Abstractions.Enums;
using Some.Abstractions.Properties;
using Some.Dto.Addresses;
using Some.Dto.Monies;

// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Dto.Properties;

public record UpdatePropertyInput : IScalarProperty<AddressInput, MoneyInput>
{
    /// <summary>
    ///     Информация о страхуемом объекте по риску имущества
    /// </summary>
    [DisplayName("Страхуемый объект по риску имущества")]
    public MortgagePropertyDataInput? Property { get; init; }

    /// <summary>
    ///     Страхуемый объект по риску титула
    /// </summary>
    [DisplayName("Страхуемый объект по риску титула")]
    public TitlePropertyDataInput? Title { get; init; }

    /// <summary>
    ///     Действительная (рыночная) стоимость
    /// </summary>
    [DisplayName("Действительная (рыночная) стоимость")]
    public MoneyInput ActualPrice { get; set; } = null!;

    /// <summary>
    ///     Адрес
    /// </summary>
    [DisplayName("Адрес")]
    public AddressInput Address { get; set; } = null!;

    /// <summary>
    ///     Кадастровый номер
    /// </summary>
    [DisplayName("Кадастровый номер")]
    public string CadastralNumber { get; init; } = null!;

    /// <summary>
    ///     Тип недвижимости
    /// </summary>
    [DisplayName("Тип недвижимости")]
    public PropertyType Type { get; set; }
}