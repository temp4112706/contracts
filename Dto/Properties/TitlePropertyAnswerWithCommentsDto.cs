﻿using Some.Abstractions.Enums;
using Some.Abstractions.Properties.Title;

namespace Some.Dto.Properties;

/// <summary>
///     Ответ с комментарием, для анкеты по титулу.
/// </summary>
public record TitlePropertyAnswerWithCommentsDto : ITitlePropertyAnswerWithComments
{
    /// <summary>
    ///     Ответ да или нет
    /// </summary>
    public bool Answer { get; init; }

    /// <summary>
    ///     Код вопроса
    /// </summary>
    public TitlePropertyQuestionCode Code { get; init; }

    /// <summary>
    ///     Комментарий
    /// </summary>
    public string? Comments { get; init; }
}