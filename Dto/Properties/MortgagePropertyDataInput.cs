﻿using System.ComponentModel;
using Some.Abstractions.Enums;
using Some.Abstractions.Properties.Mortgage;
using Some.Dto.Monies;

namespace Some.Dto.Properties;

/// <summary>
///     Страхуемый объект по риску имущества
/// </summary>
public record MortgagePropertyDataInput : IMortgageProperty<MoneyInput>
{
    /// <summary>
    ///     Материал конструкции дома
    /// </summary>
    [DisplayName("Материал конструкции дома")]
    public ConstructionMaterial? ConstructionMaterial { get; init; }

    /// <summary>
    ///     Этаж
    /// </summary>
    [DisplayName("Этаж")]
    public int? FloorNumber { get; init; }

    /// <summary>
    ///     Фундамент
    /// </summary>
    [DisplayName("Фундамент")]
    public FoundationMaterial? Foundation { get; init; }

    /// <summary>
    ///     Материал межэтажных перекрытий
    /// </summary>
    [DisplayName("Материал межэтажных перекрытий")]
    public InterfloorOverlapMaterial? InterfloorOverlapMaterial { get; init; }

    /// <summary>
    ///     Имеются внутренние и/или внешние повреждения/дефекты имущества
    /// </summary>
    [DisplayName("Имеются внутренние и/или внешние повреждения/дефекты имущества")]
    public bool? IsDamaged { get; set; }

    /// <summary>
    ///     Средства противопожарной защиты
    /// </summary>
    [DisplayName("Средства противопожарной защиты")]
    public bool? IsFireProtected { get; set; }

    /// <summary>
    ///     Находится аварийном состоянии или нет
    /// </summary>
    [DisplayName("Аварийное состояние")]
    public bool? IsInEmergencyCondition { get; init; }

    /// <summary>
    ///     Наличие источников повышенной пожарной опасности
    /// </summary>
    [DisplayName("Наличие источников повышенной пожарной опасности")]
    public bool? IsInFireDangerZone { get; set; }

    /// <summary>
    ///     Находится в зоне опасных природных явлений
    /// </summary>
    [DisplayName("Находится в зоне опасных природных явлений")]
    public bool? IsInNaturalHazardsZone { get; set; }

    /// <summary>
    ///     Соседство с объектами повышенной опасности
    /// </summary>
    [DisplayName("Соседство с объектами повышенной опасности")]
    public bool? IsInNeighborhoodDangerZone { get; set; }

    /// <summary>
    ///     Является объектом незавершенного строительства
    /// </summary>
    [DisplayName("Является объектом незавершенного строительства")]
    public bool? IsInProgressConstruction { get; set; }

    /// <summary>
    ///     Застраховано ли в настоящее время недвижимое имущество в другой в компании?
    /// </summary>
    [DisplayName("Застраховано ли в настоящее время недвижимое имущество в другой в компании?")]
    public bool? IsInsuredByOtherInsurer { get; set; }

    /// <summary>
    ///     Несоответствие режима использования объекта недвижимого имущества
    /// </summary>
    [DisplayName("Несоответствие режима использования объекта недвижимого имущества")]
    public bool? IsInUseModeMismatch { get; set; }

    /// <summary>
    ///     Проводилась перепланировка недвижимого имущества
    /// </summary>
    [DisplayName("Проводилась перепланировка недвижимого имущества")]
    public bool? IsRedeveloped { get; set; }

    /// <summary>
    ///     Средства, системы безопасности и охраны
    /// </summary>
    [DisplayName("Средства, системы безопасности и охраны")]
    public bool? IsSecuritySystem { get; set; }

    /// <summary>
    ///     Датчики протечки воды, блокирующие подачу воды в квартире
    /// </summary>
    [DisplayName("Датчики протечки воды, блокирующие подачу воды в квартире")]
    public bool? IsWaterLeakageSensors { get; set; }

    /// <summary>
    ///     Решетки либо рольставни на окнах (для первого этажа)
    /// </summary>
    [DisplayName("Решетки либо рольставни на окнах (для первого этажа)")]
    public bool? IsWindowsProtected { get; set; }

    /// <summary>
    ///     Жилая площадь, кв.м.
    /// </summary>
    [DisplayName("Жилая площадь")]
    public float? LivingArea { get; init; }

    /// <summary>
    ///     Этажность
    /// </summary>
    [DisplayName("Этажность")]
    public int? NumberOfFloors { get; init; }

    /// <summary>
    ///     Число комнат
    /// </summary>
    [DisplayName("Число комнат")]
    public int? NumberOfRooms { get; init; }

    /// <inheritdoc />
    [DisplayName("Степень износа")]
    public PropertyPercentWear? PercentWear { get; set; }

    /// <summary>
    ///     Фактическая цена приобретения недвижимости
    /// </summary>
    [DisplayName("Фактическая цена приобретения недвижимости")]
    public MoneyInput PurchasePrice { get; init; } = null!;

    /// <summary>
    ///     Общая площадь, кв.м.
    /// </summary>
    [DisplayName("Общая площадь")]
    public float? TotalArea { get; init; }

    /// <summary>
    ///     Год постройки
    /// </summary>
    [DisplayName("Год постройки")]
    public int? YearOfConstruction { get; init; }
}