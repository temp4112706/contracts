﻿using System.ComponentModel;
using Some.Abstractions.Enums;
using Some.Abstractions.Properties;
using Some.Abstractions.Properties.Mortgage;
using Some.Abstractions.Properties.Title;
using Some.Dto.Addresses;
using Some.Dto.Monies;

// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Some.Dto.Properties;

/// <summary>
///     Данные о страхуемом объекте
/// </summary>
public record FullPropertyInput : IScalarProperty<AddressInput, MoneyInput>,
    IMortgageProperty<MoneyInput>,
    ITitleProperty<TitlePropertyAnswerWithCommentsInput>
{
    /// <summary>
    ///     Материал конструкции дома
    /// </summary>
    [DisplayName("Материал конструкции дома")]
    public ConstructionMaterial? ConstructionMaterial { get; init; }

    /// <summary>
    ///     Этаж
    /// </summary>
    [DisplayName("Этаж")]
    public int? FloorNumber { get; init; }

    /// <summary>
    ///     Фундамент
    /// </summary>
    [DisplayName("Фундамент")]
    public FoundationMaterial? Foundation { get; init; }

    /// <summary>
    ///     Материал межэтажных перекрытий
    /// </summary>
    [DisplayName("Материал межэтажных перекрытий")]
    public InterfloorOverlapMaterial? InterfloorOverlapMaterial { get; init; }

    /// <inheritdoc />
    [DisplayName("Имеются внутренние и/или внешние повреждения/дефекты имущества")]
    public bool? IsDamaged { get; set; }

    /// <inheritdoc />
    [DisplayName("Средства противопожарной защиты")]
    public bool? IsFireProtected { get; set; }

    /// <summary>
    ///     Находится аварийном состоянии или нет
    /// </summary>
    [DisplayName("Аварийное состояние")]
    public bool? IsInEmergencyCondition { get; init; }

    /// <inheritdoc />
    [DisplayName("Наличие источников повышенной пожарной опасности")]
    public bool? IsInFireDangerZone { get; set; }

    /// <inheritdoc />
    [DisplayName("Находится в зоне опасных природных явлений")]
    public bool? IsInNaturalHazardsZone { get; set; }

    /// <inheritdoc />
    [DisplayName("Соседство с объектами повышенной опасности")]
    public bool? IsInNeighborhoodDangerZone { get; set; }

    /// <inheritdoc />
    [DisplayName("Является объектом незавершенного строительства")]
    public bool? IsInProgressConstruction { get; set; }

    /// <inheritdoc />
    [DisplayName("Застраховано ли в настоящее время недвижимое имущество в другой в компании?")]
    public bool? IsInsuredByOtherInsurer { get; set; }

    /// <inheritdoc />
    [DisplayName("Несоответствие режима использования объекта недвижимого имущества")]
    public bool? IsInUseModeMismatch { get; set; }

    /// <inheritdoc />
    [DisplayName("Проводилась перепланировка недвижимого имущества")]
    public bool? IsRedeveloped { get; set; }

    /// <inheritdoc />
    [DisplayName("Средства, системы безопасности и охраны")]
    public bool? IsSecuritySystem { get; set; }

    /// <inheritdoc />
    [DisplayName("Датчики протечки воды, блокирующие подачу воды в квартире")]
    public bool? IsWaterLeakageSensors { get; set; }

    /// <inheritdoc />
    [DisplayName("Решетки либо рольставни на окнах (для первого этажа)")]
    public bool? IsWindowsProtected { get; set; }

    /// <summary>
    ///     Жилая площадь, кв.м.
    /// </summary>
    [DisplayName("Жилая площадь")]
    public float? LivingArea { get; init; }

    /// <summary>
    ///     Этажность
    /// </summary>
    [DisplayName("Этажность")]
    public int? NumberOfFloors { get; init; }

    /// <summary>
    ///     Число комнат
    /// </summary>
    [DisplayName("Число комнат")]
    public int? NumberOfRooms { get; init; }

    /// <inheritdoc />
    [DisplayName("Степень износа")]
    public PropertyPercentWear? PercentWear { get; set; }

    /// <summary>
    ///     Фактическая цена приобретения недвижимости
    /// </summary>
    [DisplayName("Фактическая цена приобретения недвижимости")]
    public MoneyInput PurchasePrice { get; init; } = null!;

    /// <summary>
    ///     Общая площадь, кв.м.
    /// </summary>
    [DisplayName("Общая площадь")]
    public float? TotalArea { get; init; }

    /// <summary>
    ///     Год постройки
    /// </summary>
    [DisplayName("Год постройки")]
    public int? YearOfConstruction { get; init; }

    /// <summary>
    ///     Действительная (рыночная) стоимость
    /// </summary>
    [DisplayName("Действительная (рыночная) стоимость")]
    public MoneyInput ActualPrice { get; set; } = null!;

    /// <summary>
    ///     Адрес
    /// </summary>
    [DisplayName("Адрес")]
    public AddressInput Address { get; set; } = null!;

    /// <summary>
    ///     Кадастровый номер
    /// </summary>
    [DisplayName("Кадастровый номер")]
    public string CadastralNumber { get; init; } = null!;

    /// <summary>
    ///     Тип недвижимости
    /// </summary>
    [DisplayName("Тип недвижимости")]
    public PropertyType Type { get; set; }

    /// <summary>
    ///     Количество сделок
    /// </summary>
    [DisplayName("Количество сделок")]
    public int? AmountOfDeals { get; init; }

    /// <summary>
    ///     Анкета по титулу
    /// </summary>
    [Description("Анкета по титулу")]
    public ICollection<TitlePropertyAnswerWithCommentsInput>? AnswerWithComments { get; init; }

    /// <summary>
    ///     Документ, подтверждающий стоимость недвижимого имущества
    /// </summary>
    [Description("Документ, подтверждающий стоимость недвижимого имущества")]
    public string? DocumentOfPrice { get; init; }

    /// <summary>
    ///     Основание последнего перехода права собственности
    /// </summary>
    [Description("Основание последнего перехода права собственности")]
    public string? DocumentRegistration { get; init; }

    /// <summary>
    ///     Количество переходов в течение последних 12 месяцев
    /// </summary>
    [Description("Количество переходов в течение последних 12 месяцев")]
    public int? HopCount { get; init; }

    /// <summary>
    ///     Тип собственника (ФЛ/ЮЛ). Если ЮЛ, то true
    /// </summary>
    [Description("Тип собственника (ФЛ/ЮЛ).")]
    public bool? IsLegalEntity { get; init; }

    /// <summary>
    ///     Рынок недвижимости: первичный/вторичный
    /// </summary>
    [DisplayName("Рынок недвижимости")]
    public bool? IsPrimary { get; init; }

    /// <summary>
    ///     Основание приобретения - ПИФ
    /// </summary>
    [Description("Основание приобретения")]
    public bool? Pif { get; init; }

    /// <summary>
    ///     Сделка совершается по доверенности
    /// </summary>
    [Description("Сделка совершается по доверенности")]
    public bool? PowerOfAttorney { get; init; }

    /// <summary>
    ///     Дата регистрации права
    /// </summary>
    [Description("Дата регистрации права")]
    public DateTime? RegistrationDate { get; init; }
}